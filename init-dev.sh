#!/bin/bash

echo \#\!\/bin\/bash >> init.sh

echo export APP_ENV=$APP_ENV_DEV >> init.sh
echo export TAG_NUMBER=$CI_PIPELINE_IID >> init.sh
echo export SPRING_ACTIVE_PROFILE=$APP_ENV_DEV >> init.sh
echo export APP_NAME=$APP_NAME >> init.sh
echo export APP_PORT=$APP_PORT_DEV >> init.sh
echo export SERVER=$SERVER_DEV >> init.sh
echo export REPOSITORY_URL=$REPOSITORY_URL >> init.sh
echo export ECR_URL=$ECR_URL >> init.sh
echo export ECR_USERNAME=$ECR_USERNAME >> init.sh
echo export SENTRY_DNS=$SENTRY_DNS >> init.sh
echo export SENTRY_TRACES_SAMPLE_RATE=$SENTRY_TRACES_SAMPLE_RATE >> init.sh
echo export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID >> init.sh
echo export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY >> init.sh
echo export AWS_BUCKET_NAME=$AWS_BUCKET_NAME >> init.sh
echo export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION >> init.sh
echo export S3_STATIC_FOLDER=$S3_STATIC_FOLDER >> init.sh
echo export DB_SERVER_DEV=$DB_SERVER_DEV >> init.sh
echo export DB_PORT_DEV=$DB_PORT_DEV >> init.sh
echo export DB_NAME_DEV=$DB_NAME_DEV >> init.sh
echo export DB_USERNAME_DEV=$DB_USERNAME_DEV >> init.sh
echo export DB_PASSWORD_DEV=$DB_PASSWORD_DEV >> init.sh
echo export PROD_MAIL_DRIVER=$PROD_MAIL_DRIVER >> init.sh
echo export PROD_MAIL_HOST=$PROD_MAIL_HOST >> init.sh
echo export PROD_MAIL_PORT=$PROD_MAIL_PORT >> init.sh
echo export PROD_MAIL_USERNAME=$PROD_MAIL_USERNAME >> init.sh
echo export PROD_MAIL_PASSWORD=$PROD_MAIL_PASSWORD >> init.sh
echo export MAIL_FROM_ADDRESS=$MAIL_FROM_ADDRESS >> init.sh
echo export MAIL_FROM_NAME=$MAIL_FROM_NAME >> init.sh
echo export PROD_MAIL_ENCRYPTION=$PROD_MAIL_ENCRYPTION >> init.sh
echo export MIDTRANS_CLIENTKEY=$MIDTRANS_CLIENTKEY >> init.sh
echo export MIDTRANS_ENABLEDLOG=$MIDTRANS_ENABLEDLOG >> init.sh
echo export MIDTRANS_ISPRODUCTION=$MIDTRANS_ISPRODUCTION >> init.sh
echo export MIDTRANS_SERVERKEY=$MIDTRANS_SERVERKEY >> init.sh

echo aws ecr get-login-password --region ap-southeast-1 | docker login --username $ECR_USERNAME --password-stdin $ECR_URL >> init.sh
echo docker stop $APP_NAME >> init.sh
echo docker system prune -af >> init.sh
echo docker pull $REPOSITORY_URL:$CI_PIPELINE_IID >> init.sh
echo docker-compose -f docker-compose.yml up -d >> init.sh
