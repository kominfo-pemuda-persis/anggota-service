package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PekerjaanDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Pekerjaan;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PekerjaanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.PekerjaanVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class PekerjaanServiceTest extends PekerjaanVariables {
    private static final String SUCCESS = "SUCCESS";
    private static final String PEKERJAAN_NOT_FOUND = "Pekerjaan Not Found";
    private static final Long ID_PEKERJAAN = 1L;

    @InjectMocks
    private PekerjaanService pekerjaanService;

    @Mock
    private PekerjaanRepository pekerjaanRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(pekerjaanRepository);
        verifyNoMoreInteractions(anggotaService);
    }

    @Test
    void pekerjaanListWhenSuccess() {
        Page<Pekerjaan> pekerjaanPage = mock(Page.class);
        when(pekerjaanPage.getContent()).thenReturn(PEKERJAAN_LIST);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(modelMapper.map(PEKERJAAN, PekerjaanDto.class)).thenReturn(PEKERJAAN_DTO);
        when(pekerjaanRepository.findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(PEKERJAAN_PAGE);
        BaseResponse<Page<PekerjaanDto>> baseResponse = pekerjaanService.pekerjaanList(pageableTest, false, NPA);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(pekerjaanRepository).findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest));
        verify(modelMapper).map(PEKERJAAN, PekerjaanDto.class);
    }

    @Test
    void createPekerjaanWhenSuccess() {
        when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
        when(modelMapper.map(PEKERJAAN, PekerjaanDto.class)).thenReturn(PEKERJAAN_DTO);
        when(pekerjaanRepository.save(any(Pekerjaan.class))).thenReturn(PEKERJAAN);
        BaseResponse<PekerjaanDto> baseResponse = pekerjaanService.create(NPA, PEKERJAAN_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(anggotaService).findByNpa(NPA);
        verify(pekerjaanRepository).save(any(Pekerjaan.class));
        verify(modelMapper).map(PEKERJAAN, PekerjaanDto.class);
    }

    @Test
    void updatePekerjaanWhenSuccess() {
        when(pekerjaanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong()))
                .thenReturn(Optional.of(PEKERJAAN));
        when(pekerjaanRepository.save(any(Pekerjaan.class))).thenReturn(PEKERJAAN);
        when(modelMapper.map(PEKERJAAN, PekerjaanDto.class)).thenReturn(PEKERJAAN_DTO);
        BaseResponse<PekerjaanDto> baseResponse = pekerjaanService.update(anyString(), anyLong(), PEKERJAAN_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(pekerjaanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong());
        verify(pekerjaanRepository).save(any(Pekerjaan.class));
        verify(modelMapper).map(PEKERJAAN, PekerjaanDto.class);
        verify(modelMapper).map(PEKERJAAN_DTO, PEKERJAAN);
    }

    @Test
    void updatePekerjaanWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(pekerjaanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            pekerjaanService.update(anyString(), anyLong(), PEKERJAAN_DTO);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(PEKERJAAN_NOT_FOUND));

        verify(pekerjaanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong());
    }

    @Test
    void deletePekerjaanWhenSuccess() {
        when(pekerjaanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong()))
                .thenReturn(Optional.of(PEKERJAAN));
        when(pekerjaanRepository.save(any(Pekerjaan.class))).thenReturn(PEKERJAAN);
        BaseResponse<PekerjaanDto> baseResponse = pekerjaanService.delete(NPA, ID_PEKERJAAN);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(pekerjaanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(NPA, ID_PEKERJAAN);
        verify(pekerjaanRepository).save(any(Pekerjaan.class));
    }

    @Test
    void deletePekerjaanWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(pekerjaanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            pekerjaanService.delete(NPA, ID_PEKERJAAN);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(PEKERJAAN_NOT_FOUND));

        verify(pekerjaanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong());
    }

    @Test
    void restorePekerjaanWhenSuccess() {
        when(pekerjaanRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdPekerjaan(NPA, LONG_ID))
                .thenReturn(Optional.of(PEKERJAAN));
        when(pekerjaanRepository.save(any(Pekerjaan.class))).thenReturn(PEKERJAAN);
        when(modelMapper.map(PEKERJAAN, PekerjaanDto.class)).thenReturn(PEKERJAAN_DTO);
        BaseResponse<PekerjaanDto> baseResponse = pekerjaanService.restore(NPA, LONG_ID);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(pekerjaanRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdPekerjaan(NPA, LONG_ID);
        verify(pekerjaanRepository).save(any(Pekerjaan.class));
        verify(modelMapper).map(PEKERJAAN, PekerjaanDto.class);
    }

    @Test
    void restorePekerjaanWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(pekerjaanRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            pekerjaanService.restore(NPA, ID_PEKERJAAN);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(PEKERJAAN_NOT_FOUND));
        verify(pekerjaanRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdPekerjaan(anyString(), anyLong());
    }
}
