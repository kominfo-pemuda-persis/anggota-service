package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPjDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PerformaPjRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.*;

@ExtendWith(MockitoExtension.class)
class PerformaPjServiceTest {
    @InjectMocks
    private PerformaPjService performaPjService;

    @Mock
    private PerformaPjRepository performaPjRepository;

    @Mock
    private CabangService cabangService;

    @Mock
    private DaerahService daerahService;

    @Mock
    private WilayahService wilayahService;

    @Mock
    private HelperUtilService helperUtilService;

    @Mock
    private ModelMapper modelMapper;

    private Cabang cabang;
    private Daerah daerah;
    private Wilayah wilayah;
    private Provinsi provinsi;
    private Kabupaten kabupaten;
    private Kecamatan kecamatan;
    private Desa desa;
    private PerformaPj performaPj;

    @BeforeEach
    void setUp() {
        cabang = Cabang.builder().kdPc("KC-1").build();
        daerah = Daerah.builder().kdPd("PD-02").build();
        wilayah = Wilayah.builder().kdPw("WIL-01").build();
        provinsi = Provinsi.builder().id("PROV1").build();
        kecamatan = Kecamatan.builder().id("KEC").build();
        kabupaten = Kabupaten.builder().id("KAB").build();
        desa = Desa.builder().id("DES").build();
        performaPj = PerformaPj.builder()
                .id(1L)
                .kodePj("PJ")
                .cabang(cabang)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .desa(desa)
                .deletedAt(null)
                .alamat("Jakarta")
                .kecamatan(kecamatan)
                .build();
    }

    @Test
    void getListPerformaPj() {
        List<PerformaPj> performaPjs = new ArrayList<>();
        performaPjs.add(performaPj);
        Page<PerformaPj> pagedResponse = new PageImpl(performaPjs);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(performaPjRepository.findByDeletedAtIsNull(pageableTest)).thenReturn(pagedResponse);
        List<PerformaPj> listResult = performaPjService.getListPerformaPj(pageableTest, false);
        assertEquals(performaPjs.size(), listResult.size());
    }

    @Test
    void create() {
        when(cabangService.getByKodeCabang(cabang.getKdPc())).thenReturn(cabang);
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getKecamatanById(kecamatan.getId())).thenReturn(kecamatan);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(performaPjRepository.save(any(PerformaPj.class))).thenReturn(performaPj);
        PerformaPj result = performaPjService.create(performaPj);
        assertEquals(result.getId(), performaPj.getId());
    }

    @Test
    void update() {
        PerformaPjDto performaPjDto =
                PerformaPjDto.builder().alamat("Bandung").kodePj("PJ").build();
        PerformaPj result1 = PerformaPj.builder()
                .id(1L)
                .kodePj("PJ")
                .cabang(cabang)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .desa(desa)
                .alamat("Bandung")
                .kecamatan(kecamatan)
                .build();
        Configuration configuration = mock(Configuration.class);
        when(performaPjRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(performaPj));
        when(cabangService.getByKodeCabang(cabang.getKdPc())).thenReturn(cabang);
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getKecamatanById(kecamatan.getId())).thenReturn(kecamatan);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(modelMapper.getConfiguration()).thenReturn(configuration);
        when(performaPjRepository.save(performaPj)).thenReturn(result1);

        PerformaPj result = performaPjService.update(performaPjDto, 1L);
        assertEquals(result.getAlamat(), performaPjDto.getAlamat());
    }

    @Test
    void delete() {
        PerformaPj result1 = PerformaPj.builder()
                .id(1L)
                .kodePj("PJ")
                .cabang(cabang)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .desa(desa)
                .deletedAt(LocalDateTime.now())
                .alamat("Bandung")
                .kecamatan(kecamatan)
                .build();
        when(performaPjRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(performaPj));
        when(performaPjRepository.save(performaPj)).thenReturn(result1);
        PerformaPj result = performaPjService.delete(1L);
        assertTrue(result.getDeletedAt() != null);
    }

    @Test
    void restore() {
        PerformaPj result1 = PerformaPj.builder()
                .id(1L)
                .kodePj("PJ")
                .cabang(cabang)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .desa(desa)
                .deletedAt(LocalDateTime.now())
                .alamat("Bandung")
                .kecamatan(kecamatan)
                .build();
        when(performaPjRepository.findByIdAndDeletedAtIsNotNull(any(Long.class)))
                .thenReturn(Optional.of(result1));
        when(performaPjRepository.save(result1)).thenReturn(performaPj);
        PerformaPj result = performaPjService.restore(1L);
        assertTrue(result.getDeletedAt() == null);
    }
}
