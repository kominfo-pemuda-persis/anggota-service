package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Daerah;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import java.time.LocalDate;
import java.util.Collections;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class DaerahVariables {
    public final DaerahRequest DAERAH_REQUEST = DaerahRequest.builder()
            .diresmikan(LocalDate.now())
            .kdPd("KDPD")
            .namaPd("Nama PD")
            .build();
    public final Daerah DAERAH = Daerah.builder()
            .diresmikan(LocalDate.now())
            .kdPd("KDPD")
            .namaPd("Nama PD")
            .build();
    public final Page<Daerah> DAERAH_PAGE = new PageImpl<>(Collections.singletonList(DAERAH));
}
