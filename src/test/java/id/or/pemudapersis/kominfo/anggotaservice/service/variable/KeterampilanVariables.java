package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KeterampilanDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Keterampilan;
import java.util.Collections;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class KeterampilanVariables extends AnggotaVariableTest {
    private static final String NAMA_KETERAMPILAN = "String Keterampilan";
    public final Keterampilan KETERAMPILAN = Keterampilan.builder()
            .keterampilan(NAMA_KETERAMPILAN)
            .idKeterampilan(LONG_ID)
            .anggota(ANGGOTA)
            .build();
    public final KeterampilanDto KETERAMPILAN_DTO = KeterampilanDto.builder()
            .idKeterampilan(LONG_ID)
            .keterampilan(NAMA_KETERAMPILAN)
            .build();
    public final List<Keterampilan> KETERAMPILAN_LIST = Collections.singletonList(Keterampilan.builder()
            .keterampilan(NAMA_KETERAMPILAN)
            .idKeterampilan(LONG_ID)
            .anggota(ANGGOTA)
            .build());
    public final Page<Keterampilan> KETERAMPILAN_PAGE = new PageImpl<>(Collections.singletonList(KETERAMPILAN));
}
