package id.or.pemudapersis.kominfo.anggotaservice.service;

import static id.or.pemudapersis.kominfo.anggotaservice.service.OrganisasiServiceTest.SUCCESS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.dto.WilayahDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import id.or.pemudapersis.kominfo.anggotaservice.repository.WilayahRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import jakarta.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;

@ExtendWith(MockitoExtension.class)
class WilayahServiceTest {
    @Mock
    private WilayahRepository wilayahRepository;

    @Mock
    private EntityManager entityManager;

    @InjectMocks
    private WilayahService wilayahService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    public void setUp() {
        wilayahService = new WilayahService(wilayahRepository, modelMapper);
    }

    @Test
    void getAllWilayah() {
        List<Wilayah> wilayahList = new ArrayList<>();
        //        ArrayListSession session = mock(Session.class);
        //        Filter filter = mock(Filter.class);
        Wilayah wilayah = Wilayah.builder()
                .kdPw("PW-1")
                .namaWilayah("Bandung Barat")
                // .isDeleted(false)
                .build();

        wilayahList.add(wilayah);
        Page<Wilayah> pagedResponse = new PageImpl(wilayahList);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("kodeWilayah"));
        when(wilayahRepository.findAll(pageableTest)).thenReturn(pagedResponse);
        BaseResponse<Page<WilayahDto>> listResult = wilayahService.getAllWilayah(pageableTest, false);
        assertEquals(wilayahList.size(), listResult.getData().getTotalElements());
    }

    @Test
    void updateWilayah() {
        Wilayah wilayah =
                Wilayah.builder().kdPw("PW-1").namaWilayah("Bandung Barat").build();
        String kodeWilayah = "PW-1";
        when(wilayahRepository.findByKdPw(kodeWilayah)).thenReturn(Optional.of(wilayah));
        when(wilayahRepository.save(wilayah)).thenReturn(wilayah);
        BaseResponse<WilayahDto> result = wilayahService.updateWilayah(wilayah, kodeWilayah);
        assertEquals(result.getMessage(), SUCCESS);
    }
}
