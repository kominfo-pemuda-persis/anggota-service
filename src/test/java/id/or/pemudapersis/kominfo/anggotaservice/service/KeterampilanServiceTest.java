package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KeterampilanDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Keterampilan;
import id.or.pemudapersis.kominfo.anggotaservice.repository.KeterampilanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.KeterampilanVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class KeterampilanServiceTest extends KeterampilanVariables {

    private static final String SUCCESS = "SUCCESS";
    private static final String KETERAMPILAN_NOT_FOUND = "Keterampilan Not Found";
    private static final Long ID_KETERAMPILAN = 1L;

    @InjectMocks
    private KeterampilanService keterampilanService;

    @Mock
    private KeterampilanRepository keterampilanRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(anggotaService);
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(keterampilanRepository);
    }

    @Test
    void keterampilanListNotDeleted() {
        Page<Keterampilan> keterampilanPage = mock(Page.class);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("idKeterampilan"));
        when(keterampilanPage.getContent()).thenReturn(KETERAMPILAN_LIST);
        when(modelMapper.map(KETERAMPILAN, KeterampilanDto.class)).thenReturn(KETERAMPILAN_DTO);
        when(keterampilanRepository.findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(KETERAMPILAN_PAGE);
        BaseResponse<Page<KeterampilanDto>> listBaseResponse =
                keterampilanService.keterampilanList(pageableTest, false, NPA);
        assertEquals(listBaseResponse.getMessage(), SUCCESS);
        verify(keterampilanRepository).findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest));
        verify(modelMapper).map(KETERAMPILAN, KeterampilanDto.class);
    }

    @Test
    void keterampilanListDeleted() {
        Page<Keterampilan> keterampilanPage = mock(Page.class);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("idKeterampilan"));
        when(keterampilanPage.getContent()).thenReturn(KETERAMPILAN_LIST);
        when(modelMapper.map(KETERAMPILAN, KeterampilanDto.class)).thenReturn(KETERAMPILAN_DTO);
        when(keterampilanRepository.findByDeletedAtIsNotNullAndAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(KETERAMPILAN_PAGE);
        BaseResponse<Page<KeterampilanDto>> listBaseResponse =
                keterampilanService.keterampilanList(pageableTest, true, NPA);
        assertEquals(listBaseResponse.getMessage(), SUCCESS);
        verify(keterampilanRepository).findByDeletedAtIsNotNullAndAnggotaNpa(anyString(), eq(pageableTest));
        verify(modelMapper).map(KETERAMPILAN, KeterampilanDto.class);
    }

    @Test
    void create() {
        when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
        when(modelMapper.map(KETERAMPILAN, KeterampilanDto.class)).thenReturn(KETERAMPILAN_DTO);
        when(keterampilanRepository.save(any(Keterampilan.class))).thenReturn(KETERAMPILAN);
        BaseResponse<KeterampilanDto> keterampilanDtoBaseResponse = keterampilanService.create(NPA, KETERAMPILAN_DTO);
        assertEquals(keterampilanDtoBaseResponse.getMessage(), SUCCESS);
        verify(keterampilanRepository).save(any(Keterampilan.class));
        verify(anggotaService).findByNpa(NPA);
        verify(modelMapper).map(KETERAMPILAN, KeterampilanDto.class);
    }

    @Test
    void update() {
        when(modelMapper.map(KETERAMPILAN, KeterampilanDto.class)).thenReturn(KETERAMPILAN_DTO);
        when(keterampilanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN))
                .thenReturn(Optional.of(KETERAMPILAN));
        when(keterampilanRepository.save(any(Keterampilan.class))).thenReturn(KETERAMPILAN);
        BaseResponse<KeterampilanDto> keterampilanDtoBaseResponse =
                keterampilanService.update(NPA, ID_KETERAMPILAN, KETERAMPILAN_DTO);
        assertEquals(keterampilanDtoBaseResponse.getMessage(), SUCCESS);
        verify(keterampilanRepository).save(any(Keterampilan.class));
        verify(keterampilanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN);
        verify(modelMapper).map(KETERAMPILAN, KeterampilanDto.class);
        verify(modelMapper).map(KETERAMPILAN_DTO, KETERAMPILAN);
    }

    @Test
    void updateKeterampilanNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(keterampilanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN))
                    .thenReturn(Optional.empty());
            keterampilanService.update(NPA, ID_KETERAMPILAN, KETERAMPILAN_DTO);
        });

        String exceptionMessage = exception.getMessage();

        assertTrue(exceptionMessage.contains(KETERAMPILAN_NOT_FOUND));
        verify(keterampilanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN);
    }

    @Test
    void delete() {
        when(modelMapper.map(KETERAMPILAN, KeterampilanDto.class)).thenReturn(KETERAMPILAN_DTO);
        when(keterampilanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN))
                .thenReturn(Optional.of(KETERAMPILAN));
        when(keterampilanRepository.save(any(Keterampilan.class))).thenReturn(KETERAMPILAN);
        BaseResponse<KeterampilanDto> keterampilanDtoBaseResponse = keterampilanService.delete(NPA, ID_KETERAMPILAN);
        assertEquals(keterampilanDtoBaseResponse.getMessage(), SUCCESS);
        verify(keterampilanRepository).save(any(Keterampilan.class));
        verify(keterampilanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN);
        verify(modelMapper).map(KETERAMPILAN, KeterampilanDto.class);
    }

    @Test
    void deleteKeterampilanNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(keterampilanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN))
                    .thenReturn(Optional.empty());
            keterampilanService.delete(NPA, ID_KETERAMPILAN);
        });

        String exceptionMessage = exception.getMessage();

        assertTrue(exceptionMessage.contains(KETERAMPILAN_NOT_FOUND));
        verify(keterampilanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN);
    }

    @Test
    void restore() {
        when(modelMapper.map(KETERAMPILAN, KeterampilanDto.class)).thenReturn(KETERAMPILAN_DTO);
        when(keterampilanRepository.save(any(Keterampilan.class))).thenReturn(KETERAMPILAN);
        when(keterampilanRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN))
                .thenReturn(Optional.of(KETERAMPILAN));
        BaseResponse<KeterampilanDto> keterampilanDtoBaseResponse = keterampilanService.restore(NPA, ID_KETERAMPILAN);
        assertEquals(keterampilanDtoBaseResponse.getMessage(), SUCCESS);
        verify(keterampilanRepository).save(any(Keterampilan.class));
        verify(keterampilanRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN);
        verify(modelMapper).map(KETERAMPILAN, KeterampilanDto.class);
    }

    @Test
    void restoreKeterampilanNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(keterampilanRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN))
                    .thenReturn(Optional.empty());
            keterampilanService.restore(NPA, ID_KETERAMPILAN);
        });

        String exceptionMessage = exception.getMessage();

        assertTrue(exceptionMessage.contains(KETERAMPILAN_NOT_FOUND));
        verify(keterampilanRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeterampilan(NPA, ID_KETERAMPILAN);
    }
}
