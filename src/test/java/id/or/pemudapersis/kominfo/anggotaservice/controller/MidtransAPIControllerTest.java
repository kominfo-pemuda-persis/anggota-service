package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.midtrans.service.MidtransSnapApi;
import id.or.pemudapersis.kominfo.anggotaservice.dto.midtransDTO.Item;
import id.or.pemudapersis.kominfo.anggotaservice.service.MidtransService;
import java.util.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

@Slf4j
class MidtransAPIControllerTest {

    @InjectMocks
    private MidtransService midtransService;

    @Mock
    private MidtransSnapApi midtransSnapApi;

    private MidtransAPIController midtransAPIController;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        objectMapper = new ObjectMapper();
        midtransService = new MidtransService(midtransSnapApi);
        midtransAPIController = new MidtransAPIController(midtransService);
    }

    @Test
    public void testCheckoutAPI() throws Exception {

        List<Item> itemList = new ArrayList<>();
        itemList.add(Item.builder()
                .id(UUID.randomUUID().toString())
                .name("Infaq bulanan")
                .price(15000)
                .quantity(10)
                .build());

        Map<String, Object> checkoutResult = new HashMap<>();
        checkoutResult.put("token", "this is token");
        checkoutResult.put("redirected_url", "this is redirected URL");
        JSONObject expectedJson = new JSONObject(checkoutResult);

        when(midtransSnapApi.createTransaction(anyMap())).thenReturn(expectedJson);
        ResponseEntity<Object> response = midtransAPIController.checkout(itemList);

        Map<String, Object> bodyResult = (Map<String, Object>) response.getBody();

        log.info("result : {}", bodyResult);

        assertNotNull(bodyResult.get("data"));
        assertNull(bodyResult.get("errors"));
    }
}
