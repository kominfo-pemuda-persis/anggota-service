package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KeluargaDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Keluarga;
import id.or.pemudapersis.kominfo.anggotaservice.repository.KeluargaRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.OtonomRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.KeluargaVariabels;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class KeluargaServiceTest extends KeluargaVariabels {

    @InjectMocks
    private KeluargaService keluargaService;

    @Mock
    private KeluargaRepository keluargaRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private OtonomRepository otonomRepository;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(keluargaRepository);
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(anggotaService);
        verifyNoMoreInteractions(otonomRepository);
    }

    @Test
    void keluargaListWhenSuccess() {
        Page<Keluarga> keluargas = mock(Page.class);
        when(keluargas.getContent()).thenReturn(KELUARGA_LIST);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(modelMapper.map(KELUARGA, KeluargaDto.class)).thenReturn(KELUARGA_DTO);
        when(keluargaRepository.findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(KELUARGA_PAGE);
        BaseResponse<Page<KeluargaDto>> baseResponse = keluargaService.keluargaList(pageableTest, false, NPA);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(keluargaRepository).findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest));
        verify(modelMapper).map(KELUARGA, KeluargaDto.class);
    }

    @Test
    void keluargaCreateWhenSuccess() {
        when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
        when(otonomRepository.findById(anyLong())).thenReturn(Optional.of(OTONOM));
        when(keluargaRepository.save(any(Keluarga.class))).thenReturn(KELUARGA);
        when(modelMapper.map(KELUARGA, KeluargaDto.class)).thenReturn(KELUARGA_DTO);
        BaseResponse<KeluargaDto> baseResponse = keluargaService.create(NPA, KELUARGA_DTO);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(anggotaService).findByNpa(NPA);
        verify(otonomRepository).findById(anyLong());
        verify(keluargaRepository).save(any(Keluarga.class));
        verify(modelMapper).map(KELUARGA, KeluargaDto.class);
    }

    @Test
    void keluargaCreateWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
            when(otonomRepository.findById(anyLong())).thenReturn(Optional.empty());
            keluargaService.create(NPA, KELUARGA_DTO);
        });

        String expectedMessage = "Otonom Not Found";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        verify(anggotaService).findByNpa(NPA);
        verify(otonomRepository).findById(anyLong());
    }

    @Test
    void testUpdateWhenSuccess() {
        when(keluargaRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(anyString(), anyLong()))
                .thenReturn(Optional.of(KELUARGA));
        when(otonomRepository.findById(anyLong())).thenReturn(Optional.of(OTONOM));
        when(keluargaRepository.save(any(Keluarga.class))).thenReturn(KELUARGA);
        when(modelMapper.map(KELUARGA, KeluargaDto.class)).thenReturn(KELUARGA_DTO);
        BaseResponse<KeluargaDto> baseResponse = keluargaService.update(anyString(), anyLong(), KELUARGA_DTO);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(keluargaRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(anyString(), anyLong());
        verify(otonomRepository).findById(anyLong());
        verify(keluargaRepository).save(any(Keluarga.class));
        verify(modelMapper).map(KELUARGA, KeluargaDto.class);
        verify(modelMapper).map(KELUARGA_DTO, KELUARGA);
    }

    @Test
    void keluargaUpdateWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(keluargaRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(anyString(), anyLong()))
                    .thenReturn(Optional.of(KELUARGA));
            when(otonomRepository.findById(anyLong())).thenReturn(Optional.empty());
            keluargaService.update(anyString(), anyLong(), KELUARGA_DTO);
        });

        String expectedMessage = "Otonom Not Found";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        verify(keluargaRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(anyString(), anyLong());
        verify(otonomRepository).findById(anyLong());
    }

    @Test
    void testDelete() {
        when(keluargaRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(anyString(), anyLong()))
                .thenReturn(Optional.of(KELUARGA));
        when(keluargaRepository.save(any(Keluarga.class))).thenReturn(KELUARGA);
        BaseResponse<KeluargaDto> baseResponse = keluargaService.delete(NPA, 1L);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(keluargaRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(NPA, 1L);
        verify(keluargaRepository).save(any(Keluarga.class));
    }

    @Test
    void testRestore() {
        when(keluargaRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeluarga(NPA, 1L))
                .thenReturn(Optional.of(KELUARGA));
        when(keluargaRepository.save(any(Keluarga.class))).thenReturn(KELUARGA);
        when(modelMapper.map(KELUARGA, KeluargaDto.class)).thenReturn(KELUARGA_DTO);
        BaseResponse<KeluargaDto> baseResponse = keluargaService.restore(NPA, 1L);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(keluargaRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeluarga(NPA, 1L);
        verify(keluargaRepository).save(any(Keluarga.class));
        verify(modelMapper).map(KELUARGA, KeluargaDto.class);
    }
}
