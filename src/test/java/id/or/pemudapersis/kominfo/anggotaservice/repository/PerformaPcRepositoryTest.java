package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.*;

import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPc;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class PerformaPcRepositoryTest {
    @Autowired
    private PerformaPcRepository performaPcRepository;

    @Test
    public void testFindByDeletedAtIsNull_NullPageable() {
        Pageable pageable = null;
        try {
            performaPcRepository.findByDeletedAtIsNull(pageable);
        } catch (NullPointerException e) {
            // expected
        }
    }

    @Test
    public void testFindByDeletedAtIsNull_ValidPageable_RecordsExist() {
        Pageable pageable = PageRequest.of(0, 10);
        Page<PerformaPc> result = performaPcRepository.findByDeletedAtIsNull(pageable);
        assertNotNull(result);
    }

    @Test
    public void testFindByDeletedAtIsNull_ValidPageable_NoRecords() {
        Pageable pageable = PageRequest.of(0, 10);
        // delete all records
        performaPcRepository.deleteAll();
        Page<PerformaPc> result = performaPcRepository.findByDeletedAtIsNull(pageable);
        assertNotNull(result);
        assertTrue(result.getContent().isEmpty());
    }

    @Test
    public void testFindByDeletedAtIsNull_LargePageSize() {
        Pageable pageable = PageRequest.of(0, 1000);
        Page<PerformaPc> result = performaPcRepository.findByDeletedAtIsNull(pageable);
        assertNotNull(result);
        assertTrue(result.getContent().size() <= 1000);
    }

    @Test
    public void testFindByDeletedAtIsNull_LargePageNumber() {
        Pageable pageable = PageRequest.of(100, 10);
        Page<PerformaPc> result = performaPcRepository.findByDeletedAtIsNull(pageable);
        assertNotNull(result);
        assertTrue(result.getContent().isEmpty());
    }
}
