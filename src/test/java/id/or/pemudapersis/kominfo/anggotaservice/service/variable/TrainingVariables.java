package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.TrainingDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Training;
import java.util.Collections;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class TrainingVariables extends AnggotaVariableTest {
    public final Training TRAINING = Training.builder()
            .idTraining(LONG_ID)
            .anggota(ANGGOTA)
            .namaTraining("nama training")
            .jenis("jenis")
            .build();
    public final TrainingDto TRAINING_DTO = TrainingDto.builder()
            .idTraining(LONG_ID)
            .namaTraining("nama training")
            .jenis("jenis")
            .build();
    public final Page<Training> TRAINING_PAGE = new PageImpl<>(Collections.singletonList(TRAINING));
}
