package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KabupatenDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPcDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.JamiyyahPcRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MonografiPcRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.*;

@ExtendWith(MockitoExtension.class)
class MonografiPcServiceTest {
    @InjectMocks
    private MonografiPcService monografiPCService;

    @Mock
    private JamiyyahPcRepository jamiyyahPcRepository;

    @Mock
    private MonografiPcRepository monografiPcRepository;

    @Mock
    private CabangService cabangService;

    @Mock
    private DaerahService daerahService;

    @Mock
    private WilayahService wilayahService;

    @Mock
    private HelperUtilService helperUtilService;

    @Mock
    private ModelMapper modelMapper;

    private Cabang cabang;
    private Daerah daerah;
    private Wilayah wilayah;
    private Kecamatan kecamatan;
    private Provinsi provinsi;
    private Kabupaten kabupaten;
    private JamiyyahPc jamiyyahPc;
    private KabupatenDto kabupatenDto;

    @BeforeEach
    public void setup() {
        cabang = Cabang.builder().kdPc("KC-1").build();
        daerah = Daerah.builder().kdPd("PD-02").build();
        wilayah = Wilayah.builder().kdPw("WIL-01").build();
        provinsi = Provinsi.builder().id("PROV1").build();
        kecamatan = Kecamatan.builder().id("KEC").build();
        kabupaten = Kabupaten.builder().id("KAB").build();
        jamiyyahPc = JamiyyahPc.builder().id(1L).deletedAt(null).build();
        kabupatenDto = KabupatenDto.builder().id("KAB").build();
    }

    @Test
    void getAllMonografiPC() {
        List<MonografiPc> monografiPcList = new ArrayList<>();
        MonografiPc monografiPc = MonografiPc.builder()
                .id(1L)
                .cabang(Cabang.builder().kdPc("KC-1").build())
                .daerah(Daerah.builder().kdPd("PD-02").build())
                .build();
        monografiPcList.add(monografiPc);
        Page<MonografiPc> pagedResponse = new PageImpl(monografiPcList);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(monografiPcRepository.findByDeletedAtIsNull(pageableTest)).thenReturn(pagedResponse);
        List<MonografiPc> listResult = monografiPCService.getAllMonografiPC(pageableTest, false);
        assertEquals(monografiPcList.size(), listResult.size());
    }

    @Test
    void create() {

        MonografiPc monografiPc = MonografiPc.builder()
                .id(1L)
                .cabang(cabang)
                .daerah(daerah)
                .wilayah(wilayah)
                .kecamatan(kecamatan)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .jamiyyahPc(jamiyyahPc)
                .build();
        JamiyyahPc jamiyyahPc = JamiyyahPc.builder().monografiPc(monografiPc).build();

        when(cabangService.getByKodeCabang(cabang.getKdPc())).thenReturn(cabang);
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getKecamatanById(kecamatan.getId())).thenReturn(kecamatan);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(jamiyyahPcRepository.save(any(JamiyyahPc.class))).thenReturn(jamiyyahPc);
        MonografiPc resultMonografiPc = monografiPCService.create(monografiPc);

        assertEquals(resultMonografiPc.getId(), monografiPc.getId());
    }

    @Test
    void update() {
        MonografiPcDto monografiPcDto =
                MonografiPcDto.builder().kabupaten(kabupatenDto).build();

        MonografiPc monografiPc = MonografiPc.builder()
                .id(1L)
                .cabang(cabang)
                .daerah(daerah)
                .wilayah(wilayah)
                .kecamatan(kecamatan)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .jamiyyahPc(jamiyyahPc)
                .deletedAt(null)
                .build();
        MonografiPc monografiPcResult = MonografiPc.builder()
                .id(1L)
                .cabang(cabang)
                .daerah(daerah)
                .wilayah(wilayah)
                .kecamatan(kecamatan)
                .kabupaten(Kabupaten.builder().id("KAB").build())
                .provinsi(provinsi)
                .jamiyyahPc(jamiyyahPc)
                .build();

        Configuration configuration = mock(Configuration.class);
        Long monografiPcId = 1L;

        when(monografiPcRepository.findByIdAndDeletedAtIsNull(monografiPcId)).thenReturn(Optional.of(monografiPc));
        when(cabangService.getByKodeCabang(cabang.getKdPc())).thenReturn(cabang);
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getKecamatanById(kecamatan.getId())).thenReturn(kecamatan);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(monografiPcRepository.save(monografiPc)).thenReturn(monografiPcResult);
        when(modelMapper.getConfiguration()).thenReturn(configuration);

        MonografiPc result = monografiPCService.update(monografiPcDto, monografiPcId);
        assertEquals(result.getKecamatan().getId(), monografiPc.getKecamatan().getId());
    }

    @Test
    void delete() {
        MonografiPc monografiPc = MonografiPc.builder()
                .id(1L)
                .cabang(cabang)
                .daerah(daerah)
                .wilayah(wilayah)
                .kecamatan(kecamatan)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .jamiyyahPc(jamiyyahPc)
                .deletedAt(null)
                .build();
        MonografiPc monografiPcResult = MonografiPc.builder()
                .id(1L)
                .cabang(cabang)
                .daerah(daerah)
                .wilayah(wilayah)
                .deletedAt(LocalDateTime.now())
                .kecamatan(kecamatan)
                .kabupaten(Kabupaten.builder().id("KAB").build())
                .provinsi(provinsi)
                .jamiyyahPc(JamiyyahPc.builder()
                        .id(1L)
                        .deletedAt(LocalDateTime.now())
                        .build())
                .build();
        Long monografiPcId = 1L;
        when(monografiPcRepository.findByIdAndDeletedAtIsNull(monografiPcId)).thenReturn(Optional.of(monografiPc));
        when(monografiPcRepository.save(monografiPc)).thenReturn(monografiPcResult);
        MonografiPc result = monografiPCService.delete(1L);
        assertNotNull(result.getDeletedAt());
    }
}
