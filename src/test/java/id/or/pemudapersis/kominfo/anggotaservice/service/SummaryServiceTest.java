package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.SummaryVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import java.security.Principal;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

class SummaryServiceTest extends SummaryVariables {

    public static final String SUCCESS = "SUCCESS";

    @InjectMocks
    private SummaryService summaryService;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private PerformaPjService performaPjService;

    @Mock
    private PerformaPwService performaPwService;

    @Mock
    private PerformaPdService performaPdService;

    @Mock
    private PerformaPcService performaPcService;

    @Mock
    private LoginService loginService;

    @Mock
    private Principal principal;

    @Mock
    private MonografiPcService monografiPcService;

    @Mock
    private MonografiPdService monografiPdService;

    @Mock
    private MonografiPwService monografiPwService;

    @Mock
    private MonografiPjService monografiPjService;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(anggotaService);
        verifyNoMoreInteractions(performaPjService);
        verifyNoMoreInteractions(performaPwService);
        verifyNoMoreInteractions(performaPdService);
        verifyNoMoreInteractions(performaPcService);
        verifyNoMoreInteractions(loginService);
        verifyNoMoreInteractions(monografiPcService);
        verifyNoMoreInteractions(monografiPwService);
        verifyNoMoreInteractions(monografiPdService);
        verifyNoMoreInteractions(monografiPjService);
    }

    @Test
    void summaryPerforma() {
        when(anggotaService.countAnggota()).thenReturn(LONG_ID);
        when(performaPjService.countPerformaPj()).thenReturn(LONG_ID);
        when(performaPcService.countPerformaPc()).thenReturn(LONG_ID);
        when(performaPdService.countPerformaPd()).thenReturn(LONG_ID);
        when(performaPwService.countPerformaPw()).thenReturn(LONG_ID);
        when(loginService.countUser()).thenReturn(LONG_ID);
        BaseResponse<SummaryPerformaDto> response = summaryService.summaryPerforma(principal);
        assertEquals(response.getMessage(), SUCCESS);
        verify(anggotaService).countAnggota();
        verify(performaPcService).countPerformaPc();
        verify(performaPdService).countPerformaPd();
        verify(performaPwService).countPerformaPw();
        verify(performaPjService).countPerformaPj();
        verify(loginService).countUser();
    }

    @Test
    void summaryMonogarfi() {
        when(monografiPcService.countMonografiPc()).thenReturn(LONG_ID);
        when(monografiPdService.countMonografiPd()).thenReturn(LONG_ID);
        when(monografiPwService.countMonografiPw()).thenReturn(LONG_ID);
        when(monografiPjService.countMonografiPj()).thenReturn(LONG_ID);
        BaseResponse<SummaryMonografiDto> response = summaryService.summaryMonogarfi(principal);
        assertEquals(response.getMessage(), SUCCESS);
        verify(monografiPcService).countMonografiPc();
        verify(monografiPdService).countMonografiPd();
        verify(monografiPjService).countMonografiPj();
        verify(monografiPwService).countMonografiPw();
    }

    @Test
    void summaryGolonganDarah() {
        when(anggotaService.findAllByDeletedAtIsNull()).thenReturn(ANGGOTA_LIST);
        BaseResponse<SummaryGolDarahDto> response = summaryService.summaryGolonganDarah(principal);
        assertEquals(response.getMessage(), SUCCESS);
        verify(anggotaService).findAllByDeletedAtIsNull();
    }

    @Test
    void summaryStatusMarital() {
        when(anggotaService.findAllByDeletedAtIsNull()).thenReturn(ANGGOTA_LIST);
        BaseResponse<SummaryStatusMaritalDto> response = summaryService.summaryStatusMarital(principal);
        assertEquals(response.getMessage(), SUCCESS);
        verify(anggotaService).findAllByDeletedAtIsNull();
    }

    @Test
    void summaryStatusKeanggotaan() {
        when(anggotaService.findAllByDeletedAtIsNull()).thenReturn(ANGGOTA_LIST);
        BaseResponse<SummaryStatusKeanggotaanDto> response = summaryService.summaryStatusKeanggotaan(principal);
        assertEquals(response.getMessage(), SUCCESS);
        verify(anggotaService).findAllByDeletedAtIsNull();
    }

    @Test
    void summaryStatusPendidikan() {
        when(anggotaService.findAllByDeletedAtIsNull()).thenReturn(ANGGOTA_LIST);
        BaseResponse<SummaryStatusPendidikanDto> response = summaryService.summaryStatusPendidikan(principal);
        assertEquals(response.getMessage(), SUCCESS);
        verify(anggotaService).findAllByDeletedAtIsNull();
    }

    @Test
    void summaryAnggotaPerWilayah() {
        when(anggotaService.findAllByDeletedAtIsNull()).thenReturn(ANGGOTA_LIST);
        BaseResponse<SummaryAnggotaPerWilayahDto> response = summaryService.summaryAnggotaPerWilayah(principal);
        assertEquals(response.getMessage(), SUCCESS);
        verify(anggotaService).findAllByDeletedAtIsNull();
    }

    @Test
    void summaryUsiaAnggota() {
        when(anggotaService.findAllByDeletedAtIsNull()).thenReturn(ANGGOTA_LIST);
        BaseResponse<SummaryUsiaAnggotaDto> response = summaryService.summaryUsiaAnggota(principal);
        assertEquals(response.getMessage(), SUCCESS);
        verify(anggotaService).findAllByDeletedAtIsNull();
    }
}
