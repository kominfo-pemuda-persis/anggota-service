package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import id.or.pemudapersis.kominfo.anggotaservice.dto.RekapCaangDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.RekapCaang;
import id.or.pemudapersis.kominfo.anggotaservice.mapper.RekapCaangMapperImpl;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@DataJpaTest
class RekapCaangRepositoryTest {
    @Autowired
    private RekapCaangRepository rekapCaangRepository;

    @Test
    void shouldGetAllDataWithNoSoftDelete() {
        RekapCaangDto entity = RekapCaangDto.builder().build();
        RekapCaang newData = new RekapCaangMapperImpl().toEntity(entity);
        newData.setAnggota(null);
        rekapCaangRepository.save(newData);
        Page<RekapCaang> data = rekapCaangRepository.findAllByDeletedAtIsNull(PageRequest.of(0, 10));
        assertFalse(data.isEmpty());
    }

    @Test
    void shouldGetEmptyData() {
        RekapCaangDto entity = RekapCaangDto.builder().build();
        RekapCaang newData = new RekapCaangMapperImpl().toEntity(entity);
        newData.setDeletedAt(LocalDateTime.now());
        newData.setAnggota(null);
        rekapCaangRepository.save(newData);
        Page<RekapCaang> data = rekapCaangRepository.findAllByDeletedAtIsNull(PageRequest.of(0, 10));
        assertTrue(data.isEmpty());
    }
}
