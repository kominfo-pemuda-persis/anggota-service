package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class JwtServiceImplTest {

    @InjectMocks
    private JwtServiceImpl service;

    @Mock
    private JWTVerifier jwtVerifier;

    @BeforeEach
    void setUp() {
        service.setJwtSecret("secret");
        service.setJwtExpirationMs(86400000L);
    }

    @Test
    void generateTokenShouldReturnToken() {
        var actual = service.generateToken(
                new GenerateTokenInput("99.0008", getPermissions(), "anggota", "localhost", "", "", "", ""));

        assertThat(actual)
                .isNotNull()
                .matches(token -> decodeJwtPayload(actual).contains("99.0008"))
                .matches(token -> decodeJwtPayload(actual).contains("permissions"))
                .matches(token -> decodeJwtPayload(actual).contains("role"))
                .matches(token -> decodeJwtPayload(actual).contains("npa"));
    }

    private static String decodeJwtPayload(String actual) {
        return new String(Base64.decodeBase64(JWT.decode(actual).getPayload()), StandardCharsets.UTF_8);
    }

    private static List<String> getPermissions() {
        return Arrays.asList(
                "anggota_read_pc",
                "dashboard_read",
                "indexperformapc_create",
                "indexperformapd_create",
                "indexperformapj_create",
                "jamiyyah_create",
                "monografipc_create",
                "monografipd_create",
                "monografipj_create",
                "mutasi_read",
                "otonom_read",
                "permission_read",
                "role_read",
                "users_read");
    }

    @Nested
    class GetSubjectTest {
        @Test
        void shouldReturnSubject() {
            given(jwtVerifier.verify(anyString())).willReturn(new MockedDecodedJWT());

            var actual = service.getSubject(
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OS4wMDA4Iiwicm9sZXMiOlsiYW5nZ290YV9yZWFkX3BjIiwiZGFzaGJvYXJkX3JlYWQiLCJpbmRleHBlcmZvcm1hcGNfY3JlYXRlIiwiaW5kZXhwZXJmb3JtYXBkX2NyZWF0ZSIsImluZGV4cGVyZm9ybWFwal9jcmVhdGUiLCJqYW1peXlhaF9jcmVhdGUiLCJtb25vZ3JhZmlwY19jcmVhdGUiLCJtb25vZ3JhZmlwZF9jcmVhdGUiLCJtb25vZ3JhZmlwal9jcmVhdGUiLCJtdXRhc2lfcmVhZCIsIm90b25vbV9yZWFkIiwicGVybWlzc2lvbl9yZWFkIiwicm9sZV9yZWFkIiwidXNlcnNfcmVhZCJdLCJpc3MiOiJsb2NhbGhvc3QiLCJleHAiOjE3MDUyODc2OTd9.nry-4WyMKxfW0PXIa4Db-uhgSBz3eyPYbZPUf_tdPsU");

            assertThat(actual).isEqualTo("99.0008");
        }
    }

    @Nested
    class IsJwtValidTest {
        @Test
        void shouldReturnTrue() {
            given(jwtVerifier.verify(anyString())).willReturn(new MockedDecodedJWT());

            var actual = service.isJwtValid(
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OS4wMDA4Iiwicm9sZXMiOlsiYW5nZ290YV9yZWFkX3BjIiwiZGFzaGJvYXJkX3JlYWQiLCJpbmRleHBlcmZvcm1hcGNfY3JlYXRlIiwiaW5kZXhwZXJmb3JtYXBkX2NyZWF0ZSIsImluZGV4cGVyZm9ybWFwal9jcmVhdGUiLCJqYW1peXlhaF9jcmVhdGUiLCJtb25vZ3JhZmlwY19jcmVhdGUiLCJtb25vZ3JhZmlwZF9jcmVhdGUiLCJtb25vZ3JhZmlwal9jcmVhdGUiLCJtdXRhc2lfcmVhZCIsIm90b25vbV9yZWFkIiwicGVybWlzc2lvbl9yZWFkIiwicm9sZV9yZWFkIiwidXNlcnNfcmVhZCJdLCJpc3MiOiJsb2NhbGhvc3QiLCJleHAiOjE3MDUyODc2OTd9.nry-4WyMKxfW0PXIa4Db-uhgSBz3eyPYbZPUf_tdPsU",
                    "99.0008");

            assertThat(actual).isTrue();
        }

        @Test
        void shouldReturnFalse() {
            given(jwtVerifier.verify(anyString()))
                    .willReturn(new MockedDecodedJWT(Instant.now().minusSeconds(7200L)));

            var actual = service.isJwtValid(
                    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5OS4wMDA4Iiwicm9sZXMiOlsiYW5nZ290YV9yZWFkX3BjIiwiZGFzaGJvYXJkX3JlYWQiLCJpbmRleHBlcmZvcm1hcGNfY3JlYXRlIiwiaW5kZXhwZXJmb3JtYXBkX2NyZWF0ZSIsImluZGV4cGVyZm9ybWFwal9jcmVhdGUiLCJqYW1peXlhaF9jcmVhdGUiLCJtb25vZ3JhZmlwY19jcmVhdGUiLCJtb25vZ3JhZmlwZF9jcmVhdGUiLCJtb25vZ3JhZmlwal9jcmVhdGUiLCJtdXRhc2lfcmVhZCIsIm90b25vbV9yZWFkIiwicGVybWlzc2lvbl9yZWFkIiwicm9sZV9yZWFkIiwidXNlcnNfcmVhZCJdLCJpc3MiOiJsb2NhbGhvc3QiLCJleHAiOjE3MDUyODc2OTd9.nry-4WyMKxfW0PXIa4Db-uhgSBz3eyPYbZPUf_tdPsU",
                    "subject");

            assertThat(actual).isFalse();
        }
    }

    private static class MockedDecodedJWT implements DecodedJWT {

        private final Instant now = Instant.now();

        private Instant expiredAt;

        MockedDecodedJWT() {}

        MockedDecodedJWT(Instant expiredAt) {
            this.expiredAt = expiredAt;
        }

        @Override
        public String getToken() {
            return "token";
        }

        @Override
        public String getHeader() {
            return "header";
        }

        @Override
        public String getPayload() {
            return "payload";
        }

        @Override
        public String getSignature() {
            return "signature";
        }

        @Override
        public String getAlgorithm() {
            return "algorithm";
        }

        @Override
        public String getType() {
            return "type";
        }

        @Override
        public String getContentType() {
            return "contentType";
        }

        @Override
        public String getKeyId() {
            return "keyId";
        }

        @Override
        public Claim getHeaderClaim(String name) {
            return null;
        }

        @Override
        public String getIssuer() {
            return "localhost";
        }

        @Override
        public String getSubject() {
            return "99.0008";
        }

        @Override
        public List<String> getAudience() {
            return Collections.emptyList();
        }

        @Override
        public Date getExpiresAt() {
            return Optional.ofNullable(expiredAt).map(Date::from).orElseGet(() -> Date.from(now.plusSeconds(7200L)));
        }

        @Override
        public Date getNotBefore() {
            return Date.from(now);
        }

        @Override
        public Date getIssuedAt() {
            return Date.from(now);
        }

        @Override
        public String getId() {
            return "id";
        }

        @Override
        public Claim getClaim(String name) {
            return null;
        }

        @Override
        public Map<String, Claim> getClaims() {
            return null;
        }
    }
}
