package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.RoleDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Role;
import id.or.pemudapersis.kominfo.anggotaservice.repository.RoleRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.RoleVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import java.util.Collections;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class RoleServiceTest extends RoleVariables {

    @Mock
    private RoleRepository roleRepository;

    @InjectMocks
    private RoleService roleService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(roleRepository);
    }

    @Test
    void getAllRole() {
        Page<Role> roles = mock(Page.class);
        when(roles.getContent()).thenReturn(Collections.singletonList(ROLE));
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(roleRepository.findAll(eq(pageableTest))).thenReturn(ROLE_PAGE);
        when(modelMapper.map(ROLE, RoleDto.class)).thenReturn(ROLE_DTO);
        BaseResponse<Page<RoleDto>> baseResponse = roleService.getAllRole(pageableTest);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(roleRepository).findAll(eq(pageableTest));
        verify(modelMapper).map(ROLE, RoleDto.class);
    }
}
