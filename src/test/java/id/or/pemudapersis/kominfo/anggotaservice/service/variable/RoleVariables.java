package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.RoleDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Role;
import java.util.Collections;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class RoleVariables extends AnggotaVariableTest {
    public static final Role ROLE = Role.builder()
            .id(LONG_ID)
            .description("DESC")
            .displayName("DISPLAY")
            .build();
    public static final RoleDto ROLE_DTO = RoleDto.builder()
            .id(LONG_ID)
            .description("DESC")
            .displayName("DISPLAY")
            .build();
    public final Page<Role> ROLE_PAGE = new PageImpl<>(Collections.singletonList(ROLE));
}
