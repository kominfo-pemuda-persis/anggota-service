package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MutasiDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Mutasi;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MutasiRespository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.MutasiVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class MutasiServiceTest extends MutasiVariables {
    public static final String SUCCESS = "SUCCESS";
    public static final String MUTASI_NOT_FOUND = "Mutasi Not Found";
    public static final long ID_MUTASI = 1L;

    @InjectMocks
    private MutasiService mutasiService;

    @Mock
    private MutasiRespository mutasiRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(mutasiRepository);
        verifyNoMoreInteractions(anggotaService);
    }

    @Test
    void mutasiListWhenSuccess() {
        Page<Mutasi> mutasiPage = mock(Page.class);
        when(mutasiPage.getContent()).thenReturn(MUTASI_LIST);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(modelMapper.map(MUTASI, MutasiDto.class)).thenReturn(MUTASI_DTO);
        when(mutasiRepository.findNotDeletedByAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(MUTASI_PAGE);
        BaseResponse<Page<MutasiDto>> baseResponse = mutasiService.mutasiList(pageableTest, false, NPA);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(mutasiRepository).findNotDeletedByAnggotaNpa(anyString(), eq(pageableTest));
        verify(modelMapper).map(MUTASI, MutasiDto.class);
    }

    @Test
    void createMutasiWhenSuccess() {
        when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
        when(modelMapper.map(MUTASI, MutasiDto.class)).thenReturn(MUTASI_DTO);
        when(mutasiRepository.save(any(Mutasi.class))).thenReturn(MUTASI);
        BaseResponse<MutasiDto> baseResponse = mutasiService.create(NPA, MUTASI_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(anggotaService).findByNpa(NPA);
        verify(mutasiRepository).save(any(Mutasi.class));
        verify(modelMapper).map(MUTASI, MutasiDto.class);
    }

    @Test
    void updateMutasiWhenSuccess() {
        when(mutasiRepository.findNotDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong()))
                .thenReturn(Optional.of(MUTASI));
        when(mutasiRepository.save(any(Mutasi.class))).thenReturn(MUTASI);
        when(modelMapper.map(MUTASI, MutasiDto.class)).thenReturn(MUTASI_DTO);
        BaseResponse<MutasiDto> baseResponse = mutasiService.update(anyString(), anyLong(), MUTASI_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(mutasiRepository).findNotDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong());
        verify(mutasiRepository).save(any(Mutasi.class));
        verify(modelMapper).map(MUTASI, MutasiDto.class);
        verify(modelMapper).map(MUTASI_DTO, MUTASI);
    }

    @Test
    void updateMutasiWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(mutasiRepository.findNotDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            mutasiService.update(anyString(), anyLong(), MUTASI_DTO);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(MUTASI_NOT_FOUND));

        verify(mutasiRepository).findNotDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong());
    }

    @Test
    void deleteMutasiWhenSuccess() {
        when(mutasiRepository.findNotDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong()))
                .thenReturn(Optional.of(MUTASI));
        when(modelMapper.map(MUTASI, MutasiDto.class)).thenReturn(MUTASI_DTO);
        when(mutasiRepository.save(any(Mutasi.class))).thenReturn(MUTASI);
        BaseResponse<MutasiDto> baseResponse = mutasiService.delete(NPA, ID_MUTASI);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(mutasiRepository).findNotDeletedByIdMutasiAndAnggotaNpa(NPA, ID_MUTASI);
        verify(mutasiRepository).save(any(Mutasi.class));
        verify(modelMapper).map(MUTASI, MutasiDto.class);
    }

    @Test
    void deleteMutasiWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(mutasiRepository.findNotDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            mutasiService.delete(NPA, ID_MUTASI);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(MUTASI_NOT_FOUND));

        verify(mutasiRepository).findNotDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong());
    }

    @Test
    void restoreMutasiWhenSuccess() {
        when(mutasiRepository.findDeletedByIdMutasiAndAnggotaNpa(NPA, LONG_ID)).thenReturn(Optional.of(MUTASI));
        when(mutasiRepository.save(any(Mutasi.class))).thenReturn(MUTASI);
        when(modelMapper.map(MUTASI, MutasiDto.class)).thenReturn(MUTASI_DTO);
        BaseResponse<MutasiDto> baseResponse = mutasiService.restore(NPA, LONG_ID);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(mutasiRepository).findDeletedByIdMutasiAndAnggotaNpa(NPA, LONG_ID);
        verify(mutasiRepository).save(any(Mutasi.class));
        verify(modelMapper).map(MUTASI, MutasiDto.class);
    }

    @Test
    void restoreMutasiWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(mutasiRepository.findDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            mutasiService.restore(NPA, ID_MUTASI);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(MUTASI_NOT_FOUND));
        verify(mutasiRepository).findDeletedByIdMutasiAndAnggotaNpa(anyString(), anyLong());
    }
}
