package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import com.querydsl.core.types.Predicate;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Cabang;
import id.or.pemudapersis.kominfo.anggotaservice.repository.CabangRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.CabangTestVariable;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Collections;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class CabangServiceTest extends CabangTestVariable {
    @InjectMocks
    private CabangService cabangService;

    @Mock
    private CabangRepository cabangRepository;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(cabangRepository);
    }

    @Test
    void getList() {
        Page<Cabang> cabangs = mock(Page.class);
        String kdPd = "PD-1";
        when(cabangs.getContent()).thenReturn(Collections.singletonList(CABANG));
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(cabangRepository.findAll(any(Predicate.class), eq(pageableTest))).thenReturn(CABANG_PAGE);
        when(modelMapper.map(CABANG, CabangRequest.class)).thenReturn(CABANG_REQUEST);
        BaseResponse<Page<CabangRequest>> baseResponse = cabangService.getList(pageableTest, kdPd);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(cabangRepository).findAll(any(Predicate.class), eq(pageableTest));
        verify(modelMapper).map(CABANG, CabangRequest.class);
    }

    @Test
    void createCabangWhenSuccess() {
        when(cabangRepository.save(any(Cabang.class))).thenReturn(CABANG);
        when(modelMapper.map(CABANG, CabangRequest.class)).thenReturn(CABANG_REQUEST);
        BaseResponse<CabangRequest> baseResponse = cabangService.create(CABANG_REQUEST);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(cabangRepository).save(any(Cabang.class));
        verify(modelMapper).map(CABANG, CabangRequest.class);
    }

    @Test
    void updateCabangWhenSuccess() {
        when(cabangRepository.findBykdPc(anyString())).thenReturn(Optional.of(CABANG));
        when(cabangRepository.save(any(Cabang.class))).thenReturn(CABANG);
        when(modelMapper.map(CABANG, CabangRequest.class)).thenReturn(CABANG_REQUEST);
        BaseResponse<CabangRequest> baseResponse = cabangService.update(anyString(), CABANG_REQUEST);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(cabangRepository).save(any(Cabang.class));
        verify(cabangRepository).findBykdPc(anyString());
        verify(modelMapper).map(CABANG, CabangRequest.class);
    }

    @Test
    void updateCabangWhen_SomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(cabangRepository.findBykdPc(anyString())).thenReturn(Optional.empty());
            cabangService.update(anyString(), CABANG_REQUEST);
        });
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Kode Cabang not found"));

        verify(cabangRepository).findBykdPc(anyString());
    }

    @Test
    void deleteCabangWhenSuccess() {
        when(cabangRepository.findBykdPc(anyString())).thenReturn(Optional.of(CABANG));
        when(cabangRepository.save(any(Cabang.class))).thenReturn(CABANG);
        BaseResponse<CabangRequest> baseResponse = cabangService.delete(anyString());
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(cabangRepository).save(any(Cabang.class));
        verify(cabangRepository).findBykdPc(anyString());
    }

    @Test
    void deleteCabangWhen_SomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(cabangRepository.findBykdPc(anyString())).thenReturn(Optional.empty());
            cabangService.delete(anyString());
        });
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Kode Cabang not found"));

        verify(cabangRepository).findBykdPc(anyString());
    }

    @Test
    void getByKodeCabangWhenSuccess() {
        when(cabangRepository.findBykdPc(anyString())).thenReturn(Optional.of(CABANG));
        Cabang cabang = cabangService.getByKodeCabang(anyString());
        assertNotNull(cabang);
        verify(cabangRepository).findBykdPc(anyString());
    }

    @Test
    void getByKodeCabangWhen_SomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(cabangRepository.findBykdPc(anyString())).thenReturn(Optional.empty());
            cabangService.getByKodeCabang(anyString());
        });
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Kode Cabang not found"));

        verify(cabangRepository).findBykdPc(anyString());
    }
}
