package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPj;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.annotation.DirtiesContext;

@DataJpaTest
@Log4j2
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class MonografiPjRepositoryTest {

    @Autowired
    private MonografiPjRepository monografiPjRepository;

    @Autowired
    private DesaRepository desaRepository;

    @Autowired
    private KecamatanRepository kecamatanRepository;

    @Autowired
    private KabupatenRepository kabupatenRepository;

    @Autowired
    private ProvinsiRepository provinsiRepository;

    @Autowired
    private CabangRepository cabangRepository;

    @Autowired
    private DaerahRepository daerahRepository;

    @Autowired
    private WilayahRepository wilayahRepository;

    private ObjectMapper objectMapper;

    private String stringJsonSample =
            "{\"id\":13,\"kodePj\":\"PJ-01\",\"namaPj\":\"cab. kejora 2 edit 1x\",\"kdPc\":{\"kdPc\":\"PC.1\",\"namaPc\":\"Andir\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"kdPd\":{\"kdPd\":\"PD-1\",\"namaPd\":\"Kota Bandung\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"kdPw\":{\"kdPw\":\"PW-2\",\"namaWilayah\":\"DKI Jakarta\",\"createdAt\":\"22-09-2020 20:48:00\",\"updatedAt\":\"22-09-2020 20:48:00\"},\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"kecamatan\":{\"id\":\"1\",\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\" Japah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"desa\":{\"id\":\"1\",\"kecamatan\":{\"id\":\"1\",\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\" Japah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Japah\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"latitude\":1.1,\"longitude\":1.1,\"email\":\"3a@kejora.com\",\"noTelpon\":\"11\",\"alamat\":\"jakarta\",\"luas\":\"1\",\"bwTimur\":\"1\",\"bwBarat\":\"10\",\"bwSelatan\":\"1\",\"bwUtara\":\"1\",\"jarakProvinsi\":\"1\",\"jarakKabupaten\":\"1\",\"photo\":null,\"createdBy\":\"\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"}";

    @Disabled("Disabled until error fixed")
    @BeforeEach
    public void setup() throws Exception {
        objectMapper = new ObjectMapper();
        MonografiPj monografiPj = objectMapper.readValue(stringJsonSample, MonografiPj.class);

        try {
            provinsiRepository.save(monografiPj.getProvinsi());
            kabupatenRepository.save(monografiPj.getKabupaten());
            kecamatanRepository.save(monografiPj.getKecamatan());
            desaRepository.save(monografiPj.getDesa());
            daerahRepository.save(monografiPj.getKdPd());
            cabangRepository.save(monografiPj.getKdPc());
            wilayahRepository.save(monografiPj.getKdPw());
            monografiPjRepository.save(monografiPj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Disabled("Disabled until error fixed")
    @Test
    void testFindAll() throws Exception {
        List<MonografiPj> monografiPjList = monografiPjRepository.findAll();
        String monografiString = objectMapper.writeValueAsString(monografiPjList);
        log.info("monografi pj find all : {}", monografiString);
        assertNotNull(monografiPjList);
    }

    @Disabled("Disabled until error fixed")
    @Test
    void testFindByDeletedAtNotNull() throws Exception {
        Pageable pageable = PageRequest.of(0, 2);
        Optional<MonografiPj> monografiPjOptional = monografiPjRepository.findById(1L);
        monografiPjOptional.get().setDeletedAt(new Timestamp(System.currentTimeMillis()));

        monografiPjRepository.save(monografiPjOptional.get());

        Page<MonografiPj> monografiPjPage = monografiPjRepository.findByDeletedAtNotNull(pageable);

        List<MonografiPj> content = monografiPjPage.getContent();
        String monografiString = objectMapper.writeValueAsString(content);
        log.info("monografi pj not null : {}", monografiString);
        assertNotNull(monografiPjPage);
    }
}
