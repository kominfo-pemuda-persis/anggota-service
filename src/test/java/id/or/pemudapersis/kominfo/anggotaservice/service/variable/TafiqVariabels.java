package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.TafiqDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Tafiq;
import java.time.LocalDate;
import java.util.Collections;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class TafiqVariabels extends AnggotaVariableTest {
    public final Tafiq TAFIQ = Tafiq.builder()
            .idTafiq(LONG_ID)
            .anggota(ANGGOTA)
            .lokasi("LOKASI")
            .tanggalMasuk(LocalDate.now())
            .tanggalSelesai(LocalDate.now())
            .build();
    public final TafiqDto TAFIQ_DTO = TafiqDto.builder()
            .idTafiq(LONG_ID)
            .lokasi("LOKASI")
            .tanggalMasuk(LocalDate.now())
            .tanggalSelesai(LocalDate.now())
            .build();
    public final Page<Tafiq> TAFIQ_PAGE = new PageImpl<>(Collections.singletonList(TAFIQ));
}
