package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
class AnggotaRepositoryTest {

    @MockBean
    private AnggotaRepository anggotaRepository;

    @Test
    void findByNpa() {
        Anggota anggota = Anggota.builder().id("1L").npa("99.0001").build();
        Optional<Anggota> optionalAnggota = Optional.of(anggota);
        Mockito.when(anggotaRepository.findByNpa("99.0001")).thenReturn(optionalAnggota);
        Optional<Anggota> anggotaResult = anggotaRepository.findByNpa("99.0001");
        assertEquals(anggotaResult.get().getNpa(), anggota.getNpa());
    }
}
