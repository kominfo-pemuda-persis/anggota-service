package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Login;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Permission;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Role;
import id.or.pemudapersis.kominfo.anggotaservice.repository.LoginRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.mail.MailService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UserDetails;

@ExtendWith(MockitoExtension.class)
public class LoginServiceTest {
    @Mock
    private LoginRepository loginRepository;

    @Mock
    private MailService mailService;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private LoginService loginService;

    @BeforeEach
    public void setUp() {
        loginService = new LoginService(loginRepository, mailService, modelMapper);
    }

    @Test
    public void loginSuccess() {
        String npa = "001";
        Anggota anggota = Anggota.builder().id("1L").npa("001").build();
        Login login = Login.builder()
                .idLogin("1")
                .anggota(anggota)
                .password("$2a$10$msEsC67VjkIsA3kQPcQZM.6Vi1tUx1EIH/MhIN0nzxLbtC/jB40tu")
                .activCode("Y")
                .emailConfirm(1)
                .statusAktif(1)
                .build();

        when(loginRepository.findByAnggotaNpa(npa)).thenReturn(Optional.of(login));

        UserDetails userDetails = loginService.loadUserByUsername(npa);

        assertEquals(npa, userDetails.getUsername());
    }

    @Test
    public void loginFail() {
        String npa = "001";
        Throwable exception = assertThrows(BusinessException.class, () -> loginService.loadUserByUsername(npa));
        assertEquals("Invalid NPA/Username or Password", exception.getMessage());
    }

    @Nested
    class FindUserByNpa {
        @Test
        void shouldThrowExceptionWhenNpaNotFound() {
            given(loginRepository.findByAnggotaNpa(anyString())).willReturn(Optional.empty());

            assertThatThrownBy(() -> loginService.findUserByNpa("12345"))
                    .isInstanceOf(BusinessException.class)
                    .hasMessage("USER NOT FOUND");
        }

        @Test
        void shouldReturnDto() {
            var actual = prepareAndExecute();

            assertThat(actual)
                    .isNotNull()
                    .matches(dto -> 1 == dto.getAuthorities().size())
                    .matches(dto -> "anggota".equals(dto.roleName()));
        }

        private LoginDto prepareAndExecute() {
            given(loginRepository.findByAnggotaNpa(anyString()))
                    .willReturn(Optional.of(Login.builder()
                            .idLogin("04167d386bd648d49de247e06e7a2bff")
                            .password("$2a$10$KRW4Qo3QoaagauPsC.WVNeMYXYxCWBybmdbS0R1W9dXJ64DWo8x6i")
                            .roles(Set.of(Role.builder()
                                    .name("anggota")
                                    .permissions(Set.of(Permission.builder()
                                            .module("users")
                                            .permission("read")
                                            .build()))
                                    .build()))
                            .anggota(Anggota.builder().npa("99.0008").build())
                            .build()));

            return loginService.findUserByNpa("99.0008");
        }

        @Test
        void shouldFindByAnggotaNpa() {
            prepareAndExecute();

            verify(loginRepository).findByAnggotaNpa("99.0008");
        }
    }
}
