package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class SummaryVariables extends AnggotaVariableTest {
    public final Anggota ANGGOTA1 = Anggota.builder()
            .golDarah("O")
            .statusMerital("Menikah")
            .jenisKeanggotaan("Biasa")
            .pc(Cabang.builder().namaPc("PCA").build())
            .pd(Daerah.builder().namaPd("PDB").build())
            .pw(Wilayah.builder().namaWilayah("Wilayah 1").build())
            .jenjangPendidikan(MasterPendidikan.builder().pendidikan("SD").build())
            .tanggalLahir(LocalDate.now().minusYears(15))
            .build();

    public final Anggota ANGGOTA2 = Anggota.builder()
            .golDarah("A")
            .statusMerital("Single")
            .jenisKeanggotaan("Tersiar")
            .pc(Cabang.builder().namaPc("PCB").build())
            .pd(Daerah.builder().namaPd("PDB").build())
            .pw(Wilayah.builder().namaWilayah("Wilayah 1").build())
            .jenjangPendidikan(MasterPendidikan.builder().pendidikan("S3").build())
            .tanggalLahir(LocalDate.now().minusYears(36))
            .build();

    public final Anggota ANGGOTA3 = Anggota.builder()
            .golDarah("AB")
            .jenjangPendidikan(MasterPendidikan.builder().pendidikan("MI").build())
            .statusMerital("Menikah")
            .jenisKeanggotaan("Tersiar")
            .pc(Cabang.builder().namaPc("PCL").build())
            .pd(Daerah.builder().namaPd("PDC").build())
            .pw(Wilayah.builder().namaWilayah("Wilayah 3").build())
            .tanggalLahir(LocalDate.now().minusYears(24))
            .build();

    public final Anggota ANGGOTA4 = Anggota.builder()
            .golDarah("B")
            .tanggalLahir(LocalDate.now().minusYears(29))
            .jenisKeanggotaan("Kehormatan")
            .pc(Cabang.builder().namaPc("PCBL").build())
            .pd(Daerah.builder().namaPd("PDC").build())
            .pw(Wilayah.builder().namaWilayah("Wilayah 3").build())
            .jenjangPendidikan(MasterPendidikan.builder().pendidikan("SMK").build())
            .statusMerital("Duda")
            .build();

    public final List<Anggota> ANGGOTA_LIST = Arrays.asList(ANGGOTA2, ANGGOTA3, ANGGOTA1, ANGGOTA4);
}
