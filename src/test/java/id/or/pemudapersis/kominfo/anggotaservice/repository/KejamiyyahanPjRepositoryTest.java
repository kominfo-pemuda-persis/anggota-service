package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.or.pemudapersis.kominfo.anggotaservice.entity.KejamiyyahanPj;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

@DataJpaTest
@Log4j2
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class KejamiyyahanPjRepositoryTest {

    @Autowired
    private KejamiyyahanPjRepository kejamiyyahanPjRepository;

    @Autowired
    private DesaRepository desaRepository;

    @Autowired
    private KecamatanRepository kecamatanRepository;

    @Autowired
    private KabupatenRepository kabupatenRepository;

    @Autowired
    private ProvinsiRepository provinsiRepository;

    @Autowired
    private CabangRepository cabangRepository;

    @Autowired
    private DaerahRepository daerahRepository;

    @Autowired
    private WilayahRepository wilayahRepository;

    private ObjectMapper objectMapper;

    private String kejamiyyahanJsonString =
            "{\"id\":13,\"kodePj\":\"PJ-01\",\"namaPj\":\"cab. kejora 2 edit 1x\",\"monografiPj\":{\"id\":13,\"kodePj\":\"PJ-01\",\"namaPj\":\"cab. kejora 2 edit 1x\",\"kdPc\":{\"kdPc\":\"PC.1\",\"namaPc\":\"Andir\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"kdPd\":{\"kdPd\":\"PD-1\",\"namaPd\":\"Kota Bandung\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"kdPw\":{\"kdPw\":\"PW-2\",\"namaWilayah\":\"DKI Jakarta\",\"createdAt\":\"22-09-2020 20:48:00\",\"updatedAt\":\"22-09-2020 20:48:00\"},\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"kecamatan\":{\"id\":\"1\",\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\" Japah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"desa\":{\"id\":\"1\",\"kecamatan\":{\"id\":\"1\",\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\" Japah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Japah\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"latitude\":1.1,\"longitude\":1.1,\"email\":\"3a@kejora.com\",\"noTelpon\":\"11\",\"alamat\":\"jakarta\",\"luas\":\"1\",\"bwTimur\":\"1\",\"bwBarat\":\"10\",\"bwSelatan\":\"1\",\"bwUtara\":\"1\",\"jarakProvinsi\":\"1\",\"jarakKabupaten\":\"1\",\"photo\":null,\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"kdPc\":{\"kdPc\":\"PC.1\",\"namaPc\":\"Andir\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"kdPd\":{\"kdPd\":\"PD-1\",\"namaPd\":\"Kota Bandung\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"kdPw\":{\"kdPw\":\"PW-2\",\"namaWilayah\":\"DKI Jakarta\",\"createdAt\":\"22-09-2020 20:48:00\",\"updatedAt\":\"22-09-2020 20:48:00\"},\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"kecamatan\":{\"id\":\"1\",\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\" Japah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"desa\":{\"id\":\"1\",\"kecamatan\":{\"id\":\"1\",\"kabupaten\":{\"id\":\"1\",\"provinsi\":{\"id\":\"1\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\" Japah\",\"createdAt\":\"22-09-2020 20:47:58\",\"updatedAt\":\"22-09-2020 20:47:58\"},\"nama\":\"Japah\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"},\"ketua\":\"haris\",\"sekretaris\":\"haris\",\"bendahara\":\"haris\",\"hariNgantor\":\"Selasa\",\"waktuNgantor\":\"11:11:00\",\"musjamTerakhirMasehi\":\"31-12-2020\",\"musjamTerakhirHijriyyah\":\"31-12;2020\",\"anggotaBiasa\":1,\"anggotaLuarBiasa\":1,\"tidakHerReg\":\"1\",\"mutasiPersis\":\"1\",\"mutasiTempat\":\"1\",\"meninggalDunia\":\"1\",\"mengundurkanDiri\":\"1\",\"calonAnggota\":\"1\",\"createdAt\":\"22-09-2020\",\"updatedAt\":\"22-09-2020\"}";

    @Disabled("Disabled until error fixed")
    @BeforeEach
    public void setup() throws Exception {
        objectMapper = new ObjectMapper();
        KejamiyyahanPj kejamiyyahanPj = objectMapper.readValue(kejamiyyahanJsonString, KejamiyyahanPj.class);
        try {

            provinsiRepository.save(kejamiyyahanPj.getProvinsi());
            kabupatenRepository.save(kejamiyyahanPj.getKabupaten());
            kecamatanRepository.save(kejamiyyahanPj.getKecamatan());
            desaRepository.save(kejamiyyahanPj.getDesa());
            daerahRepository.save(kejamiyyahanPj.getKdPd());
            cabangRepository.save(kejamiyyahanPj.getKdPc());
            wilayahRepository.save(kejamiyyahanPj.getKdPw());
            kejamiyyahanPjRepository.save(kejamiyyahanPj);
        } catch (Exception e) {
            System.out.println("error disini");
            e.printStackTrace();
        }
    }

    @Disabled("Disabled until error fixed")
    @Test
    void testFindAll() throws Exception {
        List<KejamiyyahanPj> kejamiyyahanPjList = kejamiyyahanPjRepository.findAll();

        String kejamiyyahanPjString = objectMapper.writeValueAsString(kejamiyyahanPjList);
        log.info("kejamiyyahan pj : {}", kejamiyyahanPjString);
        assertNotNull(kejamiyyahanPjList);
    }

    @Disabled("Disabled until error fixed")
    @Test
    void testGetMonografiPjId() throws Exception {
        Optional<KejamiyyahanPj> kejamiyyahanPjOptional = kejamiyyahanPjRepository.findByMonografiPj_id(1L);

        log.info("kejamiyyahan pj get by id : {}", kejamiyyahanPjOptional);
        assertNotNull(kejamiyyahanPjOptional.get());
    }
}
