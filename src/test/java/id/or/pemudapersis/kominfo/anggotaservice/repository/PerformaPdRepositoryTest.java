package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Daerah;
import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPd;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
class PerformaPdRepositoryTest {

    @Autowired
    private PerformaPdRepository performaPdRepository;

    @Autowired
    private DaerahRepository daerahRepository;

    private PerformaPd performaPd;
    private Daerah daerah;

    @BeforeEach
    void setUp() {
        daerah = Daerah.builder().kdPd("PD").namaPd("PD-Nama").build();

        performaPd = PerformaPd.builder()
                .daerah(daerah)
                .ketuaPd("Hasan")
                .alamat("Bandung")
                .build();
        daerahRepository.save(daerah);
        performaPdRepository.save(performaPd);
    }

    @Test
    void findByDeletedAtIsNull() {
        Pageable pageable = PageRequest.of(0, 2);
        Page<PerformaPd> performaPds = performaPdRepository.findByDeletedAtIsNull(pageable);
        assertFalse(performaPds.isEmpty());
    }

    @Test
    void findByDaerah_KdPd() {
        Optional<PerformaPd> optionalPerformaPd = performaPdRepository.findByDaerahKdPd("PD");
        assertTrue(optionalPerformaPd.isPresent());
    }
}
