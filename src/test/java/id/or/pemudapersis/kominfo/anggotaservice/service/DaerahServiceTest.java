package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Daerah;
import id.or.pemudapersis.kominfo.anggotaservice.repository.DaerahRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.DaerahVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Collections;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

class DaerahServiceTest extends DaerahVariables {

    @InjectMocks
    private DaerahService daerahService;

    @Mock
    private DaerahRepository daerahRepository;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(daerahRepository);
    }

    @Test
    void getList() {
        Page<Daerah> daerahs = mock(Page.class);
        when(daerahs.getContent()).thenReturn(Collections.singletonList(DAERAH));
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(daerahRepository.findAll(any(Specification.class), eq(pageableTest)))
                .thenReturn(DAERAH_PAGE);
        when(modelMapper.map(DAERAH, DaerahRequest.class)).thenReturn(DAERAH_REQUEST);
        BaseResponse<Page<DaerahRequest>> baseResponse = daerahService.getList("PW-1", pageableTest);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(daerahRepository).findAll(any(Specification.class), eq(pageableTest));
        verify(modelMapper).map(DAERAH, DaerahRequest.class);
    }

    @Test
    void saveOneDaerahWhenSuccess() {
        when(daerahRepository.save(any(Daerah.class))).thenReturn(DAERAH);
        when(modelMapper.map(DAERAH, DaerahRequest.class)).thenReturn(DAERAH_REQUEST);
        BaseResponse<DaerahRequest> baseResponse = daerahService.saveOne(DAERAH_REQUEST);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(daerahRepository).save(any(Daerah.class));
        verify(modelMapper).map(DAERAH, DaerahRequest.class);
    }

    @Test
    void updateOneDaerahWhenSuccess() {
        when(daerahRepository.findBykdPd(anyString())).thenReturn(Optional.of(DAERAH));
        when(daerahRepository.save(any(Daerah.class))).thenReturn(DAERAH);
        when(modelMapper.map(DAERAH, DaerahRequest.class)).thenReturn(DAERAH_REQUEST);
        BaseResponse<DaerahRequest> baseResponse = daerahService.updateOne(anyString(), DAERAH_REQUEST);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(daerahRepository).save(any(Daerah.class));
        verify(daerahRepository).findBykdPd(anyString());
        verify(modelMapper).map(DAERAH, DaerahRequest.class);
    }

    @Test
    void updateOneDaerahWhen_SomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(daerahRepository.findBykdPd(anyString())).thenReturn(Optional.empty());
            daerahService.updateOne(anyString(), DAERAH_REQUEST);
        });
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Kode Daerah not found"));

        verify(daerahRepository).findBykdPd(anyString());
    }

    @Test
    void softDeleteDaerahWhenSuccess() {
        when(daerahRepository.findBykdPd(anyString())).thenReturn(Optional.of(DAERAH));
        when(daerahRepository.save(any(Daerah.class))).thenReturn(DAERAH);
        BaseResponse<DaerahRequest> baseResponse = daerahService.softDelete(anyString());
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(daerahRepository).save(any(Daerah.class));
        verify(daerahRepository).findBykdPd(anyString());
    }

    @Test
    void softDeleteDaerahWhen_SomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(daerahRepository.findBykdPd(anyString())).thenReturn(Optional.empty());
            daerahService.softDelete(anyString());
        });
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Kode Daerah not found"));

        verify(daerahRepository).findBykdPd(anyString());
    }

    @Test
    void getByKodeDaerahWhenSuccess() {
        when(daerahRepository.findBykdPd(anyString())).thenReturn(Optional.of(DAERAH));
        Daerah daerah = daerahService.getByKodeDaerah(anyString());
        assertNotNull(daerah);
        verify(daerahRepository).findBykdPd(anyString());
    }

    @Test
    void getByKodeDaerahWhen_SomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(daerahRepository.findBykdPd(anyString())).thenReturn(Optional.empty());
            daerahService.getByKodeDaerah(anyString());
        });
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains("Kode Daerah not found"));

        verify(daerahRepository).findBykdPd(anyString());
    }
}
