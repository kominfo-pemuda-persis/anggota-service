package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Daerah;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@DataJpaTest
@Slf4j
class DaerahRepositoryTest {

    @Autowired
    private DaerahRepository daerahRepository;

    private Daerah daerah;

    @BeforeEach
    public void setup() {
        daerah = Daerah.builder()
                .kdPd("aaaaaTest")
                .namaPd("testPD")
                .kdPw("PW-1")
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .updatedAt(new Timestamp(System.currentTimeMillis()))
                .build();
        daerahRepository.save(daerah);
    }

    @Test
    void testRepository() {
        List<Daerah> daerahList = daerahRepository.findAll(Sort.by("kdPd"));
        log.info("{}", daerahList.get(0));
        assertEquals(daerahList.get(0).getKdPd(), daerah.getKdPd());
    }

    @Test
    void testFindByIdAndIsDeleteFalse() {
        Optional<Daerah> optionalDaerah = daerahRepository.findBykdPd("aaaaaTest");
        log.info("{}", optionalDaerah.get());
        assertEquals(optionalDaerah.get().getKdPd(), daerah.getKdPd());
    }

    @Test
    void testFindByIsDeletedFalse() {
        Pageable pageable = PageRequest.of(0, 1);

        Page<Daerah> daerahPage = daerahRepository.findAll(pageable);
        List<Daerah> content = daerahPage.getContent();
        assertEquals(content.get(0).getKdPd(), daerah.getKdPd());
    }
}
