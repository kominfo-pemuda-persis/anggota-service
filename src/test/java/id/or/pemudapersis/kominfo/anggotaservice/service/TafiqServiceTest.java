package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.TafiqDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Tafiq;
import id.or.pemudapersis.kominfo.anggotaservice.repository.TafiqRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.TafiqVariabels;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Collections;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class TafiqServiceTest extends TafiqVariabels {

    private static final String SUCCESS = "SUCCESS";
    private static final String TAFIQ_NOT_FOUND = "Tafiq Not Found";
    private static final Long ID_TAFIQ = 1L;

    @InjectMocks
    private TafiqService tafiqService;

    @Mock
    private TafiqRepository tafiqRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(tafiqRepository);
        verifyNoMoreInteractions(anggotaService);
    }

    @Test
    void tafiqListWhenSuccess() {
        Page<Tafiq> tafiqPage = mock(Page.class);
        when(tafiqPage.getContent()).thenReturn(Collections.singletonList(TAFIQ));
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(modelMapper.map(TAFIQ, TafiqDto.class)).thenReturn(TAFIQ_DTO);
        when(tafiqRepository.findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(TAFIQ_PAGE);
        BaseResponse<Page<TafiqDto>> baseResponse = tafiqService.tafiqList(pageableTest, false, NPA);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(tafiqRepository).findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest));
        verify(modelMapper).map(TAFIQ, TafiqDto.class);
    }

    @Test
    void createTafiqWhenSuccess() {
        when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
        when(modelMapper.map(TAFIQ, TafiqDto.class)).thenReturn(TAFIQ_DTO);
        when(tafiqRepository.save(any(Tafiq.class))).thenReturn(TAFIQ);
        BaseResponse<TafiqDto> baseResponse = tafiqService.create(NPA, TAFIQ_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(anggotaService).findByNpa(NPA);
        verify(tafiqRepository).save(any(Tafiq.class));
        verify(modelMapper).map(TAFIQ, TafiqDto.class);
    }

    @Test
    void updateTafiqWhenSuccess() {
        when(tafiqRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong()))
                .thenReturn(Optional.of(TAFIQ));
        when(tafiqRepository.save(any(Tafiq.class))).thenReturn(TAFIQ);
        when(modelMapper.map(TAFIQ, TafiqDto.class)).thenReturn(TAFIQ_DTO);
        BaseResponse<TafiqDto> baseResponse = tafiqService.update(anyString(), anyLong(), TAFIQ_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(tafiqRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong());
        verify(tafiqRepository).save(any(Tafiq.class));
        verify(modelMapper).map(TAFIQ, TafiqDto.class);
        verify(modelMapper).map(TAFIQ_DTO, TAFIQ);
    }

    @Test
    void updateTafiqWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(tafiqRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            tafiqService.update(anyString(), anyLong(), TAFIQ_DTO);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TAFIQ_NOT_FOUND));

        verify(tafiqRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong());
    }

    @Test
    void deleteTafiqWhenSuccess() {
        when(tafiqRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong()))
                .thenReturn(Optional.of(TAFIQ));
        when(tafiqRepository.save(any(Tafiq.class))).thenReturn(TAFIQ);
        BaseResponse<TafiqDto> baseResponse = tafiqService.delete(NPA, ID_TAFIQ);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(tafiqRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(NPA, ID_TAFIQ);
        verify(tafiqRepository).save(any(Tafiq.class));
    }

    @Test
    void deleteTafiqWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(tafiqRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            tafiqService.delete(NPA, ID_TAFIQ);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TAFIQ_NOT_FOUND));

        verify(tafiqRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong());
    }

    @Test
    void restoreTafiqWhenSuccess() {
        when(tafiqRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdTafiq(NPA, LONG_ID))
                .thenReturn(Optional.of(TAFIQ));
        when(tafiqRepository.save(any(Tafiq.class))).thenReturn(TAFIQ);
        when(modelMapper.map(TAFIQ, TafiqDto.class)).thenReturn(TAFIQ_DTO);
        BaseResponse<TafiqDto> baseResponse = tafiqService.restore(NPA, LONG_ID);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(tafiqRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdTafiq(NPA, LONG_ID);
        verify(tafiqRepository).save(any(Tafiq.class));
        verify(modelMapper).map(TAFIQ, TafiqDto.class);
    }

    @Test
    void restoreTafiqWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(tafiqRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            tafiqService.restore(NPA, ID_TAFIQ);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TAFIQ_NOT_FOUND));
        verify(tafiqRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdTafiq(anyString(), anyLong());
    }
}
