package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.OrganisasiDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Organisasi;
import id.or.pemudapersis.kominfo.anggotaservice.repository.OrganisasiRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.OrganisasiVariabels;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class OrganisasiServiceTest extends OrganisasiVariabels {
    public static final String SUCCESS = "SUCCESS";
    public static final String ORGANISASI_NOT_FOUND = "Organisasi Not Found";
    public static final long ID_ORGANISASI = 1L;

    @InjectMocks
    private OrganisasiService organisasiService;

    @Mock
    private OrganisasiRepository organisasiRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(organisasiRepository);
        verifyNoMoreInteractions(anggotaService);
    }

    @Test
    void organisasiListWhenSuccess() {
        Page<Organisasi> organisasiPage = mock(Page.class);
        when(organisasiPage.getContent()).thenReturn(ORGANISASI_LIST);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(modelMapper.map(ORGANISASI, OrganisasiDto.class)).thenReturn(ORGANISASI_DTO);
        when(organisasiRepository.findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(ORGANISASI_PAGE);
        BaseResponse<Page<OrganisasiDto>> baseResponse = organisasiService.organisasiList(pageableTest, false, NPA);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(organisasiRepository).findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest));
        verify(modelMapper).map(ORGANISASI, OrganisasiDto.class);
    }

    @Test
    void createOrganisasiWhenSuccess() {
        when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
        when(modelMapper.map(ORGANISASI, OrganisasiDto.class)).thenReturn(ORGANISASI_DTO);
        when(organisasiRepository.save(any(Organisasi.class))).thenReturn(ORGANISASI);
        BaseResponse<OrganisasiDto> baseResponse = organisasiService.create(NPA, ORGANISASI_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(anggotaService).findByNpa(NPA);
        verify(organisasiRepository).save(any(Organisasi.class));
        verify(modelMapper).map(ORGANISASI, OrganisasiDto.class);
    }

    @Test
    void updateOrganisasiWhenSuccess() {
        when(organisasiRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong()))
                .thenReturn(Optional.of(ORGANISASI));
        when(organisasiRepository.save(any(Organisasi.class))).thenReturn(ORGANISASI);
        when(modelMapper.map(ORGANISASI, OrganisasiDto.class)).thenReturn(ORGANISASI_DTO);
        BaseResponse<OrganisasiDto> baseResponse = organisasiService.update(anyString(), anyLong(), ORGANISASI_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(organisasiRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong());
        verify(organisasiRepository).save(any(Organisasi.class));
        verify(modelMapper).map(ORGANISASI, OrganisasiDto.class);
        verify(modelMapper).map(ORGANISASI_DTO, ORGANISASI);
    }

    @Test
    void updateOrganisasiWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(organisasiRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            organisasiService.update(anyString(), anyLong(), ORGANISASI_DTO);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(ORGANISASI_NOT_FOUND));

        verify(organisasiRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong());
    }

    @Test
    void deleteOrganisasiWhenSuccess() {
        when(organisasiRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong()))
                .thenReturn(Optional.of(ORGANISASI));
        when(organisasiRepository.save(any(Organisasi.class))).thenReturn(ORGANISASI);
        BaseResponse<OrganisasiDto> baseResponse = organisasiService.delete(NPA, ID_ORGANISASI);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(organisasiRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(NPA, ID_ORGANISASI);
        verify(organisasiRepository).save(any(Organisasi.class));
    }

    @Test
    void deleteOrganisasiWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(organisasiRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            organisasiService.delete(NPA, ID_ORGANISASI);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(ORGANISASI_NOT_FOUND));

        verify(organisasiRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong());
    }

    @Test
    void restoreOrganisasiWhenSuccess() {
        when(organisasiRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdOrganisasi(NPA, LONG_ID))
                .thenReturn(Optional.of(ORGANISASI));
        when(organisasiRepository.save(any(Organisasi.class))).thenReturn(ORGANISASI);
        when(modelMapper.map(ORGANISASI, OrganisasiDto.class)).thenReturn(ORGANISASI_DTO);
        BaseResponse<OrganisasiDto> baseResponse = organisasiService.restore(NPA, LONG_ID);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(organisasiRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdOrganisasi(NPA, LONG_ID);
        verify(organisasiRepository).save(any(Organisasi.class));
        verify(modelMapper).map(ORGANISASI, OrganisasiDto.class);
    }

    @Test
    void restoreOrganisasiWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(organisasiRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            organisasiService.restore(NPA, ID_ORGANISASI);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(ORGANISASI_NOT_FOUND));
        verify(organisasiRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdOrganisasi(anyString(), anyLong());
    }
}
