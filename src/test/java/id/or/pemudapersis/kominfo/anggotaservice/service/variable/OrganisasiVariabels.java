package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.OrganisasiDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Organisasi;
import java.util.Collections;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class OrganisasiVariabels extends AnggotaVariableTest {
    public static final String STRING_NAMA = "String Nama";
    public static final String JABATAN = "JABATAN";
    public final OrganisasiDto ORGANISASI_DTO = OrganisasiDto.builder()
            .idOrganisasi(LONG_ID)
            .namaOrganisasi(STRING_NAMA)
            .jabatan(JABATAN)
            .lokasi(STRING_NAMA)
            .tahunMulai(1)
            .tahunSelesai(2)
            .build();
    public final Organisasi ORGANISASI = Organisasi.builder()
            .anggota(ANGGOTA)
            .idOrganisasi(LONG_ID)
            .namaOrganisasi(STRING_NAMA)
            .jabatan(JABATAN)
            .build();
    public static final List<Organisasi> ORGANISASI_LIST = Collections.singletonList(Organisasi.builder()
            .anggota(ANGGOTA)
            .idOrganisasi(LONG_ID)
            .namaOrganisasi(STRING_NAMA)
            .jabatan(JABATAN)
            .build());
    public final Page<Organisasi> ORGANISASI_PAGE = new PageImpl<>(Collections.singletonList(ORGANISASI));
}
