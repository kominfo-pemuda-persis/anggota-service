package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KabupatenDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPdDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.JamiyyahPdRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MonografiPdRepository;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class MonografiPdServiceTest {

    @InjectMocks
    private MonografiPdService monografiPdService;

    @Mock
    private MonografiPdRepository monografiPdRepository;

    @Mock
    private DaerahService daerahService;

    @Mock
    private WilayahService wilayahService;

    @Mock
    private HelperUtilService helperUtilService;

    @Mock
    private JamiyyahPdRepository jamiyyahPcRepository;

    @Mock
    private ModelMapper modelMapper;

    private Daerah daerah;

    private Wilayah wilayah;

    private Kabupaten kabupaten;

    private Provinsi provinsi;

    private MonografiPd monografiPd;

    private JamiyyahPd jamiyyahPd;

    @BeforeEach
    void setUp() {

        daerah = Daerah.builder().kdPd("D1").build();
        wilayah = Wilayah.builder().kdPw("PW1").build();
        kabupaten = Kabupaten.builder().id("KAB").build();
        provinsi = Provinsi.builder().id("PROV").build();
        jamiyyahPd = JamiyyahPd.builder().id(1L).build();
        monografiPd = MonografiPd.builder()
                .daerah(daerah)
                .wilayah(wilayah)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .jamiyyahPd(jamiyyahPd)
                .build();
    }

    @Test
    void getList() {
        when(monografiPdRepository.findByDeletedAtIsNull(PageRequest.of(0, 25)))
                .thenReturn(new PageImpl<>(Collections.singletonList(mockMonografiPd())));

        List<MonografiPd> actualList = monografiPdService.getList(PageRequest.of(0, 25), false);

        Assertions.assertEquals(1, actualList.size(), "Size should be 1");

        MonografiPd firstElement = actualList.get(0);
        Assertions.assertEquals("alamat utama", firstElement.getAlamatLengkap(), "Should be equals");
    }

    private static MonografiPd mockMonografiPd() {
        return MonografiPd.builder()
                .alamatLengkap("alamat utama")
                .wilayah(Wilayah.builder().kdPw("WIL-001").build())
                .provinsi(Provinsi.builder().id("Prov-1").build())
                .kabupaten(Kabupaten.builder().id("KAB-001").build())
                .build();
    }

    @Test
    void create() {
        MonografiPd monografiPd = MonografiPd.builder()
                .id(1L)
                .daerah(daerah)
                .wilayah(wilayah)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .jamiyyahPd(jamiyyahPd)
                .build();
        JamiyyahPd jamiyyahPd = JamiyyahPd.builder().monografiPd(monografiPd).build();

        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(jamiyyahPcRepository.save(any(JamiyyahPd.class))).thenReturn(jamiyyahPd);
        MonografiPd resultMonografiPd = monografiPdService.create(monografiPd);

        assertEquals(resultMonografiPd.getId(), monografiPd.getId());
    }

    @Test
    void update() {
        MonografiPdDto monografiPdDto = MonografiPdDto.builder()
                .kabupaten(KabupatenDto.builder().id("KAB").build())
                .build();

        MonografiPd monografiPd = MonografiPd.builder()
                .id(1L)
                .daerah(daerah)
                .wilayah(wilayah)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .jamiyyahPd(jamiyyahPd)
                .deletedAt(null)
                .build();
        MonografiPd monografiPdResult = MonografiPd.builder()
                .id(1L)
                .daerah(daerah)
                .wilayah(wilayah)
                .kabupaten(Kabupaten.builder().id("KAB").build())
                .provinsi(provinsi)
                .jamiyyahPd(jamiyyahPd)
                .build();

        Configuration configuration = mock(Configuration.class);
        Long monografiPdId = 1L;

        when(monografiPdRepository.findByIdAndDeletedAtIsNull(monografiPdId)).thenReturn(Optional.of(monografiPd));
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(monografiPdRepository.save(monografiPd)).thenReturn(monografiPdResult);
        when(modelMapper.getConfiguration()).thenReturn(configuration);

        MonografiPd result = monografiPdService.update(monografiPdDto, monografiPdId);
        assertEquals(result.getKabupaten().getId(), monografiPd.getKabupaten().getId());
    }

    @Test
    void delete() {
        MonografiPd monografiPd = MonografiPd.builder()
                .id(1L)
                .daerah(daerah)
                .wilayah(wilayah)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .jamiyyahPd(jamiyyahPd)
                .deletedAt(null)
                .build();
        MonografiPd monografiPdResult = MonografiPd.builder()
                .id(1L)
                .daerah(daerah)
                .wilayah(wilayah)
                .deletedAt(LocalDateTime.now())
                .kabupaten(Kabupaten.builder().id("KAB").build())
                .provinsi(provinsi)
                .jamiyyahPd(JamiyyahPd.builder()
                        .id(1L)
                        .deletedAt(LocalDateTime.now())
                        .build())
                .build();
        Long monografiPdId = 1L;
        when(monografiPdRepository.findByIdAndDeletedAtIsNull(monografiPdId)).thenReturn(Optional.of(monografiPd));
        when(monografiPdRepository.save(monografiPd)).thenReturn(monografiPdResult);
        MonografiPd result = monografiPdService.delete(1L);
        assertNotNull(result.getDeletedAt());
    }
}
