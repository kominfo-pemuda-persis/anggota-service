package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPwDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.ProvinsiDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.JamiyyahPw;
import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPw;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import id.or.pemudapersis.kominfo.anggotaservice.repository.JamiyyahPwRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MonografiPwRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.*;

@ExtendWith(MockitoExtension.class)
class MonografiPwServiceTest {

    @InjectMocks
    private MonografiPwService monografiPwService;

    @Mock
    private MonografiPwRepository monografiPwRepository;

    @Mock
    private WilayahService wilayahService;

    @Mock
    private HelperUtilService helperUtilService;

    @Mock
    private JamiyyahPwRepository jamiyyahPwRepository;

    @Mock
    private ModelMapper modelMapper;

    private Wilayah wilayah;
    private Provinsi provinsi;
    private JamiyyahPw jamiyyahPw;
    private ProvinsiDto provinsiDto;

    @BeforeEach
    void setUp() {
        wilayah = Wilayah.builder().kdPw("WIL-01").build();
        provinsi = Provinsi.builder().id("PROV").build();
        jamiyyahPw = JamiyyahPw.builder().id(1L).build();
        provinsiDto = ProvinsiDto.builder().id("PROV").build();
    }

    @Test
    void monografiPwList() {
        List<MonografiPw> monografiPwList = new ArrayList<>();
        MonografiPw monografiPw = MonografiPw.builder()
                .id(1L)
                .wilayah(Wilayah.builder().kdPw("WIL-1").build())
                .provinsi(Provinsi.builder().id("PROV-1").build())
                .build();
        monografiPwList.add(monografiPw);
        Page<MonografiPw> pagedResponse = new PageImpl(monografiPwList);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(monografiPwRepository.findByDeletedAtIsNull(pageableTest)).thenReturn(pagedResponse);
        List<MonografiPw> listResult = monografiPwService.monografiPwList(pageableTest, false);
        assertEquals(monografiPwList.size(), listResult.size());
    }

    @Test
    void create() {
        MonografiPw monografiPw = MonografiPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .jamiyyahPw(jamiyyahPw)
                .build();
        JamiyyahPw jamiyyahPw = JamiyyahPw.builder().monografiPw(monografiPw).build();

        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(jamiyyahPwRepository.save(any(JamiyyahPw.class))).thenReturn(jamiyyahPw);
        MonografiPw resultMonografiPw = monografiPwService.create(monografiPw);

        assertEquals(resultMonografiPw.getId(), monografiPw.getId());
    }

    @Test
    void update() {
        MonografiPwDto monografiPwDto =
                MonografiPwDto.builder().provinsi(provinsiDto).build();

        MonografiPw monografiPw = MonografiPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .jamiyyahPw(jamiyyahPw)
                .build();
        MonografiPw monografiPwResult =
                MonografiPw.builder().id(1L).wilayah(wilayah).provinsi(provinsi).build();

        Configuration configuration = mock(Configuration.class);
        Long monografiPwId = 1L;

        when(monografiPwRepository.findByIdAndDeletedAtIsNull(monografiPwId)).thenReturn(Optional.of(monografiPw));
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(monografiPwRepository.save(monografiPw)).thenReturn(monografiPwResult);
        when(modelMapper.getConfiguration()).thenReturn(configuration);

        MonografiPw result = monografiPwService.update(monografiPwDto, monografiPwId);
        assertEquals(result.getProvinsi().getId(), monografiPw.getProvinsi().getId());
    }

    @Test
    void delete() {
        MonografiPw monografiPw = MonografiPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .jamiyyahPw(jamiyyahPw)
                .deletedAt(null)
                .build();
        MonografiPw monografiPwResult = MonografiPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .deletedAt(LocalDateTime.now())
                .build();

        Long monografiPwId = 1L;

        when(monografiPwRepository.findByIdAndDeletedAtIsNull(monografiPwId)).thenReturn(Optional.of(monografiPw));
        when(monografiPwRepository.save(monografiPw)).thenReturn(monografiPwResult);

        MonografiPw result = monografiPwService.delete(monografiPwId);
        assertNotNull(result.getDeletedAt());
    }

    @Test
    void restore() {

        MonografiPw monografiPw = MonografiPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .jamiyyahPw(jamiyyahPw)
                .deletedAt(null)
                .build();
        MonografiPw monografiPwResult = MonografiPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .deletedAt(LocalDateTime.now())
                .build();

        Long monografiPwId = 1L;

        when(monografiPwRepository.findByIdAndDeletedAtIsNotNull(monografiPwId))
                .thenReturn(Optional.of(monografiPwResult));
        when(monografiPwRepository.save(monografiPwResult)).thenReturn(monografiPw);

        MonografiPw result = monografiPwService.restore(monografiPwId);
        assertNull(result.getDeletedAt());
    }
}
