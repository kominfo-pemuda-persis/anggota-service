package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import java.io.InputStream;
import java.util.Collections;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public abstract class AnggotaVariableTest {
    public final InputStream FOTO_STREAM =
            Thread.currentThread().getContextClassLoader().getResourceAsStream("src/images/admin-1.png");

    public static final String STRING_ID = "STRING_ID";

    public static final String NPA = "9000.1";

    public static final String STRING_NAMA = "NAMA";

    public static final Long LONG_ID = 1L;

    public static final Provinsi PROVINSI = Provinsi.builder().build();

    public static final ProvinsiDto PROVINSI_DTO =
            ProvinsiDto.builder().id(STRING_ID).build();

    public static final Kabupaten KABUPATEN = Kabupaten.builder().id(STRING_ID).build();
    public static final PendidikanDto PENDIDIKAN_DTO =
            PendidikanDto.builder().idPendidikan(LONG_ID).build();
    public static final Pendidikan PENDIDIKAN = Pendidikan.builder()
            .idPendidikan(LONG_ID)
            .jenjangPendidikan(MasterPendidikan.builder().id(STRING_ID).build())
            .build();

    public static final KabupatenDto KABUPATEN_DTO =
            KabupatenDto.builder().id(STRING_ID).build();

    public static final Kecamatan KECAMATAN = Kecamatan.builder().id(STRING_ID).build();

    public static final KecamatanDto KECAMATAN_DTO =
            KecamatanDto.builder().id(STRING_ID).build();

    public static final MasterPendidikan MASTER_PENDIDIKAN =
            MasterPendidikan.builder().id(STRING_ID).build();

    public static final MasterPendidikanDto MASTER_PENDIDIKAN_DTO =
            MasterPendidikanDto.builder().id(STRING_ID).build();

    public static final Desa DESA = Desa.builder().id(STRING_ID).build();

    public static final DesaDto DESA_DTO = DesaDto.builder().id(STRING_ID).build();

    public static final Wilayah WILAYAH =
            Wilayah.builder().kdPw(STRING_ID).namaWilayah(STRING_NAMA).build();

    public static final WilayahDto WILAYAH_DTO =
            WilayahDto.builder().kdPw(STRING_ID).namaWilayah(STRING_NAMA).build();

    public static final Cabang CABANG =
            Cabang.builder().kdPc(STRING_ID).namaPc(STRING_NAMA).build();

    public static final CabangRequest CABANG_REQUEST =
            CabangRequest.builder().kdPc(STRING_ID).namaPc(STRING_NAMA).build();

    public static final Daerah DAERAH =
            Daerah.builder().kdPd(STRING_ID).namaPd(STRING_NAMA).build();

    public static final DaerahRequest DAERAH_REQUEST =
            DaerahRequest.builder().kdPd(STRING_ID).build();

    public static final Anggota ANGGOTA = Anggota.builder()
            .id(STRING_ID)
            .npa(NPA)
            .nama(STRING_NAMA)
            .namaPj(STRING_NAMA)
            .kecamatan(KECAMATAN)
            .kabupaten(KABUPATEN)
            .provinsi(PROVINSI)
            .desa(DESA)
            .foto("default.png")
            .jenjangPendidikan(MASTER_PENDIDIKAN)
            .pw(WILAYAH)
            .pd(DAERAH)
            .pc(CABANG)
            .build();

    public final AnggotaRequestDto ANGGOTA_REQUEST_DTO = AnggotaRequestDto.builder()
            .npa(NPA)
            .kabupaten(KABUPATEN_DTO)
            .kecamatan(KECAMATAN_DTO)
            .provinsi(PROVINSI_DTO)
            .desa(DESA_DTO)
            .pw(WILAYAH_DTO)
            .pc(CABANG_REQUEST)
            .jenjangPendidikan(MASTER_PENDIDIKAN_DTO)
            .pd(DAERAH_REQUEST)
            .build();

    public static final AnggotaDto ANGGOTA_DTO = AnggotaDto.builder()
            .id(STRING_ID)
            .npa(NPA)
            .jenjangPendidikan(MASTER_PENDIDIKAN_DTO)
            .kabupaten(KABUPATEN_DTO)
            .kecamatan(KECAMATAN_DTO)
            .provinsi(PROVINSI_DTO)
            .desa(DESA_DTO)
            .pw(WILAYAH_DTO)
            .pc(CABANG_REQUEST)
            .pendidikans(Set.copyOf(Collections.singletonList(PENDIDIKAN_DTO)))
            .pd(DAERAH_REQUEST)
            .build();

    public final Page<Anggota> ANGGOTA_PAGE = new PageImpl<>(Collections.singletonList(ANGGOTA));

    public static final Login LOGIN = Login.builder()
            .idLogin(STRING_ID)
            .anggota(Anggota.builder().build())
            .build();

    protected static Anggota mockAnggota() {
        return Anggota.builder()
                .npa("99.0002")
                .nama("Uzumaki Naruto (superadmin)")
                .email("uzumaki_naruto@konohagakure.co.jp")
                .build();
    }

    public static final AnggotaFilterDto ANGGOTA_FILTER_DTO =
            AnggotaFilterDto.builder().build();
}
