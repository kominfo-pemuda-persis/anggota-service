package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PendidikanDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Pendidikan;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MasterPendidikanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PendidikanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.PendidikanVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Collections;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class PendidikanServiceTest extends PendidikanVariables {
    private static final String SUCCESS = "SUCCESS";
    private static final Long ID_PENDIDIKAN = 1L;
    private static final String PENDIDIKAN_NOT_FOUND = "Pendidikan Not Found";
    public static final String JENJANG_PENDIDIKAN_NOT_FOUND = "Jenjang Pendidikan Not Found";

    @InjectMocks
    private PendidikanService pendidikanService;

    @Mock
    private PendidikanRepository pendidikanRepository;

    @Mock
    private MasterPendidikanRepository masterPendidikanRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(pendidikanRepository);
        verifyNoMoreInteractions(masterPendidikanRepository);
        verifyNoMoreInteractions(anggotaService);
        verifyNoMoreInteractions(modelMapper);
    }

    @Test
    void pendidikanListWhenSuccess() {
        Page<Pendidikan> pendidikanPage = mock(Page.class);
        when(pendidikanPage.getContent()).thenReturn(Collections.singletonList(PENDIDIKAN));
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(pendidikanRepository.findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(PENDIDIKAN_PAGE);
        when(modelMapper.map(PENDIDIKAN, PendidikanDto.class)).thenReturn(PENDIDIKAN_DTO);
        BaseResponse<Page<PendidikanDto>> baseResponse = pendidikanService.pendidikanList(pageableTest, false, NPA);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(modelMapper).map(PENDIDIKAN, PendidikanDto.class);
        verify(pendidikanRepository).findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest));
    }

    @Test
    void createPendidikanWhenSuccess() {
        when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
        when(modelMapper.map(PENDIDIKAN, PendidikanDto.class)).thenReturn(PENDIDIKAN_DTO);
        when(pendidikanRepository.save(any(Pendidikan.class))).thenReturn(PENDIDIKAN);
        when(masterPendidikanRepository.findById(STRING_ID)).thenReturn(Optional.of(MASTER_PENDIDIKAN));
        BaseResponse<PendidikanDto> baseResponse = pendidikanService.create(NPA, PENDIDIKAN_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(anggotaService).findByNpa(NPA);
        verify(pendidikanRepository).save(any(Pendidikan.class));
        verify(masterPendidikanRepository).findById(STRING_ID);
        verify(modelMapper).map(PENDIDIKAN, PendidikanDto.class);
    }

    @Test
    void createPendidikanWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
            when(masterPendidikanRepository.findById(STRING_ID)).thenReturn(Optional.empty());
            pendidikanService.create(NPA, PENDIDIKAN_DTO);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(JENJANG_PENDIDIKAN_NOT_FOUND));

        verify(anggotaService).findByNpa(NPA);
        verify(masterPendidikanRepository).findById(STRING_ID);
    }

    @Test
    void updatePendidikanWhenSuccess() {
        when(pendidikanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong()))
                .thenReturn(Optional.of(PENDIDIKAN));
        when(pendidikanRepository.save(any(Pendidikan.class))).thenReturn(PENDIDIKAN);
        when(modelMapper.map(PENDIDIKAN, PendidikanDto.class)).thenReturn(PENDIDIKAN_DTO);
        when(masterPendidikanRepository.findById(STRING_ID)).thenReturn(Optional.of(MASTER_PENDIDIKAN));
        BaseResponse<PendidikanDto> baseResponse = pendidikanService.update(anyString(), anyLong(), PENDIDIKAN_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(pendidikanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong());
        verify(pendidikanRepository).save(any(Pendidikan.class));
        verify(modelMapper).map(PENDIDIKAN, PendidikanDto.class);
        verify(masterPendidikanRepository).findById(STRING_ID);
        verify(modelMapper).map(PENDIDIKAN_DTO, PENDIDIKAN);
    }

    @Test
    void updatePendidikanWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(pendidikanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong()))
                    .thenReturn(Optional.of(PENDIDIKAN));
            when(masterPendidikanRepository.findById(STRING_ID)).thenReturn(Optional.empty());
            pendidikanService.update(anyString(), anyLong(), PENDIDIKAN_DTO);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(JENJANG_PENDIDIKAN_NOT_FOUND));
        verify(masterPendidikanRepository).findById(STRING_ID);
        verify(pendidikanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong());
    }

    @Test
    void deletePendidikanWhenSuccess() {
        when(pendidikanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong()))
                .thenReturn(Optional.of(PENDIDIKAN));
        when(pendidikanRepository.save(any(Pendidikan.class))).thenReturn(PENDIDIKAN);
        BaseResponse<PendidikanDto> baseResponse = pendidikanService.delete(NPA, ID_PENDIDIKAN);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(pendidikanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(NPA, ID_PENDIDIKAN);
        verify(pendidikanRepository).save(any(Pendidikan.class));
    }

    @Test
    void deletePendidikanWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(pendidikanRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            pendidikanService.delete(NPA, ID_PENDIDIKAN);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(PENDIDIKAN_NOT_FOUND));

        verify(pendidikanRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong());
    }

    @Test
    void restorePendidikanWhenSuccess() {
        when(pendidikanRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdPendidikan(NPA, LONG_ID))
                .thenReturn(Optional.of(PENDIDIKAN));
        when(pendidikanRepository.save(any(Pendidikan.class))).thenReturn(PENDIDIKAN);
        when(modelMapper.map(PENDIDIKAN, PendidikanDto.class)).thenReturn(PENDIDIKAN_DTO);
        BaseResponse<PendidikanDto> baseResponse = pendidikanService.restore(NPA, LONG_ID);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(pendidikanRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdPendidikan(NPA, LONG_ID);
        verify(pendidikanRepository).save(any(Pendidikan.class));
        verify(modelMapper).map(PENDIDIKAN, PendidikanDto.class);
    }

    @Test
    void restorePendidikanWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(pendidikanRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            pendidikanService.restore(NPA, ID_PENDIDIKAN);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(PENDIDIKAN_NOT_FOUND));
        verify(pendidikanRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdPendidikan(anyString(), anyLong());
    }
}
