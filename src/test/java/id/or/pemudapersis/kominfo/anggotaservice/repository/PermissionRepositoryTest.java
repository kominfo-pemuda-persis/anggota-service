package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertFalse;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Permission;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Role;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
class PermissionRepositoryTest {

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @BeforeEach
    void setUp() {
        testEntityManager.persist(Role.builder().build());
    }

    @Test
    void findAll() {
        Role role = Role.builder().id(1L).build();
        Permission permission = Permission.builder()
                .role(role)
                .module("read-anggotaTest")
                .permission("Read Anggota Test")
                .build();
        permissionRepository.save(permission);
        List<Permission> permissions = permissionRepository.findAll();
        assertFalse(permissions.isEmpty());
    }
}
