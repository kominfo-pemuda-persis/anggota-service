package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.constant.GlobalMessage;
import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.exception.ResourceNotFoundException;
import id.or.pemudapersis.kominfo.anggotaservice.repository.AnggotaRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.LoginRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CheckAnggotaRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.SendEmailAnggotaRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.mail.MailMessageContent;
import id.or.pemudapersis.kominfo.anggotaservice.service.mail.MailService;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.AnggotaVariableTest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import jakarta.mail.MessagingException;
import java.io.IOException;
import java.security.Principal;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;

class AnggotaServiceTest extends AnggotaVariableTest {
    @InjectMocks
    private AnggotaService anggotaService;

    @Mock
    private AnggotaRepository anggotaRepository;

    @Mock
    private HelperUtilService helperUtilService;

    @Mock
    private CabangService cabangService;

    @Mock
    private WilayahService wilayahService;

    @Mock
    private DaerahService daerahService;

    @Mock
    private LoginService loginService;

    @Mock
    private MailService mailService;

    @Mock
    private Principal principal;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private StorageService storageService;

    @Mock
    private LoginRepository loginRepository;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(loginService);
        verifyNoMoreInteractions(anggotaRepository);
        verifyNoMoreInteractions(helperUtilService);
        verifyNoMoreInteractions(cabangService);
        verifyNoMoreInteractions(wilayahService);
        verifyNoMoreInteractions(daerahService);
    }

    @Test
    void createAnggotaWhenSuccess() {
        when(principal.getName()).thenReturn("9000.1");
        when(loginService.getUserByNpa(anyString())).thenReturn(LOGIN);
        when(modelMapper.map(ANGGOTA_REQUEST_DTO, Anggota.class)).thenReturn(ANGGOTA);
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.empty());
        when(helperUtilService.getProvinsiById(anyString())).thenReturn(PROVINSI);
        when(helperUtilService.getKecamatanById(anyString())).thenReturn(KECAMATAN);
        when(helperUtilService.getKabupatenById(anyString())).thenReturn(KABUPATEN);
        when(helperUtilService.getDesaByid(anyString())).thenReturn(DESA);
        when(cabangService.getByKodeCabang(anyString())).thenReturn(CABANG);
        when(daerahService.getByKodeDaerah(anyString())).thenReturn(DAERAH);
        when(wilayahService.getByKodeWilayah(anyString())).thenReturn(WILAYAH);
        when(helperUtilService.getMasterPendidikanById(anyString())).thenReturn(MASTER_PENDIDIKAN);
        when(modelMapper.map(ANGGOTA_DTO, Anggota.class)).thenReturn(ANGGOTA);
        when(modelMapper.map(ANGGOTA, AnggotaDto.class)).thenReturn(ANGGOTA_DTO);
        when(modelMapper.map(PENDIDIKAN, PendidikanDto.class)).thenReturn(PENDIDIKAN_DTO);
        when(anggotaRepository.save(any(Anggota.class))).thenReturn(ANGGOTA);
        BaseResponse<AnggotaDto> result = anggotaService.create(principal, ANGGOTA_REQUEST_DTO);
        assertEquals("SUCCESS", result.getMessage());
        verify(loginService).getUserByNpa(anyString());
        verify(anggotaRepository).findByNpa(anyString());
        verify(anggotaRepository).save(ANGGOTA);
        verify(helperUtilService).getProvinsiById(anyString());
        verify(helperUtilService).getDesaByid(anyString());
        verify(helperUtilService).getKabupatenById(anyString());
        verify(helperUtilService).getKecamatanById(anyString());
        verify(helperUtilService).getMasterPendidikanById(anyString());
        verify(cabangService).getByKodeCabang(anyString());
        verify(daerahService).getByKodeDaerah(anyString());
        verify(wilayahService).getByKodeWilayah(anyString());
    }

    @Test
    void createAnggotaWhenNpaAlreadyExist() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(principal.getName()).thenReturn("9000.1");
            when(loginService.getUserByNpa(anyString())).thenReturn(LOGIN);
            when(modelMapper.map(ANGGOTA_REQUEST_DTO, Anggota.class)).thenReturn(ANGGOTA);
            when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.of(ANGGOTA));
            anggotaService.create(principal, ANGGOTA_REQUEST_DTO);
        });

        String expectedMessage = "NPA ALREADY EXIST";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        verify(loginService).getUserByNpa(anyString());
        verify(anggotaRepository).findByNpa(anyString());
    }

    @Test
    void updateWhenSuccess() {
        when(anggotaRepository.findById(anyString())).thenReturn(Optional.of(ANGGOTA));
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.empty());
        when(helperUtilService.getProvinsiById(anyString())).thenReturn(PROVINSI);
        when(helperUtilService.getKecamatanById(anyString())).thenReturn(KECAMATAN);
        when(helperUtilService.getKabupatenById(anyString())).thenReturn(KABUPATEN);
        when(helperUtilService.getDesaByid(anyString())).thenReturn(DESA);
        when(cabangService.getByKodeCabang(anyString())).thenReturn(CABANG);
        when(daerahService.getByKodeDaerah(anyString())).thenReturn(DAERAH);
        when(wilayahService.getByKodeWilayah(anyString())).thenReturn(WILAYAH);
        when(modelMapper.map(ANGGOTA, AnggotaDto.class)).thenReturn(ANGGOTA_DTO);
        when(modelMapper.map(ANGGOTA_DTO, Anggota.class)).thenReturn(ANGGOTA);
        when(helperUtilService.getMasterPendidikanById(anyString())).thenReturn(MASTER_PENDIDIKAN);

        when(anggotaRepository.save(any(Anggota.class))).thenReturn(ANGGOTA);
        BaseResponse<AnggotaDto> result = anggotaService.update(principal, STRING_ID, ANGGOTA_REQUEST_DTO);
        assertEquals("SUCCESS", result.getMessage());
        verify(anggotaRepository).findById(anyString());
        verify(anggotaRepository).findByNpa(anyString());
        verify(anggotaRepository).save(any(Anggota.class));
        verify(helperUtilService).getProvinsiById(anyString());
        verify(helperUtilService).getDesaByid(anyString());
        verify(helperUtilService).getKabupatenById(anyString());
        verify(helperUtilService).getKecamatanById(anyString());
        verify(helperUtilService).getMasterPendidikanById(anyString());
        verify(cabangService).getByKodeCabang(anyString());
        verify(daerahService).getByKodeDaerah(anyString());
        verify(wilayahService).getByKodeWilayah(anyString());
    }

    @Test
    void updateWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(anggotaRepository.findById(anyString())).thenReturn(Optional.empty());
            anggotaService.update(principal, STRING_ID, ANGGOTA_REQUEST_DTO);
        });

        String expectedMessage = "ANGGOTA NOT FOUND";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));

        verify(anggotaRepository).findById(anyString());
    }

    @Test
    void updateCurrentAnggotaWhenSuccess() {
        when(principal.getName()).thenReturn("9000.1");
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.of(ANGGOTA));
        when(helperUtilService.getProvinsiById(anyString())).thenReturn(PROVINSI);
        when(helperUtilService.getKecamatanById(anyString())).thenReturn(KECAMATAN);
        when(helperUtilService.getKabupatenById(anyString())).thenReturn(KABUPATEN);
        when(helperUtilService.getDesaByid(anyString())).thenReturn(DESA);
        when(cabangService.getByKodeCabang(anyString())).thenReturn(CABANG);
        when(daerahService.getByKodeDaerah(anyString())).thenReturn(DAERAH);
        when(wilayahService.getByKodeWilayah(anyString())).thenReturn(WILAYAH);
        when(helperUtilService.getMasterPendidikanById(anyString())).thenReturn(MASTER_PENDIDIKAN);
        when(anggotaRepository.save(any(Anggota.class))).thenReturn(ANGGOTA);
        when(modelMapper.map(ANGGOTA, AnggotaDto.class)).thenReturn(ANGGOTA_DTO);
        when(modelMapper.map(PENDIDIKAN, PendidikanDto.class)).thenReturn(PENDIDIKAN_DTO);
        BaseResponse<AnggotaDto> result =
                anggotaService.updateCurrentAnggota(principal, STRING_ID, ANGGOTA_REQUEST_DTO);
        assertEquals("SUCCESS", result.getMessage());
        verify(anggotaRepository, times(2)).findByNpa(anyString());
        verify(anggotaRepository, atLeast(1)).findByNpa(anyString());
        verify(anggotaRepository).save(any(Anggota.class));
        verify(helperUtilService).getProvinsiById(anyString());
        verify(helperUtilService).getDesaByid(anyString());
        verify(helperUtilService).getKabupatenById(anyString());
        verify(helperUtilService).getKecamatanById(anyString());
        verify(helperUtilService).getMasterPendidikanById(anyString());
        verify(cabangService).getByKodeCabang(anyString());
        verify(daerahService).getByKodeDaerah(anyString());
        verify(wilayahService).getByKodeWilayah(anyString());
    }

    @Test
    void updateCurrentAnggotaWhenAccessDenied() {

        Exception exception = assertThrows(BusinessException.class, () -> {
            when(principal.getName()).thenReturn("1011.1");
            anggotaService.updateCurrentAnggota(principal, STRING_ID, ANGGOTA_REQUEST_DTO);
        });

        String expectedMessage = "Access is denied";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void updateCurrentAnggotaWhenSomeDataNotFound() {

        Exception exception = assertThrows(BusinessException.class, () -> {
            when(principal.getName()).thenReturn("9000.1");
            when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.empty());
            anggotaService.updateCurrentAnggota(principal, STRING_ID, ANGGOTA_REQUEST_DTO);
        });

        String expectedMessage = "User Not Found";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        verify(anggotaRepository).findByNpa(anyString());
    }

    @Test
    void getAllAnggotaWhenSuccess() {
        when(anggotaRepository.findAll(any(Specification.class), any(Pageable.class)))
                .thenReturn(Page.empty());

        var actual = anggotaService.getAllAnggota(ANGGOTA_FILTER_DTO, Pageable.ofSize(10));

        assertThat(actual).satisfies(res -> {
            assertThat(res.getStatus()).isEqualTo(HttpStatus.OK);
            assertThat(res.getMessage()).isEqualTo("SUCCESS");
            assertThat(res.getData()).isEqualTo(Page.empty());
        });

        verify(anggotaRepository).findAll(any(Specification.class), any(Pageable.class));
    }

    @Test
    void findByNpaWhenSuccess() {
        when(anggotaRepository.findByNpa(NPA)).thenReturn(Optional.of(ANGGOTA));
        Anggota anggota = anggotaService.findByNpa(NPA);
        assertNotNull(anggota);
        verify(anggotaRepository).findByNpa(NPA);
    }

    @Test
    void findByNpaWhenDataNotFound() {

        Exception exception = assertThrows(BusinessException.class, () -> {
            when(anggotaRepository.findByNpa(NPA)).thenReturn(Optional.empty());
            anggotaService.findByNpa(NPA);
        });

        String expectedMessage = GlobalMessage.ANGGOTA_NOT_FOUND.message;
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        verify(anggotaRepository).findByNpa(anyString());
    }

    @Test
    void check_whenAnggotaFound_shouldReturnDto() {
        when(anggotaRepository.findByNpa(ArgumentMatchers.anyString())).thenReturn(Optional.of(mockAnggota()));

        BaseResponse<CheckAnggotaDto> actual = anggotaService.check(CheckAnggotaRequest.of("99.0002"));

        verify(anggotaRepository).findByNpa(ArgumentMatchers.anyString());
        assertNotNull(actual);
        assertEquals(HttpStatus.OK, actual.getStatus());

        CheckAnggotaDto actualData = actual.getData();
        assertNotNull(actualData);
        assertEquals(CheckAnggotaDto.valueOf(mockAnggota()), actualData);
    }

    @Test
    void check_whenAnggotaNotFound_shouldReturnDto() {
        when(anggotaRepository.findByNpa(ArgumentMatchers.anyString())).thenReturn(Optional.empty());

        BaseResponse<CheckAnggotaDto> actual = anggotaService.check(CheckAnggotaRequest.of("99.0002"));

        verify(anggotaRepository).findByNpa(ArgumentMatchers.anyString());
        assertNotNull(actual);
        assertEquals(HttpStatus.NOT_FOUND, actual.getStatus());
        assertEquals("Data anggota tidak ditemukan", actual.getMessage());

        CheckAnggotaDto actualData = actual.getData();
        assertNull(actualData);
    }

    @Test
    void sendEmail_whenAnggotaNotFound_shouldThrowException() {
        when(anggotaRepository.findByNpa(ArgumentMatchers.anyString())).thenReturn(Optional.empty());

        assertThrows(
                ResourceNotFoundException.class,
                () -> anggotaService.sendEmail(
                        SendEmailAnggotaRequest.of("99.0002", "uzumaki_naruto@konohagakure.co.jp")),
                "Data anggota tidak ditemukan");
        verify(anggotaRepository).findByNpa(ArgumentMatchers.anyString());
    }

    @Test
    void sendEmail_whenAnggotaExists_shouldSendEmail() throws MessagingException {
        when(anggotaRepository.findByNpa(ArgumentMatchers.anyString())).thenReturn(Optional.of(mockAnggota()));

        anggotaService.sendEmail(SendEmailAnggotaRequest.of("99.0002", "uzumaki_naruto@konohagakure.co.jp"));

        verify(anggotaRepository).findByNpa(ArgumentMatchers.anyString());
        verify(mailService).sendMail(ArgumentMatchers.any(MailMessageContent.class));
    }

    @Test
    void uploadFotoAngotaByNpa_whenSuccess() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "admin-1.png", "image/png", FOTO_STREAM);
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.of(ANGGOTA));
        when(storageService.uploadFile(file, eq(anyString()))).thenReturn(STRING_ID);
        when(anggotaRepository.save(any(Anggota.class))).thenReturn(ANGGOTA);
        BaseResponse<String> result = anggotaService.uploadFotoAngotaByNpa(principal, anyString(), file);
        assertEquals("SUCCESS", result.getMessage());
        verify(anggotaRepository).findByNpa(anyString());
        verify(anggotaRepository).save(any(Anggota.class));
    }

    @Test
    void uploadFotoAngotaByNpa_whenSomeDataNotFound() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "admin-1.png", "image/png", FOTO_STREAM);
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(anggotaRepository.findByNpa(NPA)).thenReturn(Optional.empty());
            anggotaService.uploadFotoAngotaByNpa(principal, anyString(), file);
        });

        String expectedMessage = GlobalMessage.ANGGOTA_NOT_FOUND.message;
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        verify(anggotaRepository).findByNpa(anyString());
    }

    @Test
    void downloadFotoAngotaByNpa_whenSuccess() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "admin-1.png", "image/png", FOTO_STREAM);
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.of(ANGGOTA));
        when(storageService.downloadFile(anyString())).thenReturn(file.getBytes());
        Map<String, Object> result = anggotaService.downloadFotoAngotaByNpa(anyString(), principal);
        assertNotNull(result.get("data"));
        verify(anggotaRepository).findByNpa(anyString());
    }

    @Test
    void downloadFotoAngotaByNpa_WhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(anggotaRepository.findByNpa(NPA)).thenReturn(Optional.empty());
            anggotaService.downloadFotoAngotaByNpa(anyString(), principal);
        });

        String expectedMessage = GlobalMessage.ANGGOTA_NOT_FOUND.message;
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
        verify(anggotaRepository).findByNpa(anyString());
    }

    @Test
    void detailFotoAnggotaByNpa_WhenSuccess() {
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.of(ANGGOTA));
        when(storageService.getUriEndPoint(anyString())).thenReturn(STRING_ID);
        BaseResponse<BaseImageDto> result = anggotaService.detailFotoAnggotaByNpa(principal, anyString());
        assertEquals("SUCCESS", result.getMessage());
        verify(anggotaRepository).findByNpa(anyString());
    }

    @Test
    void uploadFotoProfile_WhenSuccess() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "admin-1.png", "image/png", FOTO_STREAM);
        when(principal.getName()).thenReturn(STRING_NAMA);
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.of(ANGGOTA));
        when(storageService.uploadFile(file, eq(anyString()))).thenReturn(STRING_ID);
        when(anggotaRepository.save(any(Anggota.class))).thenReturn(ANGGOTA);
        BaseResponse<String> result = anggotaService.uploadFotoProfile(principal, file);
        assertEquals("SUCCESS", result.getMessage());
        verify(anggotaRepository).findByNpa(anyString());
        verify(anggotaRepository).save(any(Anggota.class));
    }

    @Test
    void detailFotoProfile_WhenSuccess() {
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.of(ANGGOTA));
        when(principal.getName()).thenReturn(STRING_NAMA);
        when(storageService.getUriEndPoint(anyString())).thenReturn(STRING_ID);
        BaseResponse<BaseImageDto> result = anggotaService.detailFotoProfile(principal);
        assertEquals("SUCCESS", result.getMessage());
        verify(anggotaRepository).findByNpa(anyString());
    }

    @Test
    void downloadFotoProfile() throws IOException {
        MockMultipartFile file = new MockMultipartFile("file", "admin-1.png", "image/png", FOTO_STREAM);
        when(principal.getName()).thenReturn(STRING_NAMA);
        when(anggotaRepository.findByNpa(anyString())).thenReturn(Optional.of(ANGGOTA));
        when(storageService.downloadFile(anyString())).thenReturn(file.getBytes());
        Map<String, Object> result = anggotaService.downloadFotoProfile(principal);
        assertNotNull(result.get("data"));
        verify(anggotaRepository).findByNpa(anyString());
    }
}
