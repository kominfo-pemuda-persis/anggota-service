package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPcDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PerformaPcRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.*;

@ExtendWith(MockitoExtension.class)
class PerformaPcServiceTest {

    @InjectMocks
    private PerformaPcService performaPcService;

    @Mock
    private PerformaPcRepository performaPcRepository;

    @Mock
    private CabangService cabangService;

    @Mock
    private DaerahService daerahService;

    @Mock
    private WilayahService wilayahService;

    @Mock
    private HelperUtilService helperUtilService;

    @Mock
    private ModelMapper modelMapper;

    private Cabang cabang;
    private Daerah daerah;
    private Wilayah wilayah;
    private Provinsi provinsi;
    private Kabupaten kabupaten;
    private Kecamatan kecamatan;
    private PerformaPc performaPc;

    @BeforeEach
    void setUp() {
        cabang = Cabang.builder().kdPc("KC-1").build();
        daerah = Daerah.builder().kdPd("PD-02").build();
        wilayah = Wilayah.builder().kdPw("WIL-01").build();
        provinsi = Provinsi.builder().id("PROV1").build();
        kecamatan = Kecamatan.builder().id("KEC").build();
        kabupaten = Kabupaten.builder().id("KAB").build();
        performaPc = PerformaPc.builder()
                .cabang(cabang)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .deletedAt(null)
                .alamat("Jakarta")
                .kecamatan(kecamatan)
                .build();
    }

    @Test
    void performaPcList() {
        List<PerformaPc> performaPcs = new ArrayList<>();
        performaPcs.add(performaPc);
        Page<PerformaPc> pagedResponse = new PageImpl(performaPcs);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(performaPcRepository.findByDeletedAtIsNull(pageableTest)).thenReturn(pagedResponse);
        List<PerformaPc> listResult = performaPcService.performaPcList(pageableTest, false);
        assertEquals(performaPcs.size(), listResult.size());
    }

    @Test
    void create() {
        when(performaPcRepository.findByCabangKdPc(performaPc.getCabang().getKdPc()))
                .thenReturn(Optional.empty());
        when(cabangService.getByKodeCabang(cabang.getKdPc())).thenReturn(cabang);
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getKecamatanById(kecamatan.getId())).thenReturn(kecamatan);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(performaPcRepository.save(any(PerformaPc.class))).thenReturn(performaPc);
        PerformaPc result = performaPcService.create(performaPc);
        assertEquals(result.getId(), performaPc.getId());
    }

    @Test
    void update() {
        PerformaPcDto performaPcDto = PerformaPcDto.builder().alamat("Bandung").build();
        PerformaPc result1 = PerformaPc.builder()
                .id(1L)
                .cabang(cabang)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .alamat("Bandung")
                .kecamatan(kecamatan)
                .build();
        Configuration configuration = mock(Configuration.class);
        when(performaPcRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(performaPc));
        when(cabangService.getByKodeCabang(cabang.getKdPc())).thenReturn(cabang);
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getKecamatanById(kecamatan.getId())).thenReturn(kecamatan);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(modelMapper.getConfiguration()).thenReturn(configuration);
        when(performaPcRepository.save(performaPc)).thenReturn(result1);

        PerformaPc result = performaPcService.update(performaPcDto, 1L);
        assertEquals(result.getAlamat(), performaPcDto.getAlamat());
    }

    @Test
    void delete() {
        PerformaPc result1 = PerformaPc.builder()
                .id(1L)
                .cabang(cabang)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .alamat("Bandung")
                .deletedAt(LocalDateTime.now())
                .kecamatan(kecamatan)
                .build();
        when(performaPcRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(performaPc));
        when(performaPcRepository.save(performaPc)).thenReturn(result1);
        PerformaPc result = performaPcService.delete(1L);
        assertTrue(result.getDeletedAt() != null);
    }

    @Test
    void restore() {
        PerformaPc result1 = PerformaPc.builder()
                .id(1L)
                .cabang(cabang)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .alamat("Bandung")
                .deletedAt(LocalDateTime.now())
                .kecamatan(kecamatan)
                .build();
        when(performaPcRepository.findByIdAndDeletedAtIsNotNull(1L)).thenReturn(Optional.of(result1));
        when(performaPcRepository.save(result1)).thenReturn(performaPc);
        PerformaPc result = performaPcService.restore(1L);
        assertTrue(result.getDeletedAt() == null);
    }
}
