package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import id.or.pemudapersis.kominfo.anggotaservice.dto.RekapCaangDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.RekapCaang;
import id.or.pemudapersis.kominfo.anggotaservice.mapper.RekapCaangMapper;
import id.or.pemudapersis.kominfo.anggotaservice.repository.RekapCaangRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@ExtendWith(MockitoExtension.class)
public class RekapCaangServiceTest {
    @InjectMocks
    private RekapCaangService rekapCaangService;

    @Mock
    private RekapCaangRepository rekapCaangRepository;

    @Mock
    private RekapCaangMapper rekapCaangMapper;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(rekapCaangRepository);
    }

    @Test
    void getList() {
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(rekapCaangRepository.findAllByDeletedAtIsNull(eq(pageableTest))).thenReturn(mockPageRekapCaangList());
        BaseResponse<Page<RekapCaangDto>> baseResponse = rekapCaangService.getAllData(pageableTest);
        assertEquals(baseResponse.getMessage(), "SUCCESS");
        verify(rekapCaangRepository).findAllByDeletedAtIsNull(eq(pageableTest));
    }

    private PageImpl<RekapCaang> mockPageRekapCaangList() {
        return new PageImpl<>(List.of(
                RekapCaang.builder().name("obj1").build(),
                RekapCaang.builder().name("obj2").build(),
                RekapCaang.builder().name("obj3").build()));
    }
}
