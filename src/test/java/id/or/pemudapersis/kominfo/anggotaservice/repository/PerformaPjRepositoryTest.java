package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.*;

import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPj;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@DataJpaTest
class PerformaPjRepositoryTest {

    @Autowired
    private PerformaPjRepository performaPjRepository;

    private PerformaPj performaPj;

    @BeforeEach
    void setUp() {
        performaPj = PerformaPj.builder().namaPj("PJ-TEST").kodePj("KODE_PJ").build();
    }

    @Test
    void findByDeletedAtIsNull() {
        performaPjRepository.save(performaPj);
        Pageable pageable = PageRequest.of(0, 2);
        Page<PerformaPj> performaPjs = performaPjRepository.findByDeletedAtIsNull(pageable);
        assertFalse(performaPjs.isEmpty());
    }

    @Test
    void findByKodePj() {
        performaPjRepository.save(performaPj);
        Optional<PerformaPj> performaPjOptional = performaPjRepository.findByKodePjAndDeletedAtIsNull("KODE_PJ");
        assertTrue(performaPjOptional.isPresent());
    }
}
