package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MutasiDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Mutasi;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class MutasiVariables extends AnggotaVariableTest {
    public final Mutasi MUTASI = Mutasi.builder()
            .idMutasi(LONG_ID)
            .anggota(ANGGOTA)
            .tanggalMutasi(LocalDate.now())
            .build();
    public final MutasiDto MUTASI_DTO =
            MutasiDto.builder().idMutasi(LONG_ID).tanggalMutasi(LocalDate.now()).build();
    public final List<Mutasi> MUTASI_LIST = Collections.singletonList(Mutasi.builder()
            .idMutasi(LONG_ID)
            .anggota(ANGGOTA)
            .tanggalMutasi(LocalDate.now())
            .build());
    public final Page<Mutasi> MUTASI_PAGE = new PageImpl<>(Collections.singletonList(MUTASI));
}
