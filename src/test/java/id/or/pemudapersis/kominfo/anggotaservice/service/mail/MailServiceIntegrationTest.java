package id.or.pemudapersis.kominfo.anggotaservice.service.mail;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetup;
import id.or.pemudapersis.kominfo.anggotaservice.config.MailConfig;
import jakarta.mail.Address;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@Slf4j
@ContextConfiguration(
        classes = {
            MailConfig.class,
            MailService.class,
        })
@TestPropertySource(properties = {"mail.username=pp.pemuda.persis@gmail.com", "mail.password="})
@ExtendWith(SpringExtension.class)
class MailServiceIntegrationTest {

    @Autowired
    private MailService mailService;

    @Autowired
    private JavaMailSenderImpl javaMailSender;

    private GreenMail mailServer;

    @BeforeEach
    void setUp() {
        // mock mail server
        mailServer = new GreenMail(new ServerSetup(3025, null, "smtp"));
        mailServer.setUser("pp.pemuda.persis@gmail.com", "");
        mailServer.start();

        // config mail sender
        javaMailSender.setHost("localhost");
        javaMailSender.setPort(3025);
    }

    @Test
    void sendMail() throws MessagingException {
        mailService.sendMail(new MailMessageContent("Judul email", "cbot59@gmail.com", "default", null));

        MimeMessage[] receivedMessages = mailServer.getReceivedMessages();
        log.debug("receivedMessages size: {}", receivedMessages.length); // output = 1
        MimeMessage receivedMessage = receivedMessages[0];
        log.debug("headers: {}", GreenMailUtil.getHeaders(receivedMessage));

        // assertions
        String actualSubject = receivedMessage.getSubject();
        assertEquals("Judul email", actualSubject, "Should be matched");

        String actualFrom = receivedMessage.getFrom()[0].toString();
        assertEquals("Aktivasi AnaOnline <pp.pemuda.persis@gmail.com>", actualFrom, "Should be matched");

        Address[] recipients = receivedMessage.getRecipients(Message.RecipientType.TO);
        assertEquals(1, recipients.length, "Should only 1 recipient");
        assertEquals("cbot59@gmail.com", recipients[0].toString(), "Should be matched");
    }

    @AfterEach
    void tearDown() {
        mailServer.stop();
    }
}
