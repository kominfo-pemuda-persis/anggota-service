package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.or.pemudapersis.kominfo.anggotaservice.repository.CabangRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.CabangService;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.CabangTestVariable;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import java.time.LocalDate;
import java.util.Collections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
public class CabangControllerTest extends CabangTestVariable {
    @InjectMocks
    private CabangController cabangController;

    @Mock
    private CabangRepository cabangRepository;

    @Mock
    private CabangService cabangService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        cabangController = new CabangController(cabangService);
        mockMvc = standaloneSetup(cabangController).build();
    }

    @Test
    public void getListCabang() throws Exception {
        Pageable pageableTest = PageRequest.of(1, 10, Sort.by("kdPc"));
        String kdPd = "PD-1";
        // WHEN
        when(cabangService.getList(pageableTest, kdPd))
                .thenReturn(BaseResponse.<Page<CabangRequest>>builder()
                        .data(new PageImpl<>(Collections.singletonList(CABANG_REQUEST), pageableTest, 1))
                        .status(HttpStatus.OK)
                        .build());

        mockMvc.perform(get("/v1/api/cabang?pageNo=1&pageSize=10&sortBy=kdPc&kdPd=PD-1")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void createCabang() throws Exception {
        CabangRequest request = CabangRequest.builder()
                .kdPc("TES")
                .namaPc("TES")
                .diresmikan(LocalDate.now())
                .build();
        // WHEN
        when(cabangService.create(request))
                .thenReturn(BaseResponse.<CabangRequest>builder()
                        .data(CABANG_REQUEST)
                        .status(HttpStatus.OK)
                        .build());

        mockMvc.perform(post("/v1/api/cabang")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void updateCabang() throws Exception {
        CabangRequest request = CabangRequest.builder()
                .kdPc("TES")
                .namaPc("TES")
                .diresmikan(LocalDate.now())
                .build();
        // WHEN
        lenient()
                .when(cabangService.update("TES", request))
                .thenReturn(BaseResponse.<CabangRequest>builder()
                        .data(CABANG_REQUEST)
                        .status(HttpStatus.OK)
                        .build());
        mockMvc.perform(put("/v1/api/cabang/{kd}", "TES")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteCabang() throws Exception {
        // WHEN
        when(cabangService.delete("TEST"))
                .thenReturn(BaseResponse.<CabangRequest>builder()
                        .data(CABANG_REQUEST)
                        .status(HttpStatus.OK)
                        .build());
        mockMvc.perform(delete("/v1/api/cabang/{kd}", "TEST")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
}
