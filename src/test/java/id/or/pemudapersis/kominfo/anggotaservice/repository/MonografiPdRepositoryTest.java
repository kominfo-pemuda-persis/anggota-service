package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Kabupaten;
import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPd;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import id.or.pemudapersis.kominfo.anggotaservice.service.MonografiPdService;
import java.time.LocalDateTime;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class MonografiPdRepositoryTest {

    @Mock
    private MonografiPdRepository monografiPdRepository;

    @InjectMocks
    private MonografiPdService monografiPdService;

    private Wilayah wilayah;
    private Provinsi provinsi;
    private MonografiPd monografiPd;
    private Kabupaten kabupaten;

    @BeforeEach
    void setUp() {
        wilayah = Wilayah.builder().kdPw("PW-6").build();
        provinsi = Provinsi.builder().id("PROV1").build();
        monografiPd = MonografiPd.builder()
                .wilayah(wilayah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .deletedAt(null)
                .namaPd("namapd")
                .latitude(new Double(2))
                .longitude(new Double(3))
                .alamatAlternatif("alternatif1")
                .noKontak("123")
                .luas(4)
                .btsWilayahSelatan("sel")
                .btsWilayahTimur("timur")
                .btsWilayahBarat("barat")
                .build();
    }

    @Test
    void delete() {
        MonografiPd monografiPd1 = MonografiPd.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .deletedAt(LocalDateTime.now())
                .namaPd("namapd2")
                .latitude(new Double(2))
                .longitude(new Double(3))
                .alamatAlternatif("alternatif1")
                .noKontak("123")
                .luas(4)
                .btsWilayahSelatan("sel")
                .btsWilayahTimur("timur")
                .btsWilayahBarat("barat")
                .build();
        when(monografiPdRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(monografiPd));
        when(monografiPdRepository.save(monografiPd)).thenReturn(monografiPd1);

        MonografiPd result = monografiPdService.delete(1L);
        assertTrue(result.getDeletedAt() != null);
    }
}
