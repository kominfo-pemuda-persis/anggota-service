package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPw;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
class PerformaPwRepositoryTest {
    @Autowired
    private PerformaPwRepository performaPwRepository;

    @Autowired
    private WilayahRepository wilayahRepository;

    private PerformaPw performaPw;
    private Wilayah wilayah;

    @BeforeEach
    void setUp() {
        wilayah = Wilayah.builder().kdPw("WIL").namaWilayah("Wilayah").build();
        wilayahRepository.save(wilayah);
        performaPw = PerformaPw.builder()
                .wilayah(wilayah)
                .deletedAt(null)
                .wilayah(wilayah)
                .build();
        performaPwRepository.save(performaPw);
    }

    @Test
    void findByDeletedAtIsNull() {
        Pageable pageable = PageRequest.of(0, 2);
        Page<PerformaPw> performaPwPage = performaPwRepository.findByDeletedAtIsNull(pageable);
        assertFalse(performaPwPage.isEmpty());
    }

    @Test
    void findByWilayah_IdAndDeletedAtIsNull() {
        Optional<PerformaPw> performaPwOptional = performaPwRepository.findByWilayahKdPwAndDeletedAtIsNull("WIL");
        assertTrue(performaPwOptional.isPresent());
    }
}
