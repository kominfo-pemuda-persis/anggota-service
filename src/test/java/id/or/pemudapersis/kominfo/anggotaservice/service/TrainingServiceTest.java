package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

import id.or.pemudapersis.kominfo.anggotaservice.dto.TrainingDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Training;
import id.or.pemudapersis.kominfo.anggotaservice.repository.TrainingRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.TrainingVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Collections;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

class TrainingServiceTest extends TrainingVariables {
    private static final String SUCCESS = "SUCCESS";
    private static final String TRAINING_NOT_FOUND = "Training Not Found";
    private static final Long ID_TRAINING = 1L;

    @InjectMocks
    private TrainingService trainingService;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private AnggotaService anggotaService;

    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(modelMapper);
        verifyNoMoreInteractions(trainingRepository);
        verifyNoMoreInteractions(anggotaService);
    }

    @Test
    void trainingListWhenSuccess() {
        Page<Training> trainingPage = mock(Page.class);
        when(trainingPage.getContent()).thenReturn(Collections.singletonList(TRAINING));
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(modelMapper.map(TRAINING, TrainingDto.class)).thenReturn(TRAINING_DTO);
        when(trainingRepository.findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest)))
                .thenReturn(TRAINING_PAGE);
        BaseResponse<Page<TrainingDto>> baseResponse = trainingService.trainingList(pageableTest, false, NPA);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(trainingRepository).findByDeletedAtIsNullAndAnggotaNpa(anyString(), eq(pageableTest));
        verify(modelMapper).map(TRAINING, TrainingDto.class);
    }

    @Test
    void createTrainingWhenSuccess() {
        when(anggotaService.findByNpa(NPA)).thenReturn(ANGGOTA);
        when(modelMapper.map(TRAINING, TrainingDto.class)).thenReturn(TRAINING_DTO);
        when(trainingRepository.save(any(Training.class))).thenReturn(TRAINING);
        BaseResponse<TrainingDto> baseResponse = trainingService.create(NPA, TRAINING_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(anggotaService).findByNpa(NPA);
        verify(trainingRepository).save(any(Training.class));
        verify(modelMapper).map(TRAINING, TrainingDto.class);
    }

    @Test
    void updateTrainingWhenSuccess() {
        when(trainingRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(anyString(), anyLong()))
                .thenReturn(Optional.of(TRAINING));
        when(trainingRepository.save(any(Training.class))).thenReturn(TRAINING);
        when(modelMapper.map(TRAINING, TrainingDto.class)).thenReturn(TRAINING_DTO);
        BaseResponse<TrainingDto> baseResponse = trainingService.update(anyString(), anyLong(), TRAINING_DTO);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(trainingRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(anyString(), anyLong());
        verify(trainingRepository).save(any(Training.class));
        verify(modelMapper).map(TRAINING, TrainingDto.class);
        verify(modelMapper).map(TRAINING_DTO, TRAINING);
    }

    @Test
    void updateTrainingWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(trainingRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            trainingService.update(anyString(), anyLong(), TRAINING_DTO);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TRAINING_NOT_FOUND));

        verify(trainingRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(anyString(), anyLong());
    }

    @Test
    void deleteTrainingWhenSuccess() {
        when(trainingRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(anyString(), anyLong()))
                .thenReturn(Optional.of(TRAINING));
        when(trainingRepository.save(any(Training.class))).thenReturn(TRAINING);
        BaseResponse<TrainingDto> baseResponse = trainingService.delete(NPA, ID_TRAINING);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(trainingRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(NPA, ID_TRAINING);
        verify(trainingRepository).save(any(Training.class));
    }

    @Test
    void deleteTrainingWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(trainingRepository.findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            trainingService.delete(NPA, ID_TRAINING);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TRAINING_NOT_FOUND));

        verify(trainingRepository).findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(anyString(), anyLong());
    }

    @Test
    void restoreTrainingWhenSuccess() {
        when(trainingRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdTraining(NPA, LONG_ID))
                .thenReturn(Optional.of(TRAINING));
        when(trainingRepository.save(any(Training.class))).thenReturn(TRAINING);
        when(modelMapper.map(TRAINING, TrainingDto.class)).thenReturn(TRAINING_DTO);
        BaseResponse<TrainingDto> baseResponse = trainingService.restore(NPA, LONG_ID);
        assertEquals(baseResponse.getMessage(), SUCCESS);
        verify(trainingRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdTraining(NPA, LONG_ID);
        verify(trainingRepository).save(any(Training.class));
        verify(modelMapper).map(TRAINING, TrainingDto.class);
    }

    @Test
    void restoreTrainingWhenSomeDataNotFound() {
        Exception exception = assertThrows(BusinessException.class, () -> {
            when(trainingRepository.findByDeletedAtIsNotNullAndAnggotaNpaAndIdTraining(anyString(), anyLong()))
                    .thenReturn(Optional.empty());
            trainingService.restore(NPA, ID_TRAINING);
        });

        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(TRAINING_NOT_FOUND));
        verify(trainingRepository).findByDeletedAtIsNotNullAndAnggotaNpaAndIdTraining(anyString(), anyLong());
    }
}
