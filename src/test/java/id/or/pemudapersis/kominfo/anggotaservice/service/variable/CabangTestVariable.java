package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Cabang;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import java.time.LocalDate;
import java.util.Collections;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class CabangTestVariable {
    public static final Cabang CABANG = Cabang.builder()
            .kdPc("KDPC")
            .namaPc("NAMA_PC")
            .diresmikan(LocalDate.now())
            .build();
    public static final CabangRequest CABANG_REQUEST = CabangRequest.builder()
            .kdPc("KDPC")
            .namaPc("NAMA_PC")
            .diresmikan(LocalDate.now())
            .build();
    public static final Page<Cabang> CABANG_PAGE = new PageImpl(Collections.singletonList(CABANG));
}
