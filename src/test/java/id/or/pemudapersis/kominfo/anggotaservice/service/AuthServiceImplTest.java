package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.AnggotaVariableTest;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

@ExtendWith(MockitoExtension.class)
class AuthServiceImplTest extends AnggotaVariableTest {

    @InjectMocks
    private AuthServiceImpl service;

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private JwtService jwtService;

    @Test
    void authenticateShouldReturnDto() {
        var actual = prepareAndExecute();

        assertThat(actual).isNotNull().matches(dto -> "token".equals(dto.getAccessToken()));
    }

    private AuthenticateDto prepareAndExecute() {
        given(authenticationManager.authenticate(any(Authentication.class)))
                .willReturn(new UsernamePasswordAuthenticationToken(
                        Login.builder()
                                .anggota(Anggota.builder().npa("99.0008").build())
                                .roles(Collections.emptySet())
                                .password("password")
                                .anggota(Anggota.builder()
                                        .nama("Fulan")
                                        .pc(Cabang.builder()
                                                .kdPc("PC")
                                                .kdPd("PD")
                                                .build())
                                        .pw(Wilayah.builder().kdPw("PW").build())
                                        .pd(Daerah.builder()
                                                .kdPd("PD")
                                                .kdPw("PW")
                                                .build())
                                        .build())
                                .build(),
                        "password"));
        given(jwtService.generateToken(any(GenerateTokenInput.class))).willReturn("token");

        return service.authenticate(new AuthenticateInput("99.0008", "password", "localhost"));
    }

    @Test
    void authenticateShouldAuthenticateByAuthenticationManager() {
        prepareAndExecute();

        verify(authenticationManager).authenticate(new UsernamePasswordAuthenticationToken("99.0008", "password"));
    }

    @Test
    void authenticateShouldGenerateTokenByJwtService() {
        prepareAndExecute();

        verify(jwtService)
                .generateToken(new GenerateTokenInput(
                        "99.0008", Collections.emptyList(), "", "localhost", "PD", "PC", "PW", "Fulan"));
    }
}
