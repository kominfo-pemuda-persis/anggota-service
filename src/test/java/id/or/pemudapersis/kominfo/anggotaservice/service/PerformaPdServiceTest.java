package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPdDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PerformaPdRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.*;

@ExtendWith(MockitoExtension.class)
class PerformaPdServiceTest {
    @InjectMocks
    private PerformaPdService performaPdService;

    @Mock
    private PerformaPdRepository performaPdRepository;

    @Mock
    private DaerahService daerahService;

    @Mock
    private WilayahService wilayahService;

    @Mock
    private HelperUtilService helperUtilService;

    @Mock
    private ModelMapper modelMapper;

    private Daerah daerah;
    private Wilayah wilayah;
    private Provinsi provinsi;
    private Kabupaten kabupaten;
    private PerformaPd performaPd;

    @BeforeEach
    void setUp() {
        wilayah = Wilayah.builder().kdPw("WIL-01").build();
        provinsi = Provinsi.builder().id("PROV1").build();
        kabupaten = Kabupaten.builder().id("KAB").build();
        daerah = Daerah.builder().kdPd("PD").namaPd("PD-Nama").build();

        performaPd = PerformaPd.builder()
                .id(1L)
                .daerah(daerah)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .deletedAt(null)
                .ketuaPd("Hasan")
                .alamat("Bandung")
                .build();
    }

    @Test
    void performaPdList() {
        List<PerformaPd> performaPds = new ArrayList<>();
        performaPds.add(performaPd);
        Page<PerformaPd> pagedResponse = new PageImpl(performaPds);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(performaPdRepository.findByDeletedAtIsNull(pageableTest)).thenReturn(pagedResponse);
        List<PerformaPd> listResult = performaPdService.performaPdList(pageableTest, false);
        assertEquals(performaPds.size(), listResult.size());
    }

    @Test
    void create() {
        when(performaPdRepository.findByDaerahKdPd(performaPd.getDaerah().getKdPd()))
                .thenReturn(Optional.empty());
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(performaPdRepository.save(any(PerformaPd.class))).thenReturn(performaPd);
        PerformaPd result = performaPdService.create(performaPd);
        assertEquals(result.getId(), performaPd.getId());
    }

    @Test
    void update() {
        PerformaPdDto performaPdDto = PerformaPdDto.builder().alamat("Bandung").build();
        PerformaPd result1 = PerformaPd.builder()
                .id(1L)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .alamat("Bandung")
                .build();
        Configuration configuration = mock(Configuration.class);
        when(performaPdRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(performaPd));
        when(daerahService.getByKodeDaerah(daerah.getKdPd())).thenReturn(daerah);
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getKabupatenById(kabupaten.getId())).thenReturn(kabupaten);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(modelMapper.getConfiguration()).thenReturn(configuration);
        when(performaPdRepository.save(performaPd)).thenReturn(result1);

        PerformaPd result = performaPdService.update(performaPdDto, 1L);
        assertEquals(result.getAlamat(), performaPdDto.getAlamat());
    }

    @Test
    void delete() {
        PerformaPd result1 = PerformaPd.builder()
                .id(1L)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .alamat("Bandung")
                .deletedAt(LocalDateTime.now())
                .build();
        when(performaPdRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(performaPd));
        when(performaPdRepository.save(performaPd)).thenReturn(result1);
        PerformaPd result = performaPdService.delete(1L);
        assertTrue(result.getDeletedAt() != null);
    }

    @Test
    void restore() {
        PerformaPd result1 = PerformaPd.builder()
                .id(1L)
                .wilayah(wilayah)
                .daerah(daerah)
                .provinsi(provinsi)
                .kabupaten(kabupaten)
                .provinsi(provinsi)
                .alamat("Bandung")
                .deletedAt(LocalDateTime.now())
                .build();
        when(performaPdRepository.findByIdAndDeletedAtIsNotNull(1L)).thenReturn(Optional.of(result1));
        when(performaPdRepository.save(result1)).thenReturn(performaPd);
        PerformaPd result = performaPdService.restore(1L);
        assertTrue(result.getDeletedAt() == null);
    }
}
