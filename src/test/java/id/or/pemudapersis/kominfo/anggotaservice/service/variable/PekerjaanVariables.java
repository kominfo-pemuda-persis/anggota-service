package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PekerjaanDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Pekerjaan;
import java.util.Collections;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class PekerjaanVariables extends AnggotaVariableTest {
    public final Pekerjaan PEKERJAAN = Pekerjaan.builder()
            .pekerjaan("PEKERJAAN")
            .alamat("ALAMAT")
            .idPekerjaan(LONG_ID)
            .keterangan("KETERANGAN")
            .tahunMulai(1)
            .anggota(ANGGOTA)
            .tahunSelesai(2)
            .build();
    public final PekerjaanDto PEKERJAAN_DTO = PekerjaanDto.builder()
            .pekerjaan("PEKERJAAN")
            .alamat("ALAMAT")
            .idPekerjaan(LONG_ID)
            .keterangan("KETERANGAN")
            .tahunMulai(1)
            .tahunSelesai(2)
            .build();
    public static final List<Pekerjaan> PEKERJAAN_LIST = Collections.singletonList(Pekerjaan.builder()
            .pekerjaan("PEKERJAAN")
            .alamat("ALAMAT")
            .idPekerjaan(LONG_ID)
            .keterangan("KETERANGAN")
            .tahunMulai(1)
            .anggota(ANGGOTA)
            .tahunSelesai(2)
            .build());
    public final Page<Pekerjaan> PEKERJAAN_PAGE = new PageImpl<>(Collections.singletonList(PEKERJAAN));
}
