package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPwDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPw;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PerformaPwRepository;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;
import org.springframework.data.domain.*;

@ExtendWith(MockitoExtension.class)
class PerformaPwServiceTest {
    @InjectMocks
    private PerformaPwService performaPwService;

    @Mock
    private PerformaPwRepository performaPwRepository;

    @Mock
    private WilayahService wilayahService;

    @Mock
    private HelperUtilService helperUtilService;

    @Mock
    private ModelMapper modelMapper;

    private Wilayah wilayah;
    private Provinsi provinsi;
    private PerformaPw performaPw;

    @BeforeEach
    void setUp() {
        wilayah = Wilayah.builder().kdPw("WIL-01").build();
        provinsi = Provinsi.builder().id("PROV1").build();
        performaPw = PerformaPw.builder()
                .wilayah(wilayah)
                .provinsi(provinsi)
                .provinsi(provinsi)
                .deletedAt(null)
                .alamat("Jakarta")
                .build();
    }

    @Test
    void performaPwList() {
        List<PerformaPw> performaPws = new ArrayList<>();
        performaPws.add(performaPw);
        Page<PerformaPw> pagedResponse = new PageImpl(performaPws);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("id"));
        when(performaPwRepository.findByDeletedAtIsNull(pageableTest)).thenReturn(pagedResponse);
        List<PerformaPw> listResult = performaPwService.performaPwList(pageableTest, false);
        assertEquals(performaPws.size(), listResult.size());
    }

    @Test
    void create() {
        when(performaPwRepository.findByWilayahKdPwAndDeletedAtIsNull(
                        performaPw.getWilayah().getKdPw()))
                .thenReturn(Optional.empty());
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(performaPwRepository.save(any(PerformaPw.class))).thenReturn(performaPw);
        PerformaPw result = performaPwService.create(performaPw);
        assertEquals(result.getId(), performaPw.getId());
    }

    @Test
    void update() {
        PerformaPwDto performaPwDto = PerformaPwDto.builder().alamat("Bandung").build();
        PerformaPw result1 = PerformaPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .provinsi(provinsi)
                .alamat("Bandung")
                .build();
        Configuration configuration = mock(Configuration.class);
        when(performaPwRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(performaPw));
        when(wilayahService.getByKodeWilayah(wilayah.getKdPw())).thenReturn(wilayah);
        when(helperUtilService.getProvinsiById(provinsi.getId())).thenReturn(provinsi);
        when(modelMapper.getConfiguration()).thenReturn(configuration);
        when(performaPwRepository.save(performaPw)).thenReturn(result1);

        PerformaPw result = performaPwService.update(performaPwDto, 1L);
        assertEquals(result.getAlamat(), performaPwDto.getAlamat());
    }

    @Test
    void delete() {
        PerformaPw result1 = PerformaPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .provinsi(provinsi)
                .deletedAt(LocalDateTime.now())
                .alamat("Bandung")
                .build();
        when(performaPwRepository.findByIdAndDeletedAtIsNull(1L)).thenReturn(Optional.of(performaPw));
        when(performaPwRepository.save(performaPw)).thenReturn(result1);
        PerformaPw result = performaPwService.delete(1L);
        assertTrue(result.getDeletedAt() != null);
    }

    @Test
    void restore() {
        PerformaPw result1 = PerformaPw.builder()
                .id(1L)
                .wilayah(wilayah)
                .provinsi(provinsi)
                .provinsi(provinsi)
                .deletedAt(LocalDateTime.now())
                .alamat("Bandung")
                .build();
        when(performaPwRepository.findByIdAndDeletedAtIsNotNull(1L)).thenReturn(Optional.of(result1));
        when(performaPwRepository.save(result1)).thenReturn(performaPw);
        PerformaPw result = performaPwService.restore(1L);
        assertTrue(result.getDeletedAt() == null);
    }
}
