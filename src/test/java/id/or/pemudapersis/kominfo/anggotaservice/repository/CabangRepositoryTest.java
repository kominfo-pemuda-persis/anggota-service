package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertFalse;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Cabang;
import java.sql.Timestamp;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@DataJpaTest
class CabangRepositoryTest {
    @Autowired
    private CabangRepository cabangRepository;

    @Test
    void findAll() {
        Cabang cabang = Cabang.builder()
                .kdPc("TEST")
                .namaPc("TEST")
                .kdPd("PD-1")
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .updatedAt(new Timestamp(System.currentTimeMillis()))
                .build();
        cabangRepository.save(cabang);
        List<Cabang> cabangList = cabangRepository.findAll();
        assertFalse(cabangList.isEmpty());
    }

    @Test
    void findByIsDeleted() {
        Cabang cabang = Cabang.builder()
                .kdPc("TEST")
                .namaPc("TEST")
                .kdPd("PD-1")
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .updatedAt(new Timestamp(System.currentTimeMillis()))
                .build();
        cabangRepository.save(cabang);
        Pageable pageableTest = PageRequest.of(0, 10, Sort.by("kdPc"));
        Page<Cabang> cabangList = cabangRepository.findAll(pageableTest);
        assertFalse(cabangList.getContent().isEmpty());
    }
}
