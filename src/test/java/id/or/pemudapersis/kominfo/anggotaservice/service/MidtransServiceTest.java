package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.midtrans.service.MidtransSnapApi;
import id.or.pemudapersis.kominfo.anggotaservice.dto.midtransDTO.Item;
import java.util.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@Slf4j
class MidtransServiceTest {

    private MidtransService midtransService;

    @Mock
    private MidtransSnapApi midtransSnapApi;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        midtransService = new MidtransService(midtransSnapApi);
    }

    @Test
    public void testCheckout() throws Exception {
        List<Item> itemList = new ArrayList<>();
        itemList.add(Item.builder()
                .id(UUID.randomUUID().toString())
                .name("Infaq bulanan")
                .price(15000)
                .quantity(10)
                .build());

        Map<String, Object> expected = new HashMap<>();
        expected.put("token", "this is token");
        expected.put("redirected_url", "this is redirect url");
        JSONObject expectedJson = new JSONObject(expected);
        when(midtransSnapApi.createTransaction(anyMap())).thenReturn(expectedJson);
        Map<String, Object> result = midtransService.checkout(itemList);

        log.info("result : {}", result);

        assertNotNull(result.get("token"));
    }
}
