package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MasterPendidikanDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.PendidikanDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.MasterPendidikan;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Pendidikan;
import java.util.Collections;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class PendidikanVariables extends AnggotaVariableTest {

    public final MasterPendidikan MASTER_PENDIDIKAN =
            MasterPendidikan.builder().pendidikan("PENDIDIKAN").id(STRING_ID).build();

    public final MasterPendidikanDto MASTER_PENDIDIKAN_DTO =
            MasterPendidikanDto.builder().pendidikan("PENDIDIKAN").id(STRING_ID).build();

    public final Pendidikan PENDIDIKAN = Pendidikan.builder()
            .idPendidikan(LONG_ID)
            .anggota(ANGGOTA)
            .jenisPendidikan("JENIS_PENDIDIDIKAN")
            .jenjangPendidikan(MASTER_PENDIDIKAN)
            .instansi("INSTANSI")
            .jurusan("JURUSAN")
            .tahunKeluar(1)
            .tahunMasuk(2)
            .build();
    public final PendidikanDto PENDIDIKAN_DTO = PendidikanDto.builder()
            .idPendidikan(LONG_ID)
            .jenisPendidikan("JENIS_PENDIDIDIKAN")
            .jenjangPendidikan(MASTER_PENDIDIKAN_DTO)
            .instansi("INSTANSI")
            .jurusan("JURUSAN")
            .tahunKeluar(1)
            .tahunMasuk(2)
            .build();

    public final List<Pendidikan> PENDIDIKAN_LIST = Collections.singletonList(Pendidikan.builder()
            .idPendidikan(LONG_ID)
            .anggota(ANGGOTA)
            .jenjangPendidikan(MasterPendidikan.builder()
                    .pendidikan("PENDIDIKAN")
                    .id(STRING_ID)
                    .build())
            .jenisPendidikan("JENIS_PENDIDIDIKAN")
            .instansi("INSTANSI")
            .jurusan("JURUSAN")
            .tahunKeluar(1)
            .tahunMasuk(2)
            .build());
    public final Page<Pendidikan> PENDIDIKAN_PAGE = new PageImpl<>(Collections.singletonList(PENDIDIKAN));
}
