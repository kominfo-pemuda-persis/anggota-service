package id.or.pemudapersis.kominfo.anggotaservice.service.variable;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KeluargaDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Keluarga;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Otonom;
import java.util.Collections;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

public class KeluargaVariabels extends AnggotaVariableTest {
    public static final Otonom OTONOM = Otonom.builder().idOtonom(1L).build();
    public final Keluarga KELUARGA =
            Keluarga.builder().idKeluarga(1L).anggota(ANGGOTA).otonom(OTONOM).build();
    public final KeluargaDto KELUARGA_DTO = KeluargaDto.builder()
            .idKeluarga(1L)
            .namaKeluarga("NAMA KELUARGA")
            .alamat("ALAMAT KELUARGA")
            .hubungan("AYAH")
            .jumlahAnak(1)
            .keterangan("KETERANGAN")
            .otonom(OTONOM)
            .anggota(ANGGOTA_DTO)
            .build();
    public static final List<Keluarga> KELUARGA_LIST = Collections.singletonList(
            Keluarga.builder().idKeluarga(1L).anggota(ANGGOTA).otonom(OTONOM).build());
    public final Page<Keluarga> KELUARGA_PAGE = new PageImpl<>(Collections.singletonList(KELUARGA));
}
