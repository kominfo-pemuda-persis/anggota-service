package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Kabupaten;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Kecamatan;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import id.or.pemudapersis.kominfo.anggotaservice.repository.KabupatenRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.KecamatanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.ProvinsiRepository;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class HelperUtilServiceTest {

    @InjectMocks
    private HelperUtilService helperUtilService;

    @Mock
    private ProvinsiRepository provinsiRepository;

    @Mock
    private KabupatenRepository kabupatenRepository;

    @Mock
    private KecamatanRepository kecamatanRepository;

    @Test
    void getProvinsiById() {
        Provinsi provinsi = Provinsi.builder().id("PROV").build();
        when(provinsiRepository.findById("PROV")).thenReturn(Optional.of(provinsi));
        Provinsi result = helperUtilService.getProvinsiById("PROV");
        assertEquals(result.getId(), provinsi.getId());
    }

    @Test
    void getKabupatenById() {
        Kabupaten kabupaten = Kabupaten.builder().id("KAB").build();
        when(kabupatenRepository.findById("KAB")).thenReturn(Optional.of(kabupaten));
        Kabupaten result = helperUtilService.getKabupatenById("KAB");
        assertEquals(result.getId(), kabupaten.getId());
    }

    @Test
    void getKecamatanById() {
        Kecamatan kecamatan = Kecamatan.builder().id("KEC").build();
        when(kecamatanRepository.findById("KEC")).thenReturn(Optional.of(kecamatan));
        Kecamatan result = helperUtilService.getKecamatanById("KEC");
        assertEquals(result.getId(), kecamatan.getId());
    }
}
