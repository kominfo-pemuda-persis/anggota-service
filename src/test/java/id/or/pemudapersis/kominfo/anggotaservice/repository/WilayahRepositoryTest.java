package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertFalse;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
class WilayahRepositoryTest {
    @Autowired
    private WilayahRepository wilayahRepository;

    @Test
    void findAll() {
        Wilayah wilayah = Wilayah.builder()
                .kdPw("PW-1")
                .namaWilayah("Bandung Barat")
                .createdAt(LocalDateTime.now())
                .updatedAt(LocalDateTime.now())
                .build();
        wilayahRepository.save(wilayah);
        List<Wilayah> wilayahList = wilayahRepository.findAll();
        assertFalse(wilayahList.isEmpty());
    }
}
