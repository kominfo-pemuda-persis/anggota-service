package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Desa;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Kabupaten;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Kecamatan;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
class DesaRepositoryTest {

    @Autowired
    private DesaRepository desaRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Disabled("Disabled until error fixed")
    @BeforeEach
    void setUp() {
        Provinsi provinsi = testEntityManager.persist(Provinsi.builder().build());
        Kabupaten kabupaten = testEntityManager.persist(Kabupaten.builder()
                .id("DESA-001")
                .provinsi(provinsi)
                .nama("KABUPATEN SUKAMAJU")
                .build());
        Kecamatan kecamatan = testEntityManager.persist(Kecamatan.builder()
                .id("KEC-001")
                .kabupaten(kabupaten)
                .nama("KECAMATAN SUKAMAJU")
                .build());
        testEntityManager.persist(Desa.builder()
                .id("Des-001")
                .kecamatan(kecamatan)
                .nama("Desa SUKAMAJU")
                .build());
    }

    @Disabled("Disabled until error fixed")
    @Test
    void testFindAll() {
        List<Desa> desa = desaRepository.findAll();

        assertNotNull(desa.get(0));
    }
}
