package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPjDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.*;
import id.or.pemudapersis.kominfo.anggotaservice.request.MonografiPJRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional
@Log4j2
public class MonografiPjServiceTest {

    @InjectMocks
    private MonografiPjService monografiPjService;

    @Mock
    private MonografiPjRepository monografiPjRepository;

    @Mock
    private KejamiyyahanPjRepository kejamiyyahanPjRepository;

    @Mock
    private DesaRepository desaRepository;

    @Mock
    private KabupatenRepository kabupatenRepository;

    @Mock
    private KecamatanRepository kecamatanRepository;

    @Mock
    private ProvinsiRepository provinsiRepository;

    @Mock
    private DaerahRepository daerahRepository;

    @Mock
    private CabangRepository cabangRepository;

    @Mock
    private WilayahRepository wilayahRepository;

    private ObjectMapper objectMapper;

    private String monografiPJRequestJsonSample = "{\n" + "    \"kodePj\": \"PJ-01\",\n"
            + "    \"namaPj\": \"cab. kejora 2\",\n"
            + "    \"kdPc\": \"PC.1\",\n"
            + "    \"kdPd\": \"PD-1\",\n"
            + "    \"kdPw\": \"PW-2\",\n"
            + "    \"provinsi\": \"33\",\n"
            + "    \"kabupaten\": \"3316\",\n"
            + "    \"kecamatan\": \"3316130\",\n"
            + "    \"desa\": \"3316130011\",\n"
            + "    \"latitude\": 1.1,\n"
            + "    \"longitude\": 1.1,\n"
            + "    \"email\": \"1@kejora.com\",\n"
            + "    \"noTelpon\": \"11\",\n"
            + "    \"alamat\": \"jakarta\",\n"
            + "    \"luas\": \"1\",\n"
            + "    \"bwTimur\": \"1\",\n"
            + "    \"bwBarat\": \"1\",\n"
            + "    \"bwSelatan\": \"1\",\n"
            + "    \"bwUtara\": \"1\",\n"
            + "    \"jarakProvinsi\": \"1\",\n"
            + "    \"jarakKabupaten\": \"1\",\n"
            + "    \"photo\": null,\n"
            + "    \"ketua\": \"haris\",\n"
            + "    \"sekretaris\": \"haris\",\n"
            + "    \"bendahara\": \"haris\",\n"
            + "    \"hariNgantor\": \"selasa\",\n"
            + "    \"anggotaBiasa\": 1,\n"
            + "    \"anggotaLuarBiasa\": 1,\n"
            + "    \"tidakHerReg\": \"1\",\n"
            + "    \"mutasiPersis\": \"1\",\n"
            + "    \"mutasiTempat\": \"1\",\n"
            + "    \"mengundurkanDiri\": \"1\",\n"
            + "    \"meninggalDunia\": \"1\",\n"
            + "    \"calonAnggota\": \"1\",\n"
            + "    \"waktuNgantor\": \"11:11:00\",\n"
            + "    \"musjamTerakhirMasehi\": \"2020-12-31\",\n"
            + "    \"musjamTerakhirHijriyyah\": \"2020-12-31\"\n"
            + "}";

    private String kejamiyyahanJsonSample =
            "{\"id\":3,\"kodePj\":\"PJ-01\",\"namaPj\":\"cab. kejora\",\"monografiPj\":{\"id\":15,\"kodePj\":\"PJ-01\",\"namaPj\":\"cab. kejora\",\"kdPc\":{\"kdPc\":\"PC.1\",\"namaPc\":\"Andir\",\"createdAt\":\"07-09-2020\",\"updatedAt\":\"07-09-2020\"},\"kdPd\":{\"kdPd\":\"PD-1\",\"namaPd\":\"Kota Bandung\",\"createdAt\":\"07-09-2020\",\"updatedAt\":\"07-09-2020\"},\"kdPw\":{\"kdPw\":\"PW-2\",\"namaWilayah\":\"DKI Jakarta\",\"createdAt\":\"08-09-2020 00:33:24\",\"updatedAt\":\"08-09-2020 00:33:24\"},\"provinsi\":{\"id\":\"33\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"kabupaten\":{\"id\":\"3316\",\"provinsi\":{\"id\":\"33\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"kecamatan\":{\"id\":\"3316130\",\"kabupaten\":{\"id\":\"3316\",\"provinsi\":{\"id\":\"33\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\" Japah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"desa\":{\"id\":\"3316130011\",\"kecamatan\":{\"id\":\"3316130\",\"kabupaten\":{\"id\":\"3316\",\"provinsi\":{\"id\":\"33\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\" Japah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\"Japah\",\"createdAt\":\"07-09-2020\",\"updatedAt\":\"07-09-2020\"},\"latitude\":1.1,\"longitude\":1.1,\"email\":\"1@111.com\",\"noTelpon\":\"11\",\"alamat\":\"jakarta\",\"luas\":\"1\",\"bwTimur\":\"1\",\"bwBarat\":\"1\",\"bwSelatan\":\"1\",\"bwUtara\":\"1\",\"jarakProvinsi\":\"1\",\"jarakKabupaten\":\"1\",\"photo\":null,\"createdAt\":\"11-09-2020\",\"updatedAt\":null},\"kdPc\":{\"kdPc\":\"PC.1\",\"namaPc\":\"Andir\",\"createdAt\":\"07-09-2020\",\"updatedAt\":\"07-09-2020\"},\"kdPd\":{\"kdPd\":\"PD-1\",\"namaPd\":\"Kota Bandung\",\"createdAt\":\"07-09-2020\",\"updatedAt\":\"07-09-2020\"},\"kdPw\":{\"kdPw\":\"PW-2\",\"namaWilayah\":\"DKI Jakarta\",\"createdAt\":\"08-09-2020 00:33:24\",\"updatedAt\":\"08-09-2020 00:33:24\"},\"provinsi\":{\"id\":\"33\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"kabupaten\":{\"id\":\"3316\",\"provinsi\":{\"id\":\"33\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"kecamatan\":{\"id\":\"3316130\",\"kabupaten\":{\"id\":\"3316\",\"provinsi\":{\"id\":\"33\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\" Japah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"desa\":{\"id\":\"3316130011\",\"kecamatan\":{\"id\":\"3316130\",\"kabupaten\":{\"id\":\"3316\",\"provinsi\":{\"id\":\"33\",\"namaProvinsi\":\"Jawa Tengah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\"Kab. Blora\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\" Japah\",\"createdAt\":\"08-09-2020 00:33:21\",\"updatedAt\":\"08-09-2020 00:33:21\"},\"nama\":\"Japah\",\"createdAt\":\"07-09-2020\",\"updatedAt\":\"07-09-2020\"},\"ketua\":\"haris\",\"sekretaris\":\"haris\",\"bendahara\":\"haris\",\"hariNgantor\":\"Selasa\",\"waktuNgantor\":\"11:11:00\",\"musjamTerakhirMasehi\":\"31-12-2020\",\"musjamTerakhirHijriyyah\":\"31-12;2020\",\"anggotaBiasa\":1,\"anggotaLuarBiasa\":1,\"tidakHerReg\":\"1\",\"mutasiPersis\":\"1\",\"mutasiTempat\":\"1\",\"meninggalDunia\":\"1\",\"mengundurkanDiri\":\"1\",\"calonAnggota\":\"1\",\"createdAt\":\"11-09-2020\",\"updatedAt\":null}";

    @BeforeEach
    public void setup() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void testGetList() throws Exception {
        // data no exist
        //        String kejamiyyahanPjStringJson = "{\"id\":1,\"kodePj\":\"pj-01\",\"namaPj\":\"test
        // pj\",\"monografiPj\":{\"id\":2,\"kodePj\":\"pj-01\",\"namaPj\":\"test
        // pj\",\"kdPc\":{\"kd\":0,\"kdPc\":\"PC.1\",\"namaPc\":\"Andir\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\",\"isDeleted\":false},\"kdPd\":{\"kdPd\":\"PD-1\",\"namaPd\":\"Kota Bandung\",\"createdAt\":\"11-08-2020\",\"createdBy\":null,\"updatedAt\":\"11-08-2020\",\"updatedBy\":null,\"deleted\":false},\"kdPw\":{\"id\":\"PW-1\",\"namaWilayah\":\"Jawa Barat\",\"createdAt\":\"11-08-2020 20:35:31\",\"updatedAt\":\"11-08-2020 20:35:31\"},\"provinsi\":{\"id\":\"11\",\"nama\":\"Aceh\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\"},\"kabupaten\":{\"id\":\"1101\",\"provinsi\":\"11\",\"nama\":\"Kab. Simeulue\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\"},\"kecamatan\":{\"id\":\"1101010\",\"kabupaten\":\"1101\",\"nama\":\" Teupah Selatan\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\"},\"desa\":{\"id\":\"1101010001\",\"kecamatan\":\"1101010\",\"nama\":\"Latiung\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\"},\"latitude\":1.23123,\"longitude\":3.321312,\"email\":\"test@ts.com\",\"noTelpon\":\"0812381238\",\"alamat\":\"jakarta\",\"luas\":\"10\",\"bwTimur\":\"10\",\"bwBarat\":\"10\",\"bwSelatan\":\"10\",\"bwUtara\":\"10\",\"jarakProvinsi\":\"10\",\"jarakKabupaten\":\"10\",\"photo\":\"-\",\"createdBy\":\"\",\"createdAt\":null,\"updatedBy\":null,\"updatedAt\":null,\"deleted\":false},\"kdPc\":{\"kd\":0,\"kdPc\":\"PC.1\",\"namaPc\":\"Andir\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\",\"isDeleted\":false},\"kdPd\":{\"id\":0,\"kdPd\":\"PD-1\",\"namaPd\":\"Kota Bandung\",\"createdAt\":\"11-08-2020\",\"createdBy\":null,\"updatedAt\":\"11-08-2020\",\"updatedBy\":null,\"deleted\":false},\"kdPw\":{\"id\":0,\"kodeWilayah\":\"PW-1\",\"namaWilayah\":\"Jawa Barat\",\"createdAt\":\"11-08-2020 20:35:31\",\"updatedAt\":\"11-08-2020 20:35:31\",\"isDeleted\":false},\"provinsi\":{\"id\":\"11\",\"nama\":\"Aceh\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\"},\"kabupaten\":{\"id\":\"1101\",\"provinsi\":\"11\",\"nama\":\"Kab. Simeulue\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\"},\"kecamatan\":{\"id\":\"1101010\",\"kabupaten\":\"1101\",\"nama\":\" Teupah Selatan\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\"},\"desa\":{\"id\":\"1101010001\",\"kecamatan\":\"1101010\",\"nama\":\"Latiung\",\"createdAt\":\"11-08-2020\",\"updatedAt\":\"11-08-2020\"},\"ketua\":\"haris\",\"sekretaris\":\"haris\",\"bendahara\":\"haris\",\"hariNgantor\":\"Senin\",\"waktuNgantor\":\"04:08:039\",\"musjamTerakhirMasehi\":\"15-08-2020\",\"musjamTerakhirHijriyyah\":\"15-08;2020\",\"anggotaBiasa\":10,\"anggotaLuarBiasa\":10,\"tidakHerreg\":\"\",\"mutasiPersis\":\"10\",\"mutasiTempat\":\"10\",\"meninggalDunia\":\"10\",\"mengundurkanDiri\":\"10\",\"calonAnggota\":\"100\",\"createdAt\":null,\"createdBy\":null,\"updatedAt\":null,\"updatedBy\":null,\"deleted\":false}";

        KejamiyyahanPj kejamiyyahanPj = objectMapper.readValue(kejamiyyahanJsonSample, KejamiyyahanPj.class);

        Page<MonografiPj> monografiPjPage = new PageImpl<>(Arrays.asList(kejamiyyahanPj.getMonografiPj()));

        when(monografiPjRepository.findByDeletedAtIsNull(any(Pageable.class))).thenReturn(monografiPjPage);

        Pageable pageable = PageRequest.of(0, 2);

        List<MonografiPjDto> monografiPjDtoList = monografiPjService.getList(pageable);

        String resultsString = objectMapper.writeValueAsString(monografiPjDtoList);
        log.info("result : {}", resultsString);

        assertNotNull(monografiPjDtoList);
    }

    @Test
    public void testAddMonografiPj() throws Exception {
        MonografiPJRequest monografiPJRequest =
                objectMapper.readValue(monografiPJRequestJsonSample, MonografiPJRequest.class);

        KejamiyyahanPj dummyKejamiyyahanPj = objectMapper.readValue(kejamiyyahanJsonSample, KejamiyyahanPj.class);
        MonografiPj dummyMonografiPj = dummyKejamiyyahanPj.getMonografiPj();
        Desa dummyDesa = dummyKejamiyyahanPj.getDesa();
        Kabupaten dummyKabupaten = dummyKejamiyyahanPj.getKabupaten();
        Kecamatan dummyKecamatan = dummyKejamiyyahanPj.getKecamatan();
        Provinsi dummyProvinsi = dummyKejamiyyahanPj.getProvinsi();
        Cabang dummyCabang = dummyKejamiyyahanPj.getKdPc();
        Daerah dummyDaerah = dummyKejamiyyahanPj.getKdPd();
        Wilayah dummyWilayah = dummyKejamiyyahanPj.getKdPw();

        when(desaRepository.findById(anyString())).thenReturn(Optional.of(dummyDesa));
        when(kabupatenRepository.findById(anyString())).thenReturn(Optional.of(dummyKabupaten));
        when(kecamatanRepository.findById(anyString())).thenReturn(Optional.of(dummyKecamatan));
        when(provinsiRepository.findById(anyString())).thenReturn(Optional.of(dummyProvinsi));
        when(cabangRepository.findById(anyString())).thenReturn(Optional.of(dummyCabang));
        when(daerahRepository.findById(anyString())).thenReturn(Optional.of(dummyDaerah));
        when(wilayahRepository.findById(anyString())).thenReturn(Optional.of(dummyWilayah));

        String result = monografiPjService.addMonografiPj(monografiPJRequest);

        assertEquals("sukses", result);
    }

    @Test
    public void testEdit() throws Exception {

        MonografiPJRequest monografiPJRequest =
                objectMapper.readValue(monografiPJRequestJsonSample, MonografiPJRequest.class);

        KejamiyyahanPj dummyKejamiyyahanPj = objectMapper.readValue(kejamiyyahanJsonSample, KejamiyyahanPj.class);
        MonografiPj dummyMonografiPj = dummyKejamiyyahanPj.getMonografiPj();
        Desa dummyDesa = dummyKejamiyyahanPj.getDesa();
        Kabupaten dummyKabupaten = dummyKejamiyyahanPj.getKabupaten();
        Kecamatan dummyKecamatan = dummyKejamiyyahanPj.getKecamatan();
        Provinsi dummyProvinsi = dummyKejamiyyahanPj.getProvinsi();
        Cabang dummyCabang = dummyKejamiyyahanPj.getKdPc();
        Daerah dummyDaerah = dummyKejamiyyahanPj.getKdPd();
        Wilayah dummyWilayah = dummyKejamiyyahanPj.getKdPw();

        when(monografiPjRepository.findById(anyLong())).thenReturn(Optional.of(dummyMonografiPj));
        when(kejamiyyahanPjRepository.findByMonografiPj_id(anyLong())).thenReturn(Optional.of(dummyKejamiyyahanPj));
        when(desaRepository.findById(anyString())).thenReturn(Optional.of(dummyDesa));
        when(kabupatenRepository.findById(anyString())).thenReturn(Optional.of(dummyKabupaten));
        when(kecamatanRepository.findById(anyString())).thenReturn(Optional.of(dummyKecamatan));
        when(provinsiRepository.findById(anyString())).thenReturn(Optional.of(dummyProvinsi));
        when(cabangRepository.findById(anyString())).thenReturn(Optional.of(dummyCabang));
        when(daerahRepository.findById(anyString())).thenReturn(Optional.of(dummyDaerah));
        when(wilayahRepository.findById(anyString())).thenReturn(Optional.of(dummyWilayah));

        String result = monografiPjService.editMonografiPj(3L, monografiPJRequest);

        assertEquals(result, "sukses");
    }

    @Test
    public void testDeletePJ() throws Exception {

        KejamiyyahanPj dummyKejamiyyahanPj = objectMapper.readValue(kejamiyyahanJsonSample, KejamiyyahanPj.class);

        when(kejamiyyahanPjRepository.findByMonografiPj_id(anyLong())).thenReturn(Optional.of(dummyKejamiyyahanPj));

        String result = monografiPjService.deleteMonografiPJ(1L);

        assertEquals(result, "sukses");
    }
}
