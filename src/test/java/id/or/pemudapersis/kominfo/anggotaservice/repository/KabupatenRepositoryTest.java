package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Kabupaten;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
class KabupatenRepositoryTest {

    @Autowired
    private KabupatenRepository kabupatenRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Disabled("Disabled until error fixed")
    @BeforeEach
    void setUp() {
        Provinsi provinsi = testEntityManager.persist(Provinsi.builder().build());
        testEntityManager.persist(Kabupaten.builder()
                .id("DESA-001")
                .provinsi(provinsi)
                .nama("KABUPATEN SUKAMAJU")
                .build());
    }

    @Disabled("Disabled until error fixed")
    @Test
    void testFindAll() {
        List<Kabupaten> kabupatenList = kabupatenRepository.findAll();

        assertNotNull(kabupatenList.get(0));
    }
}
