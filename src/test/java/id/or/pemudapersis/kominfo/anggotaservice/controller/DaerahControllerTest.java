package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.DaerahService;
import id.or.pemudapersis.kominfo.anggotaservice.service.variable.DaerahVariables;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import java.time.LocalDate;
import java.util.Collections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(MockitoExtension.class)
class DaerahControllerTest extends DaerahVariables {

    @InjectMocks
    private DaerahController daerahController;

    @Mock
    private DaerahService daerahService;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        daerahController = new DaerahController(daerahService);
        mockMvc = standaloneSetup(daerahController).build();
    }

    @Test
    public void testGetList() throws Exception {
        Pageable pageableTest = PageRequest.of(1, 10, Sort.by("kdPc"));
        // WHEN
        when(daerahService.getList("PW-1", pageableTest))
                .thenReturn(BaseResponse.<Page<DaerahRequest>>builder()
                        .data(new PageImpl<>(Collections.singletonList(DAERAH_REQUEST), pageableTest, 1))
                        .status(HttpStatus.OK)
                        .build());

        mockMvc.perform(get("/v1/api/daerah?kdPw=PW-1&pageNo=1&pageSize=10&sortBy=kdPc")
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void testSaveDaerah() throws Exception {
        DaerahRequest request = DaerahRequest.builder()
                .kdPd("TES")
                .namaPd("TES")
                .diresmikan(LocalDate.now())
                .build();
        // WHEN
        when(daerahService.saveOne(request))
                .thenReturn(BaseResponse.<DaerahRequest>builder()
                        .data(DAERAH_REQUEST)
                        .status(HttpStatus.OK)
                        .build());

        mockMvc.perform(post("/v1/api/daerah")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdate() throws Exception {
        DaerahRequest request = DaerahRequest.builder()
                .kdPd("TES")
                .namaPd("TES")
                .diresmikan(LocalDate.now())
                .build();
        // WHEN
        lenient()
                .when(daerahService.updateOne("TES", request))
                .thenReturn(BaseResponse.<DaerahRequest>builder()
                        .data(DAERAH_REQUEST)
                        .status(HttpStatus.OK)
                        .build());

        mockMvc.perform(put("/v1/api/daerah/{kdPd}", "TES")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .content(new ObjectMapper().writeValueAsString(request))
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void testSoftDelete() throws Exception {

        when(daerahService.softDelete("TEST"))
                .thenReturn(BaseResponse.<DaerahRequest>builder()
                        .data(DAERAH_REQUEST)
                        .status(HttpStatus.OK)
                        .build());
        mockMvc.perform(delete("/v1/api/daerah/{kdPd}", "TEST")
                        .accept(MediaType.APPLICATION_JSON_VALUE)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
}
