package id.or.pemudapersis.kominfo.anggotaservice.repository;

import static org.junit.jupiter.api.Assertions.assertFalse;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import java.sql.Timestamp;
import java.util.List;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
// @Testcontainers
// @SpringBootTest(
//        properties = {
//                "spring.jpa.generate-ddl=true",
//                "spring.datasource.url=jdbc:tc:mysql:8:///test"
//        },
//        webEnvironment = RANDOM_PORT
// )
class ProvinsiRepositoryTest {

    @Autowired
    private ProvinsiRepository provinsiRepository;

    @Disabled("Disabled until error fixed")
    @Test
    void testFindAll() {
        Provinsi provinsi = Provinsi.builder()
                .id("TEST")
                .namaProvinsi("TEST")
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .updatedAt(new Timestamp(System.currentTimeMillis()))
                .build();
        provinsiRepository.save(provinsi);
        List<Provinsi> provinsiList = provinsiRepository.findAll();
        assertFalse(provinsiList.isEmpty());
    }
}
