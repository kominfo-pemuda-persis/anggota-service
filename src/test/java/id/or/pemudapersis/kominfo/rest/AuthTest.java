package id.or.pemudapersis.kominfo.rest;

import static io.restassured.RestAssured.given;

import com.nimbusds.jose.shaded.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

@Disabled("broken test")
@ExtendWith(MockitoExtension.class)
public class AuthTest extends BaseTestData {

    @BeforeAll
    static void setUp() {
        RestAssured.baseURI = "http://localhost:8080";
    }

    @Test
    public void login() throws Exception {
        ;
        String npa = this.getLoginNPA();
        String password = this.getLoginPassword();
        String json = new Gson().toJson(Map.of("npa", npa, "password", password));
        Response response = given().contentType(ContentType.JSON).body(json).post("/v1/api/auth");
        response.prettyPrint();
        Map data = new Gson().fromJson(response.getBody().asString(), Map.class);
        this.setToken(data);
        response.then().statusCode(HttpStatus.OK.value());
    }
}
