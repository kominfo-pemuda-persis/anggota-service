package id.or.pemudapersis.kominfo.rest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.ex.ConfigurationException;

public class BaseTestData {
    PropertiesConfiguration config = new PropertiesConfiguration();

    void setToken(Map<String, String> data) throws FileNotFoundException, ConfigurationException, IOException {
        if (this.config.isEmpty()) config.read(new FileReader(new File("main.properties")));
        config.clearProperty("token");
        config.addProperty("token", data.get("accessToken"));
        config.write(new FileWriter("main.properties"));
    }

    String getToken() throws FileNotFoundException, ConfigurationException, IOException {
        if (!loadConfig()) return "";
        return this.config.getString("token");
    }

    String getLoginNPA() throws FileNotFoundException, ConfigurationException, IOException {
        if (!loadConfig()) return "";
        return this.config.getString("npa");
    }

    String getLoginPassword() throws FileNotFoundException, ConfigurationException, IOException {
        if (!loadConfig()) return "";
        return this.config.getString("login_password");
    }

    String getHeaderAuthBasicUsername() throws FileNotFoundException, ConfigurationException, IOException {
        if (!loadConfig()) return "";
        return this.config.getString("basic_username");
    }

    String getHeaderAuthBasicPassword() throws FileNotFoundException, ConfigurationException, IOException {
        if (!loadConfig()) return "";
        return this.config.getString("basic_password");
    }

    String getRefreshToken() throws FileNotFoundException, ConfigurationException, IOException {
        if (!loadConfig()) return "";
        return this.config.getString("refresh_token");
    }

    String getEmail() throws FileNotFoundException, ConfigurationException, IOException {
        if (!loadConfig()) return "";
        return this.config.getString("email");
    }

    boolean loadConfig() throws FileNotFoundException, ConfigurationException, IOException {
        try {
            if (this.config.isEmpty()) config.read(new FileReader(new File("main.properties")));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    Map<String, String> setAuthToken() throws FileNotFoundException, ConfigurationException, IOException {
        return Map.of("Authorization", "Bearer ".concat(getToken()));
    }
}
