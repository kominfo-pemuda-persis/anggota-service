package id.or.pemudapersis.kominfo.rest;

import static io.restassured.RestAssured.given;

import id.or.pemudapersis.kominfo.anggotaservice.dto.RekapCaangDto;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import java.time.LocalDate;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

@Disabled("broken test")
@ExtendWith(MockitoExtension.class)
public class RekapCaangTest extends BaseTestData {

    @BeforeAll
    static void setUp() {
        RestAssured.baseURI = "http://localhost:8080";
    }

    @Test
    public void getList() throws Exception {
        ;
        Response response = given().contentType(ContentType.JSON)
                .accept(MediaType.APPLICATION_JSON.toString())
                .headers(setAuthToken())
                .when()
                .get("/v1/api/rekap-caang");
        response.prettyPrint();
        response.then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void create() throws Exception {
        ;
        RekapCaangDto request = RekapCaangDto.builder()
                .name("test")
                .npa("99.0001")
                .email("and@com.id")
                .phoneNumber("12345678")
                .jumlah("200")
                .pathFolder("test")
                .pathExcel("oke")
                .pathImage("test")
                .status("APPROVED")
                .tanggalMaruf(LocalDate.now())
                .updatedAt(LocalDateTime.now())
                .createdAt(LocalDateTime.now())
                .build();
        Response response = given().contentType(ContentType.JSON)
                .accept(MediaType.APPLICATION_JSON.toString())
                .headers(setAuthToken())
                .when()
                .body(request)
                .post("/v1/api/rekap-caang");
        response.prettyPrint();
        response.then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void delete() throws Exception {
        ;
        Response response = given().contentType(ContentType.JSON)
                .accept(MediaType.APPLICATION_JSON.toString())
                .headers(setAuthToken())
                .when()
                .delete("/v1/api/rekap-caang/5");
        response.prettyPrint();
        response.then().statusCode(HttpStatus.OK.value());
    }
}
