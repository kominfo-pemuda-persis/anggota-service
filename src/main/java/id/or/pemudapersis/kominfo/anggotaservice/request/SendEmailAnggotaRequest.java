package id.or.pemudapersis.kominfo.anggotaservice.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class SendEmailAnggotaRequest {

    @NotBlank(message = "NPA tidak boleh kosong")
    @Pattern(regexp = "\\d{2}.\\d{4}", message = "NPA harus dalam format xx.xxxx")
    String npa;

    @NotBlank(message = "Email tidak boleh kosong")
    @Email(message = "Format email tidak sesuai")
    String email;
}
