package id.or.pemudapersis.kominfo.anggotaservice.entity;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Set;
import lombok.*;

@Entity
@Table(name = "t_role")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Role implements Serializable {
    @Id
    @Column(name = "id_role")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "display_name")
    private String displayName;

    @Column(name = "description")
    private String description;

    @Column(name = "status_aktif")
    private Integer status;

    @OneToMany(mappedBy = "role")
    private Set<Permission> permissions;
}
