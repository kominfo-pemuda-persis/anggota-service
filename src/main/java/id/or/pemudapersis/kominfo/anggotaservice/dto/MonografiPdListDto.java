package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPd;
import lombok.Value;
import org.springframework.data.domain.Page;

@Value
public class MonografiPdListDto {
    Page<MonografiPdDto> monografiPd;

    public static MonografiPdListDto valueOf(Page<MonografiPd> monografiPds) {
        // Page<MonografiPdDto> pdDtos = monografiPds.map(MonografiPdDto::from);
        return null;
    }
}
