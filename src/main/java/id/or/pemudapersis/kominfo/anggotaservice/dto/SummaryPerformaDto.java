package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SummaryPerformaDto {
    private Long performaPj;
    private Long performaPc;
    private Long performaPw;
    private Long performaPd;
    private Long jmlAnggota;
    private Long jmlUser;
}
