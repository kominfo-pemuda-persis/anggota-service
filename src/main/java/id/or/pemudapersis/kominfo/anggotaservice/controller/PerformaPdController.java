package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.beans.support.PagedListHolder.DEFAULT_PAGE_SIZE;
import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPdDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPdListDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPd;
import id.or.pemudapersis.kominfo.anggotaservice.service.PerformaPdService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/api/performa/pd")
@Tag(name = "Performa PD", description = "Endpoint for managing Performa PD")
public class PerformaPdController {
    private final PerformaPdService performaPdService;
    private final ModelMapper modelMapper;

    public PerformaPdController(PerformaPdService performaPdService, ModelMapper modelMapper) {
        this.performaPdService = performaPdService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @Operation(
            summary = "Get all Indek Performa PD Anggota Persis",
            description = "Get all Indek Performa PD Anggota Persis.",
            tags = {"Performa PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<Object> getPerformaPdListDtos(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<PerformaPdListDto> performaPds = performaPdService.performaPdList(paging, false).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", performaPds);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Add new Indek Performa PD Anggota Persis",
            description = "Add new Indek Performa PD Anggota Persis.",
            tags = {"Performa PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> create(@RequestBody @Valid PerformaPdDto performaPdDto) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        PerformaPd performaPd = convertToEntity(performaPdDto);
        performaPdService.create(performaPd);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Edit Indek Performa PD Anggota Persis",
            description = "Edit Indek Performa PD Anggota Persis.",
            tags = {"Performa PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> edit(@RequestBody @Valid PerformaPdDto performaPdDto, @PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        performaPdService.update(performaPdDto, id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @GetMapping(value = "/delete")
    @Operation(
            summary = "Get all deleted data Indek Performa PD Anggota Persis",
            description = "Get all deleted data Indek Performa PD Anggota Persis.",
            tags = {"Performa PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<Object> getPerformaPdListDeletedDtos(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false) Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<PerformaPdListDto> performaPds = performaPdService.performaPdList(paging, true).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", performaPds);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Deleted data Indek Performa PD Anggota Persis",
            description = "Deleted data Indek Performa PD Anggota Persis.",
            tags = {"Performa PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        performaPdService.delete(id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PatchMapping(value = "/{id}/restore")
    @Operation(
            summary = "Restore data Indek Performa PD Anggota Persis",
            description = "Restore data Indek Performa PD Anggota Persis.",
            tags = {"Performa PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> restore(@PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        performaPdService.restore(id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    private PerformaPd convertToEntity(PerformaPdDto performaPdDto) {
        return modelMapper.map(performaPdDto, PerformaPd.class);
    }

    private PerformaPdListDto convertToDto(PerformaPd performaPd) {
        return modelMapper.map(performaPd, PerformaPdListDto.class);
    }
}
