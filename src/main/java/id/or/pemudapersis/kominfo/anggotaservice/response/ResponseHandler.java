package id.or.pemudapersis.kominfo.anggotaservice.response;

import java.util.HashMap;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseHandler {

    private ResponseHandler() {
        throw new IllegalStateException("ResponseHandler class");
    }

    public static ResponseEntity<Object> generateResponse(
            String message, HttpStatus status, HttpStatus httpStatusCode, Object responseObj) {
        Map<String, Object> map = new HashMap<>();
        map.put("message", message);
        map.put("status", status);
        map.put("httpCode", httpStatusCode.value());
        map.put("data", responseObj);

        return new ResponseEntity<>(map, status);
    }
}
