package id.or.pemudapersis.kominfo.anggotaservice.service;

public interface AuthService {

    /**
     * Authenticate user.
     * @param input required to authenticate user
     * @return user authenticated details
     */
    AuthenticateDto authenticate(AuthenticateInput input);
}
