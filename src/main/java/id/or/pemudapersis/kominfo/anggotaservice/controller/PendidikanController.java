package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PendidikanDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.PendidikanService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/anggota")
@RequiredArgsConstructor
@Tag(name = "Pendidikan", description = "Endpoint for managing Pendidikan")
public class PendidikanController {
    private final PendidikanService pendidikanService;

    @GetMapping(value = "/{npa}/pendidikan")
    @Operation(
            summary = "Get all Pendidikan Anggota Persis",
            description = "Get all Pendidikan Anggota Persis.",
            tags = {"Pendidikan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PendidikanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<PendidikanDto>>> pendidikanList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idPendidikan")
                    String sortBy,
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<PendidikanDto>> baseResponse = pendidikanService.pendidikanList(paging, false, npa);
        return ok(baseResponse);
    }

    @PostMapping(value = "/{npa}/pendidikan")
    @Operation(
            summary = "Add new Pendidikan Anggota Persis",
            description = "Add new Pendidikan Anggota Persis.",
            tags = {"Pendidikan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PendidikanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<PendidikanDto>> create(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Valid @RequestBody PendidikanDto pendidikanDto) {
        BaseResponse<PendidikanDto> baseResponse = pendidikanService.create(npa, pendidikanDto);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{npa}/pendidikan/{idPendidikan}")
    @Operation(
            summary = "Update Pendidikan Anggota Persis",
            description = "Update Pendidikan Anggota Persis.",
            tags = {"Pendidikan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PendidikanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<PendidikanDto>> update(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Pendidikan") @PathVariable("idPendidikan") Long idPendidikan,
            @RequestBody PendidikanDto pendidikanDto) {
        BaseResponse<PendidikanDto> baseResponse = pendidikanService.update(npa, idPendidikan, pendidikanDto);
        return ok(baseResponse);
    }

    @GetMapping(value = "/{npa}/pendidikan/delete")
    @Operation(
            summary = "Get all Pendidikan By npa anggota",
            description = "Get all Pendidikan By npa anggota.",
            tags = {"Pendidikan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PendidikanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<PendidikanDto>>> pendidikanListDeleted(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idPendidikan")
                    String sortBy,
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<PendidikanDto>> baseResponse = pendidikanService.pendidikanList(paging, true, npa);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{npa}/pendidikan/{idPendidikan}")
    @Operation(
            summary = "Update Pendidikan By npa anggota deleted",
            description = "Update Pendidikan By npa anggota deleted.",
            tags = {"Pendidikan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PendidikanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<PendidikanDto>> delete(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Pendidikan") @PathVariable("idPendidikan") Long idPendidikan) {
        BaseResponse<PendidikanDto> baseResponse = pendidikanService.delete(npa, idPendidikan);
        return ok(baseResponse);
    }

    @PatchMapping(value = "/{npa}/pendidikan/{idPendidikan}/restore")
    @Operation(
            summary = "Restore Pendidikan By npa anggota deleted",
            description = "Restore Pendidikan By npa anggota deleted.",
            tags = {"Pendidikan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PendidikanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<PendidikanDto>> restore(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Pendidikan") @PathVariable("idPendidikan") Long idPendidikan) {
        BaseResponse<PendidikanDto> baseResponse = pendidikanService.restore(npa, idPendidikan);
        return ok(baseResponse);
    }
}
