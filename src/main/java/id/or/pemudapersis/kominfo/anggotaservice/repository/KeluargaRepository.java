package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Keluarga;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeluargaRepository extends JpaRepository<Keluarga, Long> {
    Page<Keluarga> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Keluarga> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Keluarga> findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeluarga(String idAnggota, Long idKeluarga);

    Optional<Keluarga> findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(String idAnggota, Long idKeluarga);
}
