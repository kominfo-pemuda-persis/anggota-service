package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KeluargaDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Keluarga;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Otonom;
import id.or.pemudapersis.kominfo.anggotaservice.repository.KeluargaRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.OtonomRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.time.LocalDateTime;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class KeluargaService {

    private static final String KELUARGA_NOT_FOUND_ERROR_MESSAGE = "Keluarga Not Found";

    private final KeluargaRepository keluargaRepository;

    private final AnggotaService anggotaService;

    private final OtonomRepository otonomRepository;

    private final ModelMapper modelMapper;

    public KeluargaService(
            KeluargaRepository keluargaRepository,
            AnggotaService anggotaService,
            OtonomRepository otonomRepository,
            ModelMapper modelMapper) {
        this.keluargaRepository = keluargaRepository;
        this.anggotaService = anggotaService;
        this.otonomRepository = otonomRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<KeluargaDto>> keluargaList(Pageable pageable, boolean isDeleted, String npa) {
        final long start = System.nanoTime();
        Page<KeluargaDto> keluargaPage;
        if (!isDeleted)
            keluargaPage = keluargaRepository
                    .findByDeletedAtIsNullAndAnggotaNpa(npa, pageable)
                    .map(this::convertToDto);
        else
            keluargaPage = keluargaRepository
                    .findByDeletedAtIsNotNullAndAnggotaNpa(npa, pageable)
                    .map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", keluargaPage);
    }

    private KeluargaDto convertToDto(Keluarga keluarga) {
        return modelMapper.map(keluarga, KeluargaDto.class);
    }

    public BaseResponse<KeluargaDto> create(String npa, KeluargaDto keluargaDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(npa);
        Otonom otonom = otonomRepository
                .findById(keluargaDto.getOtonom().getIdOtonom())
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "OTONOM_NOT_FOUND", "Otonom Not Found"));

        Keluarga keluarga = Keluarga.builder()
                .anggota(anggota)
                .namaKeluarga(keluargaDto.getNamaKeluarga())
                .alamat(keluargaDto.getAlamat())
                .hubungan(keluargaDto.getHubungan())
                .jumlahAnak(keluargaDto.getJumlahAnak())
                .keterangan(keluargaDto.getKeterangan())
                .otonom(otonom)
                .statusAnggota(keluargaDto.getStatusAnggota())
                .build();
        Keluarga result = keluargaRepository.save(keluarga);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<KeluargaDto> update(String idAnggota, Long idKeluarga, KeluargaDto keluargaDto) {
        final long start = System.nanoTime();

        Keluarga keluargaExist = keluargaRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(idAnggota, idKeluarga)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, "KELUARGA_NOT_FOUND", KELUARGA_NOT_FOUND_ERROR_MESSAGE));

        Long idOtonom = Optional.ofNullable(keluargaDto.getOtonom())
                .map(Otonom::getIdOtonom)
                .orElse(keluargaExist.getOtonom().getIdOtonom());

        Otonom otonom = otonomRepository
                .findById(idOtonom)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "OTONOM_NOT_FOUND", "Otonom Not Found"));

        keluargaDto.setOtonom(null);
        keluargaDto.setIdKeluarga(null);
        updateEntityFromDto(keluargaDto, keluargaExist);
        keluargaExist.setOtonom(otonom);
        Keluarga result = keluargaRepository.save(keluargaExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<KeluargaDto> delete(String npa, Long idKeluarga) {
        final long start = System.nanoTime();

        Keluarga keluargaExist = keluargaRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdKeluarga(npa, idKeluarga)
                .orElseThrow(
                        () -> new BusinessException(HttpStatus.NOT_FOUND, "30731", KELUARGA_NOT_FOUND_ERROR_MESSAGE));

        keluargaExist.setDeletedAt(LocalDateTime.now());
        keluargaRepository.save(keluargaExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public BaseResponse<KeluargaDto> restore(String npa, Long idKeluarga) {
        final long start = System.nanoTime();

        Keluarga keluargaExist = keluargaRepository
                .findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeluarga(npa, idKeluarga)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, "KELUARGA_NOT_FOUND", KELUARGA_NOT_FOUND_ERROR_MESSAGE));

        keluargaExist.setDeletedAt(null);
        Keluarga result = keluargaRepository.save(keluargaExist);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(Object from, Object to) {
        modelMapper.map(from, to);
    }
}
