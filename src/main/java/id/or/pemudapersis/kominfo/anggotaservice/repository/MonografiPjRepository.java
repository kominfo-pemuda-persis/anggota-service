package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPj;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonografiPjRepository extends JpaRepository<MonografiPj, Long> {

    Page<MonografiPj> findByDeletedAtNotNull(Pageable pageable);

    Page<MonografiPj> findByDeletedAtIsNull(Pageable pageable);

    Long countByDeletedAtIsNull();
}
