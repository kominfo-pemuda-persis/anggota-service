package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.JamiyyahPc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JamiyyahPcRepository extends JpaRepository<JamiyyahPc, Long> {}
