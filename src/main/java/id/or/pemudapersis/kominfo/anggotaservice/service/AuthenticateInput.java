package id.or.pemudapersis.kominfo.anggotaservice.service;

import jakarta.validation.constraints.NotBlank;
import lombok.ToString;
import lombok.Value;

@Value
public class AuthenticateInput {

    @NotBlank
    String npa;

    @NotBlank
    @ToString.Exclude
    String password;

    @NotBlank
    String host;
}
