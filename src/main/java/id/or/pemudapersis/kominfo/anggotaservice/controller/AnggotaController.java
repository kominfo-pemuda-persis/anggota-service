package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.request.CheckAnggotaRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.SendEmailAnggotaRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.AnggotaService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import jakarta.validation.Valid;
import java.security.Principal;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by IntelliJ IDEA.
 * Project : anggota-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/06/20
 * Time: 07.55
 */
@RestController
@Log4j2
@RequestMapping(value = "/v1/api/anggota")
@Tag(name = "Anggota", description = "Endpoint for managing Anggota")
public class AnggotaController {
    private static final int DEFAULT_PAGE_SIZE = 10;

    private final AnggotaService anggotaService;

    public AnggotaController(AnggotaService anggotaService) {
        this.anggotaService = anggotaService;
    }

    @GetMapping
    @ResponseBody
    @Operation(
            summary = "Get all Pemuda Persis Data",
            description = "Get all Pemuda Persis Data.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<Page<AnggotaDto>>> getListAnggota(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "keyword NPA or Name Anggota") @RequestParam(required = false) String keyword,
            @Parameter(description = "Status Merital") @RequestParam(required = false) String statusMerital,
            @Parameter(description = "Jenis  Anggota") @RequestParam(required = false) String jenisAnggota,
            @Parameter(description = "Golongan  Darah") @RequestParam(required = false) String golonganDarah,
            @Parameter(description = "Pendidikan  Terakhir") @RequestParam(required = false) String pendidikanTerakhir,
            @Parameter(description = "Level  Tafiq") @RequestParam(required = false) String levelTafiq,
            @Parameter(description = "Status HER") @RequestParam(required = false) String statusHer,
            @Parameter(description = "Status") @RequestParam(required = false) String status,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "npa")
                    String sortBy) {
        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        AnggotaFilterDto filterDto = AnggotaFilterDto.builder()
                .jenisAnggota(jenisAnggota)
                .pendidikanTerakhir(pendidikanTerakhir)
                .golonganDarah(golonganDarah)
                .statusHer(statusHer)
                .levelTafiq(levelTafiq)
                .statusMerital(statusMerital)
                .status(status)
                .keyword(keyword)
                .build();
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<AnggotaDto>> listBaseResponse = anggotaService.getAllAnggota(filterDto, paging);
        return ok(listBaseResponse);
    }

    @GetMapping(value = "/profile")
    @Operation(
            summary = "Get Pemuda Persis detail profile",
            description = "Get Pemuda Persis Data.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<AnggotaDto>> detailCurrentAnggota(Principal principal) {
        BaseResponse<AnggotaDto> baseResponse = anggotaService.getByNpa(principal.getName());
        return ok(baseResponse);
    }

    @PutMapping(value = "/profile")
    @Operation(
            summary = "Update Pemuda Persis detail profile",
            description = "Update Pemuda Persis Data.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<AnggotaDto>> updateProfile(
            @RequestBody AnggotaRequestDto anggotaRequestDto, Principal principal) {
        BaseResponse<AnggotaDto> baseResponse =
                anggotaService.updateCurrentAnggota(principal, principal.getName(), anggotaRequestDto);
        return ok(baseResponse);
    }

    @PostMapping
    @Operation(
            summary = "Add new data Anggota Pemuda Persis detail profile",
            description = "Add new data Anggota Pemuda Persis Data.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<AnggotaDto>> create(
            @RequestBody @Valid AnggotaRequestDto anggotaDto, Principal principal) {
        BaseResponse<AnggotaDto> baseResponse = anggotaService.create(principal, anggotaDto);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{idAnggota}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Update data Anggota Pemuda Persis detail",
            description = "Update data Anggota Pemuda Persis Data.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<AnggotaDto>> update(
            @RequestBody @Valid AnggotaRequestDto anggotaDto,
            @PathVariable("idAnggota") String idAnggota,
            Principal principal) {
        BaseResponse<AnggotaDto> baseResponse = anggotaService.update(principal, idAnggota, anggotaDto);
        return ok(baseResponse);
    }

    @PostMapping("/check")
    @Operation(
            summary = "Check whether an Anggota is exists",
            description = "Check whether an Anggota is exists.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<CheckAnggotaDto>> check(@RequestBody @Valid CheckAnggotaRequest request) {
        BaseResponse<CheckAnggotaDto> checkResult = anggotaService.check(request);
        return ResponseEntity.status(checkResult.getStatus()).body(checkResult);
    }

    @PostMapping("/sendEmail")
    @Operation(
            summary = "Send an EMail to Anggota",
            description = "Send an EMail to Anggota.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<Void> sendEmail(@RequestBody @Valid SendEmailAnggotaRequest request)
            throws MessagingException {
        anggotaService.sendEmail(request);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/sendEmailActivation")
    @Operation(
            summary = "Send an Email Activation to Registered Anggota",
            description = "Send an Email Activation to Anggota.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<String>> sendEmailActivation(@RequestBody @Valid SendEmailAnggotaRequest request)
            throws MessagingException {
        BaseResponse<String> response = anggotaService.sendEmailActivation(request);
        return ok(response);
    }

    @PutMapping(
            value = "/foto/{npa}",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @Operation(
            summary = "Upload foto Anggota by npa",
            description = "Upload foto Anggota by npa.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<String>> uploadFotoAngota(
            @RequestPart(value = "file") MultipartFile file, @PathVariable("npa") String npa, Principal principal) {
        BaseResponse<String> baseResponse = anggotaService.uploadFotoAngotaByNpa(principal, npa, file);
        return ok(baseResponse);
    }

    @GetMapping(value = "/foto/{npa}")
    @Operation(
            summary = "Download foto anggota by npa",
            description = "Download foto anggota by npa.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<ByteArrayResource> downloadFotoAnggota(@PathVariable String npa, Principal principal) {
        Map<String, Object> mapData = anggotaService.downloadFotoAngotaByNpa(npa, principal);
        byte[] data = (byte[]) mapData.get("data");
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity.ok()
                .contentLength(data.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + mapData.get("fileName") + "\"")
                .body(resource);
    }

    @GetMapping(value = "/foto/{npa}/detail")
    @Operation(
            summary = "Detail foto Anggota by npa",
            description = "Detail foto Anggota by npa.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<BaseImageDto>> detailFotoAnggota(
            @PathVariable("npa") String npa, Principal principal) {
        BaseResponse<BaseImageDto> baseResponse = anggotaService.detailFotoAnggotaByNpa(principal, npa);
        return ok(baseResponse);
    }

    @PutMapping(
            value = "/profile/foto",
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    @Operation(
            summary = "Update upload Foto Profile Angota",
            description = "Update upload Foto Profile Angota.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<String>> uploadFotoProfile(
            @RequestPart(value = "file") MultipartFile file, Principal principal) {
        BaseResponse<String> baseResponse = anggotaService.uploadFotoProfile(principal, file);
        return ok(baseResponse);
    }

    @GetMapping(value = "/profile/foto/detail")
    @Operation(
            summary = "detail Foto Profile Angota",
            description = "detail Foto Profile Angota.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<BaseImageDto>> detailFotoProfile(Principal principal) {
        BaseResponse<BaseImageDto> baseResponse = anggotaService.detailFotoProfile(principal);
        return ok(baseResponse);
    }

    @GetMapping(value = "/profile/foto")
    @Operation(
            summary = "Download Foto Profile Angota",
            description = "Download Foto Profile Angota.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<ByteArrayResource> downloadFotoProfile(Principal principal) {
        Map<String, Object> mapData = anggotaService.downloadFotoProfile(principal);
        byte[] data = (byte[]) mapData.get("data");
        ByteArrayResource resource = new ByteArrayResource(data);
        return ResponseEntity.ok()
                .contentLength(data.length)
                .header("Content-type", "application/octet-stream")
                .header("Content-disposition", "attachment; filename=\"" + mapData.get("fileName") + "\"")
                .body(resource);
    }

    @GetMapping(value = "/check/{npa}")
    @Operation(
            summary = "Details Profile Angota",
            description = "Details Profile Angota.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<AnggotaDetailDto>> detailAnggotaByNpa(@PathVariable String npa) {
        BaseResponse<AnggotaDetailDto> baseResponse = anggotaService.getDetailByNpa(npa);
        return ok(baseResponse);
    }

    @GetMapping(value = "/npa/{npa}")
    @Operation(
            summary = "Get by NPA Angota",
            description = "Get By NPA Angota.",
            tags = {"Anggota"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = AnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @ResponseBody
    public ResponseEntity<BaseResponse<AnggotaSimpleResponse>> getAnggotaByNpa(
            @Parameter(description = "npa Anggota") @PathVariable String npa) {
        BaseResponse<AnggotaSimpleResponse> baseResponse = anggotaService.getDetailAnggotaByNpa(npa);
        return ok(baseResponse);
    }
}
