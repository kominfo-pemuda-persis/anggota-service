package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Cabang;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CabangRepository extends JpaRepository<Cabang, String>, QuerydslPredicateExecutor<Cabang> {
    // unremark after FE release soft delete features
    //    Page<Cabang> findByIsDeletedFalse(Pageable pageable);
    //    Optional<Cabang> findByKdAndIsDeletedFalse(String kd);
    Optional<Cabang> findBykdPc(String kd);

    Page<Cabang> findBykdPd(Pageable page, String code);

    Page<Cabang> findAll(Pageable pageable);

    Page<Cabang> findAll(Specification<Cabang> specification, Pageable pageable);
}
