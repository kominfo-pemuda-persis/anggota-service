package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPjDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.*;
import id.or.pemudapersis.kominfo.anggotaservice.request.MonografiPJRequest;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
@RequiredArgsConstructor
public class MonografiPjService {

    private final KejamiyyahanPjRepository kejamiyyahanPjRepository;

    private final MonografiPjRepository monografiPjRepository;

    private final DesaRepository desaRepository;

    private final KabupatenRepository kabupatenRepository;

    private final KecamatanRepository kecamatanRepository;

    private final ProvinsiRepository provinsiRepository;

    private final DaerahRepository daerahRepository;

    private final WilayahRepository wilayahRepository;

    private final CabangRepository cabangRepository;

    private ModelMapper modelMapper = new ModelMapper();

    {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
    }

    @Transactional
    public List<MonografiPjDto> getList(Pageable pageable) {
        List<MonografiPjDto> results = new ArrayList<>();

        Page<MonografiPj> monografiPjPage = monografiPjRepository.findByDeletedAtIsNull(pageable);

        if (monografiPjPage.isEmpty()) {
            log.info("There is no data on the database");
            return results;
        }

        results = monografiPjPage.getContent().stream()
                .map(monografiPj -> convertEntityToDto(monografiPj))
                .collect(Collectors.toList());

        return results;
    }

    @Transactional
    public String addMonografiPj(MonografiPJRequest monografiPJRequest) throws Exception {

        MonografiPj monografiPj = mappingRequestToMonografaiPJ(monografiPJRequest);
        KejamiyyahanPj kejamiyyahanPj = mappingRequestToKejamiyyahanPJ(monografiPJRequest);

        setRelationalMap(monografiPJRequest, monografiPj, kejamiyyahanPj);

        kejamiyyahanPjRepository.save(kejamiyyahanPj);

        return "sukses";
    }

    @Transactional
    public String editMonografiPj(Long monografiPjId, MonografiPJRequest monografiPJRequest) throws Exception {

        Optional<MonografiPj> existedMonografiPj = monografiPjRepository.findById(monografiPjId);
        if (!existedMonografiPj.isPresent()) {
            return null;
        }

        Optional<KejamiyyahanPj> existedKejamiyyahanPj = kejamiyyahanPjRepository.findByMonografiPj_id(monografiPjId);
        if (!existedKejamiyyahanPj.isPresent()) {
            return null;
        }

        if (null != existedKejamiyyahanPj.get().getDeletedAt()
                || null != existedMonografiPj.get().getDeletedAt()) {
            return null;
        }

        MonografiPj updatedMonografiPj = mappingRequestToMonografaiPJ(monografiPJRequest, existedMonografiPj.get());
        KejamiyyahanPj updatedKejamiyyahanPj =
                mappingRequestToKejamiyyahanPJ(monografiPJRequest, existedKejamiyyahanPj.get());
        setRelationalMap(monografiPJRequest, updatedMonografiPj, updatedKejamiyyahanPj);

        kejamiyyahanPjRepository.save(updatedKejamiyyahanPj);
        return "sukses";
    }

    @Transactional
    public String deleteMonografiPJ(Long id) {

        Optional<KejamiyyahanPj> kejamiyyahanPjOptional = kejamiyyahanPjRepository.findByMonografiPj_id(id);

        if (!kejamiyyahanPjOptional.isPresent()) {
            return null;
        }

        KejamiyyahanPj kejamiyyahanPj = kejamiyyahanPjOptional.get();

        kejamiyyahanPj.getMonografiPj().setDeletedAt(new Timestamp(System.currentTimeMillis()));
        kejamiyyahanPj.setDeletedAt(new Timestamp(System.currentTimeMillis()));

        kejamiyyahanPjRepository.save(kejamiyyahanPj);

        return "sukses";
    }

    private void setRelationalMap(
            MonografiPJRequest monografiPJRequest, MonografiPj monografiPj, KejamiyyahanPj kejamiyyahanPj)
            throws Exception {

        var cabang = cabangRepository
                .findById(monografiPJRequest.getKdPc())
                .orElseThrow(() -> new Exception("Data Cabang is not found"));
        var daerah = daerahRepository
                .findById(monografiPJRequest.getKdPd())
                .orElseThrow(() -> new Exception("Data Daerah is not found"));
        var wilayah = wilayahRepository
                .findById(monografiPJRequest.getKdPw())
                .orElseThrow(() -> new Exception("Data Wilayah is not found"));
        var desa = desaRepository
                .findById(monografiPJRequest.getDesa())
                .orElseThrow(() -> new Exception("Data Desa is not found."));
        var kecamatan = kecamatanRepository
                .findById(monografiPJRequest.getKecamatan())
                .orElseThrow(() -> new Exception("Data Kecamatan is not found."));
        var kabupaten = kabupatenRepository
                .findById(monografiPJRequest.getKabupaten())
                .orElseThrow(() -> new Exception("Data Kabupaten is not found."));
        var provinsi = provinsiRepository
                .findById(monografiPJRequest.getProvinsi())
                .orElseThrow(() -> new Exception("Data Provinsi is not found"));

        monografiPj.setDesa(desa);
        kejamiyyahanPj.setDesa(desa);

        monografiPj.setKecamatan(kecamatan);
        kejamiyyahanPj.setKecamatan(kecamatan);

        monografiPj.setKabupaten(kabupaten);
        kejamiyyahanPj.setKabupaten(kabupaten);

        monografiPj.setProvinsi(provinsi);
        kejamiyyahanPj.setProvinsi(provinsi);

        monografiPj.setKdPc(cabang);
        kejamiyyahanPj.setKdPc(cabang);

        monografiPj.setKdPd(daerah);
        kejamiyyahanPj.setKdPd(daerah);

        monografiPj.setKdPw(wilayah);
        kejamiyyahanPj.setKdPw(wilayah);

        if (null == monografiPj.getCreatedAt()) {
            monografiPj.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            monografiPj.setCreatedBy("");
            kejamiyyahanPj.setCreatedAt(new Timestamp(System.currentTimeMillis()));
            // kejamiyyahanPj.setCreatedBy("");
        } else if (null == monografiPj.getUpdatedAt()) {
            monografiPj.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
            kejamiyyahanPj.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        }

        kejamiyyahanPj.setMonografiPj(monografiPj);
    }

    private MonografiPj mappingRequestToMonografaiPJ(MonografiPJRequest monografiPJRequest) {
        MonografiPj monografiPj = modelMapper.map(monografiPJRequest, MonografiPj.class);

        return monografiPj;
    }

    private MonografiPj mappingRequestToMonografaiPJ(
            MonografiPJRequest monografiPJRequest, MonografiPj existedMonografiPj) {
        MonografiPj monografiPj = modelMapper.map(monografiPJRequest, MonografiPj.class);

        modelMapper.map(monografiPj, existedMonografiPj);

        return existedMonografiPj;
    }

    private KejamiyyahanPj mappingRequestToKejamiyyahanPJ(MonografiPJRequest monografiPJRequest) {

        KejamiyyahanPj kejamiyyahanPj = modelMapper.map(monografiPJRequest, KejamiyyahanPj.class);
        return kejamiyyahanPj;
    }

    private KejamiyyahanPj mappingRequestToKejamiyyahanPJ(
            MonografiPJRequest monografiPJRequest, KejamiyyahanPj existedKejamiyyahanPj) {

        KejamiyyahanPj kejamiyyahanPj = modelMapper.map(monografiPJRequest, KejamiyyahanPj.class);
        modelMapper.map(kejamiyyahanPj, existedKejamiyyahanPj);
        return existedKejamiyyahanPj;
    }

    private MonografiPjDto convertEntityToDto(MonografiPj monografiPj) {
        return modelMapper.map(monografiPj, MonografiPjDto.class);
    }

    public Long countMonografiPj() {
        return monografiPjRepository.countByDeletedAtIsNull();
    }
}
