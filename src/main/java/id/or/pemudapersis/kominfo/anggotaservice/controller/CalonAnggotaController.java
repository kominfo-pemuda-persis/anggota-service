package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.CalonAnggotaRequestDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.CalonAnggotaResponseDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.CalonAnggotaResponseListDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.CalonAnggotaService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * Project : anggota-service
 * User: akhinahasan
 * Email: akhinahasan@gmail.com
 * Date: 29/01/24
 * Time: 07.55
 */
@RestController
@Log4j2
@RequiredArgsConstructor
@RequestMapping(value = "/v1/api/caang")
@Tag(name = "Caang", description = "Endpoint for managing Calon Anggota (Caang)")
public class CalonAnggotaController {
    private final CalonAnggotaService calonAnggotaService;
    private static final int DEFAULT_PAGE_SIZE = 10;

    @PostMapping
    @Operation(
            summary = "Add new data Calon Anggota Pemuda Persis ",
            description = "Add new data Calon Anggota Pemuda Persis Data.",
            tags = {"Caang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CalonAnggotaResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    public ResponseEntity<BaseResponse<CalonAnggotaResponseDto>> create(
            @RequestBody @Valid CalonAnggotaRequestDto calonAnggotaRequestDto) {
        BaseResponse<CalonAnggotaResponseDto> baseResponse = calonAnggotaService.create(calonAnggotaRequestDto);
        return ok(baseResponse);
    }

    @GetMapping(value = "/{idCaang}")
    @Operation(
            summary = "Details Calon Angota By Id",
            description = "Details Calon Angota By Id.",
            tags = {"Caang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CalonAnggotaResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<CalonAnggotaResponseDto>> detailCalonAnggotaById(@PathVariable Long idCaang) {
        BaseResponse<CalonAnggotaResponseDto> baseResponse = calonAnggotaService.getDetailByIdCaang(idCaang);
        return ok(baseResponse);
    }

    @GetMapping
    @ResponseBody
    @Operation(
            summary = "Get all Calon Anggota Data",
            description = "Get all Calon Anggita Data.",
            tags = {"Caang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CalonAnggotaResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<Page<CalonAnggotaResponseListDto>>> getListCalonAnggota(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "keyword Name Calon Anggota") @RequestParam(required = false, defaultValue = "")
                    String keyword,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "createdDate")
                    String sortBy) {
        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<CalonAnggotaResponseListDto>> listBaseResponse =
                calonAnggotaService.getAllCalonAnggota(keyword, paging);
        return ok(listBaseResponse);
    }

    @DeleteMapping(value = "/{idCaang}")
    @Operation(
            summary = "Delete Data Calon Anggota ",
            description = "Delete Data Calon Anggota",
            tags = {"Caang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CalonAnggotaResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<CalonAnggotaResponseDto>> delete(@PathVariable("idCaang") Long idCaang) {
        BaseResponse<CalonAnggotaResponseDto> baseResponse = calonAnggotaService.delete(idCaang);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{idCaang}")
    @Operation(
            summary = "Edit Data Calon Anggota ",
            description = "Edit Data Calon Anggota",
            tags = {"Caang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CalonAnggotaResponseDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<CalonAnggotaResponseDto>> edit(
            @PathVariable("idCaang") Long idCaang, @RequestBody @Valid CalonAnggotaRequestDto calonAnggotaRequestDto) {
        BaseResponse<CalonAnggotaResponseDto> baseResponse = calonAnggotaService.edit(idCaang, calonAnggotaRequestDto);
        return ok(baseResponse);
    }
}
