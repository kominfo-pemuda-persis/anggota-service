package id.or.pemudapersis.kominfo.anggotaservice.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@SuperBuilder
public class SummaryAnggotaPerWilayahDto {
    private List<DetailAnggotaPerWilayah> detailAnggotaPerWilayahList;
    private Long jumlahTotal;
}
