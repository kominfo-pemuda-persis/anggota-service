package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PekerjaanDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.PekerjaanService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/anggota")
@RequiredArgsConstructor
@Tag(name = "Pekerjaan", description = "Endpoint for managing Pekerjaan")
public class PekerjaanController {
    private final PekerjaanService pekerjaanService;

    @GetMapping(value = "/{npa}/pekerjaan")
    @Operation(
            summary = "Get all Pekerjaan Anggota Persis",
            description = "Get all Pekerjaan Anggota Persis.",
            tags = {"Pekerjaan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PekerjaanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<PekerjaanDto>>> pekerjaanList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idPekerjaan")
                    String sortBy,
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<PekerjaanDto>> listBaseResponse = pekerjaanService.pekerjaanList(paging, false, npa);
        return ok(listBaseResponse);
    }

    @PostMapping(value = "/{npa}/pekerjaan")
    @Operation(
            summary = "Add new Pekerjaan Anggota Persis",
            description = "Add new Pekerjaan Anggota Persis.",
            tags = {"Pekerjaan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PekerjaanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<PekerjaanDto>> create(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Valid @RequestBody PekerjaanDto pekerjaanDto) {
        BaseResponse<PekerjaanDto> baseResponse = pekerjaanService.create(npa, pekerjaanDto);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{npa}/pekerjaan/{idPekerjaan}")
    @Operation(
            summary = "Update data Pekerjaan Anggota Persis by npa anggota",
            description = "Update data Pekerjaan Anggota Persis by npa anggota.",
            tags = {"Pekerjaan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PekerjaanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<PekerjaanDto>> update(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Pekerjaan") @PathVariable("idPekerjaan") Long idPekerjaan,
            @RequestBody PekerjaanDto pekerjaanDto) {
        BaseResponse<PekerjaanDto> baseResponse = pekerjaanService.update(npa, idPekerjaan, pekerjaanDto);
        return ok(baseResponse);
    }

    @GetMapping(value = "/{npa}/pekerjaan/delete")
    @Operation(
            summary = "Delete data Pekerjaan Anggota Persis by npa anggota",
            description = "Delete data Pekerjaan Anggota Persis by npa anggota.",
            tags = {"Pekerjaan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PekerjaanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<PekerjaanDto>>> pekerjaanListDeleted(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idPekerjaan")
                    String sortBy,
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<PekerjaanDto>> listBaseResponse = pekerjaanService.pekerjaanList(paging, true, npa);
        return ok(listBaseResponse);
    }

    @DeleteMapping(value = "/{npa}/pekerjaan/{idPekerjaan}")
    @Operation(
            summary = "Update data Pekerjaan Anggota Persis by npa anggota",
            description = "Update data Pekerjaan Anggota Persis by npa anggota.",
            tags = {"Pekerjaan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PekerjaanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<Object> delete(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Pekerjaan") @PathVariable("idPekerjaan") Long idPekerjaan) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();
        pekerjaanService.delete(npa, idPekerjaan);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put("status", HttpStatus.OK);
        response.put("data", null);
        response.put("errors", null);
        return ok(response);
    }

    @PatchMapping(value = "/{npa}/pekerjaan/{idPekerjaan}/restore")
    @Operation(
            summary = "Restore data Pekerjaan Anggota Persis by npa anggota",
            description = "Restore data Pekerjaan Anggota Persis by npa anggota.",
            tags = {"Pekerjaan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = PekerjaanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<Object> restore(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Pekerjaan") @PathVariable("idPekerjaan") Long idPekerjaan) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();
        pekerjaanService.restore(npa, idPekerjaan);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put("status", HttpStatus.OK);
        response.put("data", null);
        response.put("errors", null);
        return ok(response);
    }
}
