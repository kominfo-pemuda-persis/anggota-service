package id.or.pemudapersis.kominfo.anggotaservice.config;

import id.or.pemudapersis.kominfo.anggotaservice.filter.JwtAuthenticationFilter;
import id.or.pemudapersis.kominfo.anggotaservice.service.LoginService;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfig {

    @Value("${app.urlCors}")
    private String[] urlCors;

    @Value("${app.methodCors}")
    private String[] methodCors;

    private static final String[] PUBLIC_LINK = {
        "/actuator/**",
        "/v3/api-docs",
        "/v3/api-docs/**",
        "/api-docs/**",
        "/swagger-ui",
        "/swagger-ui/**",
        "/v1/api/auth/**",
        "/v1/api/anggota/check/**",
        "/v1/api/anggota/sendEmail",
        "/v1/api/anggota/sendEmailActivation",
        "/v1/api/anggota/npa/**",
        "/v1/api/wilayah",
        "/v1/api/cabang",
        "/v1/api/daerah",
        "/v1/api/provinsi",
        "/v1/api/kabupaten",
        "/v1/api/kecamatan",
        "/v1/api/desa",
        "/v1/api/wa/**",
        "/v1/api/caang"
    };

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowCredentials(true);
        configuration.setAllowedOrigins(List.of(this.urlCors));
        configuration.setAllowedMethods(Arrays.asList(this.methodCors));
        configuration.setAllowedHeaders(List.of("*"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
            throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public DaoAuthenticationProvider getAuthenticationProvider(LoginService loginService, PasswordEncoder encoder) {
        var authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(loginService);
        authenticationProvider.setPasswordEncoder(encoder);
        return authenticationProvider;
    }

    @Bean
    public SecurityFilterChain configure(HttpSecurity http, JwtAuthenticationFilter jwtAuthenticationFilter)
            throws Exception {
        http.authorizeHttpRequests(authorize -> authorize
                        .requestMatchers(PUBLIC_LINK)
                        .permitAll()
                        .requestMatchers("/v1/api/auth")
                        .anonymous()
                        .requestMatchers("/admin/**")
                        .hasRole("superadmin")
                        .anyRequest()
                        .authenticated())
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .cors(config -> {
                    config.configurationSource(corsConfigurationSource());
                })
                .csrf(csrf -> csrf.ignoringRequestMatchers("/v1/api/**"));
        return http.build();
    }

    @Bean
    PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }
}
