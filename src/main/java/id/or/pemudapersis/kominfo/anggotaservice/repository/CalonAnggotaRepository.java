package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.CalonAnggota;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalonAnggotaRepository extends JpaRepository<CalonAnggota, Long> {
    Page<CalonAnggota> findAllByNamaContainingIgnoreCaseAndDeletedAtIsNull(String nama, Pageable pageable);

    Optional<CalonAnggota> findByIdCaangAndDeletedAtIsNull(Long aLong);
}
