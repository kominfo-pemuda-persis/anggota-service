package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Otonom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OtonomRepository extends JpaRepository<Otonom, Long> {}
