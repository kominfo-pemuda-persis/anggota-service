package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetailAnggotaPerWilayah {
    private String namaPc;
    private String namaPw;
    private String namaPd;
    private Long jumlah;
}
