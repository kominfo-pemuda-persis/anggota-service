package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.TafiqDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.TafiqService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/anggota")
@RequiredArgsConstructor
@Tag(name = "TAFIQ", description = "Endpoint for managing TAFIQ")
public class TafiqController {
    private final TafiqService tafiqService;

    private final ModelMapper modelMapper;

    @GetMapping(value = "/{npa}/tafiq")
    @Operation(
            summary = "Get all TAFIQ by NPA Anggota Persis",
            description = "Get all TAFIQ by NPA Anggota Persis.",
            tags = {"TAFIQ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TafiqDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<TafiqDto>>> tafiqList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "idTafiq")
                    String sortBy,
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<TafiqDto>> listBaseResponse = tafiqService.tafiqList(paging, false, npa);
        return ok(listBaseResponse);
    }

    @PostMapping(value = "/{npa}/tafiq")
    @Operation(
            summary = "Add new data TAFIQ by NPA Anggota Persis",
            description = "Add new data TAFIQ by NPA Anggota Persis.",
            tags = {"TAFIQ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TafiqDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<TafiqDto>> create(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Valid @RequestBody TafiqDto tafiqDto) {
        BaseResponse<TafiqDto> baseResponse = tafiqService.create(npa, tafiqDto);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{npa}/tafiq/{idTafiq}")
    @Operation(
            summary = "Update data TAFIQ by NPA Anggota Persis",
            description = "Update data TAFIQ by NPA Anggota Persis.",
            tags = {"TAFIQ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TafiqDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<TafiqDto>> update(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Tafiq") @PathVariable("idTafiq") Long idTafiq,
            @RequestBody TafiqDto tafiqDto) {
        BaseResponse<TafiqDto> baseResponse = tafiqService.update(npa, idTafiq, tafiqDto);
        return ok(baseResponse);
    }

    @GetMapping(value = "/{npa}/tafiq/delete")
    @Operation(
            summary = "Get All Deleted data TAFIQ by NPA Anggota Persis",
            description = "Get All Deleted data TAFIQ by NPA Anggota Persis.",
            tags = {"TAFIQ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TafiqDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<TafiqDto>>> tafiqListDeleted(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "idTafiq")
                    String sortBy,
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<TafiqDto>> listBaseResponse = tafiqService.tafiqList(paging, true, npa);
        return ok(listBaseResponse);
    }

    @DeleteMapping(value = "/{npa}/tafiq/{idTafiq}")
    @Operation(
            summary = "Update data TAFIQ by NPA Anggota Persis",
            description = "Update data TAFIQ by NPA Anggota Persis.",
            tags = {"TAFIQ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TafiqDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<TafiqDto>> delete(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Tafiq") @PathVariable("idTafiq") Long idTafiq) {
        BaseResponse<TafiqDto> baseResponse = tafiqService.delete(npa, idTafiq);
        return ok(baseResponse);
    }

    @PatchMapping(value = "/{npa}/tafiq/{idTafiq}/restore")
    @Operation(
            summary = "Restore data TAFIQ by NPA Anggota Persis",
            description = "Restore data TAFIQ by NPA Anggota Persis.",
            tags = {"TAFIQ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TafiqDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<TafiqDto>> restore(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Tafiq") @PathVariable("idTafiq") Long idTafiq) {
        BaseResponse<TafiqDto> baseResponse = tafiqService.restore(npa, idTafiq);
        return ok(baseResponse);
    }
}
