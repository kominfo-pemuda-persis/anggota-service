package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.TrainingDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.TrainingService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/anggota")
@RequiredArgsConstructor
@Tag(name = "Training", description = "Endpoint for managing Training")
public class TrainingController {
    private final TrainingService trainingService;

    @GetMapping(value = "/{npa}/training")
    @ResponseBody
    @Operation(
            summary = "Get all Training by NPA Anggota Persis",
            description = "Get all Training by NPA Anggota Persis.",
            tags = {"Training"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TrainingDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<Page<TrainingDto>>> trainingList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idTraining")
                    String sortBy,
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<TrainingDto>> baseResponse = trainingService.trainingList(paging, false, npa);
        return ok(baseResponse);
    }

    @PostMapping(value = "/{npa}/training")
    @Operation(
            summary = "Add new Training by NPA Anggota Persis",
            description = "Add new Training by NPA Anggota Persis.",
            tags = {"Training"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TrainingDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<TrainingDto>> create(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Valid @RequestBody TrainingDto trainingDto) {
        BaseResponse<TrainingDto> baseResponse = trainingService.create(npa, trainingDto);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{npa}/training/{idTraining}")
    @Operation(
            summary = "Update data Training by NPA Anggota Persis",
            description = "Update data Training by NPA Anggota Persis.",
            tags = {"Training"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TrainingDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<TrainingDto>> update(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Training") @PathVariable("idTraining") Long idTraining,
            @RequestBody TrainingDto trainingDto) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();
        BaseResponse<TrainingDto> baseResponse = trainingService.update(npa, idTraining, trainingDto);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put("status", HttpStatus.OK);
        response.put("data", null);
        response.put("errors", null);
        return ok(baseResponse);
    }

    @GetMapping(value = "/{npa}/training/delete")
    @Operation(
            summary = "Get All Deleted data Training by NPA Anggota Persis",
            description = "Get All Deleted data Training by NPA Anggota Persis.",
            tags = {"Training"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TrainingDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<TrainingDto>>> trainingListDeleted(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idTraining")
                    String sortBy,
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<TrainingDto>> baseResponse = trainingService.trainingList(paging, true, npa);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{npa}/training/{idTraining}")
    @Operation(
            summary = "Delete data Training by NPA Anggota Persis",
            description = "Delete data Training by NPA Anggota Persis.",
            tags = {"Training"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TrainingDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<TrainingDto>> delete(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Training") @PathVariable("idTraining") Long idTraining) {
        BaseResponse<TrainingDto> baseResponse = trainingService.delete(npa, idTraining);
        return ok(baseResponse);
    }

    @PatchMapping(value = "/{npa}/training/{idTraining}/restore")
    @Operation(
            summary = "Restore data Training by NPA Anggota Persis",
            description = "Restore data Training by NPA Anggota Persis.",
            tags = {"Training"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = TrainingDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<TrainingDto>> restore(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Training") @PathVariable("idTraining") Long idTraining) {
        BaseResponse<TrainingDto> baseResponse = trainingService.restore(npa, idTraining);
        return ok(baseResponse);
    }
}
