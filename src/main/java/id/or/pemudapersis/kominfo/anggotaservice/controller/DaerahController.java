package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.DaerahService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/api/daerah")
@Tag(name = "Daerah", description = "Endpoint for managing Daerah")
public class DaerahController {

    private final DaerahService daerahService;

    public DaerahController(DaerahService daerahService) {
        this.daerahService = daerahService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Get all Daerah Pemuda Persis Data",
            description = "Get all Daerah Pemuda Persis Data.",
            tags = {"Daerah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DaerahRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<Page<DaerahRequest>>> getList(
            @Parameter(description = "The size of page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Kode Pw") @RequestParam(required = false) String kdPw,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based sort value") @RequestParam(required = false, defaultValue = "kdPd")
                    String sortBy) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<DaerahRequest>> listBaseResponse = daerahService.getList(kdPw, pageable);
        return ok(listBaseResponse);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Add New Daerah Pemuda Persis Data",
            description = "Add New Daerah Pemuda Persis Data.",
            tags = {"Daerah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DaerahRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<DaerahRequest>> add(
            @Parameter(description = "json that contains kdPd and namaPd") @Valid @RequestBody
                    DaerahRequest daerahRequest) {
        BaseResponse<DaerahRequest> baseResponse = daerahService.saveOne(daerahRequest);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{kdPd}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Edit Data Daerah Pemuda Persis Data",
            description = "Edit Data Daerah Pemuda Persis Data.",
            tags = {"Daerah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DaerahRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<DaerahRequest>> edit(
            @Parameter(description = "Id of data daerah") @PathVariable String kdPd,
            @Parameter(description = "json that contains kdPd and namaPd") @Valid @RequestBody
                    DaerahRequest daerahRequest) {
        BaseResponse<DaerahRequest> baseResponse = daerahService.updateOne(kdPd, daerahRequest);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Delete Data Daerah Pemuda Persis Data",
            description = "Delete Data Daerah Pemuda Persis Data.",
            tags = {"Daerah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DaerahRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<DaerahRequest>> delete(
            @Parameter(description = "Id of data daerah") @PathVariable String id) {
        BaseResponse<DaerahRequest> baseResponse = daerahService.softDelete(id);
        return ok(baseResponse);
    }
}
