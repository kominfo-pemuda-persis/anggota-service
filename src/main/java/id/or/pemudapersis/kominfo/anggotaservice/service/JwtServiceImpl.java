package id.or.pemudapersis.kominfo.anggotaservice.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import java.time.Instant;
import java.util.Date;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class JwtServiceImpl implements JwtService {

    private final JWTVerifier jwtVerifier;

    @Setter(AccessLevel.PACKAGE)
    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Setter(AccessLevel.PACKAGE)
    @Value("${app.jwtExpirationMs}")
    private Long jwtExpirationMs;

    @Override
    public String generateToken(GenerateTokenInput input) {
        return JWT.create()
                .withSubject(input.npa())
                .withExpiresAt(Date.from(Instant.now().plusMillis(jwtExpirationMs)))
                .withIssuer(input.host())
                .withClaim("permissions", input.permissions())
                .withClaim("role", input.role())
                .withClaim("npa", input.npa())
                .withClaim("pw", input.pw())
                .withClaim("pd", input.pd())
                .withClaim("pc", input.pc())
                .withClaim("nama", input.nama())
                .sign(Algorithm.HMAC256(jwtSecret));
    }

    @Override
    public String getSubject(String jwt) {
        return decodeJwt(jwt).getSubject();
    }

    private DecodedJWT decodeJwt(String jwt) {
        return jwtVerifier.verify(jwt);
    }

    @Override
    public boolean isJwtValid(String jwt, String subject) {
        var jwtNotExpired = Instant.now().isBefore(decodeJwt(jwt).getExpiresAt().toInstant());
        return subject.equals(getSubject(jwt)) && jwtNotExpired;
    }
}
