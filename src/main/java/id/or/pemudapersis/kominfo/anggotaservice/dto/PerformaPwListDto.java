package id.or.pemudapersis.kominfo.anggotaservice.dto;

import jakarta.persistence.Id;
import lombok.Data;

@Data
public class PerformaPwListDto {
    @Id
    private Long id;

    private WilayahDto wilayah;

    private ProvinsiDto provinsi;

    private String alamat;

    private String ketuaPw;

    private String noHp;
}
