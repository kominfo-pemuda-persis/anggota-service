package id.or.pemudapersis.kominfo.anggotaservice.model;

public interface UserDetailView {
    String getNpa();

    String getNama();

    String getPw();

    String getPd();

    String getPc();

    String getDesa();

    String getPj();

    String getEmail();

    String getNoTelpon();

    String getAlamat();
}
