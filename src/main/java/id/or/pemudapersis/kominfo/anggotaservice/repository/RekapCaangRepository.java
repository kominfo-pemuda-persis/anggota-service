package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.RekapCaang;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RekapCaangRepository extends JpaRepository<RekapCaang, Long> {

    Optional<RekapCaang> findByAnggotaNpa(String npa);

    Page<RekapCaang> findAllByDeletedAtIsNull(Pageable pageable);
}
