package id.or.pemudapersis.kominfo.anggotaservice.model;

public interface UserView {
    String getFoto();

    String getNpa();

    String getNama();

    String getNamaDesa();

    String getPj();

    String getPc();

    String getPd();

    String getPw();

    String getEmail();

    String getStatusAktif();
}
