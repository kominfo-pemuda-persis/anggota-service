package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import jakarta.persistence.Id;
import lombok.Data;

@Data
public class PerformaPdListDto {
    @Id
    private Long id;

    private DaerahRequest daerah;

    private WilayahDto wilayah;

    private ProvinsiDto provinsi;

    private KabupatenDto kabupaten;

    private String alamat;

    private String ketuaPd;

    private String noHp;
}
