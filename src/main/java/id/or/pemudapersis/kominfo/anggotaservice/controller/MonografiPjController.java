package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPjDto;
import id.or.pemudapersis.kominfo.anggotaservice.request.MonografiPJRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.MonografiPjService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "/v1/api/monografi/pj")
@RestController
@RequiredArgsConstructor
@Tag(name = "Monografi PJ", description = "Endpoint for managing Monografi PJ")
public class MonografiPjController {

    private final MonografiPjService monografiPjService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Get all monografi pj based on pagination",
            description = "Get all monografi pj based on pagination.",
            tags = {"Monografi PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> getList(
            @Parameter(description = "The size of page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based sort value") @RequestParam(required = false, defaultValue = "kdPd")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<MonografiPjDto> monografiPjDtos = monografiPjService.getList(pageable);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", monografiPjDtos);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Add new monografi pj based on pagination",
            description = "Add new monografi pj based on pagination.",
            tags = {"Monografi PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> addMonografiPJ(
            @Parameter(description = "Request object for adding monografi PJ data") @RequestBody
                    MonografiPJRequest monografiPJRequest)
            throws Exception {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        monografiPjService.addMonografiPj(monografiPJRequest);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PutMapping(value = "/{id}")
    @Operation(
            summary = "Edit monografi pj based on pagination",
            description = "Edit monografi pj based on pagination.",
            tags = {"Monografi PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> editMonografiPJ(
            @Parameter(description = "Id of monografi PJ data that want to edited") @PathVariable Long id,
            @Parameter(description = "Monografi PJ data request") @RequestBody MonografiPJRequest monografiPJRequest)
            throws Exception {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        monografiPjService.editMonografiPj(id, monografiPJRequest);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Do soft delete for monografi PJ data.",
            description = "Do soft delete for monografi PJ data.",
            tags = {"Monografi PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> deleteMonografiPJ(
            @Parameter(description = "Id of monografi PJ that want to delete") @PathVariable Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        monografiPjService.deleteMonografiPJ(id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }
}
