package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@SuperBuilder
public class SummaryStatusKeanggotaanDto extends SummaryStatusMaritalDto {}
