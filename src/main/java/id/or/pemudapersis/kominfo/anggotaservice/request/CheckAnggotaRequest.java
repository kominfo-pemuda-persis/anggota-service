package id.or.pemudapersis.kominfo.anggotaservice.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class CheckAnggotaRequest {

    @NotNull(message = "NPA can't be null") @Pattern(regexp = "\\d{2}.\\d{4}", message = "NPA harus dalam format xx.xxxx")
    String npa;
}
