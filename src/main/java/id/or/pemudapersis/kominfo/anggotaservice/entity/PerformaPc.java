package id.or.pemudapersis.kominfo.anggotaservice.entity;

import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@Entity
@Table(name = "t_performa_pc")
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class PerformaPc extends Question {
    @Id
    @Column(name = "kd")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pc")
    private Cabang cabang;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pd")
    private Daerah daerah;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kd_pw")
    private Wilayah wilayah;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provinsi")
    private Provinsi provinsi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kota")
    private Kabupaten kabupaten;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kecamatan")
    private Kecamatan kecamatan;

    private String alamat;

    private String ketuaPc;

    private String noHp;
}
