package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PekerjaanDto {
    private Long idPekerjaan;

    // private Anggota anggota;

    private String pekerjaan;

    private String keterangan;

    private int tahunMulai;

    private int tahunSelesai;

    private String alamat;
}
