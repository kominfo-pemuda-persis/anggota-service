package id.or.pemudapersis.kominfo.anggotaservice.service;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.List;

public record GenerateTokenInput(
        @NotBlank String npa,
        @NotNull @NotEmpty List<String> permissions,
        @NotBlank String role,
        @NotBlank String host,
        @NotNull String pd,
        @NotNull String pc,
        @NotNull String pw,
        @NotNull String nama) {

    @Override
    public String toString() {
        return "GenerateTokenInput{npa='%s', permissions=%s elements, role='%s', host='%s'}"
                .formatted(npa, permissions.size(), role, host);
    }
}
