package id.or.pemudapersis.kominfo.anggotaservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "wa")
public class WhatsAppsConfig {
    private String baseUrl;
    private String sendApi;
    private String token;
    private String imageUrl;
    private String delay;
    private String type;
    private int maxRequest;
}
