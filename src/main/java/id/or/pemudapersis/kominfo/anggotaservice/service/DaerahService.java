package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Daerah;
import id.or.pemudapersis.kominfo.anggotaservice.repository.DaerahRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.specification.SpecificationHelper;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.sql.Timestamp;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
public class DaerahService {

    private static final String KODE_DAERAH_NOT_FOUND_ERROR_CODE = "KODE_DAERAH_NOT_FOUND";
    private static final String KODE_DAERAH_NOT_FOUND_ERROR_MESSAGE = "Kode Daerah not found";

    private final DaerahRepository daerahRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public DaerahService(DaerahRepository daerahRepository, ModelMapper modelMapper) {
        this.daerahRepository = daerahRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<DaerahRequest>> getList(String kodePw, Pageable pageable) {
        final long start = System.nanoTime();

        Page<DaerahRequest> daerahPage =
                daerahRepository.findAll(bySearch(kodePw), pageable).map(this::convertToDto);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", daerahPage);
    }

    public Specification<Daerah> bySearch(String kodePw) {
        return SpecificationHelper.searchAttributeEqual("kdPw", kodePw);
    }

    private DaerahRequest convertToDto(Daerah daerah) {
        return modelMapper.map(daerah, DaerahRequest.class);
    }

    @Transactional
    public BaseResponse<DaerahRequest> saveOne(DaerahRequest daerahRequest) {
        final long start = System.nanoTime();

        Daerah daerah = Daerah.builder()
                .kdPd(daerahRequest.getKdPd())
                .namaPd(daerahRequest.getNamaPd())
                // .isDeleted(false)
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .updatedAt(new Timestamp(System.currentTimeMillis()))
                .build();
        Daerah result = daerahRepository.save(daerah);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    @Transactional
    public BaseResponse<DaerahRequest> updateOne(String id, DaerahRequest daerahRequest) {
        final long start = System.nanoTime();

        Daerah daerahFounded = daerahRepository
                .findBykdPd(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, KODE_DAERAH_NOT_FOUND_ERROR_CODE, KODE_DAERAH_NOT_FOUND_ERROR_MESSAGE));
        daerahFounded.setKdPd(daerahRequest.getKdPd());
        daerahFounded.setNamaPd(daerahRequest.getNamaPd());
        daerahFounded.setDiresmikan(daerahRequest.getDiresmikan());
        daerahFounded.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        Daerah result = daerahRepository.save(daerahFounded);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    @Transactional
    public BaseResponse<DaerahRequest> softDelete(String id) {
        final long start = System.nanoTime();
        Daerah daerahFounded = daerahRepository
                .findBykdPd(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, KODE_DAERAH_NOT_FOUND_ERROR_CODE, KODE_DAERAH_NOT_FOUND_ERROR_MESSAGE));
        // daerahFounded.setDeleted(true);
        daerahFounded.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        daerahRepository.save(daerahFounded);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public Daerah getByKodeDaerah(String kodeDaerah) {
        Optional<Daerah> optionalDaerah = daerahRepository.findBykdPd(kodeDaerah);
        if (optionalDaerah.isPresent()) return optionalDaerah.get();
        throw new BusinessException(
                HttpStatus.NOT_FOUND, KODE_DAERAH_NOT_FOUND_ERROR_CODE, KODE_DAERAH_NOT_FOUND_ERROR_MESSAGE);
    }
}
