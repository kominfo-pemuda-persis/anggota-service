package id.or.pemudapersis.kominfo.anggotaservice.service;

public interface JwtService {

    /**
     * Generate JWT.
     *
     * @param input required to generate JWT
     * @return token
     */
    String generateToken(GenerateTokenInput input);

    /**
     * Extract JWT payload of subject field
     *
     * @param jwt to be extracted
     * @return subject name
     */
    String getSubject(String jwt);

    /**
     * Validate whether a JWT is valid to given subject.
     *
     * @param jwt     to be validated
     * @param subject to match against
     * @return true if JWT is valid, otherwise false
     */
    boolean isJwtValid(String jwt, String subject);
}
