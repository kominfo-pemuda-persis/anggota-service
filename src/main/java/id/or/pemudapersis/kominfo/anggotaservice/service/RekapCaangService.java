package id.or.pemudapersis.kominfo.anggotaservice.service;

import groovy.util.logging.Slf4j;
import id.or.pemudapersis.kominfo.anggotaservice.constant.GlobalMessage;
import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.RekapCaang;
import id.or.pemudapersis.kominfo.anggotaservice.mapper.RekapCaangMapper;
import id.or.pemudapersis.kominfo.anggotaservice.repository.AnggotaRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.RekapCaangRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.time.LocalDateTime;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional
@RequiredArgsConstructor
public class RekapCaangService {

    private final RekapCaangRepository rekapCaangRepository;

    private final AnggotaRepository anggotaRepository;

    private final RekapCaangMapper rekapCaangMapper;

    public BaseResponse<Page<RekapCaangDto>> getAllData(Pageable pageable) {
        final long start = System.nanoTime();
        Page<RekapCaangDto> rekapCaang =
                rekapCaangRepository.findAllByDeletedAtIsNull(pageable).map(data -> rekapCaangMapper.toDto(data));
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, rekapCaang);
    }

    public BaseResponse<RekapCaangDto> create(RekapCaangDto dto) {
        final long start = System.nanoTime();
        RekapCaang data = rekapCaangMapper.toEntity(dto);
        Anggota anggota = anggotaRepository
                .findByNpa(dto.getNpa())
                .orElseThrow(() ->
                        new BusinessException(HttpStatus.NOT_FOUND, "ANGGOTA_NOT_FOUND", "NPA Anggota Not Found"));
        data.setAnggota(anggota);
        RekapCaang entity = rekapCaangRepository.save(data);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                ((end - start) / 1000000),
                GlobalMessage.SUCCESS.message,
                rekapCaangMapper.toDto(entity));
    }

    public BaseResponse<RekapCaangDto> delete(Long id) {
        final long start = System.nanoTime();
        RekapCaang entity = rekapCaangRepository
                .findById(id)
                .orElseThrow(() ->
                        new BusinessException(HttpStatus.NOT_FOUND, "REKAP_CAANG_NOT_FOUND", "Rekap Caang not found"));
        entity.setDeletedAt(LocalDateTime.now());
        rekapCaangRepository.save(entity);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.NO_CONTENT, ((end - start) / 1000000), "SUCCESS", null);
    }
}
