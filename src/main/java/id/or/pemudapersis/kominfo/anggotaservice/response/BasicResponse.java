package id.or.pemudapersis.kominfo.anggotaservice.response;

import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicResponse {
    private Timestamp timestamp;
    private int status;
    private Object message;

    public static BasicResponse generateResponse(int statusCode, Object msg) {
        return BasicResponse.builder()
                .timestamp(new Timestamp(System.currentTimeMillis()))
                .status(statusCode)
                .message(msg)
                .build();
    }
}
