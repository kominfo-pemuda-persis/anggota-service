package id.or.pemudapersis.kominfo.anggotaservice.service;

import com.querydsl.core.BooleanBuilder;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Cabang;
import id.or.pemudapersis.kominfo.anggotaservice.entity.QCabang;
import id.or.pemudapersis.kominfo.anggotaservice.repository.CabangRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.sql.Timestamp;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class CabangService {

    private static final String KODE_CABANG_NOT_FOUND_ERROR_CODE = "KODE_CABANG_NOT_FOUND";
    private static final String KODE_CABANG_NOT_FOUND_ERROR_MESSAGE = "Kode Cabang not found";

    private final CabangRepository cabangRepository;
    private final ModelMapper modelMapper;

    public CabangService(CabangRepository cabangRepository, ModelMapper modelMapper) {
        this.cabangRepository = cabangRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<CabangRequest>> getList(Pageable pageable, String kdPd) {
        final long start = System.nanoTime();

        QCabang query = QCabang.cabang;
        BooleanBuilder builder = new BooleanBuilder();

        if (StringUtils.isNotBlank(kdPd)) {
            builder.and(query.kdPd.equalsIgnoreCase(kdPd));
        } else {
            builder.and(query.kdPc.isNotNull());
        }

        Page<CabangRequest> cabangs =
                cabangRepository.findAll(builder.getValue(), pageable).map(this::convertToDto);
        log.info("Retrieving Data Cabang ...");

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", cabangs);
    }

    public BaseResponse<CabangRequest> create(CabangRequest request) {
        final long start = System.nanoTime();

        Cabang cabang = Cabang.builder()
                .kdPc(request.getKdPc())
                .namaPc(request.getNamaPc())
                .createdAt(new Timestamp(System.currentTimeMillis()))
                // .isDeleted(false)
                .build();
        Cabang result = cabangRepository.save(cabang);
        log.info("Create new cabang (PC) ...");

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<CabangRequest> update(String kd, CabangRequest request) {
        final long start = System.nanoTime();

        Cabang cabang = cabangRepository
                .findBykdPc(kd)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, KODE_CABANG_NOT_FOUND_ERROR_CODE, KODE_CABANG_NOT_FOUND_ERROR_MESSAGE));
        cabang.setKdPc(request.getKdPc());
        cabang.setNamaPc(request.getNamaPc());
        cabang.setUpdatedAt(new Timestamp(System.currentTimeMillis()));
        Cabang result = cabangRepository.save(cabang);
        log.info("Update {} cabang kd " + kd);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private CabangRequest convertToDto(Cabang cabang) {
        return modelMapper.map(cabang, CabangRequest.class);
    }

    public BaseResponse<CabangRequest> delete(String kd) {
        final long start = System.nanoTime();

        Cabang cabang = cabangRepository
                .findBykdPc(kd)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, KODE_CABANG_NOT_FOUND_ERROR_CODE, KODE_CABANG_NOT_FOUND_ERROR_MESSAGE));
        // cabang.setIsDeleted(true);
        cabangRepository.save(cabang);
        log.info("Deleted {} cabang kd " + kd);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public Cabang getByKodeCabang(String kodeCabang) {
        Optional<Cabang> cabangOptional = cabangRepository.findBykdPc(kodeCabang);
        if (cabangOptional.isPresent()) return cabangOptional.get();
        throw new BusinessException(
                HttpStatus.NOT_FOUND, KODE_CABANG_NOT_FOUND_ERROR_CODE, KODE_CABANG_NOT_FOUND_ERROR_MESSAGE);
    }
}
