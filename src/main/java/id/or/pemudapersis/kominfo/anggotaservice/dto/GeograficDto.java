package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
public class GeograficDto {
    private double latitude;

    private double longitude;

    private String email;

    private String alamatLengkap;

    private String alamatAlternatif;

    private String noKontak;

    private int luas;

    private String btsWilayahUtara;

    private String btsWilayahSelatan;

    private String btsWilayahTimur;

    private String btsWilayahBarat;

    private int jarakIbuKotaNegara;

    private int jarakIbuKotaProvinsi;

    private int jarakIbuKotaKabupaten;
}
