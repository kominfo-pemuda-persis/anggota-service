package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SummaryMonografiDto {
    private Long monografiPJ;
    private Long monografiPd;
    private Long monografiPw;
    private Long monografiPc;
}
