package id.or.pemudapersis.kominfo.anggotaservice.service;

import lombok.Value;

@Value
public class AuthenticateDto {
    String accessToken;
}
