package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PekerjaanDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Pekerjaan;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PekerjaanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.time.LocalDateTime;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PekerjaanService {

    private static final String PEKERJAAN_NOT_FOUND_ERROR_CODE = "PEKERJAAN_NOT_FOUND";
    private static final String PEKERJAAN_NOT_FOUND_ERROR_MESSAGE = "Pekerjaan Not Found";

    private final PekerjaanRepository pekerjaanRepository;

    private final AnggotaService anggotaService;

    private final ModelMapper modelMapper;

    public PekerjaanService(
            PekerjaanRepository pekerjaanRepository, AnggotaService anggotaService, ModelMapper modelMapper) {
        this.pekerjaanRepository = pekerjaanRepository;
        this.anggotaService = anggotaService;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<PekerjaanDto>> pekerjaanList(Pageable pageable, boolean isDeleted, String npa) {
        final long start = System.nanoTime();

        Page<PekerjaanDto> pekerjaanPage;
        if (!isDeleted)
            pekerjaanPage = pekerjaanRepository
                    .findByDeletedAtIsNullAndAnggotaNpa(npa, pageable)
                    .map(this::convertToDto);
        else
            pekerjaanPage = pekerjaanRepository
                    .findByDeletedAtIsNotNullAndAnggotaNpa(npa, pageable)
                    .map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", pekerjaanPage);
    }

    private PekerjaanDto convertToDto(Pekerjaan pekerjaan) {
        return modelMapper.map(pekerjaan, PekerjaanDto.class);
    }

    public BaseResponse<PekerjaanDto> create(String npa, PekerjaanDto pekerjaanDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(npa);

        Pekerjaan pekerjaan = Pekerjaan.builder()
                .anggota(anggota)
                .alamat(pekerjaanDto.getAlamat())
                .keterangan(pekerjaanDto.getKeterangan())
                .pekerjaan(pekerjaanDto.getPekerjaan())
                .tahunMulai(pekerjaanDto.getTahunMulai())
                .tahunSelesai(pekerjaanDto.getTahunSelesai())
                .build();
        Pekerjaan result = pekerjaanRepository.save(pekerjaan);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<PekerjaanDto> update(String npa, Long idPekerjaan, PekerjaanDto pekerjaanDto) {
        final long start = System.nanoTime();

        Pekerjaan pekerjaanExist = pekerjaanRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(npa, idPekerjaan)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PEKERJAAN_NOT_FOUND_ERROR_CODE, PEKERJAAN_NOT_FOUND_ERROR_MESSAGE));

        pekerjaanDto.setIdPekerjaan(null);
        updateEntityFromDto(pekerjaanDto, pekerjaanExist);

        Pekerjaan result = pekerjaanRepository.save(pekerjaanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<PekerjaanDto> delete(String npa, Long idPekerjaan) {
        final long start = System.nanoTime();

        Pekerjaan pekerjaanExist = pekerjaanRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(npa, idPekerjaan)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PEKERJAAN_NOT_FOUND_ERROR_CODE, PEKERJAAN_NOT_FOUND_ERROR_MESSAGE));

        pekerjaanExist.setDeletedAt(LocalDateTime.now());
        Pekerjaan result = pekerjaanRepository.save(pekerjaanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public BaseResponse<PekerjaanDto> restore(String npa, Long idPekerjaan) {
        final long start = System.nanoTime();

        Pekerjaan pekerjaanExist = pekerjaanRepository
                .findByDeletedAtIsNotNullAndAnggotaNpaAndIdPekerjaan(npa, idPekerjaan)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PEKERJAAN_NOT_FOUND_ERROR_CODE, PEKERJAAN_NOT_FOUND_ERROR_MESSAGE));
        pekerjaanExist.setDeletedAt(null);
        Pekerjaan result = pekerjaanRepository.save(pekerjaanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(PekerjaanDto from, Pekerjaan to) {
        modelMapper.map(from, to);
    }
}
