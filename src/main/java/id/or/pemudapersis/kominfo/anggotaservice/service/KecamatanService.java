package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Kecamatan;
import id.or.pemudapersis.kominfo.anggotaservice.repository.KecamatanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.specification.WilayahSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class KecamatanService {

    public KecamatanRepository kecamatanRepository;

    public Page<Kecamatan> getAllKecamatan(String keyword, Pageable page) {
        return kecamatanRepository.findAll(
                WilayahSpecification.hasKodeKecamatan(keyword).or(WilayahSpecification.hasNamaKecamatan(keyword)),
                page);
    }
}
