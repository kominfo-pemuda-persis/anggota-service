package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPd;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonografiPdRepository extends JpaRepository<MonografiPd, Long> {
    Optional<MonografiPd> findByIdAndDeletedAtIsNull(Long id);

    Page<MonografiPd> findByDeletedAtIsNull(Pageable pageable);

    Page<MonografiPd> findByDeletedAtIsNotNull(Pageable pageable);

    Optional<MonografiPd> findByDaerahKdPdAndDeletedAtIsNull(String kdPd);

    Optional<MonografiPd> findByIdAndDeletedAtIsNotNull(Long id);

    Long countByDeletedAtIsNull();
}
