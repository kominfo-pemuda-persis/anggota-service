package id.or.pemudapersis.kominfo.anggotaservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "t_caang")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class CalonAnggota {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_caang")
    private Long idCaang;

    @Column(name = "nama_lengkap")
    private String nama;

    @Column(name = "tempat_lahir")
    private String tempat;

    @Column(name = "tanggal_lahir")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate tanggalLahir;

    @Column(name = "status_merital")
    private String statusMerital;

    @Column(name = "pekerjaan")
    private String pekerjaan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pw")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Wilayah pw;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pd")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Daerah pd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pc")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Cabang pc;

    @Column(name = "pj")
    private String pj;

    private String namaPj;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "desa")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Desa desa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provinsi")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Provinsi provinsi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kota")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Kabupaten kabupaten;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kecamatan")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private Kecamatan kecamatan;

    @Column(name = "gol_darah")
    private String golDarah;

    @Email
    @NotBlank(message = "Email is mandatory")
    @Column(name = "email")
    private String email;

    @Column(name = "no_telpon")
    private String noTelpon;

    @Column(name = "no_telpon2")
    private String noTelpon2;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "foto")
    private String foto;

    @Column(name = "reg_date")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate regDate;

    @Column(name = "deleted_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deletedAt;

    @Column(name = "last_updated")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime lastUpdated;

    @Column(name = "status")
    private String status;

    @Column(name = "created_date")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pendidikan_terakhir", nullable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    @ToString.Exclude
    private MasterPendidikan jenjangPendidikan;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "calonAnggota", cascade = CascadeType.ALL)
    @ToString.Exclude
    private Set<KeluargaCaang> keluargas;

    private String tempatMaruf;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate tanggalMarufStart;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate tanggalMarufEnd;
}
