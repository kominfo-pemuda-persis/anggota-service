package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PerformaPcRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class PerformaPcService {

    private static final String PERFORMA_PC_NOT_AVAILABLE_ERROR_CODE = "30531";

    private final PerformaPcRepository performaPcRepository;
    private final ModelMapper modelMapper;
    private final HelperUtilService helperUtilService;
    private final WilayahService wilayahService;
    private final DaerahService daerahService;
    private final CabangService cabangService;

    public PerformaPcService(
            PerformaPcRepository performaPcRepository,
            ModelMapper modelMapper,
            HelperUtilService helperUtilService,
            WilayahService wilayahService,
            DaerahService daerahService,
            CabangService cabangService) {
        this.performaPcRepository = performaPcRepository;
        this.modelMapper = modelMapper;
        this.helperUtilService = helperUtilService;
        this.wilayahService = wilayahService;
        this.daerahService = daerahService;
        this.cabangService = cabangService;
    }

    private void validate(PerformaPc performaPc) {
        if (performaPc.getDeletedAt() != null)
            throw new BusinessException(
                    HttpStatus.FOUND,
                    "30533",
                    " performa pc " + performaPc.getCabang().getNamaPc() + " already exists deleted record");
        throw new BusinessException(
                HttpStatus.FOUND,
                PERFORMA_PC_NOT_AVAILABLE_ERROR_CODE,
                " performa pc " + performaPc.getCabang().getNamaPc() + " already exists");
    }

    public List<PerformaPc> performaPcList(Pageable pageable, boolean isDeleted) {
        Pageable pageReq = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());
        Page<PerformaPc> performaPcPage;
        if (!isDeleted) performaPcPage = performaPcRepository.findByDeletedAtIsNull(pageReq);
        else performaPcPage = performaPcRepository.findByDeletedAtIsNotNull(pageReq);
        return performaPcPage.getContent();
    }

    public PerformaPc create(PerformaPc performaPcRequest) {
        Optional<PerformaPc> performaPcOptional = performaPcRepository.findByCabangKdPc(
                performaPcRequest.getCabang().getKdPc());
        performaPcOptional.ifPresent(this::validate);
        Cabang cabang =
                cabangService.getByKodeCabang(performaPcRequest.getCabang().getKdPc());
        Daerah daerah =
                daerahService.getByKodeDaerah(performaPcRequest.getDaerah().getKdPd());
        Wilayah wilayah =
                wilayahService.getByKodeWilayah(performaPcRequest.getWilayah().getKdPw());
        Provinsi provinsi = helperUtilService.getProvinsiById(
                performaPcRequest.getProvinsi().getId());
        Kabupaten kabupaten = helperUtilService.getKabupatenById(
                performaPcRequest.getKabupaten().getId());
        Kecamatan kecamatan = helperUtilService.getKecamatanById(
                performaPcRequest.getKecamatan().getId());

        PerformaPc performaPc = PerformaPc.builder()
                .alamat(performaPcRequest.getAlamat())
                .cabang(cabang)
                .createdDate(LocalDateTime.now())
                .daerah(daerah)
                .ketuaPc(performaPcRequest.getKetuaPc())
                .kabupaten(kabupaten)
                .kecamatan(kecamatan)
                .lastSurvey(LocalDateTime.now())
                .noHp(performaPcRequest.getNoHp())
                .q1a(performaPcRequest.getQ1a())
                .q2a(performaPcRequest.getQ2a())
                .q3a(performaPcRequest.getQ3a())
                .q4a(performaPcRequest.getQ4a())
                .q5a(performaPcRequest.getQ5a())
                .q6a(performaPcRequest.getQ6a())
                .q1b(performaPcRequest.getQ1b())
                .q2b(performaPcRequest.getQ2b())
                .q3b(performaPcRequest.getQ3b())
                .q4b(performaPcRequest.getQ4b())
                .q5b(performaPcRequest.getQ5b())
                .q6b(performaPcRequest.getQ6b())
                .q7b(performaPcRequest.getQ7b())
                .q8b(performaPcRequest.getQ8b())
                .q9b(performaPcRequest.getQ9b())
                .q10b(performaPcRequest.getQ10b())
                .q1c(performaPcRequest.getQ1c())
                .q2c(performaPcRequest.getQ2c())
                .q3c(performaPcRequest.getQ3c())
                .q1d(performaPcRequest.getQ1d())
                .q2d(performaPcRequest.getQ2d())
                .q3d(performaPcRequest.getQ3d())
                .q4d(performaPcRequest.getQ4d())
                .q5d(performaPcRequest.getQ5d())
                .q6d(performaPcRequest.getQ6d())
                .q7d(performaPcRequest.getQ7d())
                .q8d(performaPcRequest.getQ8d())
                .q9d(performaPcRequest.getQ9d())
                .q10d(performaPcRequest.getQ10d())
                .q11d(performaPcRequest.getQ11d())
                .q12d(performaPcRequest.getQ12d())
                .q13d(performaPcRequest.getQ13d())
                .q14d(performaPcRequest.getQ14d())
                .q15d(performaPcRequest.getQ15d())
                .q16d(performaPcRequest.getQ16d())
                .q17d(performaPcRequest.getQ17d())
                .q18d(performaPcRequest.getQ18d())
                .q19d(performaPcRequest.getQ19d())
                .q20d(performaPcRequest.getQ20d())
                .q21d(performaPcRequest.getQ21d())
                .q22d(performaPcRequest.getQ22d())
                .q23d(performaPcRequest.getQ23d())
                .q24d(performaPcRequest.getQ24d())
                .q25d(performaPcRequest.getQ25d())
                .q26d(performaPcRequest.getQ26d())
                .q27d(performaPcRequest.getQ27d())
                .q28d(performaPcRequest.getQ28d())
                .q1e(performaPcRequest.getQ1e())
                .q2e(performaPcRequest.getQ2e())
                .q3e(performaPcRequest.getQ3e())
                .q4e(performaPcRequest.getQ4e())
                .q5e(performaPcRequest.getQ5e())
                .q6e(performaPcRequest.getQ6e())
                .q7e(performaPcRequest.getQ7e())
                .q8e(performaPcRequest.getQ8e())
                .q9e(performaPcRequest.getQ9e())
                .q10e(performaPcRequest.getQ10e())
                .q1f(performaPcRequest.getQ1f())
                .q2f(performaPcRequest.getQ2f())
                .q3f(performaPcRequest.getQ3f())
                .q4f(performaPcRequest.getQ4f())
                .q5f(performaPcRequest.getQ5f())
                .q6f(performaPcRequest.getQ6f())
                .q7f(performaPcRequest.getQ7f())
                .provinsi(provinsi)
                .wilayah(wilayah)
                .build();

        return performaPcRepository.save(performaPc);
    }

    public PerformaPc update(PerformaPcDto performaPcDto, Long id) {
        PerformaPc performaPc = performaPcRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PC_NOT_AVAILABLE_ERROR_CODE, " Performa pc not found"));
        String kodeCabang = Optional.ofNullable(performaPcDto.getCabang())
                .map(CabangRequest::getKdPc)
                .orElse(performaPc.getCabang().getKdPc());
        String kodeDaerah = Optional.ofNullable(performaPcDto.getDaerah())
                .map(DaerahRequest::getKdPd)
                .orElse(performaPc.getDaerah().getKdPd());
        String kodeWilayah = Optional.ofNullable(performaPcDto.getWilayah())
                .map(WilayahDto::getKdPw)
                .orElse(performaPc.getWilayah().getKdPw());
        String kodeProvinsi = Optional.ofNullable(performaPcDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .orElse(performaPc.getProvinsi().getId());
        String kodeKabupaten = Optional.ofNullable(performaPcDto.getKabupaten())
                .map(KabupatenDto::getId)
                .orElse(performaPc.getKabupaten().getId());
        String kodeKecamatan = Optional.ofNullable(performaPcDto.getKecamatan())
                .map(KecamatanDto::getId)
                .orElse(performaPc.getKecamatan().getId());

        performaPcDto.setWilayah(null);
        performaPcDto.setCabang(null);
        performaPcDto.setDaerah(null);
        performaPcDto.setProvinsi(null);
        performaPcDto.setKabupaten(null);
        performaPcDto.setKecamatan(null);
        performaPcDto.setId(null);

        updateEntityFromDto(performaPcDto, performaPc);
        Cabang cabang = cabangService.getByKodeCabang(kodeCabang);
        Daerah daerah = daerahService.getByKodeDaerah(kodeDaerah);
        Wilayah wilayah = wilayahService.getByKodeWilayah(kodeWilayah);
        Provinsi provinsi = helperUtilService.getProvinsiById(kodeProvinsi);
        Kabupaten kabupaten = helperUtilService.getKabupatenById(kodeKabupaten);
        Kecamatan kecamatan = helperUtilService.getKecamatanById(kodeKecamatan);

        performaPc.setDaerah(daerah);
        performaPc.setCabang(cabang);
        performaPc.setWilayah(wilayah);
        performaPc.setProvinsi(provinsi);
        performaPc.setKabupaten(kabupaten);
        performaPc.setKecamatan(kecamatan);
        performaPc.setUpdatedDate(LocalDateTime.now());
        return performaPcRepository.save(performaPc);
    }

    public PerformaPc delete(Long id) {
        PerformaPc performaPc = performaPcRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PC_NOT_AVAILABLE_ERROR_CODE, " Performa pc not found"));

        performaPc.setDeletedAt(LocalDateTime.now());
        performaPc.setUpdatedDate(LocalDateTime.now());
        return performaPcRepository.save(performaPc);
    }

    public PerformaPc restore(Long id) {
        PerformaPc performaPc = performaPcRepository
                .findByIdAndDeletedAtIsNotNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PC_NOT_AVAILABLE_ERROR_CODE, " Performa pc not found"));
        performaPc.setDeletedAt(null);
        performaPc.setUpdatedDate(LocalDateTime.now());
        return performaPcRepository.save(performaPc);
    }

    private void updateEntityFromDto(Object from, Object to) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(from, to);
    }

    public Long countPerformaPc() {
        return performaPcRepository.countByDeletedAtIsNull();
    }
}
