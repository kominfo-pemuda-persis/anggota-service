package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MutasiDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Mutasi;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MutasiRespository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.time.LocalDateTime;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MutasiService {
    private static final String MUTASI_NOT_FOUND = "Mutasi Not Found";
    private static final String ERROR_30931 = "30931";

    private final MutasiRespository mutasiRespository;

    private final AnggotaService anggotaService;

    private final ModelMapper modelMapper;

    public MutasiService(MutasiRespository mutasiRespository, AnggotaService anggotaService, ModelMapper modelMapper) {
        this.mutasiRespository = mutasiRespository;
        this.anggotaService = anggotaService;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<MutasiDto>> mutasiList(Pageable pageable, boolean isDeleted, String npa) {
        final long start = System.nanoTime();
        Page<MutasiDto> mutasiDtoPage = isDeleted
                ? mutasiRespository.findDeletedByAnggotaNpa(npa, pageable).map(this::convertToDto)
                : mutasiRespository.findNotDeletedByAnggotaNpa(npa, pageable).map(this::convertToDto);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", mutasiDtoPage);
    }

    private MutasiDto convertToDto(Mutasi mutasi) {
        return modelMapper.map(mutasi, MutasiDto.class);
    }

    public BaseResponse<MutasiDto> create(String npa, MutasiDto mutasiDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(npa);

        Mutasi mutasi = Mutasi.builder()
                .anggota(anggota)
                .asal(mutasiDto.getAsal())
                .tanggalMutasi(mutasiDto.getTanggalMutasi())
                .tujuan(mutasiDto.getTujuan())
                .build();
        Mutasi result = mutasiRespository.save(mutasi);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<MutasiDto> update(String npa, Long idMutasi, MutasiDto mutasiDto) {
        final long start = System.nanoTime();

        Mutasi mutasiExist = mutasiRespository
                .findNotDeletedByIdMutasiAndAnggotaNpa(npa, idMutasi)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, ERROR_30931, MUTASI_NOT_FOUND));
        mutasiDto.setIdMutasi(null);
        updateEntityFromDto(mutasiDto, mutasiExist);

        Mutasi result = mutasiRespository.save(mutasiExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<MutasiDto> delete(String npa, Long idMutasi) {
        final long start = System.nanoTime();

        Mutasi mutasiExist = mutasiRespository
                .findNotDeletedByIdMutasiAndAnggotaNpa(npa, idMutasi)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, ERROR_30931, MUTASI_NOT_FOUND));
        mutasiExist.setDeletedAt(LocalDateTime.now());
        Mutasi result = mutasiRespository.save(mutasiExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<MutasiDto> restore(String npa, Long idMutasi) {
        final long start = System.nanoTime();

        Mutasi mutasiExist = mutasiRespository
                .findDeletedByIdMutasiAndAnggotaNpa(npa, idMutasi)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, ERROR_30931, MUTASI_NOT_FOUND));
        mutasiExist.setDeletedAt(null);
        Mutasi result = mutasiRespository.save(mutasiExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(MutasiDto from, Mutasi to) {
        modelMapper.map(from, to);
    }
}
