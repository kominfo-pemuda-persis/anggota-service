package id.or.pemudapersis.kominfo.anggotaservice.dto;

import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PerformaPwDto extends QuestionDto {
    @Id
    private Long id;

    private WilayahDto wilayah;

    private ProvinsiDto provinsi;

    private String alamat;

    private String ketuaPw;

    private String noHp;
}
