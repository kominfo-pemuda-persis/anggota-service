package id.or.pemudapersis.kominfo.anggotaservice.dto;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnggotaDetailRuleDto {
    private String npa;

    private String nama;

    private String pw;

    private String pd;

    private String pc;

    private String pj;

    private String desa;

    private String email;

    private String alamat;

    private Set<RoleDto> role;
}
