package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import jakarta.persistence.Id;
import lombok.Data;

@Data
public class PerformaPcListDto {
    @Id
    private Long id;

    private CabangRequest cabang;

    private DaerahRequest daerah;

    private WilayahDto wilayah;

    private ProvinsiDto provinsi;

    private KabupatenDto kabupaten;

    private KecamatanDto kecamatan;

    private String alamat;

    private String ketuaPc;

    private String noHp;
}
