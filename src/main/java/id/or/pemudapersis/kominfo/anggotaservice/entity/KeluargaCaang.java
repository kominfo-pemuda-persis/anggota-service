package id.or.pemudapersis.kominfo.anggotaservice.entity;

import jakarta.persistence.*;
import java.util.Objects;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.proxy.HibernateProxy;

@Getter
@Setter
@Entity
@Table(name = "t_caang_keluarga")
@SuperBuilder
@NoArgsConstructor
public class KeluargaCaang extends BaseKeluarga {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKeluarga;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_caang")
    private CalonAnggota calonAnggota;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy
                ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass()
                : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy
                ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass()
                : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        KeluargaCaang that = (KeluargaCaang) o;
        return getIdKeluarga() != null && Objects.equals(getIdKeluarga(), that.getIdKeluarga());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy
                ? ((HibernateProxy) this)
                        .getHibernateLazyInitializer()
                        .getPersistentClass()
                        .hashCode()
                : getClass().hashCode();
    }
}
