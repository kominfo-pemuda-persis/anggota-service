package id.or.pemudapersis.kominfo.anggotaservice.config;

import com.midtrans.Config;
import com.midtrans.ConfigFactory;
import com.midtrans.service.MidtransSnapApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MidtransConfig {

    @Value("${midtrans.server-key}")
    private String serverKey;

    @Value("${midtrans.client-key}")
    private String clientKey;

    @Value("${midtrans.is-production}")
    private Boolean isProduction;

    @Value("${midtrans.enabled-log}")
    private Boolean enabledLog;

    @Bean
    public MidtransSnapApi getMidtransSnapApi() {
        Config config = new Config(serverKey, clientKey, isProduction);
        config.setEnabledLog(enabledLog);

        return new ConfigFactory(config).getSnapApi();
    }
}
