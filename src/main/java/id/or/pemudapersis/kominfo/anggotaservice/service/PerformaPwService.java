package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPwDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.ProvinsiDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.WilayahDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPw;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PerformaPwRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class PerformaPwService {

    // TODO: confirm error code spec
    //  overlapped with PERFORMA_PD_NOT_AVAILABLE_ERROR_CODE (see: PerformaPdService.java)
    private static final String PERFORMA_PW_NOT_AVAILABLE_ERROR_CODE = "30431";

    private final PerformaPwRepository performaPwRepository;
    private final ModelMapper modelMapper;
    private final WilayahService wilayahService;
    private final HelperUtilService helperUtilService;

    public PerformaPwService(
            PerformaPwRepository performaPwRepository,
            ModelMapper modelMapper,
            WilayahService wilayahService,
            HelperUtilService helperUtilService) {
        this.performaPwRepository = performaPwRepository;
        this.modelMapper = modelMapper;
        this.wilayahService = wilayahService;
        this.helperUtilService = helperUtilService;
    }

    public List<PerformaPw> performaPwList(Pageable pageable, boolean isDeleted) {
        Pageable pageReq = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());
        Page<PerformaPw> performaPwPage;
        if (!isDeleted) performaPwPage = performaPwRepository.findByDeletedAtIsNull(pageReq);
        else performaPwPage = performaPwRepository.findByDeletedAtIsNotNull(pageReq);
        return performaPwPage.getContent();
    }

    public PerformaPw create(PerformaPw performaPwRequest) {
        Optional<PerformaPw> performaPwOptional = performaPwRepository.findByWilayahKdPwAndDeletedAtIsNull(
                performaPwRequest.getWilayah().getKdPw());
        performaPwOptional.ifPresent(this::validate);
        Wilayah wilayah =
                wilayahService.getByKodeWilayah(performaPwRequest.getWilayah().getKdPw());
        Provinsi provinsi = helperUtilService.getProvinsiById(
                performaPwRequest.getProvinsi().getId());

        PerformaPw performaPw = PerformaPw.builder()
                .alamat(performaPwRequest.getAlamat())
                .createdDate(LocalDateTime.now())
                .ketuaPw(performaPwRequest.getKetuaPw())
                .lastSurvey(LocalDateTime.now())
                .noHp(performaPwRequest.getNoHp())
                .q1a(performaPwRequest.getQ1a())
                .q2a(performaPwRequest.getQ2a())
                .q3a(performaPwRequest.getQ3a())
                .q4a(performaPwRequest.getQ4a())
                .q5a(performaPwRequest.getQ5a())
                .q6a(performaPwRequest.getQ6a())
                .q1b(performaPwRequest.getQ1b())
                .q2b(performaPwRequest.getQ2b())
                .q3b(performaPwRequest.getQ3b())
                .q4b(performaPwRequest.getQ4b())
                .q5b(performaPwRequest.getQ5b())
                .q6b(performaPwRequest.getQ6b())
                .q7b(performaPwRequest.getQ7b())
                .q8b(performaPwRequest.getQ8b())
                .q9b(performaPwRequest.getQ9b())
                .q10b(performaPwRequest.getQ10b())
                .q1c(performaPwRequest.getQ1c())
                .q2c(performaPwRequest.getQ2c())
                .q3c(performaPwRequest.getQ3c())
                .q1d(performaPwRequest.getQ1d())
                .q2d(performaPwRequest.getQ2d())
                .q3d(performaPwRequest.getQ3d())
                .q4d(performaPwRequest.getQ4d())
                .q5d(performaPwRequest.getQ5d())
                .q6d(performaPwRequest.getQ6d())
                .q7d(performaPwRequest.getQ7d())
                .q8d(performaPwRequest.getQ8d())
                .q9d(performaPwRequest.getQ9d())
                .q10d(performaPwRequest.getQ10d())
                .q11d(performaPwRequest.getQ11d())
                .q12d(performaPwRequest.getQ12d())
                .q13d(performaPwRequest.getQ13d())
                .q14d(performaPwRequest.getQ14d())
                .q15d(performaPwRequest.getQ15d())
                .q16d(performaPwRequest.getQ16d())
                .q17d(performaPwRequest.getQ17d())
                .q18d(performaPwRequest.getQ18d())
                .q19d(performaPwRequest.getQ19d())
                .q20d(performaPwRequest.getQ20d())
                .q21d(performaPwRequest.getQ21d())
                .q22d(performaPwRequest.getQ22d())
                .q23d(performaPwRequest.getQ23d())
                .q24d(performaPwRequest.getQ24d())
                .q25d(performaPwRequest.getQ25d())
                .q26d(performaPwRequest.getQ26d())
                .q27d(performaPwRequest.getQ27d())
                .q28d(performaPwRequest.getQ28d())
                .q1e(performaPwRequest.getQ1e())
                .q2e(performaPwRequest.getQ2e())
                .q3e(performaPwRequest.getQ3e())
                .q4e(performaPwRequest.getQ4e())
                .q5e(performaPwRequest.getQ5e())
                .q6e(performaPwRequest.getQ6e())
                .q7e(performaPwRequest.getQ7e())
                .q8e(performaPwRequest.getQ8e())
                .q9e(performaPwRequest.getQ9e())
                .q10e(performaPwRequest.getQ10e())
                .q1f(performaPwRequest.getQ1f())
                .q2f(performaPwRequest.getQ2f())
                .q3f(performaPwRequest.getQ3f())
                .q4f(performaPwRequest.getQ4f())
                .q5f(performaPwRequest.getQ5f())
                .q6f(performaPwRequest.getQ6f())
                .q7f(performaPwRequest.getQ7f())
                .provinsi(provinsi)
                .wilayah(wilayah)
                .build();
        return performaPwRepository.save(performaPw);
    }

    private void validate(PerformaPw performaPw) {
        if (performaPw.getDeletedAt() != null)
            throw new BusinessException(
                    HttpStatus.FOUND,
                    "30333",
                    " performa pw " + performaPw.getWilayah().getNamaWilayah() + " already exists deleted record");
        throw new BusinessException(
                HttpStatus.FOUND,
                PERFORMA_PW_NOT_AVAILABLE_ERROR_CODE,
                " performa pw " + performaPw.getWilayah().getNamaWilayah() + " already exists");
    }

    public PerformaPw update(PerformaPwDto performaPwDto, Long id) {
        PerformaPw performaPw = performaPwRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PW_NOT_AVAILABLE_ERROR_CODE, " Performa pw not found"));
        String kodeWilayah = Optional.ofNullable(performaPwDto.getWilayah())
                .map(WilayahDto::getKdPw)
                .orElse(performaPw.getWilayah().getKdPw());
        String kodeProvinsi = Optional.ofNullable(performaPwDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .orElse(performaPw.getProvinsi().getId());

        performaPw.setWilayah(null);
        performaPw.setProvinsi(null);
        performaPwDto.setId(null);
        Wilayah wilayah = wilayahService.getByKodeWilayah(kodeWilayah);
        Provinsi provinsi = helperUtilService.getProvinsiById(kodeProvinsi);

        updateEntityFromDto(performaPwDto, performaPw);
        performaPw.setProvinsi(provinsi);
        performaPw.setWilayah(wilayah);
        performaPw.setUpdatedDate(LocalDateTime.now());
        return performaPwRepository.save(performaPw);
    }

    private void updateEntityFromDto(Object from, Object to) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(from, to);
    }

    public PerformaPw delete(Long id) {
        PerformaPw performaPw = performaPwRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PW_NOT_AVAILABLE_ERROR_CODE, " Performa pw not found"));

        performaPw.setDeletedAt(LocalDateTime.now());
        performaPw.setUpdatedDate(LocalDateTime.now());
        return performaPwRepository.save(performaPw);
    }

    public PerformaPw restore(Long id) {
        PerformaPw performaPw = performaPwRepository
                .findByIdAndDeletedAtIsNotNull(id)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "30331", " Performa pj not found"));

        performaPw.setDeletedAt(null);
        performaPw.setUpdatedDate(LocalDateTime.now());
        return performaPwRepository.save(performaPw);
    }

    public Long countPerformaPw() {
        return performaPwRepository.countByDeletedAtIsNull();
    }
}
