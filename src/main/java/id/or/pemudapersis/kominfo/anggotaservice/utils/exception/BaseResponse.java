package id.or.pemudapersis.kominfo.anggotaservice.utils.exception;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@Builder
public class BaseResponse<T> {

    private HttpStatus status;

    private long took;

    private String message;

    private T data;
}
