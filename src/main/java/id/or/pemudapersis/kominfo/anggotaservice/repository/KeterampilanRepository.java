package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Keterampilan;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeterampilanRepository extends JpaRepository<Keterampilan, Long> {
    Page<Keterampilan> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Keterampilan> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Keterampilan> findByDeletedAtIsNotNullAndAnggotaNpaAndIdKeterampilan(
            String idAnggota, Long idKeterampilan);

    Optional<Keterampilan> findByDeletedAtIsNullAndAnggotaNpaAndIdKeterampilan(String idAnggota, Long idKeterampilan);
}
