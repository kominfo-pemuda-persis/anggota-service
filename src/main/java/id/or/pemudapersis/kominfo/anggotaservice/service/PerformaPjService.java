package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PerformaPjRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class PerformaPjService {

    private static final String PERFORMA_PJ_NOT_AVAILABLE_ERROR_CODE = "30331";

    private final PerformaPjRepository performaPjRepository;
    private final CabangService cabangService;
    private final DaerahService daerahService;
    private final WilayahService wilayahService;
    private final HelperUtilService helperUtilService;
    private final ModelMapper modelMapper;

    public PerformaPjService(
            PerformaPjRepository performaPjRepository,
            CabangService cabangService,
            DaerahService daerahService,
            WilayahService wilayahService,
            HelperUtilService helperUtilService,
            ModelMapper modelMapper) {
        this.performaPjRepository = performaPjRepository;
        this.cabangService = cabangService;
        this.daerahService = daerahService;
        this.wilayahService = wilayahService;
        this.helperUtilService = helperUtilService;
        this.modelMapper = modelMapper;
    }

    public List<PerformaPj> getListPerformaPj(Pageable paging, boolean isDeleted) {
        Pageable pageReq = PageRequest.of(paging.getPageNumber(), paging.getPageSize(), paging.getSort());
        Page<PerformaPj> performaPjs;
        if (!isDeleted) performaPjs = performaPjRepository.findByDeletedAtIsNull(pageReq);
        else performaPjs = performaPjRepository.findByDeletedAtIsNotNull(pageReq);
        return performaPjs.getContent();
    }

    public PerformaPj create(PerformaPj performaPjRequest) {
        Optional<PerformaPj> performaPjOptional =
                performaPjRepository.findByKodePjAndDeletedAtIsNull(performaPjRequest.getKodePj());
        performaPjOptional.ifPresent(this::validate);
        Cabang cabang =
                cabangService.getByKodeCabang(performaPjRequest.getCabang().getKdPc());
        Daerah daerah =
                daerahService.getByKodeDaerah(performaPjRequest.getDaerah().getKdPd());
        Wilayah wilayah =
                wilayahService.getByKodeWilayah(performaPjRequest.getWilayah().getKdPw());
        Provinsi provinsi = helperUtilService.getProvinsiById(
                performaPjRequest.getProvinsi().getId());
        Kabupaten kabupaten = helperUtilService.getKabupatenById(
                performaPjRequest.getKabupaten().getId());
        Kecamatan kecamatan = helperUtilService.getKecamatanById(
                performaPjRequest.getKecamatan().getId());
        Desa desa = helperUtilService.getDesaByid(performaPjRequest.getDesa().getId());

        PerformaPj performaPj = PerformaPj.builder()
                .alamat(performaPjRequest.getAlamat())
                .cabang(cabang)
                .createdDate(LocalDateTime.now())
                .daerah(daerah)
                .desa(desa)
                .kabupaten(kabupaten)
                .kecamatan(kecamatan)
                .ketuaPj(performaPjRequest.getKetuaPj())
                .kodePj(performaPjRequest.getKodePj())
                .lastSurvey(LocalDateTime.now())
                .noHp(performaPjRequest.getNoHp())
                .namaPj(performaPjRequest.getNamaPj())
                .q1a(performaPjRequest.getQ1a())
                .q2a(performaPjRequest.getQ2a())
                .q3a(performaPjRequest.getQ3a())
                .q4a(performaPjRequest.getQ4a())
                .q5a(performaPjRequest.getQ5a())
                .q6a(performaPjRequest.getQ6a())
                .q1b(performaPjRequest.getQ1b())
                .q2b(performaPjRequest.getQ2b())
                .q3b(performaPjRequest.getQ3b())
                .q4b(performaPjRequest.getQ4b())
                .q5b(performaPjRequest.getQ5b())
                .q6b(performaPjRequest.getQ6b())
                .q7b(performaPjRequest.getQ7b())
                .q8b(performaPjRequest.getQ8b())
                .q9b(performaPjRequest.getQ9b())
                .q10b(performaPjRequest.getQ10b())
                .q1c(performaPjRequest.getQ1c())
                .q2c(performaPjRequest.getQ2c())
                .q3c(performaPjRequest.getQ3c())
                .q1d(performaPjRequest.getQ1d())
                .q2d(performaPjRequest.getQ2d())
                .q3d(performaPjRequest.getQ3d())
                .q4d(performaPjRequest.getQ4d())
                .q5d(performaPjRequest.getQ5d())
                .q6d(performaPjRequest.getQ6d())
                .q7d(performaPjRequest.getQ7d())
                .q8d(performaPjRequest.getQ8d())
                .q9d(performaPjRequest.getQ9d())
                .q10d(performaPjRequest.getQ10d())
                .q11d(performaPjRequest.getQ11d())
                .q12d(performaPjRequest.getQ12d())
                .q13d(performaPjRequest.getQ13d())
                .q14d(performaPjRequest.getQ14d())
                .q15d(performaPjRequest.getQ15d())
                .q16d(performaPjRequest.getQ16d())
                .q17d(performaPjRequest.getQ17d())
                .q18d(performaPjRequest.getQ18d())
                .q19d(performaPjRequest.getQ19d())
                .q20d(performaPjRequest.getQ20d())
                .q21d(performaPjRequest.getQ21d())
                .q22d(performaPjRequest.getQ22d())
                .q23d(performaPjRequest.getQ23d())
                .q24d(performaPjRequest.getQ24d())
                .q25d(performaPjRequest.getQ25d())
                .q26d(performaPjRequest.getQ26d())
                .q27d(performaPjRequest.getQ27d())
                .q28d(performaPjRequest.getQ28d())
                .q1e(performaPjRequest.getQ1e())
                .q2e(performaPjRequest.getQ2e())
                .q3e(performaPjRequest.getQ3e())
                .q4e(performaPjRequest.getQ4e())
                .q5e(performaPjRequest.getQ5e())
                .q6e(performaPjRequest.getQ6e())
                .q7e(performaPjRequest.getQ7e())
                .q8e(performaPjRequest.getQ8e())
                .q9e(performaPjRequest.getQ9e())
                .q10e(performaPjRequest.getQ10e())
                .q1f(performaPjRequest.getQ1f())
                .q2f(performaPjRequest.getQ2f())
                .q3f(performaPjRequest.getQ3f())
                .q4f(performaPjRequest.getQ4f())
                .q5f(performaPjRequest.getQ5f())
                .q6f(performaPjRequest.getQ6f())
                .q7f(performaPjRequest.getQ7f())
                .provinsi(provinsi)
                .wilayah(wilayah)
                .build();
        return performaPjRepository.save(performaPj);
    }

    private void validate(PerformaPj performaPj) {
        if (performaPj.getDeletedAt() != null)
            throw new BusinessException(
                    HttpStatus.FOUND,
                    "30333",
                    " performa pj " + performaPj.getNamaPj() + " already exists deleted record");
        throw new BusinessException(
                HttpStatus.FOUND,
                PERFORMA_PJ_NOT_AVAILABLE_ERROR_CODE,
                " performa pj " + performaPj.getNamaPj() + " already exists");
    }

    public PerformaPj update(PerformaPjDto performaPjDto, Long id) {

        PerformaPj performaPj = performaPjRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PJ_NOT_AVAILABLE_ERROR_CODE, " Performa pj not found"));

        String kodeCabang = Optional.ofNullable(performaPjDto.getCabang())
                .map(CabangRequest::getKdPc)
                .orElse(performaPj.getCabang().getKdPc());
        String kodeDaerah = Optional.ofNullable(performaPjDto.getDaerah())
                .map(DaerahRequest::getKdPd)
                .orElse(performaPj.getDaerah().getKdPd());
        String kodeWilayah = Optional.ofNullable(performaPjDto.getWilayah())
                .map(WilayahDto::getKdPw)
                .orElse(performaPj.getWilayah().getKdPw());
        String kodeProvinsi = Optional.ofNullable(performaPjDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .orElse(performaPj.getProvinsi().getId());
        String kodeKabupaten = Optional.ofNullable(performaPjDto.getKabupaten())
                .map(KabupatenDto::getId)
                .orElse(performaPj.getKabupaten().getId());
        String kodeKecamatan = Optional.ofNullable(performaPjDto.getKecamatan())
                .map(KecamatanDto::getId)
                .orElse(performaPj.getKecamatan().getId());
        String kodeDesa = Optional.ofNullable(performaPjDto.getDesa())
                .map(DesaDto::getId)
                .orElse(performaPj.getDesa().getId());

        performaPjDto.setWilayah(null);
        performaPjDto.setCabang(null);
        performaPjDto.setDaerah(null);
        performaPjDto.setProvinsi(null);
        performaPjDto.setKabupaten(null);
        performaPjDto.setKecamatan(null);
        performaPjDto.setDesa(null);
        performaPjDto.setId(null);

        updateEntityFromDto(performaPjDto, performaPj);
        Cabang cabang = cabangService.getByKodeCabang(kodeCabang);
        Daerah daerah = daerahService.getByKodeDaerah(kodeDaerah);
        Wilayah wilayah = wilayahService.getByKodeWilayah(kodeWilayah);
        Provinsi provinsi = helperUtilService.getProvinsiById(kodeProvinsi);
        Kabupaten kabupaten = helperUtilService.getKabupatenById(kodeKabupaten);
        Kecamatan kecamatan = helperUtilService.getKecamatanById(kodeKecamatan);
        Desa desa = helperUtilService.getDesaByid(kodeDesa);

        performaPj.setDaerah(daerah);
        performaPj.setCabang(cabang);
        performaPj.setWilayah(wilayah);
        performaPj.setProvinsi(provinsi);
        performaPj.setKabupaten(kabupaten);
        performaPj.setKecamatan(kecamatan);
        performaPj.setDesa(desa);
        performaPj.setUpdatedDate(LocalDateTime.now());
        return performaPjRepository.save(performaPj);
    }

    private void updateEntityFromDto(PerformaPjDto performaPjDto, PerformaPj performaPj) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(performaPjDto, performaPj);
    }

    public PerformaPj delete(Long id) {
        PerformaPj performaPj = performaPjRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PJ_NOT_AVAILABLE_ERROR_CODE, " Performa pj not found"));

        performaPj.setDeletedAt(LocalDateTime.now());
        performaPj.setUpdatedDate(LocalDateTime.now());
        return performaPjRepository.save(performaPj);
    }

    public PerformaPj restore(Long id) {
        PerformaPj performaPj = performaPjRepository
                .findByIdAndDeletedAtIsNotNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PJ_NOT_AVAILABLE_ERROR_CODE, " Performa pj not found"));

        performaPj.setDeletedAt(null);
        performaPj.setUpdatedDate(LocalDateTime.now());
        return performaPjRepository.save(performaPj);
    }

    public Long countPerformaPj() {
        return performaPjRepository.countByDeletedAtIsNull();
    }
}
