package id.or.pemudapersis.kominfo.anggotaservice.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.validation.ValidateString;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by IntelliJ IDEA.
 * Project : anggota-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/06/20
 * Time: 07.37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class AnggotaRequestDto {

    @NotEmpty(message = "npa is Mandatory")
    private String npa;

    private String nama;

    private String tempat;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate tanggalLahir;

    @ValidateString(acceptedValues = {"Single", "Menikah", "Duda"})
    private String statusMerital;

    private String pekerjaan;

    @Valid
    private WilayahDto pw;

    @Valid
    private DaerahRequest pd;

    @Valid
    private CabangRequest pc;

    private String pj;

    private String namaPj;

    private DesaDto desa;

    @Valid
    private ProvinsiDto provinsi;

    @Valid
    private KabupatenDto kabupaten;

    @Valid
    private KecamatanDto kecamatan;

    private String golDarah;

    @Email
    @NotBlank(message = "Email is mandatory")
    private String email;

    private String noTelpon;

    private String alamat;

    @ValidateString(
            acceptedValues = {"Biasa", "Tersiar", "Kehormatan"},
            message = "Status Aktif must {acceptedValues}")
    private String jenisKeanggotaan;

    @ValidateString(
            acceptedValues = {"Aktif", "Meninggal", "Non Aktif", "Belum Heregistrasi", "Anggota Baru"},
            message = "Status Aktif must {acceptedValues}")
    private String statusAktif;

    private String foto;

    private int idOtonom;

    @Valid
    private MasterPendidikanDto jenjangPendidikan;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate masaAktifKta;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    private String levelTafiq;
    private String statusHer;
}
