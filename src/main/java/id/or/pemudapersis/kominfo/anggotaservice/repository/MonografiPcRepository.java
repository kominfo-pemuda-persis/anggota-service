package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPc;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonografiPcRepository extends JpaRepository<MonografiPc, Long> {
    Optional<MonografiPc> findByIdAndDeletedAtIsNull(Long id);

    Optional<MonografiPc> findByIdAndDeletedAtIsNotNull(Long id);

    Optional<MonografiPc> findByCabangKdPc(String kdPc);

    Page<MonografiPc> findByDeletedAtIsNull(Pageable pageable);

    Page<MonografiPc> findByDeletedAtIsNotNull(Pageable pageable);

    Long countByDeletedAtIsNull();
}
