package id.or.pemudapersis.kominfo.anggotaservice.service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.midtrans.service.MidtransSnapApi;
import id.or.pemudapersis.kominfo.anggotaservice.dto.midtransDTO.Item;
import id.or.pemudapersis.kominfo.anggotaservice.dto.midtransDTO.TransactionDetail;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MidtransService {

    private MidtransSnapApi midtransSnapApi;

    private ObjectMapper objectMapper;

    public MidtransService(MidtransSnapApi midtransSnapApi) {
        objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        this.midtransSnapApi = midtransSnapApi;
    }

    public Map<String, Object> checkout(List<Item> midtransChargeRequest) {
        final Long startTime = System.nanoTime();
        Map<String, Object> midtransBodyRequest = new HashMap<>();

        JSONObject result = null;
        try {

            Integer sum = midtransChargeRequest.parallelStream()
                    .map(item -> (item.getQuantity() * item.getPrice()))
                    .reduce(0, Integer::sum);
            midtransBodyRequest.put("item_details", midtransChargeRequest);
            TransactionDetail.TransactionDetailBuilder transactionDetail = TransactionDetail.builder()
                    .orderId(UUID.randomUUID().toString())
                    .grossAmount("" + sum);
            midtransBodyRequest.put("transaction_details", objectMapper.convertValue(transactionDetail, Map.class));
            Map<String, Object> creditCard = new HashMap<>();
            creditCard.put("secure", true);
            midtransBodyRequest.put("credit_card", creditCard);

            result = midtransSnapApi.createTransaction(midtransBodyRequest);
        } catch (Exception midtransError) {
            log.error("Error while charging the transaction.", midtransError);
        }

        final Long stopTime = System.nanoTime();
        log.debug("checkout token generation took : {}", ((stopTime - startTime / 1000L)));
        return result.toMap();
    }
}
