package id.or.pemudapersis.kominfo.anggotaservice.config;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import java.util.Arrays;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@SecurityScheme(
        name = "Authorization",
        scheme = "bearer",
        type = SecuritySchemeType.APIKEY,
        in = SecuritySchemeIn.HEADER,
        bearerFormat = "JWT")
public class OpenApi30Config {
    @Value("${app.local}")
    private String localhost;

    @Value("${app.ip}")
    private String ip;

    @Value("${app.dev}")
    private String development;

    @Value("${app.prod}")
    private String production;

    @Value("${app.web-url}")
    private String webUrl;

    @Bean
    public OpenAPI openAPI() {

        io.swagger.v3.oas.models.servers.Server currentServer = new io.swagger.v3.oas.models.servers.Server();
        currentServer.setDescription("current");
        currentServer.setUrl("/");

        io.swagger.v3.oas.models.servers.Server localServer = new io.swagger.v3.oas.models.servers.Server();
        localServer.setDescription("local");
        localServer.setUrl(localhost);

        io.swagger.v3.oas.models.servers.Server ipDevServer = new io.swagger.v3.oas.models.servers.Server();
        ipDevServer.setDescription("ip");
        ipDevServer.setUrl(ip);

        io.swagger.v3.oas.models.servers.Server devServer = new io.swagger.v3.oas.models.servers.Server();
        devServer.setDescription("dev");
        devServer.setUrl(development);

        io.swagger.v3.oas.models.servers.Server prodServer = new io.swagger.v3.oas.models.servers.Server();
        prodServer.setDescription("production");
        prodServer.setUrl(production);

        OpenAPI openAPI = new OpenAPI();
        openAPI.info(new io.swagger.v3.oas.models.info.Info()
                .title("AnaOnline API Service")
                .description("AnaOnline API Service Open API Specification")
                .version("1.0.0")
                .contact(new io.swagger.v3.oas.models.info.Contact()
                        .name("KOMINFO PP PEMUDA PERSIS")
                        .url("https://s.id/pppemudapersis")
                        .email("pp.pemuda@persis.or.id"))
                .termsOfService("TOC")
                .license(new io.swagger.v3.oas.models.info.License()
                        .name("License")
                        .url("https://s" + ".id/hendisantika")));
        openAPI.setServers(Arrays.asList(currentServer, localServer, devServer, prodServer));

        return openAPI;
    }

    public String getWebUrl() {
        return this.webUrl;
    }

    @Bean
    GroupedOpenApi defaultGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("default")
                .packagesToScan("id.or.pemudapersis.kominfo.anggotaservice.controller")
                .build();
    }

    @Bean
    GroupedOpenApi authGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("auth")
                .pathsToMatch("/v1/api/auth/**", "/v1/api/management/user/**")
                .build();
    }

    @Bean
    GroupedOpenApi monografiGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("monografi")
                .pathsToMatch("/v1/api/monografi/**")
                .build();
    }

    @Bean
    GroupedOpenApi performaGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("performa")
                .pathsToMatch("/v1/api/performa/**")
                .build();
    }

    @Bean
    GroupedOpenApi citizenshipGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("citizenship")
                .pathsToMatch("/v1/api/provinsi", "/v1/api/kabupaten", "/v1/api/kecamatan", "/v1/api/desa")
                .build();
    }

    @Bean
    GroupedOpenApi jamiyyahGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("jamiyyah")
                .pathsToMatch("/v1/api/wilayah", "/v1/api/daerah", "/v1/api/cabang")
                .build();
    }

    @Bean
    GroupedOpenApi anggotaGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("anggota")
                .pathsToMatch("/v1/api/anggota/**")
                .build();
    }

    @Bean
    GroupedOpenApi rekapCaangGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("rekap-caang")
                .pathsToMatch("/v1/api/rekap-caang/**")
                .build();
    }

    @Bean
    GroupedOpenApi blastGroupOpenApi() {
        return GroupedOpenApi.builder()
                .group("blasting")
                .pathsToMatch("/v1/api/wa/**")
                .build();
    }
}
