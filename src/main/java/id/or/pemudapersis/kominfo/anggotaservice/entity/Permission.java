package id.or.pemudapersis.kominfo.anggotaservice.entity;

import jakarta.persistence.*;
import java.io.Serializable;
import lombok.*;

@Entity
@Table(name = "t_permission_role")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Permission implements Serializable {

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

    @Id
    @Column(name = "module")
    private String module;

    @Column(name = "permission")
    private String permission;
}
