package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Login;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

@Value
public class LoginDto {

    String idLogin;
    String password;
    List<Authority> authorities;
    Role role;

    public LoginDto(Login login) {
        idLogin = login.getIdLogin();
        password = login.getPassword();
        authorities = login.getAuthorities().stream().map(Authority::new).collect(Collectors.toList());
        role = login.getRoles().stream()
                .findFirst()
                .map(roleEntity -> new Role(roleEntity.getName()))
                .orElse(Role.empty());
    }

    public List<GrantedAuthority> grantedAuthorities() {
        return authorities.stream().map(Authority::toGrantedAuthority).collect(Collectors.toList());
    }

    String roleName() {
        return role.name();
    }

    @Value
    private class Authority {
        String permission;

        Authority(GrantedAuthority grantedAuthority) {
            permission = grantedAuthority.getAuthority();
        }

        GrantedAuthority toGrantedAuthority() {
            return new SimpleGrantedAuthority(permission);
        }
    }

    private record Role(String name) {
        static Role empty() {
            return new Role("");
        }
    }
}
