package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KeluargaDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.KeluargaService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/anggota")
@RequiredArgsConstructor
@Tag(name = "Keluarga", description = "Endpoint for managing Keluarga")
public class KeluargaController {
    private final KeluargaService keluargaService;

    private final ModelMapper modelMapper;

    @GetMapping(value = "/{npa}/keluarga")
    @Operation(
            summary = "Get all Data Keluarga Pemuda Persis Data by npa Anggota",
            description = "Get all Data Keluarga Pemuda Persis Data by npa Anggota.",
            tags = {"Keluarga"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeluargaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<KeluargaDto>>> keluargaList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idKeluarga")
                    String sortBy,
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<KeluargaDto>> baseResponse = keluargaService.keluargaList(paging, false, npa);
        return ok(baseResponse);
    }

    @PostMapping(value = "/{npa}/keluarga")
    @Operation(
            summary = "Add New Data Keluarga Pemuda Persis Data by npa Anggota",
            description = "Add New Data Keluarga Pemuda Persis Data by npa Anggota.",
            tags = {"Keluarga"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeluargaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<KeluargaDto>> create(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Valid @RequestBody KeluargaDto keluargaDto) {
        BaseResponse<KeluargaDto> baseResponse = keluargaService.create(npa, keluargaDto);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{npa}/keluarga/{idKeluarga}")
    @Operation(
            summary = "Update Data Keluarga Pemuda Persis Data by npa Anggota",
            description = "Update Data Keluarga Pemuda Persis Data by npa Anggota.",
            tags = {"Keluarga"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeluargaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<KeluargaDto>> update(
            @Parameter(description = "npa anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keluarga") @PathVariable("idKeluarga") Long idKeluarga,
            @Valid @RequestBody KeluargaDto keluargaDto) {
        BaseResponse<KeluargaDto> baseResponse = keluargaService.update(npa, idKeluarga, keluargaDto);
        return ok(baseResponse);
    }

    @GetMapping(value = "/{npa}/keluarga/delete")
    @Operation(
            summary = "Delete Data Keluarga Pemuda Persis Data by npa Anggota",
            description = "Delete Data Keluarga Pemuda Persis Data by npa Anggota.",
            tags = {"Keluarga"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeluargaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<KeluargaDto>>> keluargaListDeleted(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idKeluarga")
                    String sortBy,
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<KeluargaDto>> baseResponse = keluargaService.keluargaList(paging, true, npa);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{npa}/keluarga/{idKeluarga}")
    @Operation(
            summary = "Delete Data Keluarga Pemuda Persis Data by npa Anggota",
            description = "Delete Data Keluarga Pemuda Persis Data by npa Anggota.",
            tags = {"Keluarga"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeluargaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<KeluargaDto>> delete(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keluarga") @PathVariable("idKeluarga") Long idKeluarga) {
        BaseResponse<KeluargaDto> baseResponse = keluargaService.delete(npa, idKeluarga);
        return ok(baseResponse);
    }

    @PatchMapping(value = "/{npa}/keluarga/{idKeluarga}/restore")
    @Operation(
            summary = "Restore Delete Data Keluarga Pemuda Persis Data by npa Anggota",
            description = "Restore Delete Data Keluarga Pemuda Persis Data by npa Anggota.",
            tags = {"Keluarga"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeluargaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<KeluargaDto>> restore(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keluarga") @PathVariable("idKeluarga") Long idKeluarga) {
        BaseResponse<KeluargaDto> baseResponse = keluargaService.restore(npa, idKeluarga);
        return ok(baseResponse);
    }
}
