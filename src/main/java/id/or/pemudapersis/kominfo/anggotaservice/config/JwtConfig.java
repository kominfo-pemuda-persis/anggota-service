package id.or.pemudapersis.kominfo.anggotaservice.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.JWTVerifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JwtConfig {

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Bean
    JWTVerifier jwtVerifier() {
        return JWT.require(Algorithm.HMAC256(jwtSecret)).build();
    }
}
