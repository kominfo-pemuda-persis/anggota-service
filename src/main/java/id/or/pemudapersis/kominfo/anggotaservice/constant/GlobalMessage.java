package id.or.pemudapersis.kominfo.anggotaservice.constant;

public enum GlobalMessage {
    SUCCESS("SUCCESS", "SUCCESS"),
    ERROR("ERROR", "ERROR"),
    ANGGOTA_NOT_FOUND("ANGGOTA_NOT_FOUND", "ANGGOTA NOT FOUND"),

    CALON_ANGGOTA_NOT_FOUND("CALON_ANGGOTA_NOT_FOUND", "CALON ANGGOTA NOT FOUND"),

    NPA_ALREADY_EXIST("NPA_ALREADY_EXIST", "NPA ALREADY EXIST"),
    EMAIL_HAS_BE_SENT("EMAIL_HAS_BEEN_SENT", "Email has been sent"),
    NPA_NOT_FOUND("NPA_NOT_FOUND", "NPA NOT FOUND");

    public String code;
    public String message;

    GlobalMessage(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
