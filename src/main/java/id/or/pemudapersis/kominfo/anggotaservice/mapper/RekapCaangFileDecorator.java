package id.or.pemudapersis.kominfo.anggotaservice.mapper;

import id.or.pemudapersis.kominfo.anggotaservice.dto.RekapCaangFileDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.RekapCaangFile;
import id.or.pemudapersis.kominfo.anggotaservice.service.StorageService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

@SuppressWarnings("java:S6813")
public abstract class RekapCaangFileDecorator implements RekapCaangFileMapper {

    @Autowired
    @Qualifier("delegate") private RekapCaangFileMapper delegate;

    @Autowired
    private StorageService storageService;

    private final String FOLDER_PREFIX = "registrasi/";

    @Override
    public RekapCaangFileDto toDto(RekapCaangFile entity) {
        RekapCaangFileDto dto = delegate.toDto(entity);
        String fullPath = this.storageService.getUriEndPoint(
                FOLDER_PREFIX.concat(entity.getPathFolder()).concat("/").concat(entity.getPath()));
        dto.setFullPath(fullPath);
        return dto;
    }

    @Override
    public List<RekapCaangFileDto> toDtos(List<RekapCaangFile> list) {
        if (list == null) {
            return null;
        }

        List<RekapCaangFileDto> list1 = new ArrayList<RekapCaangFileDto>(list.size());
        for (RekapCaangFile rekapCaangFile : list) {
            list1.add(toDto(rekapCaangFile));
        }

        return list1;
    }
}
