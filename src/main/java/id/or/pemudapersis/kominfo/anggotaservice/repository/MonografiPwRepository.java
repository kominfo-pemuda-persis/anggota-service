package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPw;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonografiPwRepository extends JpaRepository<MonografiPw, Long> {
    Optional<MonografiPw> findByWilayah_KdPw(String kodeWilayah);

    Optional<MonografiPw> findByIdAndDeletedAtIsNotNull(Long id);

    Optional<MonografiPw> findByIdAndDeletedAtIsNull(Long id);

    Page<MonografiPw> findByDeletedAtIsNotNull(Pageable pageReq);

    Page<MonografiPw> findByDeletedAtIsNull(Pageable pageReq);

    Long countByDeletedAtIsNull();
}
