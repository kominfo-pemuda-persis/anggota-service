package id.or.pemudapersis.kominfo.anggotaservice.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PasswordRequestDto {
    @NotEmpty(message = "npa is Mandatory")
    private String npa;

    @Email
    @NotBlank(message = "Email is mandatory")
    private String email;

    @NotEmpty(message = "password is Mandatory")
    private String password;
}
