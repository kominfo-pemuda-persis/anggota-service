package id.or.pemudapersis.kominfo.anggotaservice.service;

import com.querydsl.core.util.CollectionUtils;
import id.or.pemudapersis.kominfo.anggotaservice.config.WhatsAppsConfig;
import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.utils.StringUtil;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

/**
 * Project: anggota-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 07.17
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
@Slf4j
public class MessageWhatsAppsService {
    public static final String IMAGE = "image";

    private static final String IMAGE_URL_IS_NOT_EMPTY_ERROR_CODE = "IMAGE_URL_IS_NOT_EMPTY";

    private final AnggotaService anggotaService;
    private final CoreApiService coreApiService;
    private final WhatsAppsConfig whatsAppsConfig;

    public MessageWhatsAppsService(
            AnggotaService anggotaService, CoreApiService coreApiService, WhatsAppsConfig whatsAppsConfig) {
        this.anggotaService = anggotaService;
        this.coreApiService = coreApiService;
        this.whatsAppsConfig = whatsAppsConfig;
    }

    public BaseResponse<MessageWaAnggotaRequest> sendBlastMessageAnggota(MessageWaAnggotaRequest request) {
        final long start = System.nanoTime();
        if (IMAGE.equalsIgnoreCase(request.getMessageType()) && StringUtils.isEmpty(request.getImageUrl())) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST, IMAGE_URL_IS_NOT_EMPTY_ERROR_CODE, IMAGE_URL_IS_NOT_EMPTY_ERROR_CODE);
        }
        List<List<String>> sendTos = anggotaService.getContactNumberAllAnggota(whatsAppsConfig.getMaxRequest());
        List<Map<String, Object>> resultList = executeSendingMessage(prepareDataBlast(request), sendTos);
        final long end = System.nanoTime();
        if (resultList.isEmpty()) {
            return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", request);
        }
        log.info("[PARTIAL_FAILED] with {}", resultList);
        return ResponseBuilder.buildResponse(
                HttpStatus.PARTIAL_CONTENT, ((end - start) / 1000000), "PARTIAL_FAILED", null);
    }

    public BaseResponse<MessageWaGeneralRequest> sendBlastMessage(MessageWaGeneralRequest request) {
        final long start = System.nanoTime();
        if (StringUtils.isEmpty(request.getSendTo()))
            throw new BusinessException(HttpStatus.BAD_REQUEST, "SEND_TO_IS_NOT_EMPTY", "SEND_TO_IS_NOT_EMPTY");
        if (IMAGE.equalsIgnoreCase(request.getMessageType()) && StringUtils.isEmpty(request.getImageUrl())) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST, IMAGE_URL_IS_NOT_EMPTY_ERROR_CODE, IMAGE_URL_IS_NOT_EMPTY_ERROR_CODE);
        }
        List<List<String>> senTos = getContactNumber(request);
        List<Map<String, Object>> resultList = executeSendingMessage(prepareDataBlast(request), senTos);
        final long end = System.nanoTime();
        if (resultList.isEmpty()) {
            return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", request);
        }
        return ResponseBuilder.buildResponse(
                HttpStatus.PARTIAL_CONTENT, ((end - start) / 1000000), "PARTIAL_FAILED", resultList);
    }

    private List<Map<String, Object>> executeSendingMessage(
            MessageWhatsAppsRequest request, List<List<String>> senTos) {
        List<Map<String, Object>> errorList = new ArrayList<>();
        List<CompletableFuture<Map<String, Object>>> futures = new ArrayList<>();
        senTos.forEach(contactList -> {
            CompletableFuture<Map<String, Object>> batchFutures = sendBatch(contactList, request);
            futures.add(batchFutures);
        });
        CompletableFuture<Void> batchFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        try {
            batchFuture.get();
            futures.forEach(future -> {
                try {
                    if (!future.get().isEmpty()) errorList.add(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    log.error("[SEND MESSAGE ERROR ASYNC] {}", e.getMessage());
                    throw new RuntimeException(e);
                }
            });
        } catch (InterruptedException | ExecutionException e) {
            log.error("[SEND MESSAGE ERROR ASYNC-2] {}", e.getMessage());
            throw new RuntimeException(e);
        }
        return errorList;
    }

    private CompletableFuture<Map<String, Object>> sendBatch(
            List<String> contactList, MessageWhatsAppsRequest request) {
        return executeAsync(contactList, request);
    }

    public CompletableFuture<Map<String, Object>> executeAsync(
            List<String> contactList, MessageWhatsAppsRequest messageWhatsAppsRequest) {
        return CompletableFuture.supplyAsync(() -> {
            Map<String, Object> error = new HashMap<>();
            messageWhatsAppsRequest.setTarget(String.join(",", contactList));
            MessageWhatsAppsResponse appsResponse = coreApiService.executeSendMessageWa(messageWhatsAppsRequest);
            if (!appsResponse.isStatus()) {
                error.put("message", appsResponse.getMessage());
                error.put("contactList", contactList);
            }
            log.info("[send blast message wa] {}", appsResponse);
            return error;
        });
    }

    private List<List<String>> getContactNumber(BaseMessageWaRequest request) {
        List<String> contacts;
        Set<String> contactNumbers = new HashSet<>();
        contacts = Arrays.stream(((MessageWaGeneralRequest) request).getSendTo().split(","))
                .map(StringUtil::getValidContactNumber)
                .filter(contact -> Objects.nonNull(contact) && contactNumbers.add(contact))
                .collect(Collectors.toList());
        if (contacts.isEmpty())
            throw new BusinessException(HttpStatus.BAD_REQUEST, "SEND_TO_IS_NOT_VALID", "SEND_TO_IS_NOT_VALID");
        return CollectionUtils.partition(contacts, whatsAppsConfig.getMaxRequest());
    }

    private MessageWhatsAppsRequest prepareDataBlast(BaseMessageWaRequest request) {
        return MessageWhatsAppsRequest.builder()
                .token(whatsAppsConfig.getToken())
                .delay(Optional.ofNullable(request.getDelay())
                        .filter(integer -> integer > 0)
                        .map(String::valueOf)
                        .orElse(whatsAppsConfig.getDelay()))
                .message(request.getMessage())
                .type(request.getMessageType())
                .url(request.getImageUrl())
                .build();
    }
}
