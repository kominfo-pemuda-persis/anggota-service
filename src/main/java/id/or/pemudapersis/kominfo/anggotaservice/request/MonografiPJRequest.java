package id.or.pemudapersis.kominfo.anggotaservice.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.sql.Time;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class MonografiPJRequest {

    private String kodePj;
    private String namaPj;
    private String kdPc;
    private String kdPd;
    private String kdPw;
    private String provinsi;
    private String kabupaten;
    private String kecamatan;
    private String desa;
    private double latitude;
    private double longitude;
    private String email;
    private String noTelpon;
    private String alamat;
    private String luas;
    private String bwTimur;
    private String bwBarat;
    private String bwSelatan;
    private String bwUtara;
    private String jarakProvinsi;
    private String jarakKabupaten;
    private String photo;
    private String ketua;
    private String sekretaris;
    private String bendahara;
    private String hariNgantor;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private Time waktuNgantor;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp musjamTerakhirMasehi;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Timestamp musjamTerakhirHijriyyah;

    private int anggotaBiasa;
    private int anggotaLuarBiasa;
    private String tidakHerReg;
    private String mutasiPersis;
    private String mutasiTempat;
    private String mengundurkanDiri;
    private String meninggalDunia;
    private String calonAnggota;
}
