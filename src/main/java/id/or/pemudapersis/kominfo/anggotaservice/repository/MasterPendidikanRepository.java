package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.MasterPendidikan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterPendidikanRepository extends JpaRepository<MasterPendidikan, String> {}
