package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Daerah;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DaerahRepository extends JpaRepository<Daerah, String> {
    // Optional<Daerah> findByIdAndIsDeletedFalse(Long id);
    // Page<Daerah> findByIsDeletedFalse(Pageable pageable);
    Page<Daerah> findAll(Pageable pageable);

    Optional<Daerah> findBykdPd(String id);

    Page<Daerah> findAll(Specification<Daerah> specification, Pageable pageable);
}
