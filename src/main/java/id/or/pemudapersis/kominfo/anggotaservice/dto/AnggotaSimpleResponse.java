package id.or.pemudapersis.kominfo.anggotaservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnggotaSimpleResponse {
    private String npa;
    private String nama;
    private String email;
    private String noTelpon;
    private String foto;
    private Pw pw;
    private Pd pd;
    private Pc pc;
    private Set<RoleDto> role;

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Builder
    public static class Pw {
        private String namaWilayah;
        private String kdPw;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Builder
    public static class Pd {
        private String namaPd;
        private String kdPd;
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Builder
    public static class Pc {
        private String namaPc;
        private String kdPc;
    }
}
