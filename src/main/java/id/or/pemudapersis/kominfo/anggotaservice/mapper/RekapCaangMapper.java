package id.or.pemudapersis.kominfo.anggotaservice.mapper;

import id.or.pemudapersis.kominfo.anggotaservice.dto.RekapCaangDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.RekapCaangFileDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.RekapCaang;
import id.or.pemudapersis.kominfo.anggotaservice.entity.RekapCaangFile;
import java.util.List;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = RekapCaangFileMapper.class)
public interface RekapCaangMapper {

    @Mapping(target = "pc", source = "anggota.pc.namaPc")
    @Mapping(target = "pd", source = "anggota.pd.namaPd")
    @Mapping(target = "pw", source = "anggota.pw.namaWilayah")
    RekapCaangDto toDto(RekapCaang entity);

    @Mapping(target = "fullPath", ignore = true)
    RekapCaangFileDto rekapCaangToRekapCaangFileDto(RekapCaangFile file);

    List<RekapCaangDto> toDtos(List<RekapCaang> list);

    @InheritInverseConfiguration
    @Mapping(target = "deletedAt", ignore = true)
    RekapCaang toEntity(RekapCaangDto dto);
}
