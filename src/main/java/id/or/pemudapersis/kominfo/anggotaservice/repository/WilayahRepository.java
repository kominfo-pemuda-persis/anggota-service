package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WilayahRepository extends JpaRepository<Wilayah, String> {
    Optional<Wilayah> findByKdPw(String id);

    Page<Wilayah> findAll(Pageable pageable);
}
