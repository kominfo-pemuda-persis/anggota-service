package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPcDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPcListDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPc;
import id.or.pemudapersis.kominfo.anggotaservice.service.PerformaPcService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/api/performa/pc")
@Tag(name = "Performa PC", description = "Endpoint for managing Performa PC")
public class PerformaPcController {
    private final PerformaPcService performaPcService;
    private final ModelMapper modelMapper;

    public PerformaPcController(PerformaPcService performaPcService, ModelMapper modelMapper) {
        this.performaPcService = performaPcService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @Operation(
            summary = "Get all performa PC Anggota Persis",
            description = "Get all performa PC Anggota Persis.",
            tags = {"Performa PC"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<Object> performaPcListDtos(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<PerformaPcListDto> performaPcs = performaPcService.performaPcList(paging, false).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", performaPcs);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Add new data performa PC Anggota Persis",
            description = "Add new data performa PC Anggota Persis.",
            tags = {"Performa PC"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> create(@RequestBody @Valid PerformaPcDto performaPcDto) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        PerformaPc performaPc = convertToEntity(performaPcDto);
        performaPcService.create(performaPc);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Edit data performa PC Anggota Persis",
            description = "Edit data performa PC Anggota Persis.",
            tags = {"Performa PC"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> edit(@RequestBody @Valid PerformaPcDto performaPcDto, @PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        performaPcService.update(performaPcDto, id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @GetMapping(value = "/delete")
    @Operation(
            summary = "Delete data performa PC Anggota Persis Deleted Data",
            description = "Delete data performa PC Anggota Persis Deleted Data.",
            tags = {"Performa PC"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<Object> performaPcListDeletedDtos(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false) Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<PerformaPcListDto> performaPcs = performaPcService.performaPcList(paging, true).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", performaPcs);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Delete data performa PC Anggota Persis",
            description = "Delete data performa PC Anggota Persis.",
            tags = {"Performa PC"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        performaPcService.delete(id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PatchMapping(value = "/{id}/restore")
    @Operation(
            summary = "Restore data performa PC Anggota Persis",
            description = "Restore data performa PC Anggota Persis.",
            tags = {"Performa PC"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> restore(@PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        performaPcService.restore(id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    private PerformaPc convertToEntity(PerformaPcDto performaPcDto) {
        return modelMapper.map(performaPcDto, PerformaPc.class);
    }

    private PerformaPcListDto convertToDto(PerformaPc performaPd) {
        return modelMapper.map(performaPd, PerformaPcListDto.class);
    }
}
