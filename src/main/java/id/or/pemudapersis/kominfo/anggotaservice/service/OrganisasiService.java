package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.OrganisasiDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Organisasi;
import id.or.pemudapersis.kominfo.anggotaservice.repository.OrganisasiRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.time.LocalDateTime;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class OrganisasiService {

    private static final String ORGANISASI_NOT_FOUND_ERROR_CODE = "ORGANISASI_NOT_FOUND";
    private static final String ORGANISASI_NOT_FOUND_ERROR_MESSAGE = "Organisasi Not Found";

    private final OrganisasiRepository organisasiRepository;

    private final AnggotaService anggotaService;

    private final ModelMapper modelMapper;

    public OrganisasiService(
            OrganisasiRepository organisasiRepository, AnggotaService anggotaService, ModelMapper modelMapper) {
        this.organisasiRepository = organisasiRepository;
        this.anggotaService = anggotaService;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<OrganisasiDto>> organisasiList(Pageable pageable, boolean isDeleted, String npa) {
        final long start = System.nanoTime();

        Page<OrganisasiDto> organisasiPage;
        if (!isDeleted)
            organisasiPage = organisasiRepository
                    .findByDeletedAtIsNullAndAnggotaNpa(npa, pageable)
                    .map(this::convertToDto);
        else
            organisasiPage = organisasiRepository
                    .findByDeletedAtIsNotNullAndAnggotaNpa(npa, pageable)
                    .map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", organisasiPage);
    }

    private OrganisasiDto convertToDto(Organisasi organisasi) {
        return modelMapper.map(organisasi, OrganisasiDto.class);
    }

    public BaseResponse<OrganisasiDto> create(String npa, OrganisasiDto organisasiDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(npa);
        Organisasi organisasi = Organisasi.builder()
                .anggota(anggota)
                .jabatan(organisasiDto.getJabatan())
                .lokasi(organisasiDto.getLokasi())
                .namaOrganisasi(organisasiDto.getNamaOrganisasi())
                .tahunMulai(organisasiDto.getTahunMulai())
                .tahunSelesai(organisasiDto.getTahunSelesai())
                .build();
        Organisasi result = organisasiRepository.save(organisasi);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<OrganisasiDto> update(String npa, Long idOrganisasi, OrganisasiDto organisasiDto) {
        final long start = System.nanoTime();

        Organisasi organisasiExist = organisasiRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(npa, idOrganisasi)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, ORGANISASI_NOT_FOUND_ERROR_CODE, ORGANISASI_NOT_FOUND_ERROR_MESSAGE));
        organisasiDto.setIdOrganisasi(null);
        updateEntityFromDto(organisasiDto, organisasiExist);

        Organisasi result = organisasiRepository.save(organisasiExist);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<OrganisasiDto> delete(String idAnggota, Long idOrganisasi) {
        final long start = System.nanoTime();

        Organisasi organisasiExist = organisasiRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(idAnggota, idOrganisasi)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, ORGANISASI_NOT_FOUND_ERROR_CODE, ORGANISASI_NOT_FOUND_ERROR_MESSAGE));

        organisasiExist.setDeletedAt(LocalDateTime.now());
        Organisasi result = organisasiRepository.save(organisasiExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public BaseResponse<OrganisasiDto> restore(String npa, Long idOrganisasi) {
        final long start = System.nanoTime();

        Organisasi organisasiExist = organisasiRepository
                .findByDeletedAtIsNotNullAndAnggotaNpaAndIdOrganisasi(npa, idOrganisasi)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, ORGANISASI_NOT_FOUND_ERROR_CODE, ORGANISASI_NOT_FOUND_ERROR_MESSAGE));

        organisasiExist.setDeletedAt(null);
        Organisasi result = organisasiRepository.save(organisasiExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(OrganisasiDto from, Organisasi to) {
        modelMapper.map(from, to);
    }
}
