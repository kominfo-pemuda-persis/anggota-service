package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Kabupaten;
import id.or.pemudapersis.kominfo.anggotaservice.repository.KabupatenRepository;
import id.or.pemudapersis.kominfo.anggotaservice.specification.WilayahSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class KabupatenService {

    private final KabupatenRepository kabupatenRepository;

    public Page<Kabupaten> getAllKabupaten(String keyword, Pageable page) {
        return kabupatenRepository.findAll(
                WilayahSpecification.hasKodeKota(keyword).or(WilayahSpecification.hasNamaKota(keyword)), page);
    }
}
