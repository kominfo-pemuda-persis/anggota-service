package id.or.pemudapersis.kominfo.anggotaservice.service.mail;

import id.or.pemudapersis.kominfo.anggotaservice.config.MailConfig;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

@Service
@RequiredArgsConstructor
public class MailService {
    private final MailConfig mailConfig;

    private final JavaMailSender javaMailSender;

    private final SpringTemplateEngine templateEngine;

    public void sendMail(MailMessageContent content) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(
                message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

        //        SimpleMailMessage message = new SimpleMailMessage();
        String from = StringUtils.isEmpty(content.getFrom()) ? mailConfig.getFrom() : content.getFrom();

        helper.setSubject(content.getSubject());
        helper.setFrom("Aktivasi AnaOnline <" + from + ">");
        helper.setTo(content.getTo());

        if (StringUtils.hasText(content.getFrom())) {
            helper.setReplyTo(content.getReplyTo());
        }

        if (StringUtils.hasText(content.getCc())) {
            helper.setCc(content.getCc());
        }

        if (StringUtils.hasText(content.getBcc())) {
            helper.setBcc(content.getBcc());
        }

        Context context = new Context();
        context.setVariables(content.getProps());

        String template;
        if (content.getType().equals("welcome")) {
            template = "welcome-template";
        } else if (content.getType().equals("activation")) {
            template = "activation-template";
        } else {
            template = "default-template";
        }
        String html = templateEngine.process(template, context);

        helper.setText(html, true);

        javaMailSender.send(message);
    }
}
