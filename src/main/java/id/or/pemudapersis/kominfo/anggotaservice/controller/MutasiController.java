package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MutasiDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.MutasiService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping("/v1/api/anggota")
@RequiredArgsConstructor
@Tag(name = "Mutasi", description = "Endpoint for managing Mutasi")
public class MutasiController {

    private final MutasiService mutasiService;

    @GetMapping("/{npa}/mutasi")
    @Operation(
            summary = "Get all Mutasi Anggota Persis",
            description = "Get all Mutasi Anggota Persis.",
            tags = {"Mutasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<MutasiDto>>> mutasiList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "idMutasi")
                    String sortBy,
            @Parameter(description = "NPA Anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<MutasiDto>> baseResponse = mutasiService.mutasiList(paging, false, npa);
        return ok(baseResponse);
    }

    @PostMapping("/{npa}/mutasi")
    @Operation(
            summary = "Add Mutasi Anggota Persis",
            description = "Add Mutasi Anggota Persis.",
            tags = {"Mutasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<MutasiDto>> create(
            @Parameter(description = "NPA Anggota") @PathVariable("npa") String npa,
            @Valid @RequestBody MutasiDto mutasiDto) {
        BaseResponse<MutasiDto> baseResponse = mutasiService.create(npa, mutasiDto);
        return ok(baseResponse);
    }

    @PutMapping("/{npa}/mutasi/{idMutasi}")
    @Operation(
            summary = "Update Mutasi Anggota Persis",
            description = "Update Mutasi Anggota Persis.",
            tags = {"Mutasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<MutasiDto>> update(
            @Parameter(description = "NPA Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Mutasi") @PathVariable("idMutasi") Long idMutasi,
            @RequestBody MutasiDto mutasiDto) {
        BaseResponse<MutasiDto> baseResponse = mutasiService.update(npa, idMutasi, mutasiDto);
        return ok(baseResponse);
    }

    @GetMapping("/{npa}/mutasi/delete")
    @Operation(
            summary = "Get all deleted data Mutasi Anggota Persis",
            description = "Get all deleted data Mutasi Anggota Persis.",
            tags = {"Mutasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<Object> mutasiListDeleted(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "idMutasi")
                    String sortBy,
            @Parameter(description = "NPA Anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<MutasiDto>> mutasiDtoList = mutasiService.mutasiList(paging, true, npa);
        return ok(mutasiDtoList);
    }

    @DeleteMapping("/{npa}/mutasi/{idMutasi}")
    @Operation(
            summary = "Deleted data Mutasi Anggota Persis by NPA",
            description = "Deleted data Mutasi Anggota Persis by NPA.",
            tags = {"Mutasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<MutasiDto>> delete(
            @Parameter(description = "NPA Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Mutasi") @PathVariable("idMutasi") Long idMutasi) {
        BaseResponse<MutasiDto> baseResponse = mutasiService.delete(npa, idMutasi);
        return ok(baseResponse);
    }

    @PatchMapping("/{npa}/mutasi/{idMutasi}/restore")
    @Operation(
            summary = "Restore data Mutasi Anggota Persis by NPA",
            description = "Restore data Mutasi Anggota Persis by NPA.",
            tags = {"Mutasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<MutasiDto>> restore(
            @Parameter(description = "NPA Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Mutasi") @PathVariable("idMutasi") Long idMutasi) {
        BaseResponse<MutasiDto> baseResponse = mutasiService.restore(npa, idMutasi);
        return ok(baseResponse);
    }
}
