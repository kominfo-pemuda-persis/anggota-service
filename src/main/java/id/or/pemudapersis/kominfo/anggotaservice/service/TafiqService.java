package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.TafiqDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Tafiq;
import id.or.pemudapersis.kominfo.anggotaservice.repository.TafiqRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.time.LocalDateTime;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class TafiqService {
    private final TafiqRepository tafiqRepository;

    private final AnggotaService anggotaService;

    private final ModelMapper modelMapper;

    public TafiqService(TafiqRepository tafiqRepository, AnggotaService anggotaService, ModelMapper modelMapper) {
        this.tafiqRepository = tafiqRepository;
        this.anggotaService = anggotaService;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<TafiqDto>> tafiqList(Pageable pageable, boolean isDeleted, String idAnggota) {
        final long start = System.nanoTime();

        Page<TafiqDto> tafiqPage;
        if (!isDeleted)
            tafiqPage = tafiqRepository
                    .findByDeletedAtIsNullAndAnggotaNpa(idAnggota, pageable)
                    .map(this::convertToDto);
        else
            tafiqPage = tafiqRepository
                    .findByDeletedAtIsNotNullAndAnggotaNpa(idAnggota, pageable)
                    .map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", tafiqPage);
    }

    private TafiqDto convertToDto(Tafiq tafiq) {
        return modelMapper.map(tafiq, TafiqDto.class);
    }

    public BaseResponse<TafiqDto> create(String idAnggota, TafiqDto tafiqDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(idAnggota);
        Tafiq tafiq = Tafiq.builder()
                .anggota(anggota)
                .lokasi(tafiqDto.getLokasi())
                .tanggalMasuk(tafiqDto.getTanggalMasuk())
                .tanggalSelesai(tafiqDto.getTanggalSelesai())
                .build();
        Tafiq result = tafiqRepository.save(tafiq);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<TafiqDto> update(String idAnggota, Long idTafiq, TafiqDto tafiqDto) {
        final long start = System.nanoTime();

        Tafiq tafiqExist = tafiqRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(idAnggota, idTafiq)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "TAFIQ_NOT_FOUND", "Tafiq Not Found"));

        tafiqDto.setIdTafiq(null);
        updateEntityFromDto(tafiqDto, tafiqExist);

        Tafiq result = tafiqRepository.save(tafiqExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    public BaseResponse<TafiqDto> delete(String idAnggota, Long idTafiq) {
        final long start = System.nanoTime();

        Tafiq tafiqExist = tafiqRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(idAnggota, idTafiq)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "TAFIQ_NOT_FOUND", "Tafiq Not Found"));

        tafiqExist.setDeletedAt(LocalDateTime.now());
        tafiqRepository.save(tafiqExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public BaseResponse<TafiqDto> restore(String idAnggota, Long idTafiq) {
        final long start = System.nanoTime();

        Tafiq tafiqExist = tafiqRepository
                .findByDeletedAtIsNotNullAndAnggotaNpaAndIdTafiq(idAnggota, idTafiq)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "TAFIQ_NOT_FOUND", "Tafiq Not Found"));

        tafiqExist.setDeletedAt(null);
        Tafiq result = tafiqRepository.save(tafiqExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", convertToDto(result));
    }

    private void updateEntityFromDto(TafiqDto from, Tafiq to) {
        modelMapper.map(from, to);
    }
}
