package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPdDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPd;
import id.or.pemudapersis.kominfo.anggotaservice.service.MonografiPdService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/api/monografi/pd")
@RequiredArgsConstructor
@Tag(name = "Monografi PD", description = "Endpoint for managing Monografi PD")
public class MonografiPdController {

    private final MonografiPdService monografiPdService;
    private final ModelMapper modelMapper;

    @GetMapping
    @ResponseBody
    @Operation(
            summary = "Get Monografi PD List with pagination",
            description = "Get Monografi PD List with pagination.",
            tags = {"Monografi PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> getListMonografiPd(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<MonografiPdDto> monografiPdDtos = monografiPdService.getList(paging, false).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", monografiPdDtos);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "add new data Monografi PD",
            description = "add new data Monografi PD.",
            tags = {"Monografi PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> create(@RequestBody @Valid MonografiPdDto monografiPdDto) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();
        MonografiPd monografiPd = convertToEntity(monografiPdDto);
        monografiPdService.create(monografiPd);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Edit data Monografi PD",
            description = "Edit data Monografi PD.",
            tags = {"Monografi PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> edit(@RequestBody @Valid MonografiPdDto monografiPdDto, @PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();
        monografiPdService.update(monografiPdDto, id);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Delete data Monografi PD",
            description = "Delete data Monografi PD.",
            tags = {"Monografi PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> delete(
            @Parameter(description = "Id of data Monografi PD") @PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();
        monografiPdService.delete(id);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PatchMapping(value = "/{id}/restore")
    @Operation(
            summary = "Restore data Monografi PD",
            description = "Restore data Monografi PD.",
            tags = {"Monografi PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> restore(@PathVariable("id") Long id) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();
        monografiPdService.restore(id);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @GetMapping(value = "/delete")
    @ResponseBody
    @Operation(
            summary = "Get Monografi PD List Deleted Dta  with pagination",
            description = "Get Monografi PD List Deleted Dta  with pagination.",
            tags = {"Monografi PD"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> getListMonografiPdDelete(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<MonografiPdDto> monografiPdDtos = monografiPdService.getList(paging, true).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", monografiPdDtos);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    private MonografiPdDto convertToDto(MonografiPd monografiPd) {
        return modelMapper.map(monografiPd, MonografiPdDto.class);
    }

    private MonografiPd convertToEntity(MonografiPdDto monografiPdDto) {
        return modelMapper.map(monografiPdDto, MonografiPd.class);
    }
}
