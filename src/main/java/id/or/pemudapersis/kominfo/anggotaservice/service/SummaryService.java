package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.security.Principal;
import java.time.LocalDate;
import java.time.Period;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@Log4j2
public class SummaryService {
    public static final String OTHER = "Other";
    private final PerformaPcService performaPcService;
    private final PerformaPdService performaPdService;
    private final PerformaPwService performaPwService;
    private final PerformaPjService performaPjService;
    private final AnggotaService anggotaService;
    private final LoginService userService;
    private final MonografiPcService monografiPcService;
    private final MonografiPdService monografiPdService;
    private final MonografiPwService monografiPwService;
    private final MonografiPjService monografiPjService;

    public SummaryService(
            PerformaPcService performaPcService,
            PerformaPdService performaPdService,
            PerformaPwService performaPwService,
            PerformaPjService performaPjService,
            AnggotaService anggotaService,
            LoginService userService,
            MonografiPcService monografiPcService,
            MonografiPdService monografiPdService,
            MonografiPwService monografiPwService,
            MonografiPjService monografiPjService) {
        this.performaPcService = performaPcService;
        this.performaPdService = performaPdService;
        this.performaPwService = performaPwService;
        this.performaPjService = performaPjService;
        this.anggotaService = anggotaService;
        this.userService = userService;
        this.monografiPcService = monografiPcService;
        this.monografiPdService = monografiPdService;
        this.monografiPwService = monografiPwService;
        this.monografiPjService = monografiPjService;
    }

    public BaseResponse<SummaryPerformaDto> summaryPerforma(Principal principal) {
        final long start = System.nanoTime();

        SummaryPerformaDto result = SummaryPerformaDto.builder()
                .performaPw(performaPwService.countPerformaPw())
                .performaPc(performaPcService.countPerformaPc())
                .performaPj(performaPjService.countPerformaPj())
                .performaPd(performaPdService.countPerformaPd())
                .jmlAnggota(anggotaService.countAnggota())
                .jmlUser(userService.countUser())
                .build();

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", result);
    }

    public BaseResponse<SummaryMonografiDto> summaryMonogarfi(Principal principal) {
        final long start = System.nanoTime();

        SummaryMonografiDto result = SummaryMonografiDto.builder()
                .monografiPd(monografiPdService.countMonografiPd())
                .monografiPw(monografiPwService.countMonografiPw())
                .monografiPc(monografiPcService.countMonografiPc())
                .monografiPJ(monografiPjService.countMonografiPj())
                .build();

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", result);
    }

    public BaseResponse<SummaryGolDarahDto> summaryGolonganDarah(Principal principal) {
        final long start = System.nanoTime();

        List<Anggota> anggotaList = anggotaService.findAllByDeletedAtIsNull();
        List<DetailGolonganDarah> golonganDarahList = anggotaList.stream()
                .collect(Collectors.groupingBy(Anggota::getGolDarah, Collectors.counting()))
                .entrySet()
                .stream()
                .map(e -> DetailGolonganDarah.builder()
                        .type(e.getKey())
                        .value(e.getValue())
                        .build())
                .collect(Collectors.toList());
        SummaryGolDarahDto result = SummaryGolDarahDto.builder()
                .golonganDarahList(golonganDarahList)
                .jumlahTotal(golonganDarahList.stream()
                        .mapToLong(DetailGolonganDarah::getValue)
                        .sum())
                .build();

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", result);
    }

    public BaseResponse<SummaryStatusMaritalDto> summaryStatusMarital(Principal principal) {
        final long start = System.nanoTime();

        List<Anggota> anggotaList = anggotaService.findAllByDeletedAtIsNull();
        List<DetailStatus> detailStatuses = anggotaList.stream()
                .collect(Collectors.groupingBy(Anggota::getStatusMerital, Collectors.counting()))
                .entrySet()
                .stream()
                .map(e -> DetailStatus.builder()
                        .status(ObjectUtils.isEmpty(e.getKey()) ? OTHER : e.getKey())
                        .jumlah(e.getValue())
                        .build())
                .collect(Collectors.toList());
        SummaryStatusMaritalDto result = SummaryStatusMaritalDto.builder()
                .detailStatusList(detailStatuses)
                .jumlahTotal(detailStatuses.stream()
                        .mapToLong(DetailStatus::getJumlah)
                        .sum())
                .build();

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", result);
    }

    public BaseResponse<SummaryStatusKeanggotaanDto> summaryStatusKeanggotaan(Principal principal) {
        final long start = System.nanoTime();

        List<Anggota> anggotaList = anggotaService.findAllByDeletedAtIsNull();
        List<DetailStatus> detailStatuses = anggotaList.stream()
                .collect(Collectors.groupingBy(Anggota::getJenisKeanggotaan, Collectors.counting()))
                .entrySet()
                .stream()
                .map(e -> DetailStatus.builder()
                        .status(ObjectUtils.isEmpty(e.getKey()) ? OTHER : e.getKey())
                        .jumlah(e.getValue())
                        .build())
                .collect(Collectors.toList());
        SummaryStatusKeanggotaanDto result = SummaryStatusKeanggotaanDto.builder()
                .detailStatusList(detailStatuses)
                .jumlahTotal(detailStatuses.stream()
                        .mapToLong(DetailStatus::getJumlah)
                        .sum())
                .build();

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", result);
    }

    public BaseResponse<SummaryStatusPendidikanDto> summaryStatusPendidikan(Principal principal) {
        final long start = System.nanoTime();

        List<Anggota> anggotaList = anggotaService.findAllByDeletedAtIsNull();
        List<DetailStatus> detailStatuses = anggotaList.stream()
                .collect(Collectors.groupingBy(
                        anggota -> anggota.getJenjangPendidikan().getPendidikan(), Collectors.counting()))
                .entrySet()
                .stream()
                .map(e -> DetailStatus.builder()
                        .status(ObjectUtils.isEmpty(e.getKey()) ? OTHER : e.getKey())
                        .jumlah(e.getValue())
                        .build())
                .collect(Collectors.toList());
        SummaryStatusPendidikanDto result = SummaryStatusPendidikanDto.builder()
                .detailStatusList(detailStatuses)
                .jumlahTotal(detailStatuses.stream()
                        .mapToLong(DetailStatus::getJumlah)
                        .sum())
                .build();

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", result);
    }

    public BaseResponse<SummaryAnggotaPerWilayahDto> summaryAnggotaPerWilayah(Principal principal) {
        final long start = System.nanoTime();

        List<Anggota> anggotaList = anggotaService.findAllByDeletedAtIsNull();
        List<DetailAnggotaPerWilayah> detailAnggotaPerWilayahList = anggotaList.stream()
                .collect(Collectors.groupingBy(
                        anggota -> DetailAnggotaPerWilayah.builder()
                                .namaPc(anggota.getPc().getNamaPc())
                                .namaPd(anggota.getPd().getNamaPd())
                                .namaPw(anggota.getPw().getNamaWilayah())
                                .build(),
                        Collectors.counting()))
                .entrySet()
                .stream()
                .map(e -> {
                    e.getKey().setJumlah(e.getValue());
                    return e.getKey();
                })
                .sorted(Comparator.comparing(DetailAnggotaPerWilayah::getNamaPw))
                .collect(Collectors.toList());

        SummaryAnggotaPerWilayahDto result = SummaryAnggotaPerWilayahDto.builder()
                .detailAnggotaPerWilayahList(detailAnggotaPerWilayahList)
                .jumlahTotal(detailAnggotaPerWilayahList.stream()
                        .mapToLong(DetailAnggotaPerWilayah::getJumlah)
                        .sum())
                .build();

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", result);
    }

    public BaseResponse<SummaryUsiaAnggotaDto> summaryUsiaAnggota(Principal principal) {
        final long start = System.nanoTime();

        TreeMap<Integer, String> ranges = new TreeMap<>();
        ranges.put(0, "0 - 19");
        ranges.put(20, "20 - 25");
        ranges.put(26, "26 - 30");
        ranges.put(31, "31 - 35");
        ranges.put(36, "35 > ");

        List<Anggota> anggotaList = anggotaService.findAllByDeletedAtIsNull();

        Map<Integer, Long> groupToCount = anggotaList.stream()
                .map(anggota -> Period.between(anggota.getTanggalLahir(), LocalDate.now())
                        .getYears())
                .collect(Collectors.groupingBy(ranges::floorKey, Collectors.counting()));
        List<DetailUsiaAnggota> detailUsiaAnggotaList = ranges.entrySet().stream()
                .map(s -> DetailUsiaAnggota.builder()
                        .rangeUsia(s.getValue())
                        .jumlah(groupToCount.getOrDefault(s.getKey(), 0L))
                        .build())
                .collect(Collectors.toList());
        SummaryUsiaAnggotaDto summaryUsiaAnggotaDto = SummaryUsiaAnggotaDto.builder()
                .detailUsiaAnggotaList(detailUsiaAnggotaList)
                .jumlahTotal(detailUsiaAnggotaList.stream()
                        .mapToLong(DetailUsiaAnggota::getJumlah)
                        .sum())
                .build();

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", summaryUsiaAnggotaDto);
    }
}
