package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.PendidikanCaang;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendidikanCaangRepository extends JpaRepository<PendidikanCaang, Long> {
    Page<PendidikanCaang> findByCalonAnggotaIdCaang(Long idCaang, Pageable pageable);

    Optional<PendidikanCaang> findByCalonAnggotaIdCaangAndIdPendidikan(Long idCaang, Long idPendidikan);
}
