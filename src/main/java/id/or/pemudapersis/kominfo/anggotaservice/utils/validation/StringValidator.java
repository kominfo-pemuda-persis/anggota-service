package id.or.pemudapersis.kominfo.anggotaservice.utils.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

public class StringValidator implements ConstraintValidator<ValidateString, String> {
    private Map<String, String> stringValue;

    @Override
    public void initialize(ValidateString annotation) {
        stringValue = new LinkedHashMap<>();
        for (String val : annotation.acceptedValues()) {
            stringValue.put(val, val);
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (value == null || value.isEmpty()) {
            return true;
        }
        return Optional.ofNullable(stringValue.get(value)).isPresent();
    }
}
