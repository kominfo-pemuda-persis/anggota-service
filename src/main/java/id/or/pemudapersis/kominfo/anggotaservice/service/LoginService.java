package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.constant.GlobalMessage;
import id.or.pemudapersis.kominfo.anggotaservice.dto.AnggotaDetailRuleDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.PasswordRequestDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.RoleDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.UserDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Login;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Role;
import id.or.pemudapersis.kominfo.anggotaservice.model.UserDetailView;
import id.or.pemudapersis.kominfo.anggotaservice.model.UserView;
import id.or.pemudapersis.kominfo.anggotaservice.repository.LoginRepository;
import id.or.pemudapersis.kominfo.anggotaservice.service.mail.MailMessageContent;
import id.or.pemudapersis.kominfo.anggotaservice.service.mail.MailService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import jakarta.mail.MessagingException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@Transactional(readOnly = true)
public class LoginService implements UserDetailsService {
    private final LoginRepository loginRepository;
    private final MailService mailService;
    private final ModelMapper modelMapper;

    public LoginService(LoginRepository loginRepository, MailService mailService, ModelMapper modelMapper) {
        this.loginRepository = loginRepository;
        this.mailService = mailService;
        this.modelMapper = modelMapper;
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String npa) {
        return loginRepository
                .findByAnggotaNpa(npa)
                .orElseThrow(
                        () -> new BusinessException(HttpStatus.CONFLICT, "30020", "Invalid NPA/Username or Password"));
    }

    public Login getUserByNpa(String npa) {
        return loginRepository
                .findByAnggotaNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "USER NOT FOUND"));
    }

    /**
     * Eagerly fetch Login entity, not to confuse with {@code getUserByNpa}
     * implementation.
     *
     * @param npa to be found
     * @return login entity aggregated object
     */
    public LoginDto findUserByNpa(String npa) {
        return loginRepository
                .findByAnggotaNpa(npa)
                .map(LoginDto::new)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "USER NOT FOUND"));
    }

    public BaseResponse<UserDetailView> findUserByNpaCustom(String npa) {
        final long start = System.nanoTime();
        Login findNpa = getUserByNpa(npa);
        Optional<UserDetailView> login = Optional.ofNullable(loginRepository
                .findByNpa(findNpa.getAnggota().getNpa())
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "USER NOT FOUND")));
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, login);
    }

    @Transactional(readOnly = true)
    public BaseResponse<AnggotaDetailRuleDto> findUserByNpaCustomJpa(String npa) {
        final long start = System.nanoTime();
        Login findNpa = getUserByNpa(npa);
        AnggotaDetailRuleDto detailRuleDto = AnggotaDetailRuleDto.builder()
                .pc(findNpa.getAnggota().getPc().getNamaPc())
                .pj(findNpa.getAnggota().getPj())
                .npa(findNpa.getAnggota().getNpa())
                .nama(findNpa.getAnggota().getNama())
                .pd(findNpa.getAnggota().getPd().getNamaPd())
                .desa(findNpa.getAnggota().getDesa().getNama())
                .pw(findNpa.getAnggota().getPw().getNamaWilayah())
                .email(findNpa.getAnggota().getEmail())
                .alamat(findNpa.getAnggota().getAlamat())
                .role(findNpa.getRoles().stream().map(this::convertToDto).collect(Collectors.toSet()))
                .build();
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, detailRuleDto);
    }

    private RoleDto convertToDto(Role role) {
        return modelMapper.map(role, RoleDto.class);
    }

    public Long countUser() {
        return loginRepository.count();
    }

    private UserDto convertToDto(Login login) {
        return new ModelMapper().map(login, UserDto.class);
    }

    public BaseResponse<Page<UserView>> getAllUser(Pageable pageable) {
        final long start = System.nanoTime();
        List<UserView> allUser = loginRepository.findAllUser(pageable);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, allUser);
    }

    @Transactional
    public BaseResponse<PasswordRequestDto> changePassword(PasswordRequestDto request) {
        final long start = System.nanoTime();
        Login login = loginRepository
                .findByAnggotaNpa(request.getNpa())
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "USER NOT FOUND"));
        String email =
                Optional.of(login).map(Login::getAnggota).map(Anggota::getEmail).orElse("");
        if (email.equalsIgnoreCase(request.getEmail())) {
            throw new BusinessException(HttpStatus.BAD_REQUEST, "EMAIL_NOT_MATCH", "Email tidak sesuai");
        }
        String hashPassword = new BCryptPasswordEncoder(10, new SecureRandom()).encode(request.getPassword());
        login.setPassword(hashPassword);
        loginRepository.save(login);
        Map<String, Object> model = new HashMap<>();
        model.put("anggota", request.getNpa());
        model.put("password", request.getPassword());
        try {
            mailService.sendMail(
                    new MailMessageContent("Change Password Akun ANA Online", email, "change password", model));
        } catch (MessagingException e) {
            log.error("[CHANGE_PASSWORD_SEND_EMAIL_ERROR] {}", e.getMessage());
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR, "EMAIL_NOT_SEND", "Email tidak terkirim");
        }

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.EMAIL_HAS_BE_SENT.message, null);
    }
}
