package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Desa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DesaRepository extends JpaRepository<Desa, String>, JpaSpecificationExecutor<Desa> {}
