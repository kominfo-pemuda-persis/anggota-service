package id.or.pemudapersis.kominfo.anggotaservice.service.mail;

import id.or.pemudapersis.kominfo.anggotaservice.utils.StringUtil;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Value;

@AllArgsConstructor
@Value(staticConstructor = "valueOf")
public class MailMessageContent {
    @NotBlank
    String subject;

    @Email
    String from;

    @Email
    String replyTo;

    @NotBlank
    @Email
    String to;

    @Email
    String cc;

    @Email
    String bcc;

    String type;

    Map<String, Object> props;

    public MailMessageContent() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("name", "Sample email name");

        subject = "Default email subject";
        from = StringUtil.convertToInternetAddress("pp.pemuda.persis@gmail.com");
        replyTo = null;
        to = StringUtil.convertToInternetAddress("pp.pemuda.persis@gmail.com");
        cc = null;
        bcc = null;
        props = model;
        type = "default";
    }

    public MailMessageContent(
            @NotBlank String subject,
            @Email String from,
            @NotBlank @Email String to,
            String type,
            Map<String, Object> props) {
        this.subject = subject;
        this.from = StringUtil.convertToInternetAddress(from);
        this.to = StringUtil.convertToInternetAddress(to);
        this.type = type;
        this.props = props;
        replyTo = null;
        cc = null;
        bcc = null;
    }

    public MailMessageContent(
            @NotBlank String subject, @NotBlank @Email String to, String type, Map<String, Object> props) {
        this.subject = subject;
        this.to = StringUtil.convertToInternetAddress(to);
        this.type = type;
        this.props = props;
        from = null;
        replyTo = null;
        cc = null;
        bcc = null;
    }
}
