package id.or.pemudapersis.kominfo.anggotaservice.mapper;

import id.or.pemudapersis.kominfo.anggotaservice.dto.ProvinsiDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import java.util.List;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProvinsiMapper {

    ProvinsiDto toDto(Provinsi provinsi);

    List<ProvinsiDto> toDtos(List<Provinsi> provinsis);

    @InheritInverseConfiguration
    Provinsi toEntity(ProvinsiDto provinsiDto);
}
