package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MonografiPjDto {

    private Long id;

    private String kodePj;

    private String namaPj;

    private CabangRequest kdPc;

    private DaerahRequest kdPd;

    private WilayahDto kdPw;

    private ProvinsiDto provinsi;

    private KabupatenDto kabupaten;

    private KecamatanDto kecamatan;

    private DesaDto desa;

    private double latitude;

    private double longitude;

    private String email;

    private String noTelpon;

    private String alamat;

    private String luas;

    private String bwTimur;

    private String bwBarat;

    private String bwSelatan;

    private String bwUtara;

    private String jarakProvinsi;

    private String jarakKabupaten;

    private String photo;
}
