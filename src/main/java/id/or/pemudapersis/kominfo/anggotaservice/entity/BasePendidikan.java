package id.or.pemudapersis.kominfo.anggotaservice.entity;

import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Getter
@Setter
@ToString
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
public abstract class BasePendidikan {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_master_pendidikan")
    @ToString.Exclude
    @NotFound(action = NotFoundAction.IGNORE)
    private MasterPendidikan jenjangPendidikan;

    private String instansi;

    private String jurusan;

    private int tahunMasuk;

    private int tahunKeluar;

    private String jenisPendidikan;
}
