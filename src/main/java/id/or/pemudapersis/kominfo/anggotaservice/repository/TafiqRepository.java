package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Tafiq;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TafiqRepository extends JpaRepository<Tafiq, Long> {
    Page<Tafiq> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Tafiq> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Tafiq> findByDeletedAtIsNotNullAndAnggotaNpaAndIdTafiq(String idAnggota, Long idTafiq);

    Optional<Tafiq> findByDeletedAtIsNullAndAnggotaNpaAndIdTafiq(String idAnggota, Long idTafiq);
}
