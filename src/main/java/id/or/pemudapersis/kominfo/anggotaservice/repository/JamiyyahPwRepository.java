package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.JamiyyahPw;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JamiyyahPwRepository extends JpaRepository<JamiyyahPw, Long> {}
