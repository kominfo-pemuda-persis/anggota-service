package id.or.pemudapersis.kominfo.anggotaservice.controller;

final class ControllerConstants {
    private ControllerConstants() {}

    static final String STATUS_RESPONSE_BODY_FIELD = "status";
    static final String ERRORS_RESPONSE_BODY_FIELD = "errors";
}
