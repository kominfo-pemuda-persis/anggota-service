package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrganisasiDto {
    private Long idOrganisasi;

    // private Anggota anggota;

    private String namaOrganisasi;

    private String jabatan;

    private int tahunMulai;

    private int tahunSelesai;

    private String lokasi;
}
