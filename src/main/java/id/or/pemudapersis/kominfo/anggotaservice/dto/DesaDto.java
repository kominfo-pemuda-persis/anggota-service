package id.or.pemudapersis.kominfo.anggotaservice.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DesaDto {
    @NotEmpty
    private String id;

    // private KecamatanDto kecamatan;

    private String nama;
}
