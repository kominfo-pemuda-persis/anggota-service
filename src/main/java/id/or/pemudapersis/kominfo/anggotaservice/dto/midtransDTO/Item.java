package id.or.pemudapersis.kominfo.anggotaservice.dto.midtransDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Item {

    private String id;
    private int price;
    private int quantity;
    private String name;
    private String brand;
    private String category;

    @JsonProperty(value = "merchant_name")
    private String merchantName;
}
