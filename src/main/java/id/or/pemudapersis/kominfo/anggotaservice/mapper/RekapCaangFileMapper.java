package id.or.pemudapersis.kominfo.anggotaservice.mapper;

import id.or.pemudapersis.kominfo.anggotaservice.dto.RekapCaangFileDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.RekapCaangFile;
import java.util.List;
import org.mapstruct.DecoratedWith;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
@DecoratedWith(RekapCaangFileDecorator.class)
public interface RekapCaangFileMapper {

    RekapCaangFileDto toDto(RekapCaangFile entity);

    List<RekapCaangFileDto> toDtos(List<RekapCaangFile> list);

    @InheritInverseConfiguration
    @Mapping(target = "rekapCaang", ignore = true)
    RekapCaangFile toEntity(RekapCaangFileDto dto);
}
