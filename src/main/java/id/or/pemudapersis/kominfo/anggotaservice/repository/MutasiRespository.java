package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Mutasi;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MutasiRespository extends JpaRepository<Mutasi, Long> {
    @Query("select m from Mutasi m where m.anggota.npa = ?1 " + "and m.deletedAt is null ")
    Page<Mutasi> findNotDeletedByAnggotaNpa(String npa, Pageable pageable);

    @Query("select m from Mutasi m where m.anggota.npa = ?1 " + "and m.deletedAt is not null ")
    Page<Mutasi> findDeletedByAnggotaNpa(String npa, Pageable pageable);

    @Query("select m from Mutasi m where m.anggota.npa = ?1 " + "and m.idMutasi = ?2 " + "and m.deletedAt is not null ")
    Optional<Mutasi> findDeletedByIdMutasiAndAnggotaNpa(String npa, Long idMutasi);

    @Query("select m from Mutasi m where m.anggota.npa = ?1 " + "and m.idMutasi = ?2 " + "and m.deletedAt is null ")
    Optional<Mutasi> findNotDeletedByIdMutasiAndAnggotaNpa(String npa, Long idMutasi);
}
