package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Login;
import id.or.pemudapersis.kominfo.anggotaservice.model.UserDetailView;
import id.or.pemudapersis.kominfo.anggotaservice.model.UserView;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepository<Login, String> {
    Optional<Login> findByAnggotaNpa(String npa);

    @Query(
            value =
                    "SELECT t_anggota.npa, t_anggota.nama_lengkap as nama, t_anggota.pw, t_anggota.pd, "
                            + "t_anggota.pc, t_anggota.desa, t_anggota.pj, t_anggota.email, t_anggota.no_telpon as noTelpon, "
                            + "t_anggota.alamat FROM t_anggota JOIN t_login ON t_anggota.npa = t_login.npa WHERE t_login.npa = ?1",
            nativeQuery = true)
    Optional<UserDetailView> findByNpa(String npa);

    @Query(
            value = "SELECT t_anggota.foto, t_anggota.npa, t_anggota.nama_lengkap as nama, "
                    + "t_desa.nama as namaDesa, t_anggota.nama_pj as pj, t_pc.nama_pc as pc, t_pd.nama_pd as pd, "
                    + "t_pw.nama_pw as pw, t_anggota.email, t_anggota.status_aktif as statusAktif "
                    + "FROM t_anggota JOIN t_login ON t_anggota.npa = t_login.npa "
                    + "JOIN t_pw ON t_anggota.pw = t_pw.kd_pw "
                    + "JOIN t_pd ON t_anggota.pd = t_pd.kd_pd "
                    + "JOIN t_pc ON t_anggota.pc = t_pc.kd_pc "
                    + "JOIN t_desa ON t_anggota.desa = t_desa.id",
            nativeQuery = true)
    List<UserView> findAllUser(Pageable pageable);
}
