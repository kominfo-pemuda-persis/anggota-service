package id.or.pemudapersis.kominfo.anggotaservice.specification;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Desa;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Kabupaten;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Kecamatan;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import java.text.MessageFormat;
import org.springframework.data.jpa.domain.Specification;

public final class WilayahSpecification {

    private static final String FORMAT = "%{0}%";

    private WilayahSpecification() {
        // no-ops
    }

    public static Specification<Provinsi> hasKodeProvinsi(String kodeProvinsi) {
        return (pc, cq, cb) -> cb.like(pc.get("id"), MessageFormat.format(FORMAT, kodeProvinsi));
    }

    public static Specification<Provinsi> hasNamaProvinsi(String namaProvinsi) {
        return (pc, cq, cb) -> cb.like(pc.get("namaProvinsi"), MessageFormat.format(FORMAT, namaProvinsi));
    }

    public static Specification<Kabupaten> hasProvince(String idProvince) {
        return (pc, cq, cb) -> cb.like(pc.get("id"), MessageFormat.format("{0}%", idProvince));
    }

    public static Specification<Kabupaten> hasKodeKota(String kodeKota) {
        return (pc, cq, cb) -> cb.like(pc.get("id"), MessageFormat.format(FORMAT, kodeKota));
    }

    public static Specification<Kabupaten> hasNamaKota(String namaKota) {
        return (pc, cq, cb) -> cb.like(pc.get("nama"), MessageFormat.format(FORMAT, namaKota));
    }

    public static Specification<Kecamatan> hasKota(String idKota) {
        return (pc, cq, cb) -> cb.like(pc.get("id"), MessageFormat.format("{0}%", idKota));
    }

    public static Specification<Kecamatan> hasKodeKecamatan(String kodeKecamatan) {
        return (pc, cq, cb) -> cb.like(pc.get("id"), MessageFormat.format(FORMAT, kodeKecamatan));
    }

    public static Specification<Kecamatan> hasNamaKecamatan(String namaKecamatan) {
        return (pc, cq, cb) -> cb.like(pc.get("nama"), MessageFormat.format(FORMAT, namaKecamatan));
    }

    public static Specification<Desa> hasKecamatan(String idKecamatan) {
        return (pc, cq, cb) -> cb.like(pc.get("id"), MessageFormat.format("{0}%", idKecamatan));
    }

    public static Specification<Desa> hasKodeKelurahan(String kodeKelurahan) {
        return (pc, cq, cb) -> cb.like(pc.get("id"), MessageFormat.format(FORMAT, kodeKelurahan));
    }

    public static Specification<Desa> hasNamaKelurahan(String namaKelurahan) {
        return (pc, cq, cb) -> cb.like(pc.get("nama"), MessageFormat.format(FORMAT, namaKelurahan));
    }
}
