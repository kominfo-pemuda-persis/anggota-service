package id.or.pemudapersis.kominfo.anggotaservice.controller;

import id.or.pemudapersis.kominfo.anggotaservice.service.AuthenticateDto;
import lombok.Value;

@Value
class AuthDto {
    String accessToken;

    static AuthDto from(AuthenticateDto authenticate) {
        return new AuthDto(authenticate.getAccessToken());
    }
}
