package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.OrganisasiDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.OrganisasiService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/anggota")
@RequiredArgsConstructor
@Tag(name = "Organisasi", description = "Endpoint for managing Organisasi")
public class OrganisasiController {
    private final OrganisasiService organisasiService;

    private final ModelMapper modelMapper;

    @GetMapping(value = "/{npa}/organisasi")
    @Operation(
            summary = "Get all Organisasi Anggota Persis",
            description = "Get all Organisasi Anggota Persis.",
            tags = {"Organisasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<OrganisasiDto>>> organisasiList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idOrganisasi")
                    String sortBy,
            @Parameter(description = "Npa Anggota") @PathVariable("npa") String npa) {

        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<OrganisasiDto>> baseResponse = organisasiService.organisasiList(paging, false, npa);
        return ok(baseResponse);
    }

    @PostMapping(value = "/{npa}/organisasi")
    @Operation(
            summary = "Add new Organisasi Anggota Persis",
            description = "Add new Organisasi Anggota Persis.",
            tags = {"Organisasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<OrganisasiDto>> create(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Valid @RequestBody OrganisasiDto organisasiDto) {
        BaseResponse<OrganisasiDto> baseResponse = organisasiService.create(npa, organisasiDto);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{npa}/organisasi/{idOrganisasi}")
    @Operation(
            summary = "Update Organisasi Anggota Persis",
            description = "Update Organisasi Anggota Persis.",
            tags = {"Organisasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<OrganisasiDto>> update(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keluarga") @PathVariable("idOrganisasi") Long idOrganisasi,
            @RequestBody OrganisasiDto organisasiDto) {
        BaseResponse<OrganisasiDto> baseResponse = organisasiService.update(npa, idOrganisasi, organisasiDto);
        return ok(baseResponse);
    }

    @GetMapping(value = "/{npa}/organisasi/delete")
    @Operation(
            summary = "Get All deleted Organisasi Anggota Persis",
            description = "Get All deleted Organisasi Anggota Persis.",
            tags = {"Organisasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<OrganisasiDto>>> organisasiListDeleted(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idOrganisasi")
                    String sortBy,
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<OrganisasiDto>> baseResponse = organisasiService.organisasiList(paging, true, npa);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{npa}/organisasi/{idOrganisasi}")
    @Operation(
            summary = "Get All deleted Organisasi Anggota Persis by npa anggota",
            description = "Get All deleted Organisasi Anggota Persis by npa anggota.",
            tags = {"Organisasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<OrganisasiDto>> delete(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keluarga") @PathVariable("idOrganisasi") Long idOrganisasi) {
        BaseResponse<OrganisasiDto> baseResponse = organisasiService.delete(npa, idOrganisasi);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{npa}/organisasi/{idOrganisasi}/restore")
    @Operation(
            summary = "Restore All deleted Organisasi Anggota Persis by npa anggota",
            description = "Restore All deleted Organisasi Anggota Persis by npa anggota.",
            tags = {"Organisasi"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<OrganisasiDto>> restore(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keluarga") @PathVariable("idOrganisasi") Long idOrganisasi) {
        BaseResponse<OrganisasiDto> baseResponse = organisasiService.restore(npa, idOrganisasi);
        return ok(baseResponse);
    }
}
