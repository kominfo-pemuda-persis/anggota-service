package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.beans.support.PagedListHolder.DEFAULT_PAGE_SIZE;
import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPjDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPjListDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPj;
import id.or.pemudapersis.kominfo.anggotaservice.service.PerformaPjService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/api/performa/pj")
@Tag(name = "Performa PJ", description = "Endpoint for managing Performa PJ")
public class PerformaPjController {
    private final PerformaPjService performaPjService;
    private final ModelMapper modelMapper;

    public PerformaPjController(PerformaPjService performaPjService, ModelMapper modelMapper) {
        this.performaPjService = performaPjService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @Operation(
            summary = "Get all Indek Performa PJ Anggota Persis",
            description = "Get all Indek Performa PJ Anggota Persis.",
            tags = {"Performa PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> getListPerformaPj(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false) Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();
        final long start = System.nanoTime();

        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<PerformaPjListDto> listPerformaPj = performaPjService.getListPerformaPj(paging, false).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", listPerformaPj);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Add new Indek Performa PJ Anggota Persis",
            description = "Add new Indek Performa PJ Anggota Persis.",
            tags = {"Performa PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> create(@RequestBody @Valid PerformaPjDto performaPjDto) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();

        PerformaPj performaPj = convertToEntity(performaPjDto);
        performaPjService.create(performaPj);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Edit Indek Performa PJ Anggota Persis",
            description = "Edit Indek Performa PJ Anggota Persis.",
            tags = {"Performa PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> edit(@RequestBody @Valid PerformaPjDto performaPjDto, @PathVariable("id") Long id) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();

        performaPjService.update(performaPjDto, id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @GetMapping(value = "/delete")
    @Operation(
            summary = "Delete Indek Performa PJ Anggota Persis",
            description = "Delete Indek Performa PJ Anggota Persis.",
            tags = {"Performa PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> getListDeletedPerformaPj(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false) Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();

        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<PerformaPjListDto> listPerformaPj = performaPjService.getListPerformaPj(paging, true).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", listPerformaPj);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Delete Indek Performa PJ Anggota Persis",
            description = "Delete Indek Performa PJ Anggota Persis.",
            tags = {"Performa PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();

        performaPjService.delete(id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PatchMapping(value = "/{id}/restore")
    @Operation(
            summary = "Restore Indek Performa PJ Anggota Persis",
            description = "Restore Indek Performa PJ Anggota Persis.",
            tags = {"Performa PJ"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> restore(@PathVariable("id") Long id) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();

        performaPjService.restore(id);

        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    private PerformaPj convertToEntity(PerformaPjDto performaPjDto) {
        return modelMapper.map(performaPjDto, PerformaPj.class);
    }

    private PerformaPjListDto convertToDto(PerformaPj performaPj) {
        return modelMapper.map(performaPj, PerformaPjListDto.class);
    }
}
