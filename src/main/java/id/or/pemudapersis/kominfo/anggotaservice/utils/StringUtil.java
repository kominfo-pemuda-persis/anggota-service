package id.or.pemudapersis.kominfo.anggotaservice.utils;

import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.validation.constraints.NotNull;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class StringUtil {

    /**
     * Convert string to compatible {@link InternetAddress}
     * It wraps a mandatory try-catch to avoid incosistency usage of the {@link InternetAddress} constructor
     *
     * @param value is a {@code String} to be converted
     * @return parsed {@code InternetAddress} in {@code toString} form if successfully parsed
     * otherwise return the original {@code String} value
     */
    public static String convertToInternetAddress(@NotNull String value) {
        try {
            return new InternetAddress(value).toString();
        } catch (AddressException e) {
            log.warn("Failed parsing InternetAddress from: {}", value);
            return value;
        }
    }

    public static String getValidContactNumber(String input) {
        String phoneNumber = Optional.ofNullable(input)
                .filter(s -> (s.length() >= 10 && s.length() <= 18))
                .map(s -> s.replaceAll("[^0-9]", ""))
                .orElse(null);
        return getCountryCodePrefixedNumber(phoneNumber);
    }

    public static String getCountryCodePrefixedNumber(String number) { // returns a stripped number prefixed with 0
        if (number == null) {
            return null;
        }
        return ("62" + getStrippedPhoneNumber(number));
    }

    public static String getStrippedPhoneNumber(String number) {
        String phoneNumber = number;

        if (number != null) {
            if (number.indexOf("62") == 0) {
                phoneNumber = number.substring(2);
            } else if (number.indexOf("+62") == 0) {
                phoneNumber = number.substring(3);
            } else if (number.indexOf("0062") == 0) {
                phoneNumber = number.substring(4);
            } else if (number.indexOf('0') == 0) {
                phoneNumber = org.apache.commons.lang3.StringUtils.stripStart(number, "0");
            }
        }

        return phoneNumber;
    }
}
