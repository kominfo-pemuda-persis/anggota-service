package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.AnggotaDetailRuleDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.RoleDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Login;
import id.or.pemudapersis.kominfo.anggotaservice.model.UserView;
import id.or.pemudapersis.kominfo.anggotaservice.service.LoginService;
import id.or.pemudapersis.kominfo.anggotaservice.service.RoleService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/management/user")
@Tag(name = "User", description = "Endpoint for managing User")
public class ManagementUsersController {

    private final RoleService roleService;

    private final LoginService loginService;

    public ManagementUsersController(RoleService roleService, LoginService loginService) {
        this.roleService = roleService;
        this.loginService = loginService;
    }

    @GetMapping(value = "/role")
    @Operation(
            summary = "Get all Role Anggota Persis",
            description = "Get all Role Anggota Persis.",
            tags = {"User"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = RoleDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<RoleDto>>> getListRole(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<RoleDto>> listBaseResponse = roleService.getAllRole(paging);
        return ResponseEntity.ok(listBaseResponse);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Get all User ",
            description = "Get all User Data.",
            tags = {"User"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = Login.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<Page<UserView>>> getAllUser(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo) {
        Pageable paging = PageRequest.of(pageNo, pageSize);
        BaseResponse<Page<UserView>> allUser = loginService.getAllUser(paging);
        return ok(allUser);
    }

    @GetMapping(value = "/{npa}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Get Detail User Data by Npa",
            description = "Get Detail User Data by Npa",
            tags = {"User"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<AnggotaDetailRuleDto>> getUser(@PathVariable("npa") String npa) {
        return ok(loginService.findUserByNpaCustomJpa(npa));
    }
}
