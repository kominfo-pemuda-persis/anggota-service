package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * Project: anggota-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 07.08
 * <p>
 * Created with IntelliJ IDEA
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MessageWaGeneralRequest extends BaseMessageWaRequest {
    private String sendTo;
}
