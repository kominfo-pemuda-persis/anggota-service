package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Pendidikan;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PendidikanRepository extends JpaRepository<Pendidikan, Long> {
    Page<Pendidikan> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Pendidikan> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Pendidikan> findByDeletedAtIsNotNullAndAnggotaNpaAndIdPendidikan(String idAnggota, Long idPendidikan);

    Optional<Pendidikan> findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(String idAnggota, Long idPendidikan);
}
