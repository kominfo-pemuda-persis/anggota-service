package id.or.pemudapersis.kominfo.anggotaservice.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class SummaryStatusMaritalDto {
    private List<DetailStatus> detailStatusList;
    private Long jumlahTotal;
}
