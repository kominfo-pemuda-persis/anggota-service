package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import lombok.Value;

@Value
public class CheckAnggotaDto {
    String npa;
    String nama;
    String email;

    private CheckAnggotaDto(Anggota anggota) {
        npa = anggota.getNpa();
        nama = anggota.getNama();
        email = anggota.getEmail();
    }

    public static CheckAnggotaDto valueOf(Anggota anggota) {
        return new CheckAnggotaDto(anggota);
    }
}
