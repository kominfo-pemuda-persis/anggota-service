package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Login;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;

    @Override
    public AuthenticateDto authenticate(AuthenticateInput input) {
        var authenticated = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(input.getNpa(), input.getPassword()));

        var principal = (Login) authenticated.getPrincipal();
        var permissions = principal.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());
        var role = principal.firstRole();
        var pd = principal.getAnggota().getPd().getKdPd();
        var pc = principal.getAnggota().getPc().getKdPc();
        var pw = principal.getAnggota().getPw().getKdPw();
        var nama = principal.getAnggota().getNama();

        return new AuthenticateDto(jwtService.generateToken(
                new GenerateTokenInput(input.getNpa(), permissions, role, input.getHost(), pd, pc, pw, nama)));
    }
}
