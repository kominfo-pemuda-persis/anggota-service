package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.JamiyyahPcRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MonografiPcRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Log4j2
@Transactional
public class MonografiPcService {
    private final MonografiPcRepository monografiPCRepository;
    private final CabangService cabangService;
    private final DaerahService daerahService;
    private final WilayahService wilayahService;
    private final HelperUtilService helperUtilService;
    private final JamiyyahPcRepository jamiyyahPcRepository;
    private final ModelMapper modelMapper;

    public MonografiPcService(
            MonografiPcRepository monografiPCRepository,
            CabangService cabangService,
            DaerahService daerahService,
            HelperUtilService helperUtilService,
            WilayahService wilayahService,
            JamiyyahPcRepository jamiyyahPcRepository,
            ModelMapper modelMapper) {
        this.monografiPCRepository = monografiPCRepository;
        this.cabangService = cabangService;
        this.daerahService = daerahService;
        this.helperUtilService = helperUtilService;
        this.wilayahService = wilayahService;
        this.jamiyyahPcRepository = jamiyyahPcRepository;
        this.modelMapper = modelMapper;
    }

    public List<MonografiPc> getAllMonografiPC(Pageable pageable, boolean isDeleted) {
        Page<MonografiPc> monografiPdPage;

        if (!isDeleted) monografiPdPage = monografiPCRepository.findByDeletedAtIsNull(pageable);
        else monografiPdPage = monografiPCRepository.findByDeletedAtIsNotNull(pageable);

        return monografiPdPage.getContent();
    }

    public MonografiPc create(MonografiPc monografiPc) {
        Optional<MonografiPc> optionalMonografiPc =
                monografiPCRepository.findByCabangKdPc(monografiPc.getCabang().getKdPc());
        optionalMonografiPc.ifPresent(this::validate);

        Cabang cabang = cabangService.getByKodeCabang(monografiPc.getCabang().getKdPc());
        Daerah daerah = daerahService.getByKodeDaerah(monografiPc.getDaerah().getKdPd());
        Wilayah wilayah =
                wilayahService.getByKodeWilayah(monografiPc.getWilayah().getKdPw());
        Provinsi provinsi =
                helperUtilService.getProvinsiById(monografiPc.getProvinsi().getId());
        Kabupaten kabupaten =
                helperUtilService.getKabupatenById(monografiPc.getKabupaten().getId());
        Kecamatan kecamatan =
                helperUtilService.getKecamatanById(monografiPc.getKecamatan().getId());

        JamiyyahPc jamiyyahPc = JamiyyahPc.builder()
                .kecamatan(kecamatan)
                .daerah(daerah)
                .bendahara(monografiPc.getJamiyyahPc().getBendahara())
                .bidAdministrasi(monografiPc.getJamiyyahPc().getBidAdministrasi())
                .bidDakwah(monografiPc.getJamiyyahPc().getBidDakwah())
                .bidEkonomi(monografiPc.getJamiyyahPc().getBidEkonomi())
                .bidHal(monografiPc.getJamiyyahPc().getBidHal())
                .bidHumasPublikasi(monografiPc.getJamiyyahPc().getBidHumasPublikasi())
                .bidJamiyyah(monografiPc.getJamiyyahPc().getBidJamiyyah())
                .bidKaderisasi(monografiPc.getJamiyyahPc().getBidKaderisasi())
                .bidOrgSeni(monografiPc.getJamiyyahPc().getBidOrgSeni())
                .bidPendidikan(monografiPc.getJamiyyahPc().getBidPendidikan())
                .bidSosial(monografiPc.getJamiyyahPc().getBidSosial())
                .cabang(cabang)
                .countAnggotaBiasa(monografiPc.getJamiyyahPc().getCountAnggotaBiasa())
                .countAnggotaIstimewa(monografiPc.getJamiyyahPc().getCountAnggotaIstimewa())
                .countAnggotaTersiar(monografiPc.getJamiyyahPc().getCountAnggotaTersiar())
                .countMeninggalDunia(monografiPc.getJamiyyahPc().getCountMeninggalDunia())
                .countMutasiPersis(monografiPc.getJamiyyahPc().getCountMutasiPersis())
                .countMutasiTempat(monografiPc.getJamiyyahPc().getCountMutasiTempat())
                .countTidakHerregistrasi(monografiPc.getJamiyyahPc().getCountTidakHerregistrasi())
                .countUndurDiri(monografiPc.getJamiyyahPc().getCountUndurDiri())
                .createdDate(LocalDateTime.now())
                .hari(monografiPc.getJamiyyahPc().getHari())
                .ketua(monografiPc.getJamiyyahPc().getKetua())
                .kabupaten(kabupaten)
                .monografiPc(MonografiPc.builder()
                        .cabang(cabang)
                        .daerah(daerah)
                        .wilayah(wilayah)
                        .provinsi(provinsi)
                        .kabupaten(kabupaten)
                        .kecamatan(kecamatan)
                        .alamatAlternatif(monografiPc.getAlamatAlternatif())
                        .alamatLengkap(monografiPc.getAlamatLengkap())
                        .btsWilayahBarat(monografiPc.getBtsWilayahBarat())
                        .btsWilayahSelatan(monografiPc.getBtsWilayahSelatan())
                        .btsWilayahTimur(monografiPc.getBtsWilayahTimur())
                        .btsWilayahUtara(monografiPc.getBtsWilayahUtara())
                        .createdDate(LocalDateTime.now())
                        .email(monografiPc.getEmail())
                        .foto(monografiPc.getFoto())
                        .jarakIbuKotaNegara(monografiPc.getJarakIbuKotaNegara())
                        .latitude(monografiPc.getLatitude())
                        .longitude(monografiPc.getLongitude())
                        .luas(monografiPc.getLuas())
                        .noKontak(monografiPc.getNoKontak())
                        .updatedDate(LocalDateTime.now())
                        .build())
                .musyCabLastHijri(monografiPc.getJamiyyahPc().getMusyCabLastHijri())
                .musyCabLastMasehi(monografiPc.getJamiyyahPc().getMusyCabLastMasehi())
                .pembantuUmum1(monografiPc.getJamiyyahPc().getPembantuUmum1())
                .pembantuUmum2(monografiPc.getJamiyyahPc().getPembantuUmum2())
                .pembantuUmum3(monografiPc.getJamiyyahPc().getPembantuUmum3())
                .penasehat1(monografiPc.getJamiyyahPc().getPenasehat1())
                .penasehat2(monografiPc.getJamiyyahPc().getPenasehat2())
                .penasehat3(monografiPc.getJamiyyahPc().getPenasehat3())
                .provinsi(provinsi)
                .pukul(monografiPc.getJamiyyahPc().getPukul())
                .sekretaris(monografiPc.getJamiyyahPc().getSekretaris())
                .updatedDate(LocalDateTime.now())
                .wilayah(wilayah)
                .wklBendahara(monografiPc.getJamiyyahPc().getWklBendahara())
                .wklBidAdministrasi(monografiPc.getJamiyyahPc().getWklBidAdministrasi())
                .wklBidDakwah(monografiPc.getJamiyyahPc().getWklBidDakwah())
                .wklBidEkonomi(monografiPc.getJamiyyahPc().getWklBidEkonomi())
                .wklBidHal(monografiPc.getJamiyyahPc().getWklBidHal())
                .wklBidHumasPublikasi(monografiPc.getJamiyyahPc().getWklBidHumasPublikasi())
                .wklBidJamiyyah(monografiPc.getJamiyyahPc().getWklBidJamiyyah())
                .wklBidKaderisasi(monografiPc.getJamiyyahPc().getWklBidKaderisasi())
                .wklBidOrgSeni(monografiPc.getJamiyyahPc().getWklBidOrgSeni())
                .wklBidPendidikan(monografiPc.getJamiyyahPc().getWklBidPendidikan())
                .wklKetua(monografiPc.getJamiyyahPc().getWklKetua())
                .wklSekretaris(monografiPc.getJamiyyahPc().getWklSekretaris())
                .build();
        return jamiyyahPcRepository.save(jamiyyahPc).getMonografiPc();
    }

    public MonografiPc update(MonografiPcDto monografiPcDto, Long id) {

        MonografiPc monografiPcEntity = monografiPCRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "30131", " monografi pc not found"));

        String kodeCabang = Optional.ofNullable(monografiPcDto.getCabang())
                .map(CabangRequest::getKdPc)
                .orElse(monografiPcEntity.getCabang().getKdPc());
        String kodeDaerah = Optional.ofNullable(monografiPcDto.getDaerah())
                .map(DaerahRequest::getKdPd)
                .orElse(monografiPcEntity.getDaerah().getKdPd());
        String kodeWilayah = Optional.ofNullable(monografiPcDto.getWilayah())
                .map(WilayahDto::getKdPw)
                .orElse(monografiPcEntity.getWilayah().getKdPw());
        String kodeProvinsi = Optional.ofNullable(monografiPcDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .orElse(monografiPcEntity.getProvinsi().getId());
        String kodeKabupaten = Optional.ofNullable(monografiPcDto.getKabupaten())
                .map(KabupatenDto::getId)
                .orElse(monografiPcEntity.getKabupaten().getId());
        String kodeKecamatan = Optional.ofNullable(monografiPcDto.getKecamatan())
                .map(KecamatanDto::getId)
                .orElse(monografiPcEntity.getKecamatan().getId());

        // the persisted entity to update with the DTO, modelMapper will map the property instead of instantiate a new
        // entity. Hibernate will not allow it
        monografiPcDto.setCabang(null);
        monografiPcDto.setDaerah(null);
        monografiPcDto.setWilayah(null);
        monografiPcDto.setProvinsi(null);
        monografiPcDto.setKabupaten(null);
        monografiPcDto.setKecamatan(null);
        monografiPcDto.setId(null);

        updateEntityFromDto(monografiPcDto, monografiPcEntity);
        Cabang cabang = cabangService.getByKodeCabang(kodeCabang);
        Daerah daerah = daerahService.getByKodeDaerah(kodeDaerah);
        Wilayah wilayah = wilayahService.getByKodeWilayah(kodeWilayah);
        Provinsi provinsi = helperUtilService.getProvinsiById(kodeProvinsi);
        Kabupaten kabupaten = helperUtilService.getKabupatenById(kodeKabupaten);
        Kecamatan kecamatan = helperUtilService.getKecamatanById(kodeKecamatan);
        monografiPcEntity.setCabang(cabang);
        monografiPcEntity.setDaerah(daerah);
        monografiPcEntity.setKecamatan(kecamatan);
        monografiPcEntity.setWilayah(wilayah);
        monografiPcEntity.setProvinsi(provinsi);
        monografiPcEntity.setKabupaten(kabupaten);
        monografiPcEntity.getJamiyyahPc().setCabang(cabang);
        monografiPcEntity.getJamiyyahPc().setDaerah(daerah);
        monografiPcEntity.getJamiyyahPc().setWilayah(wilayah);
        monografiPcEntity.getJamiyyahPc().setProvinsi(provinsi);
        monografiPcEntity.getJamiyyahPc().setKecamatan(kecamatan);
        monografiPcEntity.getJamiyyahPc().setKabupaten(kabupaten);
        monografiPcEntity.setUpdatedDate(LocalDateTime.now());
        monografiPcEntity.getJamiyyahPc().setUpdatedDate(LocalDateTime.now());
        return monografiPCRepository.save(monografiPcEntity);
    }

    public MonografiPc delete(Long id) {
        Optional<MonografiPc> monografiOptional = monografiPCRepository.findByIdAndDeletedAtIsNull(id);
        if (!monografiOptional.isPresent()) {
            log.info("Monografi PC not found {} ", id);
            throw new BusinessException(HttpStatus.NOT_FOUND, "30131", " monografi pc not found");
        }

        MonografiPc monografi = monografiOptional.get();
        Optional.ofNullable(monografi.getJamiyyahPc())
                .ifPresent(jamiyyahPc -> jamiyyahPc.setDeletedAt(LocalDateTime.now()));
        monografi.setDeletedAt(LocalDateTime.now());
        return monografiPCRepository.save(monografi);
    }

    private void updateEntityFromDto(MonografiPcDto monografiPcDto, MonografiPc monografiPc) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(monografiPcDto, monografiPc);
    }

    private void validate(MonografiPc monografiPc) {
        if (monografiPc.getDeletedAt() != null)
            throw new BusinessException(
                    HttpStatus.FOUND,
                    "30133",
                    " Monografi PC " + monografiPc.getCabang().getNamaPc() + " already exists deleted record");
        throw new BusinessException(
                HttpStatus.FOUND,
                "30131",
                " Monografi PC " + monografiPc.getCabang().getNamaPc() + " already exists");
    }

    public MonografiPc restore(Long id) {
        Optional<MonografiPc> monografiOptional = monografiPCRepository.findByIdAndDeletedAtIsNotNull(id);
        if (!monografiOptional.isPresent()) {
            log.info("Monografi PC not found {} ", id);
            throw new BusinessException(HttpStatus.NOT_FOUND, "30131", " monografi pc not found");
        }

        MonografiPc monografi = monografiOptional.get();
        Optional.ofNullable(monografi.getJamiyyahPc()).ifPresent(jamiyyahPc -> jamiyyahPc.setDeletedAt(null));
        monografi.setDeletedAt(null);
        return monografiPCRepository.save(monografi);
    }

    public Long countMonografiPc() {
        return monografiPCRepository.countByDeletedAtIsNull();
    }
}
