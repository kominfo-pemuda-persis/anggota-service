package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPj;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PerformaPjRepository extends JpaRepository<PerformaPj, Long> {
    Page<PerformaPj> findByDeletedAtIsNull(Pageable pageable);

    Optional<PerformaPj> findByKodePjAndDeletedAtIsNull(String kodePj);

    Optional<PerformaPj> findByIdAndDeletedAtIsNull(Long id);

    Optional<PerformaPj> findByIdAndDeletedAtIsNotNull(Long id);

    Page<PerformaPj> findByDeletedAtIsNotNull(Pageable pageReq);

    Long countByDeletedAtIsNull();
}
