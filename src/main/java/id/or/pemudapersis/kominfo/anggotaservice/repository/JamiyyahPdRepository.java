package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.JamiyyahPd;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JamiyyahPdRepository extends JpaRepository<JamiyyahPd, Long> {}
