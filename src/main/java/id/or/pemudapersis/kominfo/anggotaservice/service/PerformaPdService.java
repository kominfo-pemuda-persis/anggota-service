package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KabupatenDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.PerformaPdDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.ProvinsiDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.WilayahDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PerformaPdRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class PerformaPdService {

    private static final String PERFORMA_PD_NOT_AVAILABLE_ERROR_CODE = "30431";

    private final PerformaPdRepository performaPdRepository;
    private final DaerahService daerahService;
    private final WilayahService wilayahService;
    private final HelperUtilService helperUtilService;
    private final ModelMapper modelMapper;

    public PerformaPdService(
            PerformaPdRepository performaPdRepository,
            DaerahService daerahService,
            WilayahService wilayahService,
            HelperUtilService helperUtilService,
            ModelMapper modelMapper) {
        this.performaPdRepository = performaPdRepository;
        this.daerahService = daerahService;
        this.wilayahService = wilayahService;
        this.helperUtilService = helperUtilService;
        this.modelMapper = modelMapper;
    }

    public List<PerformaPd> performaPdList(Pageable paging, boolean isDeleted) {
        Pageable pageReq = PageRequest.of(paging.getPageNumber(), paging.getPageSize(), paging.getSort());
        Page<PerformaPd> performaPds;
        if (!isDeleted) performaPds = performaPdRepository.findByDeletedAtIsNull(pageReq);
        else performaPds = performaPdRepository.findByDeletedAtIsNotNull(pageReq);
        return performaPds.getContent();
    }

    public PerformaPd create(PerformaPd performaPdRequest) {
        Optional<PerformaPd> performaPdOptional = performaPdRepository.findByDaerahKdPd(
                performaPdRequest.getDaerah().getKdPd());
        performaPdOptional.ifPresent(this::validate);
        Daerah daerah =
                daerahService.getByKodeDaerah(performaPdRequest.getDaerah().getKdPd());
        Wilayah wilayah =
                wilayahService.getByKodeWilayah(performaPdRequest.getWilayah().getKdPw());
        Provinsi provinsi = helperUtilService.getProvinsiById(
                performaPdRequest.getProvinsi().getId());
        Kabupaten kabupaten = helperUtilService.getKabupatenById(
                performaPdRequest.getKabupaten().getId());

        PerformaPd performaPd = PerformaPd.builder()
                .alamat(performaPdRequest.getAlamat())
                .createdDate(LocalDateTime.now())
                .daerah(daerah)
                .ketuaPd(performaPdRequest.getKetuaPd())
                .kabupaten(kabupaten)
                .lastSurvey(LocalDateTime.now())
                .noHp(performaPdRequest.getNoHp())
                .q1a(performaPdRequest.getQ1a())
                .q2a(performaPdRequest.getQ2a())
                .q3a(performaPdRequest.getQ3a())
                .q4a(performaPdRequest.getQ4a())
                .q5a(performaPdRequest.getQ5a())
                .q6a(performaPdRequest.getQ6a())
                .q1b(performaPdRequest.getQ1b())
                .q2b(performaPdRequest.getQ2b())
                .q3b(performaPdRequest.getQ3b())
                .q4b(performaPdRequest.getQ4b())
                .q5b(performaPdRequest.getQ5b())
                .q6b(performaPdRequest.getQ6b())
                .q7b(performaPdRequest.getQ7b())
                .q8b(performaPdRequest.getQ8b())
                .q9b(performaPdRequest.getQ9b())
                .q10b(performaPdRequest.getQ10b())
                .q1c(performaPdRequest.getQ1c())
                .q2c(performaPdRequest.getQ2c())
                .q3c(performaPdRequest.getQ3c())
                .q1d(performaPdRequest.getQ1d())
                .q2d(performaPdRequest.getQ2d())
                .q3d(performaPdRequest.getQ3d())
                .q4d(performaPdRequest.getQ4d())
                .q5d(performaPdRequest.getQ5d())
                .q6d(performaPdRequest.getQ6d())
                .q7d(performaPdRequest.getQ7d())
                .q8d(performaPdRequest.getQ8d())
                .q9d(performaPdRequest.getQ9d())
                .q10d(performaPdRequest.getQ10d())
                .q11d(performaPdRequest.getQ11d())
                .q12d(performaPdRequest.getQ12d())
                .q13d(performaPdRequest.getQ13d())
                .q14d(performaPdRequest.getQ14d())
                .q15d(performaPdRequest.getQ15d())
                .q16d(performaPdRequest.getQ16d())
                .q17d(performaPdRequest.getQ17d())
                .q18d(performaPdRequest.getQ18d())
                .q19d(performaPdRequest.getQ19d())
                .q20d(performaPdRequest.getQ20d())
                .q21d(performaPdRequest.getQ21d())
                .q22d(performaPdRequest.getQ22d())
                .q23d(performaPdRequest.getQ23d())
                .q24d(performaPdRequest.getQ24d())
                .q25d(performaPdRequest.getQ25d())
                .q26d(performaPdRequest.getQ26d())
                .q27d(performaPdRequest.getQ27d())
                .q28d(performaPdRequest.getQ28d())
                .q1e(performaPdRequest.getQ1e())
                .q2e(performaPdRequest.getQ2e())
                .q3e(performaPdRequest.getQ3e())
                .q4e(performaPdRequest.getQ4e())
                .q5e(performaPdRequest.getQ5e())
                .q6e(performaPdRequest.getQ6e())
                .q7e(performaPdRequest.getQ7e())
                .q8e(performaPdRequest.getQ8e())
                .q9e(performaPdRequest.getQ9e())
                .q10e(performaPdRequest.getQ10e())
                .q1f(performaPdRequest.getQ1f())
                .q2f(performaPdRequest.getQ2f())
                .q3f(performaPdRequest.getQ3f())
                .q4f(performaPdRequest.getQ4f())
                .q5f(performaPdRequest.getQ5f())
                .q6f(performaPdRequest.getQ6f())
                .q7f(performaPdRequest.getQ7f())
                .provinsi(provinsi)
                .wilayah(wilayah)
                .build();
        return performaPdRepository.save(performaPd);
    }

    private void validate(PerformaPd performaPd) {
        if (performaPd.getDeletedAt() != null)
            throw new BusinessException(
                    HttpStatus.FOUND,
                    "30433",
                    " performa pd " + performaPd.getDaerah().getNamaPd() + " already exists deleted record");
        throw new BusinessException(
                HttpStatus.FOUND,
                PERFORMA_PD_NOT_AVAILABLE_ERROR_CODE,
                " performa pd " + performaPd.getDaerah().getNamaPd() + " already exists");
    }

    public PerformaPd update(PerformaPdDto performaPdDto, Long id) {

        PerformaPd performaPd = performaPdRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PD_NOT_AVAILABLE_ERROR_CODE, " Performa PD not found"));
        String kodeDaerah = Optional.ofNullable(performaPdDto.getDaerah())
                .map(DaerahRequest::getKdPd)
                .orElse(performaPd.getDaerah().getKdPd());
        String kodeWilayah = Optional.ofNullable(performaPdDto.getWilayah())
                .map(WilayahDto::getKdPw)
                .orElse(performaPd.getWilayah().getKdPw());
        String kodeProvinsi = Optional.ofNullable(performaPdDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .orElse(performaPd.getProvinsi().getId());
        String kodeKabupaten = Optional.ofNullable(performaPdDto.getKabupaten())
                .map(KabupatenDto::getId)
                .orElse(performaPd.getKabupaten().getId());

        performaPdDto.setWilayah(null);
        performaPdDto.setDaerah(null);
        performaPdDto.setProvinsi(null);
        performaPdDto.setKabupaten(null);
        performaPdDto.setId(null);

        updateEntityFromDto(performaPdDto, performaPd);
        Daerah daerah = daerahService.getByKodeDaerah(kodeDaerah);
        Wilayah wilayah = wilayahService.getByKodeWilayah(kodeWilayah);
        Provinsi provinsi = helperUtilService.getProvinsiById(kodeProvinsi);
        Kabupaten kabupaten = helperUtilService.getKabupatenById(kodeKabupaten);

        performaPd.setDaerah(daerah);
        performaPd.setWilayah(wilayah);
        performaPd.setProvinsi(provinsi);
        performaPd.setKabupaten(kabupaten);
        performaPd.setUpdatedDate(LocalDateTime.now());
        return performaPdRepository.save(performaPd);
    }

    public PerformaPd delete(Long id) {
        PerformaPd performaPd = performaPdRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PD_NOT_AVAILABLE_ERROR_CODE, " Performa PD not found"));

        performaPd.setDeletedAt(LocalDateTime.now());
        performaPd.setUpdatedDate(LocalDateTime.now());
        return performaPdRepository.save(performaPd);
    }

    public PerformaPd restore(Long id) {
        PerformaPd performaPd = performaPdRepository
                .findByIdAndDeletedAtIsNotNull(id)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PERFORMA_PD_NOT_AVAILABLE_ERROR_CODE, " Performa PD not found"));
        performaPd.setDeletedAt(null);
        performaPd.setUpdatedDate(LocalDateTime.now());
        return performaPdRepository.save(performaPd);
    }

    private void updateEntityFromDto(PerformaPdDto performaPdDto, PerformaPd performaPd) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(performaPdDto, performaPd);
    }

    public Long countPerformaPd() {
        return performaPdRepository.countByDeletedAtIsNull();
    }
}
