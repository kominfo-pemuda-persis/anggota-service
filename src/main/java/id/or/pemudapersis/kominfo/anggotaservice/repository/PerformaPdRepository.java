package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPd;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PerformaPdRepository extends JpaRepository<PerformaPd, Long> {
    Page<PerformaPd> findByDeletedAtIsNull(Pageable pageReq);

    Optional<PerformaPd> findByDaerahKdPd(String kodeDaerah);

    Optional<PerformaPd> findByIdAndDeletedAtIsNull(Long id);

    Optional<PerformaPd> findByIdAndDeletedAtIsNotNull(Long id);

    Page<PerformaPd> findByDeletedAtIsNotNull(Pageable pageReq);

    Long countByDeletedAtIsNull();
}
