package id.or.pemudapersis.kominfo.anggotaservice.controller;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KeterampilanDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.KeterampilanService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/anggota")
@RequiredArgsConstructor
@Tag(name = "Keterampilan", description = "Endpoint for managing Keterampilan")
public class KeterampilanController {
    private final KeterampilanService keterampilanService;
    private final ModelMapper modelMapper;

    @GetMapping(value = "/{npa}/keterampilan")
    @Operation(
            summary = "Get all Keterampilan Anggota Persis by npa anggota",
            description = "Get all Keterampilan Anggota Persis by npa anggota.",
            tags = {"Keterampilan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeterampilanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public BaseResponse<Page<KeterampilanDto>> keterampilanList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idKeterampilan")
                    String sortBy,
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return keterampilanService.keterampilanList(paging, false, npa);
    }

    @PostMapping(value = "/{npa}/keterampilan")
    @Operation(
            summary = "Add new Keterampilan Anggota Persis by npa anggota",
            description = "Add new Keterampilan Anggota Persis by npa anggota.",
            tags = {"Keterampilan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeterampilanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public BaseResponse<KeterampilanDto> create(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Valid @RequestBody KeterampilanDto keterampilanDto) {
        return keterampilanService.create(npa, keterampilanDto);
    }

    @PutMapping(value = "/{npa}/keterampilan/{idKeterampilan}")
    @Operation(
            summary = "Update Keterampilan Anggota Persis by npa anggota",
            description = "Update Keterampilan Anggota Persis by npa anggota.",
            tags = {"Keterampilan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeterampilanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public BaseResponse<KeterampilanDto> update(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keterampilan") @PathVariable("idKeterampilan") Long idKeterampilan,
            @RequestBody KeterampilanDto keterampilanDto) {
        return keterampilanService.update(npa, idKeterampilan, keterampilanDto);
    }

    @GetMapping(value = "/{npa}/keterampilan/delete")
    @Operation(
            summary = "Get All Keterampilan Anggota Persis by npa anggota",
            description = "Get All Keterampilan Anggota Persis by npa anggota.",
            tags = {"Keterampilan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeterampilanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public BaseResponse<Page<KeterampilanDto>> keterampilanListDeleted(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort")
                    @RequestParam(required = false, defaultValue = "idKeterampilan")
                    String sortBy,
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        return keterampilanService.keterampilanList(paging, true, npa);
    }

    @DeleteMapping(value = "/{npa}/keterampilan/{idKeterampilan}")
    @Operation(
            summary = "Delete Keterampilan Anggota Persis by npa anggota",
            description = "Delete Keterampilan Anggota Persis by npa anggota.",
            tags = {"Keterampilan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeterampilanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public BaseResponse<KeterampilanDto> delete(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keterampilan") @PathVariable("idKeterampilan") Long idKeterampilan) {
        return keterampilanService.delete(npa, idKeterampilan);
    }

    @PatchMapping(value = "/{npa}/keterampilan/{idKeterampilan}/restore")
    @Operation(
            summary = "Restore Keterampilan Anggota Persis by npa anggota",
            description = "Restore Keterampilan Anggota Persis by npa anggota.",
            tags = {"Keterampilan"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = KeterampilanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public BaseResponse<KeterampilanDto> restore(
            @Parameter(description = "npa Anggota") @PathVariable("npa") String npa,
            @Parameter(description = "id Keterampilan") @PathVariable("idKeterampilan") Long idKeterampilan) {
        return keterampilanService.restore(npa, idKeterampilan);
    }
}
