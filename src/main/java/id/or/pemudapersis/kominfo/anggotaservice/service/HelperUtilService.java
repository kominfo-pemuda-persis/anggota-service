package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.*;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class HelperUtilService {
    private final ProvinsiRepository provinsiRepository;
    private final KabupatenRepository kabupatenRepository;
    private final KecamatanRepository kecamatanRepository;
    private final DesaRepository desaRepository;
    private final MasterPendidikanRepository masterPendidikanRepository;

    public HelperUtilService(
            ProvinsiRepository provinsiRepository,
            KabupatenRepository kabupatenRepository,
            KecamatanRepository kecamatanRepository,
            DesaRepository desaRepository,
            MasterPendidikanRepository masterPendidikanRepository) {
        this.provinsiRepository = provinsiRepository;
        this.kabupatenRepository = kabupatenRepository;
        this.kecamatanRepository = kecamatanRepository;
        this.desaRepository = desaRepository;
        this.masterPendidikanRepository = masterPendidikanRepository;
    }

    public Provinsi getProvinsiById(String provinsiId) {
        Optional<Provinsi> provinsiOptional = provinsiRepository.findById(provinsiId);
        if (provinsiOptional.isPresent()) return provinsiOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "82000", "Provinsi not found");
    }

    public Kabupaten getKabupatenById(String kabupatenId) {
        Optional<Kabupaten> kabupatenOptional = kabupatenRepository.findById(kabupatenId);
        if (kabupatenOptional.isPresent()) return kabupatenOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "83000", "Kabupaten not found");
    }

    public Kecamatan getKecamatanById(String kecamatanId) {
        Optional<Kecamatan> kecamatanOptional = kecamatanRepository.findById(kecamatanId);
        if (kecamatanOptional.isPresent()) return kecamatanOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "84000", "Kecamatan not found");
    }

    public Desa getDesaByid(String desaId) {
        Optional<Desa> desaOptional = desaRepository.findById(desaId);
        if (desaOptional.isPresent()) return desaOptional.get();
        throw new BusinessException(HttpStatus.NOT_FOUND, "85000", "Desa not found");
    }

    public MasterPendidikan getMasterPendidikanById(String idTingkatPendidikan) {
        return masterPendidikanRepository
                .findById(idTingkatPendidikan)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, "MASTER_PENDIDIKAN_NOT_FOUND", "MASTER PENDIDIKAN NOT FOUND"));
    }
}
