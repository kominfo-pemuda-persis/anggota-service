package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MessageWaAnggotaRequest;
import id.or.pemudapersis.kominfo.anggotaservice.dto.MessageWaGeneralRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.MessageWhatsAppsService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Project: anggota-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 07.12
 * <p>
 * Created with IntelliJ IDEA
 */
@RestController
@Log4j2
@RequestMapping(value = "/v1/api/wa")
@Tag(name = "WhatsApps", description = "Endpoint for send message via WhatsApps ")
public class MessageWhatsAppsController {
    private final MessageWhatsAppsService messageWhatsAppsService;

    public MessageWhatsAppsController(MessageWhatsAppsService messageWhatsAppsService) {
        this.messageWhatsAppsService = messageWhatsAppsService;
    }

    @PostMapping(value = "/blaster/general", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "send whats apps message general",
            description = "send whats apps message general.",
            tags = {"WhatsApps"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = MessageWaGeneralRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<MessageWaGeneralRequest>> sendBlastMessage(
            @Valid @RequestBody MessageWaGeneralRequest request) {
        BaseResponse<MessageWaGeneralRequest> baseResponse = messageWhatsAppsService.sendBlastMessage(request);
        return ok(baseResponse);
    }

    @PostMapping(value = "/blaster/anggota", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "send whats apps message anggota",
            description = "send whats apps message anggota.",
            tags = {"WhatsApps"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = MessageWaGeneralRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<MessageWaAnggotaRequest>> sendBlastMessageAnggota(
            @Valid @RequestBody MessageWaAnggotaRequest request) {
        BaseResponse<MessageWaAnggotaRequest> baseResponse = messageWhatsAppsService.sendBlastMessageAnggota(request);
        return ok(baseResponse);
    }
}
