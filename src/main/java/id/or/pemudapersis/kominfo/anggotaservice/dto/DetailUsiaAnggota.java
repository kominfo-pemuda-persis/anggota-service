package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetailUsiaAnggota {

    private String rangeUsia;

    private Long jumlah;
}
