package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.config.OpenApi30Config;
import id.or.pemudapersis.kominfo.anggotaservice.constant.GlobalMessage;
import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Cabang;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Daerah;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Desa;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Kabupaten;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Kecamatan;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Login;
import id.or.pemudapersis.kominfo.anggotaservice.entity.MasterPendidikan;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Role;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import id.or.pemudapersis.kominfo.anggotaservice.exception.BadRequestException;
import id.or.pemudapersis.kominfo.anggotaservice.exception.ResourceNotFoundException;
import id.or.pemudapersis.kominfo.anggotaservice.repository.AnggotaRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.LoginRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.RoleRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.CheckAnggotaRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.SendEmailAnggotaRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.mail.MailMessageContent;
import id.or.pemudapersis.kominfo.anggotaservice.service.mail.MailService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.SpecificationHelper;
import id.or.pemudapersis.kominfo.anggotaservice.utils.StringUtil;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import jakarta.mail.MessagingException;
import java.security.Principal;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.testcontainers.shaded.com.google.common.collect.Lists;

/**
 * Created by IntelliJ IDEA.
 * Project : anggota-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/06/20
 * Time: 07.36
 */
@Service
@Log4j2
@Transactional
@RequiredArgsConstructor
public class AnggotaService {
    private static final String BASE_PREFIX_IMAGE_ANGGOTA = "images/anggota/";
    private static final String STATIC_PATH = "static/";
    public static final String DATA = "data";
    public static final String FILE_NAME = "fileName";
    public static final String ANGGOTA = "anggota_";

    private final AnggotaRepository anggotaRepository;
    private final LoginRepository loginRepository;
    private final ModelMapper modelMapper;
    private final HelperUtilService helperUtilService;
    private final WilayahService wilayahService;
    private final CabangService cabangService;
    private final DaerahService daerahService;
    private final LoginService loginService;
    private final MailService mailService;
    private final StorageService storageService;
    private final RoleRepository roleRepository;
    private final OpenApi30Config apiConfig;

    // @PreAuthorize("hasAuthority('read-anggota')")
    @Transactional(readOnly = true)
    public BaseResponse<Page<AnggotaDto>> getAllAnggota(AnggotaFilterDto filterDto, Pageable pageable) {
        final long start = System.nanoTime();
        Page<Anggota> anggotas = anggotaRepository.findAll(bySearch(filterDto), pageable);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                ((end - start) / 1000000),
                GlobalMessage.SUCCESS.message,
                anggotas.map(this::convertToDtoWithRole));
    }

    public Specification<Anggota> bySearch(AnggotaFilterDto filterDto) {
        return SpecificationHelper.searchAttributeIsNull("deletedAt")
                .and(SpecificationHelper.searchAttributeContains("npa", filterDto.getKeyword())
                        .or(SpecificationHelper.searchAttributeContains("nama", filterDto.getKeyword())))
                .and(SpecificationHelper.searchAttributeEqual("jenisKeanggotaan", filterDto.getJenisAnggota()))
                .and(SpecificationHelper.searchAttributeEqual("levelTafiq", filterDto.getLevelTafiq()))
                .and(SpecificationHelper.searchAttributeEqual("golDarah", filterDto.getGolonganDarah()))
                .and(SpecificationHelper.searchAttributeEqual("statusAktif", filterDto.getStatus()))
                .and(SpecificationHelper.searchAttributeEqual("statusHer", filterDto.getStatusHer()))
                .and(SpecificationHelper.searchAttributeEqual("statusMerital", filterDto.getStatusMerital()))
                .and(SpecificationHelper.searchNestedEquals(
                        "jenjangPendidikan", "pendidikan", filterDto.getPendidikanTerakhir()));
    }

    // @PreAuthorize("hasAuthority('read-anggota')")
    public BaseResponse<AnggotaDto> getByNpa(String npa) {
        final long start = System.nanoTime();
        Anggota result = anggotaRepository
                .findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "User Not Found"));
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDtoWithRole(result));
    }

    @Transactional(readOnly = true)
    public BaseResponse<AnggotaDetailDto> getDetailByNpa(String npa) {
        final long start = System.nanoTime();
        Anggota result = anggotaRepository
                .findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "User Not Found"));
        if (!"ACTIVE".equals(result.getStatusAktif())) {
            throw new BusinessException(
                    HttpStatus.BAD_REQUEST,
                    "STATUS_ANGGOTA_IS_".concat(result.getStatusAktif()),
                    "Status anggota ".concat(result.getStatusAktif()));
        }
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDtoWithRole(result));
    }

    public Anggota findByNpa(String npa) {
        return anggotaRepository
                .findByNpa(npa)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND,
                        GlobalMessage.ANGGOTA_NOT_FOUND.code,
                        GlobalMessage.ANGGOTA_NOT_FOUND.message));
    }

    public Anggota getById(String idAnggota) {
        Optional<Anggota> optionalAnggota = anggotaRepository.findById(idAnggota);
        if (!optionalAnggota.isPresent()) {
            throw new BusinessException(
                    HttpStatus.NOT_FOUND,
                    GlobalMessage.ANGGOTA_NOT_FOUND.code,
                    GlobalMessage.ANGGOTA_NOT_FOUND.message);
        }
        return optionalAnggota.get();
    }

    // @PreAuthorize("hasAuthority('read-anggota')")
    public BaseResponse<AnggotaDto> updateCurrentAnggota(
            Principal principal, String npa, AnggotaRequestDto anggotaDto) {
        final long start = System.nanoTime();

        if (!principal.getName().equalsIgnoreCase(anggotaDto.getNpa())) {
            throw new BusinessException(HttpStatus.FORBIDDEN, "access_denied", "Access is denied");
        }

        Anggota anggotaExist = anggotaRepository
                .findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "User Not Found"));

        this.setReferenceUpdateData(anggotaDto, anggotaExist);
        anggotaExist.setNpa(npa);
        anggotaExist.setLastUpdated(LocalDateTime.now());
        Anggota result = anggotaRepository.save(anggotaExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(result));
    }

    public BaseResponse<AnggotaDto> create(Principal principal, AnggotaRequestDto anggotaDto) {
        final long start = System.nanoTime();

        Login user = loginService.getUserByNpa(principal.getName());
        Anggota anggota = convertToEntity(anggotaDto);
        this.setReferenceData(anggota, anggotaDto);
        anggota.setFoto("default.png");
        anggota.setCreatedDate(LocalDateTime.now());
        anggota.setCreatedBy(user.getIdLogin());
        anggota.setLastUpdated(LocalDateTime.now());
        Anggota result = anggotaRepository.save(anggota);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(result));
    }

    private AnggotaDto convertToDto(Anggota anggota) {
        return modelMapper.map(anggota, AnggotaDto.class);
    }

    private AnggotaDto convertToDtoWithRole(Anggota anggota) {
        AnggotaDto anggotaDto = modelMapper.map(anggota, AnggotaDto.class);
        Set<RoleDto> roleDtos = loginRepository
                .findByAnggotaNpa(anggota.getNpa())
                .map(login -> login.getRoles().stream().map(this::convertToDto).collect(Collectors.toSet()))
                .orElse(new HashSet<>());
        anggotaDto.setRole(roleDtos);
        return anggotaDto;
    }

    private AnggotaDetailDto convertToDetailDto(Anggota anggota) {
        Set<RoleDto> roleDtos = loginRepository
                .findByAnggotaNpa(anggota.getNpa())
                .map(login -> login.getRoles().stream().map(this::convertToDto).collect(Collectors.toSet()))
                .orElse(new HashSet<>());
        AnggotaDetailDto detailDto = modelMapper.map(anggota, AnggotaDetailDto.class);
        detailDto.setRole(roleDtos);
        return detailDto;
    }

    private void setReferenceData(Anggota anggota, AnggotaRequestDto dto) {
        if (anggotaRepository.findByNpa(dto.getNpa()).isPresent()) {
            throw new BusinessException(HttpStatus.FOUND, "NPA_ALREADY_EXIST", "NPA ALREADY EXIST");
        }
        Provinsi provinsi = helperUtilService.getProvinsiById(dto.getProvinsi().getId());
        Kabupaten kabupaten =
                helperUtilService.getKabupatenById(dto.getKabupaten().getId());
        Kecamatan kecamatan =
                helperUtilService.getKecamatanById(dto.getKecamatan().getId());
        MasterPendidikan masterPendidikan = helperUtilService.getMasterPendidikanById(
                dto.getJenjangPendidikan().getId());
        Desa desa = helperUtilService.getDesaByid(dto.getDesa().getId());
        Wilayah pw = wilayahService.getByKodeWilayah(dto.getPw().getKdPw());
        Cabang pc = cabangService.getByKodeCabang(dto.getPc().getKdPc());
        Daerah pd = daerahService.getByKodeDaerah(dto.getPd().getKdPd());
        anggota.setKecamatan(kecamatan);
        anggota.setJenjangPendidikan(masterPendidikan);
        anggota.setProvinsi(provinsi);
        anggota.setDesa(desa);
        anggota.setPc(pc);
        anggota.setPw(pw);
        anggota.setPd(pd);
        anggota.setKabupaten(kabupaten);
    }

    private Anggota convertToEntity(AnggotaRequestDto anggotaDto) {
        return modelMapper.map(anggotaDto, Anggota.class);
    }

    public BaseResponse<AnggotaDto> update(Principal principal, String idAnggota, AnggotaRequestDto anggotaDto) {
        final long start = System.nanoTime();
        Anggota anggotaExist = anggotaRepository
                .findById(idAnggota)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND,
                        GlobalMessage.ANGGOTA_NOT_FOUND.code,
                        GlobalMessage.ANGGOTA_NOT_FOUND.message));
        this.setReferenceUpdateData(anggotaDto, anggotaExist);
        anggotaExist.setLastUpdated(LocalDateTime.now());
        Anggota result = anggotaRepository.save(anggotaExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(result));
    }

    private void setReferenceUpdateData(AnggotaRequestDto anggotaDto, Anggota anggotaExist) {

        boolean npaIsNotUsed = anggotaRepository
                .findByNpa(anggotaDto.getNpa())
                .map(anggota -> anggota.getId().equals(anggotaExist.getId()))
                .orElse(true);

        if (!npaIsNotUsed) {
            throw new BusinessException(
                    HttpStatus.FOUND, GlobalMessage.NPA_ALREADY_EXIST.code, GlobalMessage.NPA_ALREADY_EXIST.message);
        }

        Provinsi provinsi = Optional.ofNullable(anggotaDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .map(helperUtilService::getProvinsiById)
                .orElse(anggotaExist.getProvinsi());
        anggotaExist.setProvinsi(provinsi);
        anggotaDto.setProvinsi(null);

        Kabupaten kabupaten = Optional.ofNullable(anggotaDto.getKabupaten())
                .map(KabupatenDto::getId)
                .map(helperUtilService::getKabupatenById)
                .orElse(anggotaExist.getKabupaten());
        anggotaExist.setKabupaten(kabupaten);
        anggotaDto.setKabupaten(null);

        Kecamatan kecamatan = Optional.ofNullable(anggotaDto.getKecamatan())
                .map(KecamatanDto::getId)
                .map(helperUtilService::getKecamatanById)
                .orElse(anggotaExist.getKecamatan());
        anggotaExist.setKecamatan(kecamatan);
        anggotaDto.setKecamatan(null);

        Wilayah wilayah = Optional.ofNullable(anggotaDto.getPw())
                .map(WilayahDto::getKdPw)
                .map(wilayahService::getByKodeWilayah)
                .orElse(anggotaExist.getPw());
        anggotaExist.setPw(wilayah);
        anggotaDto.setPw(null);

        Cabang cabang = Optional.ofNullable(anggotaDto.getPc())
                .map(CabangRequest::getKdPc)
                .map(cabangService::getByKodeCabang)
                .orElse(anggotaExist.getPc());
        anggotaExist.setPc(cabang);
        anggotaDto.setPc(null);

        Daerah daerah = Optional.ofNullable(anggotaDto.getPd())
                .map(DaerahRequest::getKdPd)
                .map(daerahService::getByKodeDaerah)
                .orElse(anggotaExist.getPd());
        anggotaExist.setPd(daerah);
        anggotaDto.setPd(null);

        Desa desa = Optional.ofNullable(anggotaDto.getDesa())
                .map(DesaDto::getId)
                .map(helperUtilService::getDesaByid)
                .orElse(anggotaExist.getDesa());
        anggotaExist.setDesa(desa);
        anggotaDto.setDesa(null);

        MasterPendidikan masterPendidikan = Optional.ofNullable(anggotaDto.getJenjangPendidikan())
                .map(MasterPendidikanDto::getId)
                .map(helperUtilService::getMasterPendidikanById)
                .orElse(anggotaExist.getJenjangPendidikan());
        anggotaExist.setJenjangPendidikan(masterPendidikan);
        anggotaDto.setJenjangPendidikan(null);

        modelMapper.map(anggotaDto, anggotaExist);
    }

    public BaseResponse<CheckAnggotaDto> check(CheckAnggotaRequest request) {
        return anggotaRepository
                .findByNpa(request.getNpa())
                .map(anggota -> ResponseBuilder.<BaseResponse<CheckAnggotaDto>>buildResponse(
                        HttpStatus.OK, 0, "", CheckAnggotaDto.valueOf(anggota)))
                .orElseGet(() ->
                        ResponseBuilder.buildResponse(HttpStatus.NOT_FOUND, 0, "Data anggota tidak ditemukan", null));
    }

    public void sendEmail(SendEmailAnggotaRequest request) throws MessagingException {
        Anggota anggota = anggotaRepository
                .findByNpa(request.getNpa())
                .orElseThrow(() -> new ResourceNotFoundException("Data anggota tidak ditemukan"));

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("anggota", anggota);

        mailService.sendMail(
                new MailMessageContent("Selamat datang di ANA Online", request.getEmail(), "welcome", model));
    }

    public BaseResponse<String> sendEmailActivation(SendEmailAnggotaRequest request) throws MessagingException {
        Optional<Login> login = loginRepository.findByAnggotaNpa(request.getNpa());
        Login loginData = null;
        if (login.isPresent()) {
            throw new BadRequestException("Anggota sudah terdaftar");
        }

        Anggota anggota = anggotaRepository
                .findByNpa(request.getNpa())
                .orElseThrow(() -> new BadRequestException("Database Anggota belum ada. Silahkan hubungi PC Admin"));

        String code = DigestUtils.sha256Hex(request.getNpa());
        String passwordRandom = RandomStringUtils.randomAlphanumeric(8);
        String hashPassword = new BCryptPasswordEncoder(10, new SecureRandom()).encode(passwordRandom);
        Role role =
                roleRepository.findById(1L).orElseThrow(() -> new BadRequestException("Role tidak ada di database"));
        Set<Role> rolesSet = new HashSet<>();
        rolesSet.add(role);

        loginData = Login.builder()
                .activCode(code)
                .anggota(anggota)
                .roles(rolesSet)
                .password(hashPassword)
                .createdAt(new Timestamp(new Date().getTime()))
                .updatedAt(new Timestamp(new Date().getTime()))
                .statusAktif(0)
                .emailConfirm(0)
                .build();

        loginRepository.save(loginData);

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("anggota", anggota);
        model.put("password", passwordRandom);
        model.put("link", MessageFormat.format("{0}/register/activation/{1}", apiConfig.getWebUrl(), code));

        mailService.sendMail(
                new MailMessageContent("Aktivasi Akun ANA Online", anggota.getEmail(), "activation", model));

        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                1,
                GlobalMessage.SUCCESS.message,
                "Email berhasil terkirim. Silahkan cek email untuk aktivasi akun");
    }

    public Long countAnggota() {
        return anggotaRepository.countByDeletedAtIsNull();
    }

    public List<Anggota> findAllByDeletedAtIsNull() {
        return anggotaRepository.findAllByDeletedAtIsNull();
    }

    public BaseResponse<String> uploadFotoAngotaByNpa(Principal principal, String npa, MultipartFile file) {
        final long start = System.nanoTime();
        Anggota anggota = setReferenceAndValidateData(npa, file);
        anggotaRepository.save(anggota);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                ((end - start) / 1000000),
                GlobalMessage.SUCCESS.message,
                "successfully changed foto anggota : " + anggota.getNama());
    }

    private Anggota setReferenceAndValidateData(String npa, MultipartFile file) {
        Anggota anggota = anggotaRepository
                .findByNpa(npa)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND,
                        GlobalMessage.ANGGOTA_NOT_FOUND.code,
                        GlobalMessage.ANGGOTA_NOT_FOUND.message));
        String fileName = ANGGOTA.concat(anggota.getNama())
                .concat("_")
                .concat(LocalDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy_hhmmss")))
                .concat(".")
                .concat(Objects.requireNonNull(StringUtils.getFilenameExtension(file.getOriginalFilename())));
        String prefixFileName = BASE_PREFIX_IMAGE_ANGGOTA.concat(fileName);
        storageService.uploadFile(file, prefixFileName);
        anggota.setFoto(fileName);
        return anggota;
    }

    public Map<String, Object> downloadFotoAngotaByNpa(String npa, Principal principal) {
        return getDownloadFotoMap(npa);
    }

    public BaseResponse<BaseImageDto> detailFotoAnggotaByNpa(Principal principal, String npa) {
        return getDetailFoto(npa);
    }

    public BaseResponse<String> uploadFotoProfile(Principal principal, MultipartFile file) {
        final long start = System.nanoTime();
        Anggota anggota = setReferenceAndValidateData(principal.getName(), file);
        anggotaRepository.save(anggota);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                ((end - start) / 1000000),
                GlobalMessage.SUCCESS.message,
                "Successfully changed your profile picture");
    }

    public BaseResponse<BaseImageDto> detailFotoProfile(Principal principal) {
        String npa = principal.getName();
        return getDetailFoto(npa);
    }

    private BaseResponse<BaseImageDto> getDetailFoto(String npa) {
        final long start = System.nanoTime();
        Anggota anggota = anggotaRepository
                .findByNpa(npa)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, GlobalMessage.NPA_NOT_FOUND.code, GlobalMessage.NPA_NOT_FOUND.message));
        String prefixFotoLocated;
        if ("default.png".equals(anggota.getFoto())) {
            prefixFotoLocated = STATIC_PATH.concat(anggota.getFoto());
        } else {
            prefixFotoLocated = BASE_PREFIX_IMAGE_ANGGOTA.concat(anggota.getFoto());
        }
        String imgScr = storageService.getUriEndPoint(prefixFotoLocated);

        BaseImageDto baseImageDto = BaseImageDto.builder()
                .imgScr(imgScr)
                .name(anggota.getNama().concat("(" + anggota.getNpa() + ")"))
                .build();
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, baseImageDto);
    }

    public Map<String, Object> downloadFotoProfile(Principal principal) {
        return getDownloadFotoMap(principal.getName());
    }

    private Map<String, Object> getDownloadFotoMap(String npa) {
        Anggota anggota = anggotaRepository
                .findByNpa(npa)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND,
                        GlobalMessage.ANGGOTA_NOT_FOUND.code,
                        GlobalMessage.ANGGOTA_NOT_FOUND.message));
        String prefixFotoLocated;
        if ("default.png".equals(anggota.getFoto())) {
            prefixFotoLocated = STATIC_PATH.concat(anggota.getFoto());
        } else {
            prefixFotoLocated = BASE_PREFIX_IMAGE_ANGGOTA.concat(anggota.getFoto());
        }

        byte[] imageByte = storageService.downloadFile(prefixFotoLocated);
        Map<String, Object> result = new LinkedHashMap<>();
        result.put(DATA, imageByte);
        result.put(FILE_NAME, anggota.getFoto());
        return result;
    }

    @Transactional(readOnly = true)
    public BaseResponse<AnggotaSimpleResponse> getDetailAnggotaByNpa(String npa) {
        final long start = System.nanoTime();
        Anggota result = anggotaRepository
                .findByNpa(npa)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "USER_NOT_FOUND", "User Not Found"));
        if (!"ACTIVE".equals(result.getStatusAktif())) {
            throw new BusinessException(HttpStatus.BAD_REQUEST, "ANGGOTA_NOT_ACTIVE", "Status anggota tidak aktif");
        }
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToSimpleDto(result));
    }

    private AnggotaSimpleResponse convertToSimpleDto(Anggota result) {
        String prefixFotoLocated;
        if ("default.png".equals(result.getFoto())) {
            prefixFotoLocated = STATIC_PATH.concat(result.getFoto());
        } else {
            prefixFotoLocated = BASE_PREFIX_IMAGE_ANGGOTA.concat(result.getFoto());
        }
        String imgScr = storageService.getUriEndPoint(prefixFotoLocated);
        Set<RoleDto> roleDtos = loginRepository
                .findByAnggotaNpa(result.getNpa())
                .map(login -> login.getRoles().stream().map(this::convertToDto).collect(Collectors.toSet()))
                .orElse(new HashSet<>());
        return AnggotaSimpleResponse.builder()
                .email(result.getEmail())
                .noTelpon(result.getNoTelpon())
                .foto(imgScr)
                .nama(result.getNama())
                .pc(AnggotaSimpleResponse.Pc.builder()
                        .namaPc(result.getPc().getNamaPc())
                        .kdPc(result.getPc().getKdPc())
                        .build())
                .pw(AnggotaSimpleResponse.Pw.builder()
                        .namaWilayah(result.getPw().getNamaWilayah())
                        .kdPw(result.getPw().getKdPw())
                        .build())
                .pd(AnggotaSimpleResponse.Pd.builder()
                        .namaPd(result.getPd().getNamaPd())
                        .kdPd(result.getPd().getKdPd())
                        .build())
                .role(roleDtos)
                .build();
    }

    private RoleDto convertToDto(Role role) {
        return modelMapper.map(role, RoleDto.class);
    }

    public List<List<String>> getContactNumberAllAnggota(int maxContactNumber) {
        Pageable pageable = PageRequest.of(0, maxContactNumber);
        Slice<Anggota> slice;
        List<String> contactList = new ArrayList<>();
        List<CompletableFuture<List<String>>> futures = new ArrayList<>();
        do {
            log.info("[PAGE SLICE] {}", pageable);
            slice = anggotaRepository.findAllByAnggota(pageable);
            CompletableFuture<List<String>> batchFutures = executeAsync(slice);
            pageable = slice.nextPageable();
            futures.add(batchFutures);
        } while (slice.hasNext());
        CompletableFuture<Void> batchFuture = CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]));
        try {
            batchFuture.get();
            futures.forEach(future -> {
                try {
                    if (!future.get().isEmpty()) contactList.addAll(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    log.error("[GET CONTACT ERROR ASYNC] {}", e.getMessage());
                    throw new RuntimeException(e);
                }
            });
        } catch (InterruptedException | ExecutionException e) {
            log.error("[GET CONTACT ERROR ASYNC-2] {}", e.getMessage());
            throw new RuntimeException(e);
        }
        if (contactList.isEmpty())
            throw new BusinessException(HttpStatus.BAD_REQUEST, "ANGGOTA_NOT_FOUND", "ANGGOTA_NOT_FOUND");
        return Lists.partition(contactList.stream().distinct().toList(), maxContactNumber);
    }

    public CompletableFuture<List<String>> executeAsync(Slice<Anggota> slice) {
        return CompletableFuture.supplyAsync(() -> {
            List<String> contactList = new ArrayList<>();
            slice.getContent().forEach(anggota -> {
                String contact = StringUtil.getValidContactNumber(anggota.getNoTelpon());
                String contact2 = StringUtil.getValidContactNumber(anggota.getNoTelpon2());
                if (Objects.nonNull(contact)) {
                    contactList.add(contact);
                }
                if (Objects.nonNull(contact2)) {
                    contactList.add(contact2);
                }
            });
            log.info("[GET DATA CONTACT] size batch  {}", slice.getContent().size());
            return contactList;
        });
    }
}
