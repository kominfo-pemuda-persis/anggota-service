package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Pekerjaan;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PekerjaanRepository extends JpaRepository<Pekerjaan, Long> {
    Page<Pekerjaan> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Pekerjaan> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Pekerjaan> findByDeletedAtIsNotNullAndAnggotaNpaAndIdPekerjaan(String idAnggota, Long idPekerjaan);

    Optional<Pekerjaan> findByDeletedAtIsNullAndAnggotaNpaAndIdPekerjaan(String idAnggota, Long idPekerjaan);
}
