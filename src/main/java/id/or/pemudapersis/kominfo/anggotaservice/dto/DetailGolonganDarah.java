package id.or.pemudapersis.kominfo.anggotaservice.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DetailGolonganDarah {
    @JsonProperty("golonganDarah")
    private String type;

    @JsonProperty("jumlah")
    private Long value;
}
