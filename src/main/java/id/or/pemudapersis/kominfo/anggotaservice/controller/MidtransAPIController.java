package id.or.pemudapersis.kominfo.anggotaservice.controller;

import id.or.pemudapersis.kominfo.anggotaservice.dto.midtransDTO.Item;
import id.or.pemudapersis.kominfo.anggotaservice.service.MidtransService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/api/midtrans")
@Tag(name = "Midtrans Transaction", description = "Endpoint for managing Midtrans Transaction")
public class MidtransAPIController {

    private final MidtransService midtransService;

    public MidtransAPIController(MidtransService midtransService) {
        this.midtransService = midtransService;
    }

    @PostMapping(value = "/checkout")
    @Operation(
            summary = "Get redirectedUrl for infaq payments",
            description = "Get redirectedUrl for infaq payments.",
            tags = {"Midtrans Transaction"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> checkout(@RequestBody List<Item> requestItems) {

        Map<String, Object> response = new HashMap<>();
        String error = null;
        Object data = null;

        Long start = System.nanoTime();

        if (requestItems.size() == 0) {
            error = "Items are empty. Please insert atleast 1 Item.";
        } else {
            data = midtransService.checkout(requestItems);
        }

        Long end = System.nanoTime();

        response.put("took", ((end - start) / 1000000));
        response.put("status", HttpStatus.OK);
        response.put("data", data);
        response.put("errors", error);

        return ResponseEntity.ok(response);
    }
}
