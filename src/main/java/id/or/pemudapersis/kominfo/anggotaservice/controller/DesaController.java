package id.or.pemudapersis.kominfo.anggotaservice.controller;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Desa;
import id.or.pemudapersis.kominfo.anggotaservice.repository.DesaRepository;
import id.or.pemudapersis.kominfo.anggotaservice.response.ResponseHandler;
import id.or.pemudapersis.kominfo.anggotaservice.specification.WilayahSpecification;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/v1/api/desa")
@Tag(name = "Desa", description = "Endpoint for managing Desa")
public class DesaController {

    private static final String RETRIEVED = "Successfully retrieved data!";
    private final DesaRepository desaRepository;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Get all Desa ",
            description = "Get all Desa Data.",
            tags = {"Desa"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = Desa.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> getAllDesa(
            @ParameterObject
                    @PageableDefault(
                            sort = {"id"},
                            direction = Sort.Direction.ASC,
                            value = 5)
                    Pageable pageable,
            @Parameter(description = "keyword Kode Desa or Name Desa")
                    @RequestParam(required = false, defaultValue = "")
                    String keyword,
            @Parameter(description = "kode kecamatan to get desa by specific kecamatan")
                    @RequestParam(required = false, defaultValue = "", name = "kode-kecamatan")
                    String kodeKecamatan) {
        try {
            Specification<Desa> spec =
                    WilayahSpecification.hasNamaKelurahan(keyword).or(WilayahSpecification.hasKodeKelurahan(keyword));

            if (StringUtils.isNotBlank(kodeKecamatan)) {
                spec = spec.and(WilayahSpecification.hasKecamatan(kodeKecamatan));
            }
            return ResponseHandler.generateResponse(
                    RETRIEVED, HttpStatus.OK, HttpStatus.OK, desaRepository.findAll(spec, pageable));
        } catch (ResponseStatusException e) {
            return ResponseHandler.generateResponse(e.getMessage(), HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND, null);
        } catch (Exception e) {
            return ResponseHandler.generateResponse(
                    e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR, null);
        }
    }
}
