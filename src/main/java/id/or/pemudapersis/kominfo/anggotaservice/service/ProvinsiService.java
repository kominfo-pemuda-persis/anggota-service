package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Provinsi;
import id.or.pemudapersis.kominfo.anggotaservice.mapper.ProvinsiMapper;
import id.or.pemudapersis.kominfo.anggotaservice.repository.ProvinsiRepository;
import id.or.pemudapersis.kominfo.anggotaservice.specification.WilayahSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProvinsiService {
    private final ProvinsiRepository provinsiRepository;
    private final ProvinsiMapper provinsiMapper;

    public Page<Provinsi> getAllProvinsi(String keyword, Pageable page) {
        return provinsiRepository.findAll(
                WilayahSpecification.hasKodeProvinsi(keyword).or(WilayahSpecification.hasNamaProvinsi(keyword)), page);
    }
}
