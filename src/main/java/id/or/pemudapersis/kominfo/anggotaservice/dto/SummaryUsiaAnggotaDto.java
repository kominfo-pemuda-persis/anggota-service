package id.or.pemudapersis.kominfo.anggotaservice.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@SuperBuilder
public class SummaryUsiaAnggotaDto {
    private List<DetailUsiaAnggota> detailUsiaAnggotaList;
    private Long jumlahTotal;
}
