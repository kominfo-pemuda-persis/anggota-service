package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class MonografiPdDto extends GeograficDto {
    @Id
    private Long id;

    private DaerahRequest daerah;

    private String namaPd;

    private WilayahDto wilayah;

    private ProvinsiDto provinsi;

    private KabupatenDto kabupaten;

    private JamiyyahPdDto jamiyyahPd;

    private String foto;
}
