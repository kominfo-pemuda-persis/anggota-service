package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.utils.validation.ValidateString;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PendidikanDto {
    private Long idPendidikan;

    // private Anggota anggota;

    private MasterPendidikanDto jenjangPendidikan;

    private String instansi;

    private String jurusan;

    //    @JsonDeserialize(using = LocalDateDeserializer.class)
    //    @JsonSerialize(using = LocalDateSerializer.class)
    //    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
    //    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private int tahunMasuk;

    //    @JsonDeserialize(using = LocalDateDeserializer.class)
    //    @JsonSerialize(using = LocalDateSerializer.class)
    //    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy")
    //    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private int tahunKeluar;

    @ValidateString(
            acceptedValues = {"Formal", "Non Formal"},
            message = "Jenis Pendidikan must match : {acceptedValues}")
    private String jenisPendidikan;
}
