package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.utils.validation.ValidateString;
import jakarta.validation.constraints.NotEmpty;
import lombok.*;
import lombok.experimental.SuperBuilder;

/**
 * Project: anggota-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 07.08
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode
public class BaseMessageWaRequest {
    @NotEmpty(message = "MESSAGE_NOT_EMPTY")
    private String message;

    @ValidateString(acceptedValues = {"text", "image", "file"})
    @NotEmpty(message = "MESSAGE_TYPE_NOT_EMPTY")
    private String messageType;

    private Integer delay;

    private String imageUrl;
}
