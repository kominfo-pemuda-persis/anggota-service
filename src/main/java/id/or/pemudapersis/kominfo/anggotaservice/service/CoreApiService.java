package id.or.pemudapersis.kominfo.anggotaservice.service;

import static org.springframework.http.MediaType.APPLICATION_JSON;

import id.or.pemudapersis.kominfo.anggotaservice.config.WhatsAppsConfig;
import id.or.pemudapersis.kominfo.anggotaservice.dto.MessageWhatsAppsRequest;
import id.or.pemudapersis.kominfo.anggotaservice.dto.MessageWhatsAppsResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClient;

/**
 * Project: anggota-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 14/07/24
 * Time: 08.23
 * <p>
 * Created with IntelliJ IDEA
 */
@Service
public class CoreApiService {
    private final WhatsAppsConfig whatsAppsConfig;

    public CoreApiService(WhatsAppsConfig whatsAppsConfig) {
        this.whatsAppsConfig = whatsAppsConfig;
    }

    public MessageWhatsAppsResponse executeSendMessageWa(MessageWhatsAppsRequest messageWhatsAppsRequest) {
        RestClient restClient = RestClient.create();
        return restClient
                .post()
                .uri(whatsAppsConfig.getBaseUrl() + whatsAppsConfig.getSendApi())
                .contentType(APPLICATION_JSON)
                .body(messageWhatsAppsRequest)
                .retrieve()
                .onStatus(HttpStatusCode::isError, (request, response) -> {
                    throw new BusinessException(
                            HttpStatus.INTERNAL_SERVER_ERROR, "SEND_MESSAGE_ERROR", "SEND_MESSAGE_ERROR");
                })
                .body(MessageWhatsAppsResponse.class);
    }
}
