package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPw;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PerformaPwRepository extends JpaRepository<PerformaPw, Long> {
    Page<PerformaPw> findByDeletedAtIsNull(Pageable pageable);

    Optional<PerformaPw> findByWilayahKdPwAndDeletedAtIsNull(String kodeWilayah);

    Optional<PerformaPw> findByIdAndDeletedAtIsNull(Long id);

    Optional<PerformaPw> findByIdAndDeletedAtIsNotNull(Long id);

    Page<PerformaPw> findByDeletedAtIsNotNull(Pageable pageReq);

    Long countByDeletedAtIsNull();
}
