package id.or.pemudapersis.kominfo.anggotaservice.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KabupatenDto {
    @NotEmpty
    private String id;

    private String nama;

    // private ProvinsiDto provinsi;
}
