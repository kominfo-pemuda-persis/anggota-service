package id.or.pemudapersis.kominfo.anggotaservice.controller;

import id.or.pemudapersis.kominfo.anggotaservice.service.AuthService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/api/auth")
@Tag(name = "User Login", description = "Endpoint for managing User Login")
class AuthController {

    private final AuthService authService;

    @PostMapping
    AuthDto post(HttpServletRequest servletRequest, @RequestBody AuthRequest request) {
        return AuthDto.from(authService.authenticate(request.toAuthInput(servletRequest.getServerName())));
    }
}
