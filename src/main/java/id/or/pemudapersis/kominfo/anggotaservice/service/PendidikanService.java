package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MasterPendidikanDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.PendidikanDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import id.or.pemudapersis.kominfo.anggotaservice.entity.MasterPendidikan;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Pendidikan;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MasterPendidikanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.PendidikanRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.time.LocalDateTime;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PendidikanService {
    public static final String SUCCESS = "SUCCESS";

    private static final String PENDIDIKAN_NOT_FOUND_ERROR_CODE = "PENDIDIKAN_NOT_FOUND";
    private static final String PENDIDIKAN_NOT_FOUND_ERROR_MESSAGE = "Pendidikan Not Found";

    private final PendidikanRepository pendidikanRepository;

    private final AnggotaService anggotaService;

    private final MasterPendidikanRepository masterPendidikanRepository;

    private final ModelMapper modelMapper;

    public PendidikanService(
            PendidikanRepository pendidikanRepository,
            AnggotaService anggotaService,
            MasterPendidikanRepository masterPendidikanRepository,
            ModelMapper modelMapper) {
        this.pendidikanRepository = pendidikanRepository;
        this.anggotaService = anggotaService;
        this.masterPendidikanRepository = masterPendidikanRepository;
        this.modelMapper = modelMapper;
    }

    public BaseResponse<Page<PendidikanDto>> pendidikanList(Pageable pageable, boolean isDeleted, String npa) {
        final long start = System.nanoTime();

        Page<PendidikanDto> pendidikanPage;
        if (!isDeleted)
            pendidikanPage = pendidikanRepository
                    .findByDeletedAtIsNullAndAnggotaNpa(npa, pageable)
                    .map(this::convertToDto);
        else
            pendidikanPage = pendidikanRepository
                    .findByDeletedAtIsNotNullAndAnggotaNpa(npa, pageable)
                    .map(this::convertToDto);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, pendidikanPage);
    }

    private PendidikanDto convertToDto(Pendidikan pendidikan) {
        return modelMapper.map(pendidikan, PendidikanDto.class);
    }

    public BaseResponse<PendidikanDto> create(String npa, PendidikanDto pendidikanDto) {
        final long start = System.nanoTime();

        Anggota anggota = anggotaService.findByNpa(npa);

        MasterPendidikan jenjangPendidikan = masterPendidikanRepository
                .findById(pendidikanDto.getJenjangPendidikan().getId())
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, "JENJANG_PENDIDIKAN_NOT_FOUND", "Jenjang Pendidikan Not Found"));

        Pendidikan pendidikan = Pendidikan.builder()
                .anggota(anggota)
                .instansi(pendidikanDto.getInstansi())
                .jenisPendidikan(pendidikanDto.getJenisPendidikan())
                .jenjangPendidikan(jenjangPendidikan)
                .jurusan(pendidikanDto.getJurusan())
                .tahunKeluar(pendidikanDto.getTahunKeluar())
                .tahunMasuk(pendidikanDto.getTahunMasuk())
                .build();
        Pendidikan result = pendidikanRepository.save(pendidikan);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, convertToDto(result));
    }

    public BaseResponse<PendidikanDto> update(String npa, Long idPendidikan, PendidikanDto pendidikanDto) {
        final long start = System.nanoTime();

        Pendidikan pendidikanExist = pendidikanRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(npa, idPendidikan)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PENDIDIKAN_NOT_FOUND_ERROR_CODE, PENDIDIKAN_NOT_FOUND_ERROR_MESSAGE));

        String idJenjangPendidikan = Optional.ofNullable(pendidikanDto.getJenjangPendidikan())
                .map(MasterPendidikanDto::getId)
                .orElse(pendidikanExist.getJenjangPendidikan().getId());

        MasterPendidikan jenjangPendidikan = masterPendidikanRepository
                .findById(idJenjangPendidikan)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, "JENJANG_PENDIDIKAN_NOT_FOUND", "Jenjang Pendidikan Not Found"));

        pendidikanDto.setJenjangPendidikan(null);
        pendidikanDto.setIdPendidikan(null);
        updateEntityFromDto(pendidikanDto, pendidikanExist);
        pendidikanExist.setJenjangPendidikan(jenjangPendidikan);

        Pendidikan result = pendidikanRepository.save(pendidikanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, convertToDto(result));
    }

    public BaseResponse<PendidikanDto> delete(String npa, Long idPendidikan) {
        final long start = System.nanoTime();

        Pendidikan pendidikanExist = pendidikanRepository
                .findByDeletedAtIsNullAndAnggotaNpaAndIdPendidikan(npa, idPendidikan)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PENDIDIKAN_NOT_FOUND_ERROR_CODE, PENDIDIKAN_NOT_FOUND_ERROR_MESSAGE));

        pendidikanExist.setDeletedAt(LocalDateTime.now());
        pendidikanRepository.save(pendidikanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, null);
    }

    public BaseResponse<PendidikanDto> restore(String npa, Long idPendidikan) {
        final long start = System.nanoTime();

        Pendidikan pendidikanExist = pendidikanRepository
                .findByDeletedAtIsNotNullAndAnggotaNpaAndIdPendidikan(npa, idPendidikan)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND, PENDIDIKAN_NOT_FOUND_ERROR_CODE, PENDIDIKAN_NOT_FOUND_ERROR_MESSAGE));

        pendidikanExist.setDeletedAt(null);
        Pendidikan result = pendidikanRepository.save(pendidikanExist);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), SUCCESS, convertToDto(result));
    }

    private void updateEntityFromDto(PendidikanDto from, Pendidikan to) {
        modelMapper.map(from, to);
    }
}
