package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Otonom;
import id.or.pemudapersis.kominfo.anggotaservice.utils.validation.ValidateString;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KeluargaDto {
    private Long idKeluarga;

    @NotEmpty(message = "nama keluarga not empty")
    private String namaKeluarga;

    @ValidateString(
            acceptedValues = {"Ayah", "Ibu", "Istri", "Suami", "Anak"},
            message = "Hubungan must match : {acceptedValues}")
    private String hubungan;

    private int jumlahAnak;

    private String alamat;

    private AnggotaDto anggota;

    private Otonom otonom;

    private String keterangan;

    @ValidateString(
            acceptedValues = {"Anggota", "Bukan Anggota"},
            message = "Status Anggota must match : {acceptedValues}")
    private String statusAnggota;
}
