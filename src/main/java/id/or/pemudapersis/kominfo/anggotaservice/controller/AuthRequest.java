package id.or.pemudapersis.kominfo.anggotaservice.controller;

import id.or.pemudapersis.kominfo.anggotaservice.service.AuthenticateInput;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

@Value
@RequiredArgsConstructor
class AuthRequest {
    String npa;

    @ToString.Exclude
    String password;

    AuthRequest() {
        npa = "";
        password = "";
    }

    AuthenticateInput toAuthInput(String host) {
        return new AuthenticateInput(npa, password, host);
    }
}
