package id.or.pemudapersis.kominfo.anggotaservice.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SummaryGolDarahDto {
    private List<DetailGolonganDarah> golonganDarahList;
    private Long jumlahTotal;
}
