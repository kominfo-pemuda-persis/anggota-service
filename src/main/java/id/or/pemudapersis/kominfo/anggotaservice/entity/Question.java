package id.or.pemudapersis.kominfo.anggotaservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class Question {
    private String q1a;
    private String q2a;
    private String q3a;
    private String q4a;
    private String q5a;
    private String q6a;
    private String q1b;
    private String q2b;
    private String q3b;
    private String q4b;
    private String q5b;
    private String q6b;
    private String q7b;
    private String q8b;
    private String q9b;
    private String q10b;
    private String q1c;
    private String q2c;
    private String q3c;
    private String q1d;
    private String q2d;
    private String q3d;
    private String q4d;
    private String q5d;
    private String q6d;
    private String q7d;
    private String q8d;
    private String q9d;
    private String q10d;
    private String q11d;
    private String q12d;
    private String q13d;
    private String q14d;
    private String q15d;
    private String q16d;
    private String q17d;
    private String q18d;
    private String q19d;
    private String q20d;
    private String q21d;
    private String q22d;
    private String q23d;
    private String q24d;
    private String q25d;
    private String q26d;
    private String q27d;
    private String q28d;
    private String q1e;
    private String q2e;
    private String q3e;
    private String q4e;
    private String q5e;
    private String q6e;
    private String q7e;
    private String q8e;
    private String q9e;
    private String q10e;
    private String q1f;
    private String q2f;
    private String q3f;
    private String q4f;
    private String q5f;
    private String q6f;
    private String q7f;

    @Column(name = "last_survey")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastSurvey;

    @Column(name = "created_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;

    @Column(name = "updated_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedDate;

    @Column(name = "deleted_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deletedAt;
}
