package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.KejamiyyahanPj;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KejamiyyahanPjRepository extends JpaRepository<KejamiyyahanPj, Long> {

    Optional<KejamiyyahanPj> findByMonografiPj_id(Long kdMonografiPj);
    // public Optional<KejamiyyahanPj> findBymonografiPjAndIsDeletedFalse(Long kdMonografiPj);
    // public Page<KejamiyyahanPj> findByIsDeletedFalse(Pageable pageable);
}
