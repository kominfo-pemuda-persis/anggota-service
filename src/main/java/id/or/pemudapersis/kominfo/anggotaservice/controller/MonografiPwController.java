package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.beans.support.PagedListHolder.DEFAULT_PAGE_SIZE;
import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPwDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.MonografiPw;
import id.or.pemudapersis.kominfo.anggotaservice.service.MonografiPwService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/api/monografi/pw")
@Tag(name = "Monografi PW", description = "Endpoint for managing Monografi PW")
public class MonografiPwController {
    private final ModelMapper modelMapper;
    private final MonografiPwService monografiPwService;

    public MonografiPwController(ModelMapper modelMapper, MonografiPwService monografiPwService) {
        this.modelMapper = modelMapper;
        this.monografiPwService = monografiPwService;
    }

    @GetMapping
    @Operation(
            summary = "Get all monografi PW based on pagination",
            description = "Get all monografi PW based on pagination.",
            tags = {"Monografi PW"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<Object> getListMonografiPw(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "id")
                    String sortBy) {
        Map<String, Object> response = new LinkedHashMap<>();

        final long start = System.nanoTime();
        if (pageSize == null) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        if (pageNo == null) {
            pageNo = 0;
        }
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        List<MonografiPwDto> monografiPwDtos = monografiPwService.monografiPwList(paging, false).stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
        final long end = System.nanoTime();

        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", monografiPwDtos);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Add new data Monografi PW based on pagination",
            description = "Add new data Monografi PW based on pagination.",
            tags = {"Monografi PW"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> create(@RequestBody @Valid MonografiPwDto monografiPwDto) {
        Map<String, Object> response = new LinkedHashMap<>();

        final long start = System.nanoTime();
        MonografiPw monografiPw = convertToEntity(monografiPwDto);
        monografiPwService.create(monografiPw);
        final long end = System.nanoTime();

        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Edit data Monografi PW based on pagination",
            description = "Edit data Monografi PW based on pagination.",
            tags = {"Monografi PW"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> edit(@RequestBody @Valid MonografiPwDto monografiPwDto, @PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();

        final long start = System.nanoTime();
        monografiPwService.update(monografiPwDto, id);
        final long end = System.nanoTime();

        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Delete data Monografi PW based on pagination",
            description = "Delete data Monografi PW based on pagination.",
            tags = {"Monografi PW"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> delete(@PathVariable("id") Long id) {
        Map<String, Object> response = new LinkedHashMap<>();

        final long start = System.nanoTime();
        monografiPwService.delete(id);
        final long end = System.nanoTime();

        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    @PatchMapping(value = "/{id}/restore")
    @Operation(
            summary = "Restore data Monografi PW based on pagination",
            description = "Restore data Monografi PW based on pagination.",
            tags = {"Monografi PW"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = ResponseEntity.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<Object> restore(@PathVariable("id") Long id) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();
        monografiPwService.restore(id);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put(ControllerConstants.STATUS_RESPONSE_BODY_FIELD, HttpStatus.OK);
        response.put("data", null);
        response.put(ControllerConstants.ERRORS_RESPONSE_BODY_FIELD, null);
        return ok(response);
    }

    private MonografiPw convertToEntity(MonografiPwDto monografiPwDto) {
        return modelMapper.map(monografiPwDto, MonografiPw.class);
    }

    private MonografiPwDto convertToDto(MonografiPw monografiPw) {
        return modelMapper.map(monografiPw, MonografiPwDto.class);
    }
}
