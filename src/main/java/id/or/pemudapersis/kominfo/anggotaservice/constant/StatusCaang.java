package id.or.pemudapersis.kominfo.anggotaservice.constant;

public enum StatusCaang {
    INIT("INIT", "INIT"),
    APPROVED_BY_PC("APPROVED_BY_PC", "APPROVED_BY_PC"),
    APPROVED_BY_PD("APPROVED_BY_PD", "APPROVED_BY_PD"),
    APPROVED_BY_PW("APPROVED_BY_PW", "APPROVED_BY_PW"),
    APPROVED_BY_PP("APPROVED_BY_PP", "APPROVED_BY_PP");

    public String code;
    public String desc;

    StatusCaang(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
