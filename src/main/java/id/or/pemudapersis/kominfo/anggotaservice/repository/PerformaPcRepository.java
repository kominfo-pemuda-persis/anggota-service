package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.PerformaPc;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PerformaPcRepository extends JpaRepository<PerformaPc, Long> {
    Page<PerformaPc> findByDeletedAtIsNull(Pageable pageable);

    Optional<PerformaPc> findByCabangKdPcAndDeletedAtIsNull(String kodeCabang);

    Optional<PerformaPc> findByCabangKdPc(String kodeCabang);

    Optional<PerformaPc> findByIdAndDeletedAtIsNull(Long id);

    Optional<PerformaPc> findByIdAndDeletedAtIsNotNull(Long id);

    Page<PerformaPc> findByDeletedAtIsNotNull(Pageable pageReq);

    Long countByDeletedAtIsNull();
}
