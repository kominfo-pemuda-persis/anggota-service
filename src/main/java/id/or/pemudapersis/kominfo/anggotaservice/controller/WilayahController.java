package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.WilayahDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import id.or.pemudapersis.kominfo.anggotaservice.service.WilayahService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j2
@RequestMapping(value = "/v1/api/wilayah")
@Tag(name = "Wilayah", description = "Endpoint for managing Wilayah")
public class WilayahController {
    private static final int DEFAULT_PAGE_SIZE = 10;

    private final WilayahService wilayahService;
    private final ModelMapper modelMapper;

    public WilayahController(WilayahService wilayahService, ModelMapper modelMapper) {
        this.wilayahService = wilayahService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @Operation(
            summary = "Get all Wilayah by NPA Anggota Persis",
            description = "Get all Wilayah by NPA Anggota Persis.",
            tags = {"Wilayah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = WilayahDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<Page<WilayahDto>>> getListWilayah(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "kdPw")
                    String sortBy) {
        Map<Object, Object> response = new HashMap<>();
        final long start = System.nanoTime();
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<WilayahDto>> wilayahList = wilayahService.getAllWilayah(paging, false);
        final long end = System.nanoTime();
        response.put("took", ((end - start) / 1000000));
        response.put("status", HttpStatus.OK);
        response.put("data", wilayahList);
        response.put("errors", null);
        return ok(wilayahList);
    }

    private Wilayah convertToEntity(WilayahDto wilayahDto) {
        return modelMapper.map(wilayahDto, Wilayah.class);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Edit Wilayah by NPA Anggota Persis",
            description = "Edit Wilayah by NPA Anggota Persis.",
            tags = {"Wilayah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = WilayahDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<WilayahDto>> edit(
            @RequestBody @Valid WilayahDto wilayahDto, @PathVariable("id") String id) {
        Wilayah wilayah = convertToEntity(wilayahDto);
        BaseResponse<WilayahDto> baseResponse = wilayahService.updateWilayah(wilayah, id);
        return ok(baseResponse);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Add new data Wilayah by NPA Anggota Persis",
            description = "Add new data Wilayah by NPA Anggota Persis.",
            tags = {"Wilayah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = WilayahDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<WilayahDto>> create(@RequestBody @Valid WilayahDto wilayahDto) {
        Wilayah wilayah = convertToEntity(wilayahDto);
        BaseResponse<WilayahDto> baseResponse = wilayahService.create(wilayah);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{id}")
    @Operation(
            summary = "Delete data Wilayah by NPA Anggota Persis",
            description = "Delete data Wilayah by NPA Anggota Persis.",
            tags = {"Wilayah"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = WilayahDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<WilayahDto>> delete(
            @Parameter(description = "id wilayah") @PathVariable("id") String kodeWilayah, Principal principal) {
        BaseResponse<WilayahDto> baseResponse = wilayahService.delete(principal, kodeWilayah);
        return ok(baseResponse);
    }
}
