package id.or.pemudapersis.kominfo.anggotaservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jakarta.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.sql.Timestamp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_kejamiyyahan_pj")
public class KejamiyyahanPj implements Serializable {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "kode_pj", nullable = false)
    private String kodePj;

    @Column(name = "nama_pj", nullable = false)
    private String namaPj;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(
            name = "kd_monografi_pj",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_kejamiyyahan_pj_kd_monografi_pj_foreign"),
            nullable = false)
    @JsonDeserialize(as = MonografiPj.class)
    @JsonSerialize(as = MonografiPj.class)
    private MonografiPj monografiPj;

    @OneToOne
    @JoinColumn(
            name = "kd_pc",
            referencedColumnName = "kd_pc",
            foreignKey = @ForeignKey(name = "t_kejamiyyahan_pj_kd_pc_foreign"),
            nullable = false)
    @JsonDeserialize(as = Cabang.class)
    @JsonSerialize(as = Cabang.class)
    private Cabang kdPc;

    @OneToOne
    @JoinColumn(
            name = "kd_pd",
            referencedColumnName = "kd_pd",
            foreignKey = @ForeignKey(name = "t_kejamiyyahan_pj_kd_pd_foreign"),
            nullable = false)
    @JsonDeserialize(as = Daerah.class)
    @JsonSerialize(as = Daerah.class)
    private Daerah kdPd;

    @OneToOne
    @JoinColumn(
            name = "kd_pw",
            referencedColumnName = "kd_pw",
            foreignKey = @ForeignKey(name = "t_kejamiyyahan_pj_kd_pw_foreign"),
            nullable = false)
    @JsonDeserialize(as = Wilayah.class)
    @JsonSerialize(as = Wilayah.class)
    private Wilayah kdPw;

    @OneToOne
    @JoinColumn(
            name = "provinsi",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_kejamiyyahan_pj_provinsi_foreign"),
            nullable = false)
    @JsonDeserialize(as = Provinsi.class)
    @JsonSerialize(as = Provinsi.class)
    private Provinsi provinsi;

    @OneToOne
    @JoinColumn(
            name = "kabupaten",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_kejamiyyahan_pj_kabupaten_foreign"),
            nullable = false)
    @JsonDeserialize(as = Kabupaten.class)
    @JsonSerialize(as = Kabupaten.class)
    private Kabupaten kabupaten;

    @OneToOne
    @JoinColumn(
            name = "kecamatan",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_kejamiyyahan_pj_kecamatan_foreign"),
            nullable = false)
    @JsonDeserialize(as = Kecamatan.class)
    @JsonSerialize(as = Kecamatan.class)
    private Kecamatan kecamatan;

    @OneToOne
    @JoinColumn(
            name = "desa",
            referencedColumnName = "id",
            foreignKey = @ForeignKey(name = "t_kejamiyyahan_pj_desa_foreign"),
            nullable = false)
    @JsonDeserialize(as = Desa.class)
    @JsonSerialize(as = Desa.class)
    private Desa desa;

    @Column(name = "ketua", nullable = false)
    private String ketua;

    @Column(name = "sekretaris", nullable = false)
    private String sekretaris;

    @Column(name = "bendahara", nullable = false)
    private String bendahara;

    @Column(name = "hari_ngantor", nullable = false)
    private String hariNgantor;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    @Column(name = "waktu_ngantor", nullable = false)
    private Time waktuNgantor;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "musjam_terakhir_masehi", nullable = false)
    private Timestamp musjamTerakhirMasehi;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM;yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "musjam_terakhir_hijriyyah", nullable = false)
    private Timestamp musjamTerakhirHijriyyah;

    @Column(name = "anggota_biasa", nullable = false)
    private int anggotaBiasa;

    @Column(name = "anggota_luar_biasa", nullable = false)
    private int anggotaLuarBiasa;

    @Column(name = "tidak_herreg", nullable = false)
    private String tidakHerReg;

    @Column(name = "mutasi_persis", nullable = false)
    private String mutasiPersis;

    @Column(name = "mutasi_tempat", nullable = false)
    private String mutasiTempat;

    @Column(name = "meninggal_dunia", nullable = false)
    private String meninggalDunia;

    @Column(name = "mengundurkan_diri", nullable = false)
    private String mengundurkanDiri;

    @Column(name = "calon_anggota", nullable = false)
    private String calonAnggota;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "created_at")
    private Timestamp createdAt;

    //    @Column(name = "created_by")
    //    private String createdBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "updated_at")
    private Timestamp updatedAt;

    //    @Column(name = "updated_by")
    //    private String updatedBy;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "deleted_at")
    private Timestamp deletedAt;
}
