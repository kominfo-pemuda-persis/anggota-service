package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.dto.SummaryAnggotaPerWilayahDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.SummaryGolDarahDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.SummaryMonografiDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.SummaryPerformaDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.SummaryStatusKeanggotaanDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.SummaryStatusMaritalDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.SummaryStatusPendidikanDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.SummaryUsiaAnggotaDto;
import id.or.pemudapersis.kominfo.anggotaservice.service.SummaryService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.security.Principal;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/api/summaries")
@Tag(name = "Summary", description = "Endpoint for managing Summary")
public class SummaryController {

    private final SummaryService summaryService;

    public SummaryController(SummaryService summaryService) {
        this.summaryService = summaryService;
    }

    @GetMapping(value = "/performa")
    @Operation(
            summary = "Get all summaries data Indek Performa Anggota Persis",
            description = "Get all summaries data Indek Performa Anggota Persis.",
            tags = {"Summary"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = SummaryPerformaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<SummaryPerformaDto>> summaryPerforma(Principal principal) {
        BaseResponse<SummaryPerformaDto> baseResponse = summaryService.summaryPerforma(principal);
        return ok(baseResponse);
    }

    @GetMapping(value = "/monografi")
    @Operation(
            summary = "Get all summaries data monografi Anggota Persis",
            description = "Get all summaries data monografi Anggota Persis.",
            tags = {"Summary"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = SummaryMonografiDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<SummaryMonografiDto>> summaryMonografi(Principal principal) {
        BaseResponse<SummaryMonografiDto> baseResponse = summaryService.summaryMonogarfi(principal);
        return ok(baseResponse);
    }

    @GetMapping(value = "/golongan-darah")
    @Operation(
            summary = "Get all summaries golongan darah data monografi Anggota Persis",
            description = "Get all summaries golongan darah data monografi Anggota Persis.",
            tags = {"Summary"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = SummaryGolDarahDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<SummaryGolDarahDto>> summaryGolonganDarah(Principal principal) {
        BaseResponse<SummaryGolDarahDto> baseResponse = summaryService.summaryGolonganDarah(principal);
        return ok(baseResponse);
    }

    @GetMapping(value = "/status-marital")
    @Operation(
            summary = "Get all summaries status marital data monografi Anggota Persis",
            description = "Get all summaries status marital data monografi Anggota Persis.",
            tags = {"Summary"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = SummaryStatusMaritalDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<SummaryStatusMaritalDto>> summaryStatusMarital(Principal principal) {
        BaseResponse<SummaryStatusMaritalDto> baseResponse = summaryService.summaryStatusMarital(principal);
        return ok(baseResponse);
    }

    @GetMapping(value = "/status-keanggotaan")
    @Operation(
            summary = "Get all summaries status keanggotaan data monografi Anggota Persis",
            description = "Get all summaries status keanggotaan data monografi Anggota Persis.",
            tags = {"Summary"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = SummaryStatusKeanggotaanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<SummaryStatusKeanggotaanDto>> summaryStatusKeanggotaan(Principal principal) {
        BaseResponse<SummaryStatusKeanggotaanDto> baseResponse = summaryService.summaryStatusKeanggotaan(principal);
        return ok(baseResponse);
    }

    @GetMapping(value = "/status-pendidikan")
    @Operation(
            summary = "Get all summaries status pendidikan data monografi Anggota Persis",
            description = "Get all summaries status pendidikan data monografi Anggota Persis.",
            tags = {"Summary"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = SummaryStatusPendidikanDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<SummaryStatusPendidikanDto>> summaryStatusPendidikan(Principal principal) {
        BaseResponse<SummaryStatusPendidikanDto> baseResponse = summaryService.summaryStatusPendidikan(principal);
        return ok(baseResponse);
    }

    @GetMapping(value = "/anggota-perwilayah")
    @Operation(
            summary = "Get all summaries anggota per wilayah data monografi Anggota Persis",
            description = "Get all summaries anggota per wilayah data monografi Anggota Persis.",
            tags = {"Summary"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = SummaryAnggotaPerWilayahDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<SummaryAnggotaPerWilayahDto>> summaryAnggotaPerWilayah(Principal principal) {
        BaseResponse<SummaryAnggotaPerWilayahDto> baseResponse = summaryService.summaryAnggotaPerWilayah(principal);
        return ok(baseResponse);
    }

    @GetMapping(value = "/usia-anggota")
    @Operation(
            summary = "Get all summaries anggota per wilayah data monografi Anggota Persis",
            description = "Get all summaries anggota per wilayah data monografi Anggota Persis.",
            tags = {"Summary"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = SummaryUsiaAnggotaDto.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    @ResponseBody
    public ResponseEntity<BaseResponse<SummaryUsiaAnggotaDto>> summaryAnggotaUsia(Principal principal) {
        BaseResponse<SummaryUsiaAnggotaDto> baseResponse = summaryService.summaryUsiaAnggota(principal);
        return ok(baseResponse);
    }
}
