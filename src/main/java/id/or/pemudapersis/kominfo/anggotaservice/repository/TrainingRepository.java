package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Training;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrainingRepository extends JpaRepository<Training, Long> {
    Page<Training> findByDeletedAtIsNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Page<Training> findByDeletedAtIsNotNullAndAnggotaNpa(String idAnggota, Pageable pageable);

    Optional<Training> findByDeletedAtIsNotNullAndAnggotaNpaAndIdTraining(String idAnggota, Long idTraining);

    Optional<Training> findByDeletedAtIsNullAndAnggotaNpaAndIdTraining(String idAnggota, Long idTraining);
}
