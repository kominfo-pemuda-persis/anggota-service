package id.or.pemudapersis.kominfo.anggotaservice.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by IntelliJ IDEA.
 * Project : anggota-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/06/20
 * Time: 07.24
 */
@Entity
@Table(name = "t_anggota")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class Anggota implements Serializable {

    private static final long serialVersionUID = -2155753233215409928L;

    @Id
    @Column(name = "id_anggota", nullable = false)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @Size(max = 32)
    private String id;

    @Column(name = "npa")
    private String npa;

    @Column(name = "nama_lengkap")
    private String nama;

    @Column(name = "tempat_lahir")
    private String tempat;

    @Column(name = "tanggal_lahir")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate tanggalLahir;

    @Column(name = "status_merital")
    private String statusMerital;

    @Column(name = "pekerjaan")
    private String pekerjaan;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pw")
    @ToString.Exclude
    private Wilayah pw;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pd")
    @ToString.Exclude
    private Daerah pd;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pc")
    @ToString.Exclude
    private Cabang pc;

    @Column(name = "pj")
    private String pj;

    private String namaPj;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "desa")
    @ToString.Exclude
    private Desa desa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provinsi")
    @ToString.Exclude
    private Provinsi provinsi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kota")
    @ToString.Exclude
    private Kabupaten kabupaten;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "kecamatan")
    @ToString.Exclude
    private Kecamatan kecamatan;

    @Column(name = "gol_darah")
    private String golDarah;

    @Email
    @NotBlank(message = "Email is mandatory")
    @Column(name = "email")
    private String email;

    @Column(name = "no_telpon")
    private String noTelpon;

    @Column(name = "no_telpon2")
    private String noTelpon2;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "jenis_keanggotaan")
    private String jenisKeanggotaan;

    @Column(name = "status_aktif")
    private String statusAktif;

    @Column(name = "foto")
    private String foto;

    @Column(name = "id_otonom")
    private int idOtonom;

    @Column(name = "masa_aktif_kta")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate masaAktifKta;

    @Column(name = "reg_date")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime regDate;

    @Column(name = "last_updated")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDateTime lastUpdated;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdDate;

    @Column(name = "deleted_at")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime deletedAt;

    @ManyToOne
    @JoinColumn(name = "pendidikan_terakhir", nullable = false)
    private MasterPendidikan jenjangPendidikan;

    private String levelTafiq;

    private String statusHer;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Anggota anggota = (Anggota) o;
        return id != null && Objects.equals(id, anggota.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
