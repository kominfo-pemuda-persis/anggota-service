package id.or.pemudapersis.kominfo.anggotaservice.dto;

import jakarta.persistence.Id;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RoleDto {
    @Id
    private Long id;

    private String name;

    private String displayName;

    private String description;

    private String status;

    private Set<PermissionDto> permissions;
}
