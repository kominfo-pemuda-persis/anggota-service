package id.or.pemudapersis.kominfo.anggotaservice.utils.exception;

import java.util.Map;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class BaseException extends RuntimeException {
    private HttpStatus httpStatus;
    private final String errorCode;
    private String errorDesc;
    private String errorMessage;
    private String rootCause;
    private Map<String, String> errorMessageMap;

    public BaseException(
            HttpStatus httpStatus, String errorCode, String rootCause, String errorDesc, String errorMessage) {
        super(rootCause);
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.errorDesc = errorDesc;
        this.errorMessage = errorMessage;
    }

    public BaseException(HttpStatus httpStatus, String errorCode, String rootCause) {
        super(rootCause);
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
    }

    public BaseException(String errorCode, String rootCause) {
        super(rootCause);
        this.errorCode = errorCode;
    }

    public BaseException(
            HttpStatus httpStatus, String errorCode, String rootCause, Map<String, String> errorMessageMap) {
        super(rootCause);
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.errorMessageMap = errorMessageMap;
    }

    public String toString() {
        HttpStatus var10000 = this.getHttpStatus();
        return "BaseException(httpStatus=" + var10000 + ", errorCode=" + this.getErrorCode() + ", errorDesc="
                + this.getErrorDesc() + ", errorMessage=" + this.getErrorMessage() + ", rootCause="
                + this.getRootCause() + ", errorMessageMap=" + this.getErrorMessageMap() + ")";
    }
}
