package id.or.pemudapersis.kominfo.anggotaservice.filter;

import id.or.pemudapersis.kominfo.anggotaservice.service.JwtService;
import id.or.pemudapersis.kominfo.anggotaservice.service.LoginService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtService jwtService;
    private final LoginService loginService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        if (request.getServletPath().contains("/v1/api/auth")) {
            filterChain.doFilter(request, response);
            return;
        }

        Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION))
                .filter(header -> header.startsWith("Bearer "))
                .ifPresentOrElse(
                        header -> doAuth(header, request, response, filterChain),
                        () -> skipFilter(request, response, filterChain));
    }

    @SneakyThrows
    private static void skipFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        filterChain.doFilter(request, response);
    }

    @SneakyThrows
    private void doAuth(
            String header, HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) {
        var jwt = StringUtils.substring(header, 7);

        if (StringUtils.isEmpty(jwt)) {
            filterChain.doFilter(request, response);
            return;
        }

        var loggedInUser = null != SecurityContextHolder.getContext().getAuthentication();
        if (loggedInUser) {
            filterChain.doFilter(request, response);
            return;
        }

        Optional.ofNullable(jwtService.getSubject(jwt))
                .filter(sub -> jwtService.isJwtValid(jwt, sub))
                .ifPresent(npa -> injectAuthenticationContext(npa, request));

        filterChain.doFilter(request, response);
    }

    private void injectAuthenticationContext(String npa, HttpServletRequest request) {
        var user = loginService.findUserByNpa(npa);
        var authentication =
                new UsernamePasswordAuthenticationToken(npa, user.getPassword(), user.grantedAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
