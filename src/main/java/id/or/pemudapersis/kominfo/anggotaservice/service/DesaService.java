package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Desa;
import id.or.pemudapersis.kominfo.anggotaservice.repository.DesaRepository;
import id.or.pemudapersis.kominfo.anggotaservice.specification.WilayahSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class DesaService {

    private final DesaRepository desaRepository;

    public Page<Desa> getAllDesa(String keyword, Pageable page) {
        return desaRepository.findAll(
                WilayahSpecification.hasKodeKelurahan(keyword).or(WilayahSpecification.hasNamaKelurahan(keyword)),
                page);
    }
}
