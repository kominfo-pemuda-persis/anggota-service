package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.KeluargaCaang;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KeluargaCaangRepository extends JpaRepository<KeluargaCaang, Long> {}
