package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Anggota;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : anggota-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 17/06/20
 * Time: 07.35
 */
@Repository
public interface AnggotaRepository extends JpaRepository<Anggota, String> {

    Optional<Anggota> findByNpa(String npa);

    Page<Anggota> findAllByDeletedAtIsNull(Pageable pageable);

    Page<Anggota> findAll(Specification<Anggota> specification, Pageable pageable);

    @Query("select t from Anggota t")
    Slice<Anggota> findAllByAnggota(Pageable pageable);

    List<Anggota> findAllByDeletedAtIsNull();

    Long countByDeletedAtIsNull();
}
