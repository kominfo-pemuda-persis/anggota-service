package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.constant.GlobalMessage;
import id.or.pemudapersis.kominfo.anggotaservice.constant.StatusCaang;
import id.or.pemudapersis.kominfo.anggotaservice.dto.*;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.CalonAnggotaRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.ResponseBuilder;
import java.time.LocalDateTime;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class CalonAnggotaService {

    private final CabangService cabangService;
    private final CalonAnggotaRepository calonAnggotaRepository;
    private final ModelMapper modelMapper;
    private final HelperUtilService helperUtilService;
    private final WilayahService wilayahService;
    private final DaerahService daerahService;

    public BaseResponse<CalonAnggotaResponseDto> create(CalonAnggotaRequestDto calonAnggotaRequestDto) {
        final long start = System.nanoTime();
        CalonAnggota calonAnggota = convertToEntity(calonAnggotaRequestDto);
        this.setReferenceData(calonAnggota, calonAnggotaRequestDto);
        calonAnggota.setFoto("default.png");
        calonAnggota.setCreatedDate(LocalDateTime.now());
        calonAnggota.setStatus(StatusCaang.INIT.code);
        CalonAnggota result = calonAnggotaRepository.save(calonAnggota);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(result));
    }

    private void setReferenceData(CalonAnggota calonAnggota, CalonAnggotaRequestDto dto) {
        Provinsi provinsi = helperUtilService.getProvinsiById(dto.getProvinsi().getId());
        Kabupaten kabupaten =
                helperUtilService.getKabupatenById(dto.getKabupaten().getId());
        Kecamatan kecamatan =
                helperUtilService.getKecamatanById(dto.getKecamatan().getId());
        MasterPendidikan masterPendidikan = helperUtilService.getMasterPendidikanById(
                dto.getJenjangPendidikan().getId());
        Desa desa = helperUtilService.getDesaByid(dto.getDesa().getId());
        Wilayah pw = wilayahService.getByKodeWilayah(dto.getPw().getKdPw());
        Cabang pc = cabangService.getByKodeCabang(dto.getPc().getKdPc());
        Daerah pd = daerahService.getByKodeDaerah(dto.getPd().getKdPd());
        calonAnggota.setKecamatan(kecamatan);
        calonAnggota.setJenjangPendidikan(masterPendidikan);
        calonAnggota.setProvinsi(provinsi);
        calonAnggota.setDesa(desa);
        calonAnggota.setPc(pc);
        calonAnggota.setPw(pw);
        calonAnggota.setPd(pd);
        calonAnggota.setKabupaten(kabupaten);
    }

    private CalonAnggota convertToEntity(CalonAnggotaRequestDto calonAnggotaRequestDto) {
        return modelMapper.map(calonAnggotaRequestDto, CalonAnggota.class);
    }

    private CalonAnggotaResponseDto convertToDto(CalonAnggota calonAnggota) {
        return modelMapper.map(calonAnggota, CalonAnggotaResponseDto.class);
    }

    private CalonAnggotaResponseListDto convertToDtoList(CalonAnggota calonAnggota) {
        return modelMapper.map(calonAnggota, CalonAnggotaResponseListDto.class);
    }

    public CalonAnggota findByIdCaang(Long idCaang) {
        return calonAnggotaRepository
                .findById(idCaang)
                .orElseThrow(() -> new BusinessException(
                        HttpStatus.NOT_FOUND,
                        GlobalMessage.CALON_ANGGOTA_NOT_FOUND.code,
                        GlobalMessage.CALON_ANGGOTA_NOT_FOUND.message));
    }

    public BaseResponse<CalonAnggotaResponseDto> getDetailByIdCaang(Long idCaang) {
        final long start = System.nanoTime();
        CalonAnggota result = this.findByIdCaang(idCaang);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), GlobalMessage.SUCCESS.message, convertToDto(result));
    }

    public BaseResponse<Page<CalonAnggotaResponseListDto>> getAllCalonAnggota(String keyword, Pageable pageable) {
        final long start = System.nanoTime();
        Page<CalonAnggota> calonAnggotas =
                calonAnggotaRepository.findAllByNamaContainingIgnoreCaseAndDeletedAtIsNull(keyword, pageable);
        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK,
                ((end - start) / 1000000),
                GlobalMessage.SUCCESS.message,
                calonAnggotas.map(this::convertToDtoList));
    }

    public BaseResponse<CalonAnggotaResponseDto> delete(Long idCaang) {
        final long start = System.nanoTime();

        CalonAnggota calonAnggota = calonAnggotaRepository
                .findByIdCaangAndDeletedAtIsNull(idCaang)
                .orElseThrow(
                        () -> new BusinessException(HttpStatus.NOT_FOUND, "ID_CAANG_NOT_FOUND", "ID Caang not found"));
        calonAnggota.setDeletedAt(LocalDateTime.now());
        calonAnggotaRepository.save(calonAnggota);
        log.info("Deleted caang with id {}", idCaang);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", null);
    }

    public BaseResponse<CalonAnggotaResponseDto> edit(Long idCaang, CalonAnggotaRequestDto calonAnggotaRequestDto) {
        final long start = System.nanoTime();

        CalonAnggota calonAnggota = calonAnggotaRepository
                .findByIdCaangAndDeletedAtIsNull(idCaang)
                .orElseThrow(
                        () -> new BusinessException(HttpStatus.NOT_FOUND, "ID_CAANG_NOT_FOUND", "ID Caang not found"));
        this.setReferenceUpdateData(calonAnggotaRequestDto, calonAnggota);
        calonAnggota.setLastUpdated(LocalDateTime.now());
        CalonAnggota result = calonAnggotaRepository.save(calonAnggota);
        log.info("update caang with id {}", idCaang);

        final long end = System.nanoTime();
        return ResponseBuilder.buildResponse(
                HttpStatus.OK, ((end - start) / 1000000), "SUCCESS", this.convertToDto(result));
    }

    private void setReferenceUpdateData(CalonAnggotaRequestDto calonAnggotaRequestDto, CalonAnggota calonAnggota) {

        Provinsi provinsi = Optional.ofNullable(calonAnggotaRequestDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .map(helperUtilService::getProvinsiById)
                .orElse(calonAnggota.getProvinsi());
        calonAnggota.setProvinsi(provinsi);
        calonAnggotaRequestDto.setProvinsi(null);

        Kabupaten kabupaten = Optional.ofNullable(calonAnggotaRequestDto.getKabupaten())
                .map(KabupatenDto::getId)
                .map(helperUtilService::getKabupatenById)
                .orElse(calonAnggota.getKabupaten());
        calonAnggota.setKabupaten(kabupaten);
        calonAnggotaRequestDto.setKabupaten(null);

        Kecamatan kecamatan = Optional.ofNullable(calonAnggotaRequestDto.getKecamatan())
                .map(KecamatanDto::getId)
                .map(helperUtilService::getKecamatanById)
                .orElse(calonAnggota.getKecamatan());
        calonAnggota.setKecamatan(kecamatan);
        calonAnggotaRequestDto.setKecamatan(null);

        Wilayah wilayah = Optional.ofNullable(calonAnggotaRequestDto.getPw())
                .map(WilayahDto::getKdPw)
                .map(wilayahService::getByKodeWilayah)
                .orElse(calonAnggota.getPw());
        calonAnggota.setPw(wilayah);
        calonAnggotaRequestDto.setPw(null);

        Cabang cabang = Optional.ofNullable(calonAnggotaRequestDto.getPc())
                .map(CabangRequest::getKdPc)
                .map(cabangService::getByKodeCabang)
                .orElse(calonAnggota.getPc());
        calonAnggota.setPc(cabang);
        calonAnggotaRequestDto.setPc(null);

        Daerah daerah = Optional.ofNullable(calonAnggotaRequestDto.getPd())
                .map(DaerahRequest::getKdPd)
                .map(daerahService::getByKodeDaerah)
                .orElse(calonAnggota.getPd());
        calonAnggota.setPd(daerah);
        calonAnggotaRequestDto.setPd(null);

        Desa desa = Optional.ofNullable(calonAnggotaRequestDto.getDesa())
                .map(DesaDto::getId)
                .map(helperUtilService::getDesaByid)
                .orElse(calonAnggota.getDesa());
        calonAnggota.setDesa(desa);
        calonAnggotaRequestDto.setDesa(null);

        MasterPendidikan masterPendidikan = Optional.ofNullable(calonAnggotaRequestDto.getJenjangPendidikan())
                .map(MasterPendidikanDto::getId)
                .map(helperUtilService::getMasterPendidikanById)
                .orElse(calonAnggota.getJenjangPendidikan());
        calonAnggota.setJenjangPendidikan(masterPendidikan);
        calonAnggotaRequestDto.setJenjangPendidikan(null);

        modelMapper.map(calonAnggotaRequestDto, calonAnggota);
    }
}
