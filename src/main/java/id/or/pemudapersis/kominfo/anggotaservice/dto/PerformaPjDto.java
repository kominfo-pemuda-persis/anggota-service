package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import jakarta.persistence.Id;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PerformaPjDto extends QuestionDto {
    @Id
    private Long id;

    private String kodePj;

    private String namaPj;

    private CabangRequest cabang;

    private DaerahRequest daerah;

    private WilayahDto wilayah;

    private String alamat;

    private String ketuaPj;

    private String noHp;

    private ProvinsiDto provinsi;

    private KabupatenDto kabupaten;

    private KecamatanDto kecamatan;

    private DesaDto desa;
}
