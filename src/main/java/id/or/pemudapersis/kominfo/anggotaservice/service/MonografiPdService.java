package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.KabupatenDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPdDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.ProvinsiDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.WilayahDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.JamiyyahPdRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MonografiPdRepository;
import id.or.pemudapersis.kominfo.anggotaservice.request.DaerahRequest;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
@RequiredArgsConstructor
public class MonografiPdService {

    private final MonografiPdRepository monografiPdRepository;
    private final DaerahService daerahService;
    private final WilayahService wilayahService;
    private final HelperUtilService helperUtilService;
    private final JamiyyahPdRepository jamiyyahPcRepository;
    private final ModelMapper modelMapper;

    public List<MonografiPd> getList(Pageable pageable, boolean isDeleted) {

        Page<MonografiPd> monografiPdPage;

        if (!isDeleted) monografiPdPage = monografiPdRepository.findByDeletedAtIsNull(pageable);
        else monografiPdPage = monografiPdRepository.findByDeletedAtIsNotNull(pageable);

        return monografiPdPage.getContent();
    }

    public MonografiPd create(MonografiPd monografiPd) {
        Optional<MonografiPd> optionalMonografiPd = monografiPdRepository.findByDaerahKdPdAndDeletedAtIsNull(
                monografiPd.getDaerah().getKdPd());
        optionalMonografiPd.ifPresent(this::validate);

        Daerah daerah = daerahService.getByKodeDaerah(monografiPd.getDaerah().getKdPd());
        Wilayah wilayah =
                wilayahService.getByKodeWilayah(monografiPd.getWilayah().getKdPw());
        Provinsi provinsi =
                helperUtilService.getProvinsiById(monografiPd.getProvinsi().getId());
        Kabupaten kabupaten =
                helperUtilService.getKabupatenById(monografiPd.getKabupaten().getId());

        JamiyyahPd jamiyyahPd = JamiyyahPd.builder()
                .daerah(daerah)
                .bendahara(monografiPd.getJamiyyahPd().getBendahara())
                .bidAdministrasi(monografiPd.getJamiyyahPd().getBidAdministrasi())
                .bidDakwah(monografiPd.getJamiyyahPd().getBidDakwah())
                .bidEkonomi(monografiPd.getJamiyyahPd().getBidEkonomi())
                .bidHal(monografiPd.getJamiyyahPd().getBidHal())
                .bidHumasPublikasi(monografiPd.getJamiyyahPd().getBidHumasPublikasi())
                .bidJamiyyah(monografiPd.getJamiyyahPd().getBidJamiyyah())
                .bidKaderisasi(monografiPd.getJamiyyahPd().getBidKaderisasi())
                .bidOrgSeni(monografiPd.getJamiyyahPd().getBidOrgSeni())
                .bidPendidikan(monografiPd.getJamiyyahPd().getBidPendidikan())
                .bidSosial(monografiPd.getJamiyyahPd().getBidSosial())
                .countAnggotaBiasa(monografiPd.getJamiyyahPd().getCountAnggotaBiasa())
                .countAnggotaIstimewa(monografiPd.getJamiyyahPd().getCountAnggotaIstimewa())
                .countAnggotaTersiar(monografiPd.getJamiyyahPd().getCountAnggotaTersiar())
                .createdDate(LocalDateTime.now())
                .hari(monografiPd.getJamiyyahPd().getHari())
                .ketua(monografiPd.getJamiyyahPd().getKetua())
                .kabupaten(kabupaten)
                .monografiPd(MonografiPd.builder()
                        .daerah(daerah)
                        .wilayah(wilayah)
                        .provinsi(provinsi)
                        .kabupaten(kabupaten)
                        .alamatAlternatif(monografiPd.getAlamatAlternatif())
                        .alamatLengkap(monografiPd.getAlamatLengkap())
                        .btsWilayahBarat(monografiPd.getBtsWilayahBarat())
                        .btsWilayahSelatan(monografiPd.getBtsWilayahSelatan())
                        .btsWilayahTimur(monografiPd.getBtsWilayahTimur())
                        .btsWilayahUtara(monografiPd.getBtsWilayahUtara())
                        .createdDate(LocalDateTime.now())
                        .email(monografiPd.getEmail())
                        .foto(monografiPd.getFoto())
                        .jarakIbuKotaNegara(monografiPd.getJarakIbuKotaNegara())
                        .latitude(monografiPd.getLatitude())
                        .longitude(monografiPd.getLongitude())
                        .luas(monografiPd.getLuas())
                        .noKontak(monografiPd.getNoKontak())
                        .updatedDate(LocalDateTime.now())
                        .build())
                .musyDaLastHijri(monografiPd.getJamiyyahPd().getMusyDaLastHijri())
                .musyDaLastMasehi(monografiPd.getJamiyyahPd().getMusyDaLastMasehi())
                .pembantuUmum1(monografiPd.getJamiyyahPd().getPembantuUmum1())
                .pembantuUmum2(monografiPd.getJamiyyahPd().getPembantuUmum2())
                .pembantuUmum3(monografiPd.getJamiyyahPd().getPembantuUmum3())
                .penasehat1(monografiPd.getJamiyyahPd().getPenasehat1())
                .penasehat2(monografiPd.getJamiyyahPd().getPenasehat2())
                .penasehat3(monografiPd.getJamiyyahPd().getPenasehat3())
                .provinsi(provinsi)
                .pukul(monografiPd.getJamiyyahPd().getPukul())
                .sekretaris(monografiPd.getJamiyyahPd().getSekretaris())
                .updatedDate(LocalDateTime.now())
                .wilayah(wilayah)
                .wklBendahara(monografiPd.getJamiyyahPd().getWklBendahara())
                .wklBidAdministrasi(monografiPd.getJamiyyahPd().getWklBidAdministrasi())
                .wklBidDakwah(monografiPd.getJamiyyahPd().getWklBidDakwah())
                .wklBidEkonomi(monografiPd.getJamiyyahPd().getWklBidEkonomi())
                .wklBidHal(monografiPd.getJamiyyahPd().getWklBidHal())
                .wklBidHumasPublikasi(monografiPd.getJamiyyahPd().getWklBidHumasPublikasi())
                .wklBidJamiyyah(monografiPd.getJamiyyahPd().getWklBidJamiyyah())
                .wklBidKaderisasi(monografiPd.getJamiyyahPd().getWklBidKaderisasi())
                .wklBidOrgSeni(monografiPd.getJamiyyahPd().getWklBidOrgSeni())
                .wklBidPendidikan(monografiPd.getJamiyyahPd().getWklBidPendidikan())
                .wklKetua(monografiPd.getJamiyyahPd().getWklKetua())
                .wklSekretaris(monografiPd.getJamiyyahPd().getWklSekretaris())
                .build();

        return jamiyyahPcRepository.save(jamiyyahPd).getMonografiPd();
    }

    public MonografiPd update(MonografiPdDto monografiPcDto, Long id) {

        MonografiPd monografiPcEntity = monografiPdRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "30131", " monografi pc not found"));

        String kodeDaerah = Optional.ofNullable(monografiPcDto.getDaerah())
                .map(DaerahRequest::getKdPd)
                .orElse(monografiPcEntity.getDaerah().getKdPd());
        String kodeWilayah = Optional.ofNullable(monografiPcDto.getWilayah())
                .map(WilayahDto::getKdPw)
                .orElse(monografiPcEntity.getWilayah().getKdPw());
        String kodeProvinsi = Optional.ofNullable(monografiPcDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .orElse(monografiPcEntity.getProvinsi().getId());
        String kodeKabupaten = Optional.ofNullable(monografiPcDto.getKabupaten())
                .map(KabupatenDto::getId)
                .orElse(monografiPcEntity.getKabupaten().getId());

        // the persisted entity to update with the DTO, modelMapper will map the property instead of instantiate a new
        // entity. Hibernate will not allow it
        monografiPcDto.setDaerah(null);
        monografiPcDto.setWilayah(null);
        monografiPcDto.setProvinsi(null);
        monografiPcDto.setKabupaten(null);
        monografiPcDto.setId(null);

        updateEntityFromDto(monografiPcDto, monografiPcEntity);
        Daerah daerah = daerahService.getByKodeDaerah(kodeDaerah);
        Wilayah wilayah = wilayahService.getByKodeWilayah(kodeWilayah);
        Provinsi provinsi = helperUtilService.getProvinsiById(kodeProvinsi);
        Kabupaten kabupaten = helperUtilService.getKabupatenById(kodeKabupaten);
        monografiPcEntity.setDaerah(daerah);
        monografiPcEntity.setWilayah(wilayah);
        monografiPcEntity.setProvinsi(provinsi);
        monografiPcEntity.setKabupaten(kabupaten);
        monografiPcEntity.getJamiyyahPd().setDaerah(daerah);
        monografiPcEntity.getJamiyyahPd().setWilayah(wilayah);
        monografiPcEntity.getJamiyyahPd().setProvinsi(provinsi);
        monografiPcEntity.getJamiyyahPd().setKabupaten(kabupaten);
        monografiPcEntity.setUpdatedDate(LocalDateTime.now());
        monografiPcEntity.getJamiyyahPd().setUpdatedDate(LocalDateTime.now());
        return monografiPdRepository.save(monografiPcEntity);
    }

    private void updateEntityFromDto(MonografiPdDto monografiPdDto, MonografiPd monografiPcEntity) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(monografiPdDto, monografiPcEntity);
    }

    private void validate(MonografiPd monografiPd) {
        if (monografiPd.getDeletedAt() != null)
            throw new BusinessException(
                    HttpStatus.FOUND,
                    "30133",
                    " Monografi PD " + monografiPd.getDaerah().getNamaPd() + " already exists deleted record");
        throw new BusinessException(
                HttpStatus.FOUND,
                "30131",
                " Monografi PD " + monografiPd.getDaerah().getNamaPd() + " already exists");
    }

    public MonografiPd delete(Long id) {
        Optional<MonografiPd> monografiPdOptional = monografiPdRepository.findByIdAndDeletedAtIsNull(id);
        if (!monografiPdOptional.isPresent()) {
            log.info("Monografi PD not found {} ", id);
            throw new BusinessException(HttpStatus.CONFLICT, "30130", "monografi PD not found");
        }

        MonografiPd monografiPd = monografiPdOptional.get();
        monografiPd.setDeletedAt(LocalDateTime.now());
        return monografiPdRepository.save(monografiPd);
    }

    public MonografiPd restore(Long id) {
        Optional<MonografiPd> monografiOptional = monografiPdRepository.findByIdAndDeletedAtIsNotNull(id);
        if (!monografiOptional.isPresent()) {
            log.info("Monografi PC not found {} ", id);
            throw new BusinessException(HttpStatus.NOT_FOUND, "30131", " monografi pc not found");
        }

        MonografiPd monografi = monografiOptional.get();
        Optional.ofNullable(monografi.getJamiyyahPd()).ifPresent(jamiyyahPc -> jamiyyahPc.setDeletedAt(null));
        monografi.setDeletedAt(null);
        return monografiPdRepository.save(monografi);
    }

    public Long countMonografiPd() {
        return monografiPdRepository.countByDeletedAtIsNull();
    }
}
