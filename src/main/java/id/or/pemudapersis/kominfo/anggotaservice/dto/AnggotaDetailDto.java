package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Cabang;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Daerah;
import id.or.pemudapersis.kominfo.anggotaservice.entity.Wilayah;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AnggotaDetailDto {
    private String npa;

    private String nama;

    private Wilayah pw;

    private Daerah pd;

    private Cabang pc;

    private Set<RoleDto> role;
}
