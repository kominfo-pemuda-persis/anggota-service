package id.or.pemudapersis.kominfo.anggotaservice.controller;

import static org.springframework.http.ResponseEntity.ok;

import id.or.pemudapersis.kominfo.anggotaservice.request.CabangRequest;
import id.or.pemudapersis.kominfo.anggotaservice.service.CabangService;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/api/cabang")
@Tag(name = "Cabang", description = "Endpoint for managing Cabang")
public class CabangController {
    private final CabangService cabangService;

    public CabangController(CabangService cabangService) {
        this.cabangService = cabangService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Get all Cabang Pemuda Persis Data",
            description = "Get all Cabang Pemuda Persis Data.",
            tags = {"Cabang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CabangRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<Page<CabangRequest>>> getList(
            @Parameter(description = "The size of the page to be returned")
                    @RequestParam(required = false, defaultValue = "10")
                    Integer pageSize,
            @Parameter(description = "Zero-based page index") @RequestParam(required = false, defaultValue = "0")
                    Integer pageNo,
            @Parameter(description = "Filter based on Kode PD") @RequestParam(required = false, defaultValue = "")
                    String kdPd,
            @Parameter(description = "Zero-based page sort") @RequestParam(required = false, defaultValue = "kdPc")
                    String sortBy) {
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy));
        BaseResponse<Page<CabangRequest>> baseResponse = cabangService.getList(paging, kdPd);
        return ok(baseResponse);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Create Data Cabang (PC) Pemuda Persis Data",
            description = "Create Data Cabang (PC) Pemuda Persis Data.",
            tags = {"Cabang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CabangRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<CabangRequest>> create(@Valid @RequestBody CabangRequest request) {
        BaseResponse<CabangRequest> baseResponse = cabangService.create(request);
        return ok(baseResponse);
    }

    @PutMapping(value = "/{kd}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Update Data Cabang (PC) Pemuda Persis Data",
            description = "Update Data Cabang (PC) Pemuda Persis Data.",
            tags = {"Cabang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CabangRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<CabangRequest>> update(
            @PathVariable("kd") String kd, @Valid @RequestBody CabangRequest request) {
        BaseResponse<CabangRequest> baseResponse = cabangService.update(kd, request);
        return ok(baseResponse);
    }

    @DeleteMapping(value = "/{kd}")
    @Operation(
            summary = "Delete Data Cabang (PC) Pemuda Persis Data",
            description = "Delete Data Cabang (PC) Pemuda Persis Data.",
            tags = {"Cabang"})
    @ApiResponses(
            value = {
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Success",
                        responseCode = "200",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = CabangRequest.class))),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Not found",
                        responseCode = "404",
                        content = @Content),
                @io.swagger.v3.oas.annotations.responses.ApiResponse(
                        description = "Internal error",
                        responseCode = "500",
                        content = @Content)
            })
    @SecurityRequirement(name = "Authorization")
    public ResponseEntity<BaseResponse<CabangRequest>> delete(@PathVariable("kd") String kd) {
        BaseResponse<CabangRequest> baseResponse = cabangService.delete(kd);
        return ok(baseResponse);
    }
}
