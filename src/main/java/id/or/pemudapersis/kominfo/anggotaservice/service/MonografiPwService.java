package id.or.pemudapersis.kominfo.anggotaservice.service;

import id.or.pemudapersis.kominfo.anggotaservice.dto.MonografiPwDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.ProvinsiDto;
import id.or.pemudapersis.kominfo.anggotaservice.dto.WilayahDto;
import id.or.pemudapersis.kominfo.anggotaservice.entity.*;
import id.or.pemudapersis.kominfo.anggotaservice.repository.JamiyyahPwRepository;
import id.or.pemudapersis.kominfo.anggotaservice.repository.MonografiPwRepository;
import id.or.pemudapersis.kominfo.anggotaservice.utils.exception.BusinessException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class MonografiPwService {
    private final MonografiPwRepository monografiPwRepository;
    private final JamiyyahPwRepository jamiyyahPwRepository;
    private final ModelMapper modelMapper;
    private final WilayahService wilayahService;
    private final HelperUtilService helperUtilService;

    public MonografiPwService(
            MonografiPwRepository monografiPwRepository,
            JamiyyahPwRepository jamiyyahPwRepository,
            ModelMapper modelMapper,
            WilayahService wilayahService,
            HelperUtilService helperUtilService) {
        this.monografiPwRepository = monografiPwRepository;
        this.jamiyyahPwRepository = jamiyyahPwRepository;
        this.modelMapper = modelMapper;
        this.wilayahService = wilayahService;
        this.helperUtilService = helperUtilService;
    }

    public List<MonografiPw> monografiPwList(Pageable pageable, boolean isDeleted) {

        Pageable pageReq = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), pageable.getSort());

        Page<MonografiPw> monografiPws;
        if (!isDeleted) monografiPws = monografiPwRepository.findByDeletedAtIsNull(pageReq);
        else monografiPws = monografiPwRepository.findByDeletedAtIsNotNull(pageReq);

        return monografiPws.getContent();
    }

    public MonografiPw create(MonografiPw monografiPw) {
        Optional<MonografiPw> optionalMonografiPw = monografiPwRepository.findByWilayah_KdPw(
                monografiPw.getWilayah().getKdPw());

        optionalMonografiPw.ifPresent(this::validate);

        Wilayah wilayah =
                wilayahService.getByKodeWilayah(monografiPw.getWilayah().getKdPw());
        Provinsi provinsi =
                helperUtilService.getProvinsiById(monografiPw.getProvinsi().getId());

        JamiyyahPw jamiyyahPw = JamiyyahPw.builder()
                .bendahara(monografiPw.getJamiyyahPw().getBendahara())
                .bidAdministrasi(monografiPw.getJamiyyahPw().getBidAdministrasi())
                .bidDakwah(monografiPw.getJamiyyahPw().getBidDakwah())
                .bidEkonomi(monografiPw.getJamiyyahPw().getBidEkonomi())
                .bidHal(monografiPw.getJamiyyahPw().getBidHal())
                .bidHumasPublikasi(monografiPw.getJamiyyahPw().getBidHumasPublikasi())
                .bidJamiyyah(monografiPw.getJamiyyahPw().getBidJamiyyah())
                .bidKaderisasi(monografiPw.getJamiyyahPw().getBidKaderisasi())
                .bidOrgSeni(monografiPw.getJamiyyahPw().getBidOrgSeni())
                .bidPendidikan(monografiPw.getJamiyyahPw().getBidPendidikan())
                .bidSosial(monografiPw.getJamiyyahPw().getBidSosial())
                .countAnggotaIstimewa(monografiPw.getJamiyyahPw().getCountAnggotaIstimewa())
                .countAnggotaTersiar(monografiPw.getJamiyyahPw().getCountAnggotaTersiar())
                .createdDate(LocalDateTime.now())
                .hari(monografiPw.getJamiyyahPw().getHari())
                .ketua(monografiPw.getJamiyyahPw().getKetua())
                .monografiPw(MonografiPw.builder()
                        .wilayah(wilayah)
                        .provinsi(provinsi)
                        .alamatAlternatif(monografiPw.getAlamatAlternatif())
                        .alamatLengkap(monografiPw.getAlamatLengkap())
                        .btsWilayahBarat(monografiPw.getBtsWilayahBarat())
                        .btsWilayahSelatan(monografiPw.getBtsWilayahSelatan())
                        .btsWilayahTimur(monografiPw.getBtsWilayahTimur())
                        .btsWilayahUtara(monografiPw.getBtsWilayahUtara())
                        .createdDate(LocalDateTime.now())
                        .email(monografiPw.getEmail())
                        .foto(monografiPw.getFoto())
                        .jarakIbuKotaNegara(monografiPw.getJarakIbuKotaNegara())
                        .latitude(monografiPw.getLatitude())
                        .longitude(monografiPw.getLongitude())
                        .luas(monografiPw.getLuas())
                        .noKontak(monografiPw.getNoKontak())
                        .namaPw(monografiPw.getNamaPw())
                        .updatedDate(LocalDateTime.now())
                        .build())
                .musyWilLastHijri(monografiPw.getJamiyyahPw().getMusyWilLastHijri())
                .musyWilLastMasehi(monografiPw.getJamiyyahPw().getMusyWilLastMasehi())
                .namaPw(monografiPw.getNamaPw())
                .pembantuUmum1(monografiPw.getJamiyyahPw().getPembantuUmum1())
                .pembantuUmum2(monografiPw.getJamiyyahPw().getPembantuUmum2())
                .pembantuUmum3(monografiPw.getJamiyyahPw().getPembantuUmum3())
                .penasehat1(monografiPw.getJamiyyahPw().getPenasehat1())
                .penasehat2(monografiPw.getJamiyyahPw().getPenasehat2())
                .penasehat3(monografiPw.getJamiyyahPw().getPenasehat3())
                .provinsi(provinsi)
                .pukul(monografiPw.getJamiyyahPw().getPukul())
                .sekretaris(monografiPw.getJamiyyahPw().getSekretaris())
                .updatedDate(LocalDateTime.now())
                .wilayah(wilayah)
                .wklBendahara(monografiPw.getJamiyyahPw().getWklBendahara())
                .wklBidAdministrasi(monografiPw.getJamiyyahPw().getWklBidAdministrasi())
                .wklBidDakwah(monografiPw.getJamiyyahPw().getWklBidDakwah())
                .wklBidEkonomi(monografiPw.getJamiyyahPw().getWklBidEkonomi())
                .wklBidHal(monografiPw.getJamiyyahPw().getWklBidHal())
                .wklBidHumasPublikasi(monografiPw.getJamiyyahPw().getWklBidHumasPublikasi())
                .wklBidJamiyyah(monografiPw.getJamiyyahPw().getWklBidJamiyyah())
                .wklBidKaderisasi(monografiPw.getJamiyyahPw().getWklBidKaderisasi())
                .wklBidOrgSeni(monografiPw.getJamiyyahPw().getWklBidOrgSeni())
                .wklBidPendidikan(monografiPw.getJamiyyahPw().getWklBidPendidikan())
                .wklKetua(monografiPw.getJamiyyahPw().getWklKetua())
                .wklSekretaris(monografiPw.getJamiyyahPw().getWklSekretaris())
                .build();
        return jamiyyahPwRepository.save(jamiyyahPw).getMonografiPw();
    }

    public MonografiPw update(MonografiPwDto monografiPwDto, Long id) {

        MonografiPw monografiPwEntity = monografiPwRepository
                .findByIdAndDeletedAtIsNull(id)
                .orElseThrow(() -> new BusinessException(HttpStatus.NOT_FOUND, "30231", " monografi pw not found"));

        String kodeWilayah = Optional.ofNullable(monografiPwDto.getWilayah())
                .map(WilayahDto::getKdPw)
                .orElse(monografiPwEntity.getWilayah().getKdPw());
        String kodeProvinsi = Optional.ofNullable(monografiPwDto.getProvinsi())
                .map(ProvinsiDto::getId)
                .orElse(monografiPwEntity.getProvinsi().getId());

        monografiPwDto.setWilayah(null);
        monografiPwDto.setProvinsi(null);
        monografiPwDto.setId(null);

        updateEntityFromDto(monografiPwDto, monografiPwEntity);
        Wilayah wilayah = wilayahService.getByKodeWilayah(kodeWilayah);
        Provinsi provinsi = helperUtilService.getProvinsiById(kodeProvinsi);

        monografiPwEntity.setWilayah(wilayah);
        monografiPwEntity.setProvinsi(provinsi);
        monografiPwEntity.getJamiyyahPw().setWilayah(wilayah);
        monografiPwEntity.getJamiyyahPw().setNamaPw(monografiPwDto.getNamaPw());
        monografiPwEntity.getJamiyyahPw().setProvinsi(provinsi);
        monografiPwEntity.setUpdatedDate(LocalDateTime.now());
        monografiPwEntity.getJamiyyahPw().setUpdatedDate(LocalDateTime.now());
        return monografiPwRepository.save(monografiPwEntity);
    }

    public MonografiPw delete(Long id) {
        Optional<MonografiPw> monografiOptional = monografiPwRepository.findByIdAndDeletedAtIsNull(id);
        if (!monografiOptional.isPresent()) {
            log.info("Monografi PW not found {} ", id);
            throw new BusinessException(HttpStatus.NOT_FOUND, "30231", " monografi pw not found");
        }

        MonografiPw monografiPw = monografiOptional.get();
        Optional.ofNullable(monografiPw.getJamiyyahPw())
                .ifPresent(jamiyyahPw -> jamiyyahPw.setDeletedAt(LocalDateTime.now()));
        monografiPw.setDeletedAt(LocalDateTime.now());
        return monografiPwRepository.save(monografiPw);
    }

    public MonografiPw restore(Long id) {
        Optional<MonografiPw> monografiOptional = monografiPwRepository.findByIdAndDeletedAtIsNotNull(id);
        if (!monografiOptional.isPresent()) {
            log.info("Monografi PW not found {} ", id);
            throw new BusinessException(HttpStatus.NOT_FOUND, "30231", " monografi pw not found");
        }

        MonografiPw monografi = monografiOptional.get();
        Optional.ofNullable(monografi.getJamiyyahPw()).ifPresent(jamiyyahPc -> jamiyyahPc.setDeletedAt(null));
        monografi.setDeletedAt(null);
        return monografiPwRepository.save(monografi);
    }

    private void validate(MonografiPw monografiPw) {
        if (monografiPw.getDeletedAt() != null)
            throw new BusinessException(
                    HttpStatus.FOUND,
                    "30233",
                    " Monografi PC " + monografiPw.getNamaPw() + " already exists deleted record");
        throw new BusinessException(
                HttpStatus.FOUND, "30231", " Monografi PC " + monografiPw.getNamaPw() + " already exists");
    }

    private void updateEntityFromDto(MonografiPwDto monografiPwDto, MonografiPw monografiPw) {
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        modelMapper.map(monografiPwDto, monografiPw);
    }

    public Long countMonografiPw() {
        return monografiPwRepository.countByDeletedAtIsNull();
    }
}
