package id.or.pemudapersis.kominfo.anggotaservice.specification;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Kabupaten;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

@UtilityClass
public class KabupatenSpecification {

    public static Specification<Kabupaten> hasId(String id) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("id"), id);
    }

    public static Specification<Kabupaten> hasNama(String nama) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("nama"), nama);
    }
}
