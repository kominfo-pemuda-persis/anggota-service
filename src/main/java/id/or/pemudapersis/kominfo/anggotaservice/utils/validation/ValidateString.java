package id.or.pemudapersis.kominfo.anggotaservice.utils.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = StringValidator.class)
public @interface ValidateString {

    String[] acceptedValues();

    String message() default "must match \"{acceptedValues}\"";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
