package id.or.pemudapersis.kominfo.anggotaservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * Project: anggota-service
 * <p>
 * User: hasan.sanusi
 * Email: hasan.sanusi@dansmultipro.com
 * Telegram: @hasansanusi
 * Date: 04/12/24
 * Time: 21.52
 * <p>
 * Created with IntelliJ IDEA
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class AnggotaFilterDto {
    private String keyword;
    private String statusMerital;
    private String jenisAnggota;
    private String golonganDarah;
    private String pendidikanTerakhir;
    private String levelTafiq;
    private String statusHer;
    private String status;
}
