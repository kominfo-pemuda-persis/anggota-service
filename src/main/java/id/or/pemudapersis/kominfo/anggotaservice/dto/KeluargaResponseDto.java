package id.or.pemudapersis.kominfo.anggotaservice.dto;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Otonom;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class KeluargaResponseDto {
    private Long idKeluarga;

    private String namaKeluarga;

    private String hubungan;

    private int jumlahAnak;

    private String alamat;

    private Otonom otonom;

    private String keterangan;

    private String statusAnggota;
}
