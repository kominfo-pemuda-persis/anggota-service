package id.or.pemudapersis.kominfo.anggotaservice.repository;

import id.or.pemudapersis.kominfo.anggotaservice.entity.Organisasi;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganisasiRepository extends JpaRepository<Organisasi, Long> {
    Page<Organisasi> findByDeletedAtIsNullAndAnggotaNpa(String npa, Pageable pageable);

    Page<Organisasi> findByDeletedAtIsNotNullAndAnggotaNpa(String npa, Pageable pageable);

    Optional<Organisasi> findByDeletedAtIsNotNullAndAnggotaNpaAndIdOrganisasi(String npa, Long idOrganisasi);

    Optional<Organisasi> findByDeletedAtIsNullAndAnggotaNpaAndIdOrganisasi(String npa, Long idOrganisasi);
}
