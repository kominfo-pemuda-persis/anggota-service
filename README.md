# Anggota Service Guidelines

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-orange.svg)](https://sonarcloud.io/summary/new_code?id=kominfo-pemuda-persis_anggota-service)

### API Documentation
[localhost](http://localhost:8080/swagger-ui/index.html)

[remote](http://206.189.147.212:8080/swagger-ui/index.html)

#### Securing REST API with Spring Boot Authorization Server

To run this repo, please follow these command :

1. Paste this command to your terminal

    `mvn clean spring-boot:run`

2. Launch POSTMAN to generate TOKEN (grant type _Client Credentials_)

    ![Authorization Tab](images/authorization-tab1.png "Authorization Tab")

    ![Authorization Tab](images/authorization-tab2.png "Authorization Tab")

    ![Authorization Tab](images/generated-access-token1.png "Authorization Tab")

3. Launch POSTMAN to generate TOKEN (grant type _Authorization Code_)

    ![Authorization Tab](images/authcode-1.png "Authorization Tab")

    ![Authorization Tab](images/authcode-2.png "Authorization Tab")

    ![Authorization Tab](images/authcode-3.png "Authorization Tab")

    ![Authorization Tab](images/generated-access-token2.png "Authorization Tab")

3. Accessing without Token

    ![Accessing without Token](images/noauth.png "Accessing without Token")

4. Accessing Resource With Token

    ![Accessing Resource with Token](images/oauth.png "Accessing Resource with Token")

Note  : use postman collection for easy, just import environment & file  postman collection

#### Get All Role 
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to generate TOKEN with credentials that have 'read-role' permission 
   example:`username:99.0002 ,password:12345`
   
   List User Role:
   
   |#|Role|NPA|Password|
   |---|---|---|---|
   |1|anggota|99.0001|12345|
   |2|superadmin|99.0002|12345|
   |3|admin_pp|99.0003|12345|
   |4|tasykil_pp|99.0004|12345|
   |5|admin_pw|99.0005|12345|
   |6|tasykil_pw|99.0006|12345|
   |7|admin_pd|99.0007|12345|
   |8|tasykil_pd|99.0008|12345|
   |9|admin_pc|99.0009|12345|
   |10|tasykil_pc|99.0010|12345|
   |11|admin_pj|99.0011|12345|
   
      ![with credentials that have 'read-role' permission](images/admin-1.png "with credentials that have 'read-role' permission").
       
      ![Access Role](images/admin-2.png "Access Role")
  
3. Launch POSTMAN to generate TOKEN with credentials that do not have 'read-role' permission 
   example:`username:99.0001 ,password:12345`
      ![with credentials that do not have 'read-role' permission](images/anggota-1.png "with credentials that do not have 'read-role' permission").
       
      ![Access Role](images/anggota-2.png "Access Role")


#### View And Edit My Profile
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to Login
   example:`username:99.0001 ,password:12345`
         ![login](images/profile-view-1.png "login")
         
3. Accessing my profile at:`/v1/api/anggota/profile`
        ![my profile](images/profile-view-2.png "my profile")
        
4. Edit your profile at :`/v1/api/anggota/profile` example: `"golDarah": "O"` to `"golDarah": "Z"`

    ![edit](images/profile-edit-1.png "edit")
    
    result :
    
    ![edit](images/profile-edit-2.png "edit")
    
5. Edit other profile at :`/v1/api/anggota/profile` example: `"npa": "99.0001",` to `"npa": "99.0002",`

    ![edit](images/profile-edit-1.png "edit")
     *cannot allow editing other anggota's profiles 

#### Get List Wilayah (PW)
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to Login
   example:`username:99.0001 ,password:12345`
         ![login](images/profile-view-1.png "login")
         
3. Accessing data wilayah at:`/v1/api/wilayah` 
       ![Wilayah](images/wilayah-list.png "Wilayah")
               
#### Edit Wilayah (PW)
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to Login
   example:`username:99.0001 ,password:12345`
         ![login](images/profile-view-1.png "login")
         
3. Accessing data wilayah at:`/v1/api/wilayah` 
       ![Wilayah](images/wilayah-list.png "Wilayah")
             
4. Accessing edit wilayah at:`/v1/api/wilayah/{id}` example: `"kodeWilayah": "PW-1"` to `"kodeWilayah": "PW-8"` 
          ![edit-wilayah](images/wilayah-1.png "edit Wilayah")
Note  : `/{id}` is id wilayah.  

    result :
          ![result wilayah](images/wilayah-2.png "result wilayah")
    
#### List Monografi Level PC
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to Login
   example:`username:99.0001 ,password:12345`
         ![login](images/profile-view-1.png "login")
         
3. Accessing data all monografi level PC at:`/v1/api/monografi/pc` 
        ![monografipc-list](images/monografi-pc-list.png "monografi-pc list")

#### Add Monografi Level PC
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to Login
   example:`username:99.0001 ,password:12345`
         ![login](images/profile-view-1.png "login")
3. Accessing add data monografi-pc at: `/v1/api/monografi/pc` 
  
     example : ```{
                 "latitude": 0.615096,
                 "longitude": 6.3503983868E8,
                 "email": "hasanwastuti.gada@yahoo.co.id",
                 "alamatLengkap": "Gg. Camar No. 65",
                 "alamatAlternatif": "Psr. Katamso No. 174, Bandung 54542, Gorontalo",
                 "noKontak": "089736364",
                 "luas": 9,
                 "btsWilayahUtara": "Palangka Raya",
                 "btsWilayahSelatan": "Mojokerto",
                 "btsWilayahTimur": "Sabang",
                 "btsWilayahBarat": "Batam",
                 "jarakIbuKotaNegara": 4,
                 "id": 1,
                 "cabang": {
                     "kdPc": "PC.120",
                     "namaPc": "Cibeber"
                 },
                 "daerah": {
                     "kdPd": "PD-4",
                     "namaPd": "Kabupaten Subang"
                 },
                 "wilayah": {
                     "id": "PW-5",
                     "namaWilayah": "Jawa Barat",
                     "isDeleted": null
                 },
                 "provinsi": {
                     "id": "13",
                     "namaProvinsi": "Sumatera Barat"
                 },
                 "kabupaten": {
                     "id": "8108",
                     "nama": "Kab. Seram Bagian Barat"
                 },
                 "kecamatan": {
                     "id": "1101010",
                     "nama": " Kwanyar"
                 },
                 "foto": null,
                 "jamiyyah": {
                     "ketua": "Hasan Cornelia Laksmiwati",
                     "wklKetua": "Hasan Zulaikha Haryanti S.E.I",
                     "sekretaris": "Raisa Hartati S.I.Kom",
                     "wklSekretaris": "Harimurti Omar Pradipta M.Kom.",
                     "bendahara": "Tina Lailasari",
                     "wklBendahara": "Jefri Kajen Hutasoit",
                     "bidJamiyyah": "Dariati Irawan",
                     "wklBidJamiyyah": "Harimurti Wadi Simanjuntak",
                     "bidKaderisasi": "Cakrawangsa Jailani S.IP",
                     "wklBidKaderisasi": "Prayitna Darman Dongoran",
                     "bidAdministrasi": "Jamil Samosir S.IP",
                     "wklBidAdministrasi": "Titi Lidya Maryati M.Ak",
                     "bidPendidikan": "Ajiono Raihan Rajata S.I.Kom",
                     "wklBidPendidikan": "Bakti Bakiadi Suryono",
                     "bidDakwah": "Almira Yuliarti",
                     "wklBidDakwah": "Taufik Endra Ardianto",
                     "bidHumasPublikasi": "Mulyono Putra M.TI.",
                     "wklBidHumasPublikasi": "Gina Hani Oktaviani S.IP",
                     "bidHal": "Cawisono Putu Wijaya S.Gz",
                     "wklBidHal": "Kasiyah Widiastuti",
                     "bidOrgSeni": "Vega Pranowo",
                     "wklBidOrgSeni": "Ophelia Andriani",
                     "bidSosial": "Calista Rika Namaga",
                     "bidEkonomi": "Yono Prasetyo Waluyo",
                     "wklBidEkonomi": "Luwes Uwais",
                     "penasehat1": "Olivia Widiastuti",
                     "penasehat2": "Karimah Wahyuni",
                     "penasehat3": "Vera Rahayu Handayani",
                     "pembantuUmum1": "Dono Siregar",
                     "pembantuUmum2": "Jatmiko Simbolon",
                     "pembantuUmum3": "Mutia Mardhiyah",
                     "hari": "Selasa",
                     "pukul": "01-01-1970 09:04:00",
                     "musyCabLastMasehi": "25-12-2018",
                     "musyCabLastHijri": "25-09-1440",
                     "countAnggotaBiasa": 7,
                     "countAnggotaTersiar": 3,
                     "countAnggotaIstimewa": 5,
                     "countTidakHerregistrasi": 1,
                     "countMutasiPersis": 1,
                     "countMutasiTempat": 9,
                     "countUndurDiri": 4,
                     "countMeninggalDunia": 3
                 }
             }```
             
  ![monografi-pc-add](images/monografi-pc-add.png "monografi-pc-add")

#### Edit Monografi Level PC
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to Login
   example:`username:99.0001 ,password:12345`
         ![login](images/profile-view-1.png "login")
         
3. Accessing edit data monografi-pc at: `/v1/api/monografi/pc/{id}`
example : edit kab value 

 ![editMonografi](images/monografi-pc-edit.png "edit Monografi")

Note  : `/{id}` is id monografi-pc.   

#### List Monografi Level PW
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to Login
   example:`username:99.0001 ,password:12345`
         ![login](images/profile-view-1.png "login")
         
3. Accessing data all monografi level PW at:`/v1/api/monografi/pw` 
        ![monografipw-list](images/monografi-pw-list.png "monografi-pw list")           
        
    
  #### List Indek Performa Level PJ
  
  1. Paste this command to your terminal
  
     `mvn clean spring-boot:run`
      
  2. Launch POSTMAN to Login
     example:`username:99.0001 ,password:12345`
           ![login](images/profile-view-1.png "login")
  3. Accessing data all indek performa level PJ at:`/v1/api/performa/pj` 
             ![performapj-list](images/performa-pj-list.png "performa-pj list")           
             
#### Add Indek Performa Level PJ
1. Paste this command to your terminal

   `mvn clean spring-boot:run`
    
2. Launch POSTMAN to Login
   example:`username:99.0001 ,password:12345`
         ![login](images/profile-view-1.png "login")
3. Accessing add data indek performa level PJ at: `/v1/api/performa/pj` 
         example : ```{
                              "q1a": "25'%' sesuai SK",
                              "q2a": "Sebagian besar mempunyai",
                              "q3a": "Sebagian besar memahami",
                              "q4a": "Pernah membuat",
                              "q5a": "Tidak pernah",
                              "q6a": "Berjalan > tiga kali sebulan",
                              "q1b": "Jumlah anggota > 25 orang",
                              "q2b": "Sering dengan seluruh anggota",
                              "q3b": "Berjalan < tiga kali sebulan",
                              "q4b": "Tidak ada",
                              "q5b": "Kurang dari 50% anggota hadir",
                              "q6b": "Tidak",
                              "q7b": "75% anggota hadir",
                              "q8b": "Tidak ada",
                              "q9b": "Ada dan jarang mengikuti",
                              "q10b": "Setiap kegiatan ada publikasi",
                              "q1c": "Lebih dari 10 orang",
                              "q2c": "Tidak ada",
                              "q3c": "Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan",
                              "q1d": "Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan",
                              "q2d": "Sebagian besar belum",
                              "q3d": "Sudah",
                              "q4d": "Belum memiliki",
                              "q5d": "Jarang digunakan",
                              "q6d": "Surat-surat masuk tidak diarsipkan",
                              "q7d": "Memiliki",
                              "q8d": "Jarang digunakan",
                              "q9d": "Surat-surat keluar tidak diarsipkan",
                              "q10d": "Memiliki",
                              "q11d": "Belum digunakan",
                              "q12d": "Tidak ada Buku Induk Anggota",
                              "q13d": "Tidak ada Buku Induk Anggota",
                              "q14d": "Memiliki",
                              "q15d": "Tidak sesuai",
                              "q16d": "Belum memiliki",
                              "q17d": "Tidak sesuai",
                              "q18d": "Tidak sesuai",
                              "q19d": "Belum memiliki",
                              "q20d": "Belum digunakan",
                              "q21d": "Memiliki",
                              "q22d": "Tidak ada Buku Daftar Hadir Kegiatan",
                              "q23d": "Belum memiliki",
                              "q24d": "Sering digunakan",
                              "q25d": "Memiliki",
                              "q26d": "Memiliki",
                              "q27d": "Sebagian besar data sudah lengkap diisi",
                              "q28d": "Tidak ada Buku Inventaris",
                              "q1e": "Sesuai",
                              "q2e": "Tidak ada",
                              "q3e": "Belum pernah",
                              "q4e": "Ya",
                              "q5e": "Tidak",
                              "q6e": "Tidak",
                              "q7e": "Tidak",
                              "q8e": "Ya",
                              "q9e": "Belum pernah",
                              "q10e": "50% PJ hadir",
                              "q1f": "100% anggota memenuhi",
                              "q2f": "Ya",
                              "q3f": "Tidak",
                              "q4f": "Tidak",
                              "q5f": "Tidak",
                              "q6f": "Ya",
                              "q7f": "Saldo",
                              "lastSurvey": "09-09-2020 20:00:20",
                              "deletedAt": null,
                              "id": 1,
                              "kodePj": "PJ-445",
                              "namaPj": "Baru PJ",
                              "cabang": {
                                  "kdPc": "PC.1",
                                  "namaPc": "Andir"
                              },
                              "daerah": {
                                  "kdPd": "PD-5",
                                  "namaPd": "Kabupaten Bogor"
                              },
                              "wilayah": {
                                  "id": "PW-2",
                                  "namaWilayah": "DKI Jakarta",
                                  "isDeleted": null
                              },
                              "alamat": "Jr. Halim No. 837, Probolinggo 54149, Gorontalo",
                              "ketuaPj": "Jarwi Hutagalung",
                              "noHp": "089599664",
                              "provinsi": {
                                  "id": "65",
                                  "namaProvinsi": "Kalimantan Utara"
                              },
                              "kabupaten": {
                                  "id": "5314",
                                  "nama": "Kab. Rote Ndao"
                              },
                              "kecamatan": {
                                  "id": "1118010",
                                  "nama": " Meureudu"
                              },
                              "desa": {
                                  "id": "1101030023",
                                  "nama": "Suak Baru"
                              }
                          }```
 
   ![indekperforma-pj-add](images/performa-pj-add.png "indek performa-pj-add")
 
 #### Edit Indek Performa Level PJ
 1. Paste this command to your terminal
 
    `mvn clean spring-boot:run`
     
 2. Launch POSTMAN to Login
    example:`username:99.0001 ,password:12345`
          ![login](images/profile-view-1.png "login")
          
 3. Accessing edit data indek performa level PJ at: `/v1/api/performa/pj/{id}`
 example : edit namaPj value 
 
  ![editindekperformapj](images/performa-pj-edit.png "edit performa pj")
 
 Note  : `/{id}` is id indek performa level PJ.         
 
 
   
  #### List Indek Performa Level PD
  
  1. Paste this command to your terminal
  
     `mvn clean spring-boot:run`
      
  2. Launch POSTMAN to Login
     example:`username:99.0001 ,password:12345`
           ![login](images/profile-view-1.png "login")
  3. Accessing data all indek performa level PD at:`/v1/api/performa/pj` 
             ![performapd-list](images/performa-pd-list.png "performa-pd list")           
 
  #### Edit Indek Performa Level PD
  1. Paste this command to your terminal
  
     `mvn clean spring-boot:run`
      
  2. Launch POSTMAN to Login
     example:`username:99.0001 ,password:12345`
           ![login](images/profile-view-1.png "login")
           
  3. Accessing edit data indek performa level Pd at: `/v1/api/performa/pd/{id}`
  example : edit ketuaPd value 
  
   ![editindekperformapd](images/performa-pd-edit.png "edit performa pd")
  
  Note  : `/{id}` is id indek performa level PD.  
  
   #### Add Indek Performa Level PD
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
   3. Accessing add data indek performa level PD at: `/v1/api/performa/pj` 
            example :```{
                                "q1a": "50'%' sesuai SK",
                                "q2a": "Sebagian besar mempunyai",
                                "q3a": "Semua tidak memahami",
                                "q4a": "Pernah membuat",
                                "q5a": "Membuat secara berkala",
                                "q6a": "Berjalan < tiga kali sebulan",
                                "q1b": "Jumlah anggota kurang dari 25 orang",
                                "q2b": "Sering dengan sebagian besar anggota",
                                "q3b": "Berjalan < tiga kali sebulan",
                                "q4b": "Kegiatan rutin > tiga kali sebulan",
                                "q5b": "75% anggota hadir",
                                "q6b": "Tidak",
                                "q7b": "50% anggota hadir",
                                "q8b": "Ada dan jarang mengikuti",
                                "q9b": "Ada dan selalu mengikuti",
                                "q10b": "Setiap kegiatan ada publikasi",
                                "q1c": "Tidak ada",
                                "q2c": "Tidak ada",
                                "q3c": "Tidak ada calon anggota yang mengikuti program pembinaan",
                                "q1d": "Tidak ada calon anggota yang mengikuti program pembinaan",
                                "q2d": "Sudah",
                                "q3d": "Belum",
                                "q4d": "Belum memiliki",
                                "q5d": "Jarang digunakan",
                                "q6d": "Sebagian besar surat masuk diarsipkan",
                                "q7d": "Belum memiliki",
                                "q8d": "Jarang digunakan",
                                "q9d": "Sebagian besar surat keluar tidak diarsipkan",
                                "q10d": "Memiliki",
                                "q11d": "Belum digunakan",
                                "q12d": "Sudah terisi",
                                "q13d": "Sebagian besar tidak sesuai",
                                "q14d": "Memiliki",
                                "q15d": "Tidak sesuai",
                                "q16d": "Belum memiliki",
                                "q17d": "Sesuai",
                                "q18d": "Sesuai",
                                "q19d": "Memiliki",
                                "q20d": "Jarang digunakan",
                                "q21d": "Belum memiliki",
                                "q22d": "Selalu digunakan",
                                "q23d": "Memiliki",
                                "q24d": "Sering digunakan",
                                "q25d": "Belum memiliki",
                                "q26d": "Belum memiliki",
                                "q27d": "Sudah",
                                "q28d": "Belum",
                                "q1e": "Tidak sesuai",
                                "q2e": "Ada",
                                "q3e": "Satu tahun satu kali",
                                "q4e": "Tidak",
                                "q5e": "Ya",
                                "q6e": "Tidak",
                                "q7e": "Tidak",
                                "q8e": "Ya",
                                "q9e": "Belum pernah",
                                "q10e": "Kurang dari 50% PJ hadir",
                                "q1f": "100% anggota memenuhi",
                                "q2f": "Ya",
                                "q3f": "Tidak",
                                "q4f": "Tidak",
                                "q5f": "Tidak",
                                "q6f": "Ya",
                                "q7f": "Saldo",
                                "lastSurvey": "15-09-2020 05:17:12",
                                "deletedAt": null,
                                "daerah": {
                                    "kdPd": "PD-20",
                                    "namaPd": "Kabupaten Bandung"
                                },
                                "wilayah": {
                                    "id": "PW-3",
                                    "namaWilayah": "Jawa Timur",
                                    "isDeleted": null
                                },
                                "provinsi": {
                                    "id": "82",
                                    "namaProvinsi": "Maluku Utara"
                                },
                                "kabupaten": {
                                    "id": "1115",
                                    "nama": "Kab. Nagan Raya"
                                },
                                "alamat": "Dk. Jakarta No. 13, Tegal 34692, SulBar",
                                "ketuaPd": "Lala Puji Astuti",
                                "noHp": "089438002"
                            }```
                            
      ![indekperforma-pd-add](images/performa-pd-add.png "indek performa-pd-add")


 #### List Indek Performa Level PW
  
  1. Paste this command to your terminal
  
     `mvn clean spring-boot:run`
      
  2. Launch POSTMAN to Login
     example:`username:99.0001 ,password:12345`
           ![login](images/profile-view-1.png "login")
  3. Accessing data all indek performa level PW at:`/v1/api/performa/pw` 
             ![performapd-list](images/performa-pw-list.png "performa-pd list")           
 
 
   #### Edit Indek Performa Level PW
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing edit data indek performa level PW at: `/v1/api/performa/pw/{id}`
   example : edit ketuaPw value 
             ![performapd-list](images/performa-pw-edit.png "performa-pd list")           
   
   Note  : `/{id}` is id indek performa level PW.  
   
   
  #### Add Indek Performa Level PW
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
   3. Accessing add data indek performa level PW at: `/v1/api/performa/pw` 
            example :```    {
                                "q1a": "25'%' sesuai SK",
                                "q2a": "Semua mempunyai",
                                "q3a": "Sebagian besar memahami",
                                "q4a": "Tidak pernah",
                                "q5a": "Tidak pernah",
                                "q6a": "Berjalan > tiga kali sebulan",
                                "q1b": "Jumlah anggota > 25 orang",
                                "q2b": "Sering dengan sebagian besar anggota",
                                "q3b": "Berjalan > tiga kali sebulan",
                                "q4b": "Tidak ada",
                                "q5b": "50% anggota hadir",
                                "q6b": "Tidak",
                                "q7b": "100% anggota hadir",
                                "q8b": "Ada dan jarang mengikuti",
                                "q9b": "Ada dan jarang mengikuti",
                                "q10b": "Sebagian besar kegiatan ada publikasi",
                                "q1c": "Lebih dari 10 orang",
                                "q2c": "Ada",
                                "q3c": "Sebagian besar calon  anggota yang tercatat mengikuti program pembinaan",
                                "q1d": "Semua calon anggota yang tercatat mengikuti program pembinaan",
                                "q2d": "Sebagian besar sudah",
                                "q3d": "Belum",
                                "q4d": "Belum memiliki",
                                "q5d": "Belum digunakan",
                                "q6d": "Sebagian besar surat masuk diarsipkan",
                                "q7d": "Memiliki",
                                "q8d": "Jarang digunakan",
                                "q9d": "Sebagian besar surat keluar tidak diarsipkan",
                                "q10d": "Belum memiliki",
                                "q11d": "Belum digunakan",
                                "q12d": "Sebagian besar data belum diisi",
                                "q13d": "Sesuai",
                                "q14d": "Belum memiliki",
                                "q15d": "Sesuai",
                                "q16d": "Memiliki",
                                "q17d": "Sesuai",
                                "q18d": "Sesuai",
                                "q19d": "Memiliki",
                                "q20d": "Jarang digunakan",
                                "q21d": "Memiliki",
                                "q22d": "Jarang digunakan",
                                "q23d": "Memiliki",
                                "q24d": "Belum digunakan",
                                "q25d": "Belum memiliki",
                                "q26d": "Memiliki",
                                "q27d": "Sudah",
                                "q28d": "Tidak ada Buku Inventaris",
                                "q1e": "Sesuai",
                                "q2e": "Tidak ada",
                                "q3e": "Satu tahun satu kali",
                                "q4e": "Tidak",
                                "q5e": "Ya",
                                "q6e": "Ya",
                                "q7e": "Ya",
                                "q8e": "Ya",
                                "q9e": "> 6 kali setahun",
                                "q10e": "Kurang dari 50% PJ hadir",
                                "q1f": "50% anggota memenuhi",
                                "q2f": "Tidak",
                                "q3f": "Tidak",
                                "q4f": "Tidak",
                                "q5f": "Tidak",
                                "q6f": "Tidak",
                                "q7f": "Saldo",
                                "lastSurvey": "15-09-2020 05:17:12",
                                "deletedAt": null,
                                "id": 1,
                                "wilayah": {
                                    "id": "PW-4",
                                    "namaWilayah": "Banten"
                                },
                                "provinsi": {
                                    "id": "51",
                                    "namaProvinsi": "Bali"
                                },
                                "alamat": "Ds. HOS. Cjokroaminoto (Pasirkaliki) No. 130, Tangerang Selatan 34030, SulUt",
                                "ketuaPw": "Opan Lukita Iswahyudi S.I.Kom",
                                "noHp": "089802391"
                            }```
                            
      ![indekperforma-pw-add](images/performa-pw-add.png "indek performa-pd-add")


 #### List Indek Performa Level PC
  
  1. Paste this command to your terminal
  
     `mvn clean spring-boot:run`
      
  2. Launch POSTMAN to Login
     example:`username:99.0001 ,password:12345`
           ![login](images/profile-view-1.png "login")
  3. Accessing data all indek performa level PC at:`/v1/api/performa/pc` 
             ![performapd-list](images/performa-pc-list.png "performa-pd list")           
 
 
   #### Edit Indek Performa Level PC
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing edit data indek performa level PC at: `/v1/api/performa/pc/{id}`
   example : edit ketuaPc value 
             ![performapd-list](images/performa-pc-edit.png "performa-pd list")           
   
   Note  : `/{id}` is id indek performa level PC.  
   
   
  #### Add Indek Performa Level PC
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
   3. Accessing add data indek performa level PC at: `/v1/api/performa/pw` 
            example :```    {
                                "q1a": "25'%' sesuai SK",
                                "q2a": "Semua tidak mempunyai",
                                "q3a": "Sebagian besar tidak memahami",
                                "q4a": "Membuat secara berkala",
                                "q5a": "Pernah membuat",
                                "q6a": "Berjalan > tiga kali sebulan",
                                "q1b": "Jumlah anggota kurang dari 25 orang",
                                "q2b": "Tidak pernah",
                                "q3b": "Tidak berjalan",
                                "q4b": "Kegiatan rutin jamaah kurang dari tiga kali sebulan",
                                "q5b": "100% anggota hadir",
                                "q6b": "Tidak",
                                "q7b": "75% anggota hadir",
                                "q8b": "Ada dan jarang mengikuti",
                                "q9b": "Ada dan sering mengikuti",
                                "q10b": "Setiap kegiatan ada publikasi",
                                "q1c": "Tidak ada",
                                "q2c": "Tidak ada",
                                "q3c": "Semua calon anggota yang tercatat mengikuti program pembinaan",
                                "q1d": "Sebagian kecil calon anggota yang tercatat mengikuti program pembinaan",
                                "q2d": "Sebagian besar belum",
                                "q3d": "Sudah",
                                "q4d": "Belum memiliki",
                                "q5d": "Jarang digunakan",
                                "q6d": "Seluruh surat masuk diarsipkan",
                                "q7d": "Belum memiliki",
                                "q8d": "Jarang digunakan",
                                "q9d": "Sebagian besar surat keluar tidak diarsipkan",
                                "q10d": "Memiliki",
                                "q11d": "Belum digunakan",
                                "q12d": "Sudah terisi",
                                "q13d": "Tidak sesuai",
                                "q14d": "Belum memiliki",
                                "q15d": "Sesuai",
                                "q16d": "Memiliki",
                                "q17d": "Sesuai",
                                "q18d": "Tidak sesuai",
                                "q19d": "Belum memiliki",
                                "q20d": "Belum digunakan",
                                "q21d": "Memiliki",
                                "q22d": "Selalu digunakan",
                                "q23d": "Memiliki",
                                "q24d": "Sering digunakan",
                                "q25d": "Belum memiliki",
                                "q26d": "Belum memiliki",
                                "q27d": "Sebagian besar data sudah lengkap diisi",
                                "q28d": "Sebagian besar data sudah sesuai",
                                "q1e": "Tidak sesuai",
                                "q2e": "Ada",
                                "q3e": "Satu kali",
                                "q4e": "Ya",
                                "q5e": "Ya",
                                "q6e": "Ya",
                                "q7e": "Tidak",
                                "q8e": "Tidak",
                                "q9e": "Kurang dari 3 kali setahun",
                                "q10e": "Kurang dari 50% PJ hadir",
                                "q1f": "75% anggota memenuhi",
                                "q2f": "Ya",
                                "q3f": "Ya",
                                "q4f": "Ya",
                                "q5f": "Tidak",
                                "q6f": "Ya",
                                "q7f": "Nol",
                                "lastSurvey": "15-09-2020 05:17:12",
                                "deletedAt": null,
                                "id": 1,
                                "cabang": {
                                    "kdPc": "PC.170",
                                    "namaPc": "Pasirwangi"
                                },
                                "daerah": {
                                    "kdPd": "PD-3",
                                    "namaPd": "Kabupaten Cianjur"
                                },
                                "wilayah": {
                                    "id": "PW-5",
                                    "namaWilayah": "Bali"
                                },
                                "provinsi": {
                                    "id": "51",
                                    "namaProvinsi": "Bali"
                                },
                                "kabupaten": {
                                    "id": "3371",
                                    "nama": "Kota Magelang"
                                },
                                "kecamatan": {
                                    "id": "1608030",
                                    "nama": " Buay Pemaca"
                                },
                                "alamat": "Dk. Lembong No. 711, Probolinggo 42280, NTB",
                                "ketuaPc": "Jindra Kemal Nainggolan S.E.",
                                "noHp": "089887897"
                            }```
                            
      ![indekperforma-pw-add](images/performa-pc-add.png "indek performa-pd-add")

   
   #### Soft Deleted Indek Performa Level PW
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing delete data indek performa level PW at: `/v1/api/performa/pw/{id}`
            ![performapw-delete](images/performa-pw-delete.png "performa-pw list")           
   Note  : `/{id}` is id indek performa level PW.  
   
   
   #### Soft Deleted Indek Performa Level PJ
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing delete data indek performa level PJ at: `/v1/api/performa/pj/{id}`
                ![performapj-delete](images/performa-pw-delete.png "performa-pj delete")           
  Note  : `/{id}` is id indek performa level PJ.  
   
   
   #### Soft Deleted Indek Performa Level PC
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing delete data indek performa level PC at: `/v1/api/performa/pc/{id}`
 
       ![performapc-delete](images/performa-pc-delete.png "performa-pc delete")           
   
   Note  : `/{id}` is id indek performa level PC.    
   
                                  
   #### Soft Deleted Indek Performa Level Pd
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing delete data indek performa level PD at: `/v1/api/performa/pd/{id}`
 
       ![performapd-delete](images/performa-pd-delete.png "performa-pd delete")           
   
   Note  : `/{id}` is id indek performa level PD.
   
   #### Soft Delete Monografi PC
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing delete data Monografi PC at: `/v1/api/monografi/pc/{id}`
 
       ![monografipc-delete](images/monografi-pc-delete.png "monografi-pc delete")           
   
   Note  : `/{id}` is id Monografi PC.   
   
   #### Soft Delete Monografi PW
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing delete data Monografi PC at: `/v1/api/monografi/pw/{id}`
 
       ![monografipc-delete](images/monografi-pw-delete.png "monografi-pc delete")           
   
   Note  : `/{id}` is id Monografi PW.   
   
   
   #### Restore Monografi PW
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing delete data Monografi PC at: `/v1/api/monografi/pw/{id}/restore`
 
       ![monografipc-delete](images/monografi-pw-restore.png "monografi-pc delete")           
   
   Note  : `/{id}` is id Monografi PW.   
   
   #### Restore Monografi PC
   1. Paste this command to your terminal
   
      `mvn clean spring-boot:run`
       
   2. Launch POSTMAN to Login
      example:`username:99.0001 ,password:12345`
            ![login](images/profile-view-1.png "login")
            
   3. Accessing restore data Monografi PC at: `/v1/api/monografi/pc/{id}/restore`
 
       ![monografipc-restore](images/monografi-pc-restore.png "monografi-pc restore")           
   
   Note  : `/{id}` is id Monografi PC.
   
   
   #### List Anggota Keluarga
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing anggota keluarga at GET : `/v1/api/anggota/{id}/keluarga`
    
       ![keluarga-list](images/anggota-keluarga-list.png "keluarga list")           
      
      Note  : `/{id}` is id anggota.   
      
   #### List Anggota keterampilan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing anggota keterampilan at  GET : `/v1/api/anggota/{id}/keterampilan`
    
       ![keteramoilan-list](images/anggota-keterampilan-list.png "keterampilan list")           
      
      Note  : `/{id}` is id anggota.   
      
   #### List Anggota Pekerjaan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing anggota pekerjaan at  GET : `/v1/api/anggota/{id}/pekerjaan`
    
       ![pekerjaan-list](images/anggota-pekerjaan-list.png "pekerjaan list")           
      
      Note  : `/{id}` is id anggota.   
      
   #### List Anggota Pendidikan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing anggota pendidikan at  GET : `/v1/api/anggota/{id}/pendidikan`
    
       ![pendidikan-list](images/anggota-pendidikan-list.png "pendidikan list")           
      
      Note  : `/{id}` is id anggota.   
      
      
   #### List Anggota Training
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing anggota training at  GET : `/v1/api/anggota/{id}/training`
    
       ![training-list](images/anggota-training-list.png "training list")           
      
      Note  : `/{id}` is id anggota. 
      
   #### List data deleted Anggota Training
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing list data deleted anggota training with methode GET at : `/v1/api/anggota/{id}/training/delete`
    
       ![training-list-deleted](images/anggota-training-list-delete.png "training list deleted")           
      
      Note  : `/{id}` is id anggota. 
      
      
   #### Add Anggota Training
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing add anggota training with methode POST at : `/v1/api/anggota/{id}/training`
    
       ![training](images/anggota-training-add.png "training")           
      
      Note  : `/{id}` is id anggota. 

   #### Edit Anggota Training
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing edit anggota training with methode PUT  at : `/v1/api/anggota/{id}/training/{idTraining}`
    
       ![training](images/anggota-training-edit.png "training")           
      
      Note  : `{id}` is id anggota. and `{idTraining}` is id training

   #### Restore Anggota Training
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing restore anggota training with methode PATCH at : `/v1/api/anggota/{id}/training/{idTraining}/restore`
    
       ![training](images/anggota-training-restore.png "training")           
      
      Note  : `{id}` is id anggota. and `{idTraining}` is id training

   #### Delete Anggota Training
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing delete anggota training with methode DELETE at : `/v1/api/anggota/{id}/training/{idTraining}`
    
       ![training](images/anggota-training-edit.png "training")           
      
      Note  : `{id}` is id anggota. and `{idTraining}` is id training


  #### List data deleted Anggota Pendidikan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing list data deleted anggota pendidikan with methode GET at : `/v1/api/anggota/{id}/pendidikan/delete`
    
       ![pendidikan-list-deleted](images/anggota-pendidikan-list-delete.png "pendidikan list deleted")           
      
      Note  : `/{id}` is id anggota. 
      
      
   #### Add Anggota Pendidikan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing add anggota pendidikan with methode POST at : `/v1/api/anggota/{id}/pendidikan`
    
       ![pendidikan](images/anggota-pendidikan-add.png "pendidikan")           
      
      Note  : `/{id}` is id anggota. 

   #### Edit Anggota Pendidikan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing edit anggota pendidikan with methode PUT  at : `/v1/api/anggota/{id}/pendidikan/{idPendidikan}`
    
       ![pendidikan](images/anggota-pendidikan-edit.png "pendidikan")           
      
      Note  : `{id}` is id anggota. and `{idPendidikan}` is id pendidikan

   #### Restore Anggota Pendidikan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing restore anggota pendidikan with methode PATCH at : `/v1/api/anggota/{id}/pendidikan/{idPendidikan}/restore`
    
       ![pendidikan](images/anggota-pendidikan-restore.png "pendidikan")           
      
      Note  : `{id}` is id anggota. and `{idPendidikan}` is id pendidikan

   #### Delete Anggota Pendidikan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing delete anggota pendidikan with methode DELETE at : `/v1/api/anggota/{id}/pendidikan/{idPendidikan}`
    
       ![pendidikan](images/anggota-pendidikan-edit.png "pendidikan")           
      
      Note  : `{id}` is id anggota. and `{idTraining}` is id pendidikan
      
 #### List data deleted Anggota Keterampilan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing list data deleted anggota keterampilan with methode GET at : `/v1/api/anggota/{id}/keterampilan/delete`
    
       ![keterampilan-list-deleted](images/anggota-keterampilan-list-delete.png "keterampilan list deleted")           
      
      Note  : `/{id}` is id anggota. 
      
      
   #### Add Anggota Keterampilan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing add anggota keterampilan with methode POST at : `/v1/api/anggota/{id}/keterampilan`
    
       ![keterampilan](images/anggota-keterampilan-add.png "keterampilan")           
      
      Note  : `/{id}` is id anggota. 

   #### Edit Anggota Keterampilan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing edit anggota keterampilan with methode PUT  at : `/v1/api/anggota/{id}/keterampilan/{idKeterampilan}`
    
       ![keterampilan](images/anggota-keterampilan-edit.png "keterampilan")           
      
      Note  : `{id}` is id anggota. and `{idKeterampilan}` is id keterampilan

   #### Restore Anggota Keterampilan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing restore anggota keterampilan with methode PATCH at : `/v1/api/anggota/{id}/keterampilan/{idKeterampilan}/restore`
    
       ![keterampilan](images/anggota-keterampilan-restore.png "keterampilan")           
      
      Note  : `{id}` is id anggota. and `{idKeterampilan}` is id keterampilan

   #### Delete Anggota Keterampilan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing delete anggota keterampilan with methode DELETE at : `/v1/api/anggota/{id}/keterampilan/{idKeterampilan}`
    
       ![keterampilan](images/anggota-keterampilan-edit.png "keterampilan")           
      
      Note  : `{id}` is id anggota. and `{idTraining}` is id keterampilan
      

 #### List data deleted Anggota Pekerjaan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing list data deleted anggota pekerjaan with methode GET at : `/v1/api/anggota/{id}/pekerjaan/delete`
    
       ![pekerjaan-list-deleted](images/anggota-pekerjaan-list-delete.png "pekerjaan list deleted")           
      
      Note  : `/{id}` is id anggota. 
      
      
   #### Add Anggota Pekerjaan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing add anggota pekerjaan with methode POST at : `/v1/api/anggota/{id}/pekerjaan`
    
       ![pekerjaan](images/anggota-pekerjaan-add.png "pekerjaan")           
      
      Note  : `/{id}` is id anggota. 

   #### Edit Anggota Pekerjaan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing edit anggota pekerjaan with methode PUT  at : `/v1/api/anggota/{id}/pekerjaan/{idPekerjaan}`
    
       ![pekerjaan](images/anggota-pekerjaan-edit.png "pekerjaan")           
      
      Note  : `{id}` is id anggota. and `{idPekerjaan}` is id pekerjaan

   #### Restore Anggota Pekerjaan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing restore anggota pekerjaan with methode PATCH at : `/v1/api/anggota/{id}/pekerjaan/{idPekerjaan}/restore`
    
       ![pekerjaan](images/anggota-pekerjaan-restore.png "pekerjaan")           
      
      Note  : `{id}` is id anggota. and `{idPekerjaan}` is id pekerjaan

   #### Delete Anggota Pekerjaan
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing delete anggota pekerjaan with methode DELETE at : `/v1/api/anggota/{id}/pekerjaan/{idPekerjaan}`
    
       ![pekerjaan](images/anggota-pekerjaan-edit.png "pekerjaan")           
      
      Note  : `{id}` is id anggota. and `{idTraining}` is id pekerjaan
      
      
 #### List data deleted Anggota Keluarga
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing list data deleted anggota keluarga with methode GET at : `/v1/api/anggota/{id}/keluarga/delete`
    
       ![keluarga-list-deleted](images/anggota-keluarga-list-delete.png "keluarga list deleted")           
      
      Note  : `/{id}` is id anggota. 
      
      
   #### Add Anggota Keluarga
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing add anggota keluarga with methode POST at : `/v1/api/anggota/{id}/keluarga`
    
       ![keluarga](images/anggota-keluarga-add.png "keluarga")           
      
      Note  : `/{id}` is id anggota. 

   #### Edit Anggota Keluarga
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing edit anggota keluarga with methode PUT  at : `/v1/api/anggota/{id}/keluarga/{idKeluarga}`
    
       ![keluarga](images/anggota-keluarga-edit.png "keluarga")           
      
      Note  : `{id}` is id anggota. and `{idKeluarga}` is id keluarga

   #### Restore Anggota Keluarga
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing restore anggota keluarga with methode PATCH at : `/v1/api/anggota/{id}/keluarga/{idKeluarga}/restore`
    
       ![keluarga](images/anggota-keluarga-restore.png "keluarga")           
      
      Note  : `{id}` is id anggota. and `{idKeluarga}` is id keluarga

   #### Delete Anggota Keluarga
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing delete anggota keluarga with methode DELETE at : `/v1/api/anggota/{id}/keluarga/{idKeluarga}`
    
       ![keluarga](images/anggota-keluarga-edit.png "keluarga")           
      
      Note  : `{id}` is id anggota. and `{idTraining}` is id keluarga
      
   #### Add Anggota
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing add anggota keluarga with methode POST at : `/v1/api/anggota`
     example :```{
                     "npa": "99.1009",
                     "nama": "Hatur Nuhun (admin ganteng)",
                     "tempat": "Batam",
                     "tanggalLahir": "12-12-1990",
                     "statusMerital": "Menikah",
                     "pekerjaan": "Guru",
                     "pw": {
                         "kdPw": "PW-1",
                         "namaWilayah": "Jawa Barat",
                         "diresmikan": "25-11-2020"
                     },
                     "pd": {
                         "kdPd": "PD-1",
                         "namaPd": "Kota Bandung",
                         "diresmikan": "25-11-2020"
                     },
                     "pc": {
                         "kdPc": "PC.1",
                         "namaPc": "Andir",
                         "diresmikan": "25-11-2020"
                     },
                     "pj": "PJ-1",
                     "namaPj": "Konoha",
                     "desa": {
                         "id": "1101031009",
                         "nama": "Tanjung Raya"
                     },
                     "provinsi": {
                         "id": "32",
                         "namaProvinsi": "Jawa Barat"
                     },
                     "kabupaten": {
                         "id": "3273",
                         "nama": "Kota Bandung"
                     },
                     "kecamatan": {
                         "id": "3273180",
                         "nama": " Andir"
                     },
                     "golDarah": "O",
                     "email": "almira77@gmail.co.id",
                     "noTelpon": "080989999",
                     "alamat": "Gg. Cokroaminoto No. 99, Probolinggo 55859, SulTra",
                     "jenisKeanggotaan": "Biasa",
                     "statusAktif": "Aktif",
                     "foto": "default.png",
                     "idOtonom": 3,
                     "jenjangPendidikan": {
                         "id": "16",
                         "pendidikan": "S3"
                     },
                     "masaAktifKta": "25-11-2020",
                     "regDate": "25-11-2020 04:39:43",
                     "lastUpdated": "25-11-2020 04:39:43",
                     "createdBy": "Machine",
                     "createdDate": "25-11-2020 04:39:43",
                     "levelTafiq": "0"
                 }```   
     
       ![anggota](images/anggota-create.png "anggota create")           
   
   #### Update Anggota
   1. Paste this command to your terminal
      
         `mvn clean spring-boot:run`
          
   2. Launch POSTMAN to Login
         example:`username:99.0001 ,password:12345`               
   3. Accessing add anggota keluarga with methode PUT at : `/v1/api/anggota/{idAnggota}`
    example : ```{
                     "npa": "99.1013",
                     "nama": "Hatur Uninga (admin Kasep)",
                     "tempat": "Batamk",
                     "tanggalLahir": "12-12-1990",
                     "statusMerital": "Menikah",
                     "pekerjaan": "APA",
                     "pw": {
                         "kdPw": "PW-5",
                         "namaWilayah": "Jawa Barat",
                         "diresmikan": "25-11-2021"
                     },
                     "pd": {
                         "kdPd": "PD-1",
                         "namaPd": "Kota Bandung",
                         "diresmikan": "25-11-2020"
                     },
                     "pc": {
                         "kdPc": "PC.1",
                         "namaPc": "Andir",
                         "diresmikan": "25-11-2020"
                     },
                     "pj": "PJ-1",
                     "namaPj": "Konoha",
                     "desa": {
                         "id": "1101031009",
                         "nama": "Tanjung Raya"
                     },
                     "provinsi": {
                         "id": "32",
                         "namaProvinsi": "Jawa Barat"
                     },
                     "kabupaten": {
                         "id": "3273",
                         "nama": "Kota Bandung"
                     },
                     "kecamatan": {
                         "id": "3273180",
                         "nama": " Andir"
                     },
                     "golDarah": "O",
                     "email": "almira77@gmail.co.id",
                     "noTelpon": "080989911299xx",
                     "alamat": "Gg. Cokroaminoto No. 99, Probolinggo 55859, SulTra",
                     "jenisKeanggotaan": "Biasa",
                     "statusAktif": "Aktif",
                     "foto": "default.png",
                     "idOtonom": 3,
                     "jenjangPendidikan": {
                         "id": "11",
                         "pendidikan": "S3"
                     },
                     "masaAktifKta": "25-11-2020",
                     "regDate": "25-11-2020 04:39:43",
                     "lastUpdated": "25-11-2020 04:39:43",
                     "createdBy": "Machine",
                     "createdDate": "25-11-2020 04:39:43",
                     "levelTafiq": "0"
                 }```
                 
       ![anggota](images/anggota-update.png "anggota create")           
